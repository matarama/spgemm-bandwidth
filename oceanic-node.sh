#!/bin/bash

###############################################################################

mmfPathAArr=(
	"/arc/shared/data/ufl/144/144.mtx"
	"/arc/shared/data/ufl/2cubes_sphere/2cubes_sphere.mtx"
	"/arc/shared/data/ufl/cage12/cage12.mtx"
	"/arc/shared/data/ufl/filter3D/filter3D.mtx"
	"/arc/shared/data/ufl/mac_econ_fwd500/mac_econ_fwd500.mtx"
	"/arc/shared/data/ufl/majorbasis/majorbasis.mtx"
	"/arc/shared/data/ufl/mario002/mario002.mtx"
	"/arc/shared/data/ufl/mc2depi/mc2depi.mtx"
	"/arc/shared/data/ufl/offshore/offshore.mtx"
	"/arc/shared/data/ufl/poisson3Da/poisson3Da.mtx"
	"/arc/shared/data/ufl/scircuit/scircuit.mtx"
	"/arc/shared/data/ufl/tmt_sym/tmt_sym.mtx"
	"/arc/shared/data/ufl/torso2/torso2.mtx"
	"/arc/shared/data/ufl/europe_osm/europe_osm.mtx"
	"/arc/shared/data/ufl/delaunay_n24/delaunay_n24.mtx"
    "/arc/shared/data/ufl/web-Stanford/web-Stanford.mtx"
    "/arc/shared/data/ufl/road_usa/road_usa.mtx"
    "/arc/shared/data/ufl/Freescale1/Freescale1.mtx"
    "/arc/shared/data/ufl/circuit5M_dc/circuit5M_dc.mtx"
    "/arc/shared/data/ufl/rajat31/rajat31.mtx"
    "/arc/shared/data/ufl/asia_osm/asia_osm.mtx"
    "/arc/shared/data/ufl/germany_osm/germany_osm.mtx"
    "/arc/shared/data/ufl/NLR/NLR.mtx"
    "/arc/shared/data/ufl/patents/patents.mtx"
    "/arc/shared/data/ufl/cit-Patents/cit-Patents.mtx"
    "/arc/shared/data/ufl/adaptive/adaptive.mtx"
    "/arc/shared/data/ufl/italy_osm/italy_osm.mtx"
    "/arc/shared/data/ufl/M6/M6.mtx"
	"/arc/shared/data/ufl/memchip/memchip.mtx"
	"/arc/shared/data/ufl/cage14/cage14.mtx"
)

mmfPathBArr=(
	"/arc/user/mbasaran/R-MAT/edge_factor_4/R-MAT_21.mtx"
    "/arc/user/mbasaran/R-MAT/edge_factor_4/R-MAT_22.mtx"
	"/arc/user/mbasaran/R-MAT/edge_factor_8/R-MAT_21.mtx"
    "/arc/user/mbasaran/R-MAT/edge_factor_8/R-MAT_22.mtx"
    "/arc/user/mbasaran/R-MAT/edge_factor_16/R-MAT_21.mtx"
    "/arc/user/mbasaran/R-MAT/edge_factor_16/R-MAT_22.mtx"
    "/arc/shared/data/ufl/kron_g500-logn20/kron_g500-logn20.mtx"
    "/arc/shared/data/ufl/kron_g500-logn21/kron_g500-logn21.mtx"
    "/arc/user/mbasaran/R-MAT/edge_factor_32/R-MAT_21.mtx"
    "/arc/user/mbasaran/R-MAT/edge_factor_32/R-MAT_22.mtx"
)

numThreadsArr=( 16 16 64 )
clb=64                   # bytes in cache line
iterations=3
binaries=bins/*
accSizes=16,64,512

# Select a node to use
cores_per_node=( "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15"
				 "0,1,2,3,18,19,20,21,36,37,38,39,54,55,56,57"
				 "0-15,18-33,36-51,54-69" )

taskset_params=( "0-15"
				 "0-3,18-21,36-39,54-57"
				 "0-15,18-33,36-51,54-69" )

nodeToUseArr=( 0 1 2 )

###############################################################################

for node_to_use in ${nodeToUseArr[@]}
do
	numThreads=${numThreadsArr[node_to_use]}
	cores=${cores_per_node[node_to_use]}
	pc=${taskset_params[node_to_use]}
	export KMP_AFFINITY=granularity=fine,proclist=[$cores],explicit

	noOfMatrices=${#mmfPathAArr[@]}
	for (( i = 0; i < $noOfMatrices; i++))
	do
		mmfPathA=${mmfPathAArr[i]}

		for binary in $binaries
		do
			outBinPath=$(basename $binary)_n4_t${numThreads}.csv

			cmd_core="$binary mmfPathA=$mmfPathA numBlocks=$numThreads cacheLineBytes=$clb iterations=$iterations accSizes=$accSizes"
			cmd="$cmd_core"

			echo "$cmd"
			date
			echo ""

			taskset -c $pc $cmd | tee -a $outBinPath > /dev/null
		done
	done    
    
done


