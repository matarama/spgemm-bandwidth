#!/bin/bash

###############################################################################

mmfPathAArr=(
	# "/export/shared/ufl_sparse/2cubes_sphere/2cubes_sphere.mtx"
	# "/export/shared/ufl_sparse/cage12/cage12.mtx"
	# "/export/shared/ufl_sparse/filter3D/filter3D.mtx"
	# "/export/shared/ufl_sparse/mac_econ_fwd500/mac_econ_fwd500.mtx"
	# "/export/shared/ufl_sparse/majorbasis/majorbasis.mtx"
	# "/export/shared/ufl_sparse/mario002/mario002.mtx"
	# "/export/shared/ufl_sparse/mc2depi/mc2depi.mtx"
	# "/export/shared/ufl_sparse/offshore/offshore.mtx"
    # "/export/shared/ufl_sparse/scircuit/scircuit.mtx"
	# "/export/shared/ufl_sparse/tmt_sym/tmt_sym.mtx"
	# "/export/shared/ufl_sparse/torso2/torso2.mtx"
	# "/export/shared/ufl_sparse/europe_osm/europe_osm.mtx"
	# "/export/shared/ufl_sparse/delaunay_n24/delaunay_n24.mtx"
    # "/export/shared/ufl_sparse/web-Stanford/web-Stanford.mtx"
    # "/export/shared/ufl_sparse/road_usa/road_usa.mtx"
    # "/export/shared/ufl_sparse/Freescale1/Freescale1.mtx"
    # "/export/shared/ufl_sparse/rajat31/rajat31.mtx"
    # "/export/shared/ufl_sparse/asia_osm/asia_osm.mtx"
    # "/export/shared/ufl_sparse/germany_osm/germany_osm.mtx"
    # "/export/shared/ufl_sparse/NLR/NLR.mtx"
    # "/export/shared/ufl_sparse/patents/patents.mtx"
    # "/export/shared/ufl_sparse/cit-Patents/cit-Patents.mtx"
    # "/export/shared/ufl_sparse/adaptive/adaptive.mtx"
    # "/export/shared/ufl_sparse/italy_osm/italy_osm.mtx"
    # "/export/shared/ufl_sparse/M6/M6.mtx"
	# "/export/shared/ufl_sparse/memchip/memchip.mtx"
	# "/export/shared/ufl_sparse/cage14/cage14.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_14_2.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_14_4.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_14_8.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_14_16.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_14_32.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_14_64.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_15_2.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_15_4.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_15_8.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_15_16.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_15_32.mtx"
    # "/export/shared/ufl_sparse/ER/R-MAT_16_2.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_16_4.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_16_8.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_16_16.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_16_32.mtx"
    # "/export/shared/ufl_sparse/ER/R-MAT_17_2.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_17_4.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_17_8.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_17_16.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_17_32.mtx"
    # "/export/shared/ufl_sparse/ER/R-MAT_18_2.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_18_4.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_18_8.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_18_16.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_18_32.mtx"
    # "/export/shared/ufl_sparse/ER/R-MAT_19_2.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_19_4.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_19_8.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_19_16.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_19_32.mtx"
    # "/export/shared/ufl_sparse/ER/R-MAT_20_2.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_20_4.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_20_8.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_20_16.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_20_32.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_21_2.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_21_4.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_21_8.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_21_16.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_21_32.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_22_2.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_22_4.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_22_8.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_22_16.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_22_32.mtx"
	# "/export/shared/ufl_sparse/Graph500/R-MAT_15_2.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_15_4.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_15_8.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_15_16.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_15_32.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_16_2.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_16_4.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_16_8.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_16_16.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_16_32.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_17_2.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_17_4.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_17_8.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_17_16.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_17_32.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_18_2.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_18_4.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_18_8.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_18_16.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_18_32.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_19_2.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_19_4.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_19_8.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_19_16.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_19_32.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_20_2.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_20_4.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_20_8.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_20_16.mtx"
	"/export/shared/ufl_sparse/wb-edu/wb-edu.mtx"
	"/export/shared/ufl_sparse/hugebubbles-00020/hugebubbles-00020.mtx"
	"/export/shared/ufl_sparse/ldoor/ldoor.mtx"
	"/export/shared/ufl_sparse/hugebubbles-00010/hugebubbles-00010.mtx"
	"/export/shared/ufl_sparse/hugetrace-00010/hugetrace-00010.mtx"
	"/export/shared/ufl_sparse/road_central/road_central.mtx"
	"/export/shared/ufl_sparse/gsm_106857/gsm_106857.mtx"
	"/export/shared/ufl_sparse/msdoor/msdoor.mtx"
	"/export/shared/ufl_sparse/circuit5M_dc/circuit5M_dc.mtx"
	"/export/shared/ufl_sparse/AS365/AS365.mtx"
	"/export/shared/ufl_sparse/333SP/333SP.mtx"
	"/export/shared/ufl_sparse/atmosmodm/atmosmodm.mtx"
	"/export/shared/ufl_sparse/hugetrace-00000/hugetrace-00000.mtx"
	"/export/shared/ufl_sparse/thermal2/thermal2.mtx"
	"/export/shared/ufl_sparse/G3_circuit/G3_circuit.mtx"
	"/export/shared/ufl_sparse/CurlCurl_2/CurlCurl_2.mtx"
	"/export/shared/ufl_sparse/great-britain_osm/great-britain_osm.mtx"
	"/export/shared/ufl_sparse/delaunay_n21/delaunay_n21.mtx"
	"/export/shared/ufl_sparse/sls/sls.mtx"
	"/export/shared/ufl_sparse/Hamrle3/Hamrle3.mtx"
	"/export/shared/ufl_sparse/delaunay_n22/delaunay_n22.mtx"
	"/export/shared/ufl_sparse/delaunay_n23/delaunay_n23.mtx"
	"/export/shared/ufl_sparse/eu-2005/eu-2005.mtx"
	"/export/shared/ufl_sparse/StocF-1465/StocF-1465.mtx"
	"/export/shared/ufl_sparse/Transport/Transport.mtx"
	"/export/shared/ufl_sparse/venturiLevel3/venturiLevel3.mtx"
	"/export/shared/ufl_sparse/ecology1/ecology1.mtx"
	"/export/shared/ufl_sparse/roadNet-CA/roadNet-CA.mtx"
	"/export/shared/ufl_sparse/webbase-1M/webbase-1M.mtx"
	"/export/shared/ufl_sparse/netherlands_osm/netherlands_osm.mtx"
	"/export/shared/ufl_sparse/roadNet-TX/roadNet-TX.mtx"
	"/export/shared/ufl_sparse/roadNet-PA/roadNet-PA.mtx"
	"/export/shared/ufl_sparse/citationCiteseer/citationCiteseer.mtx"
	"/export/shared/ufl_sparse/belgium_osm/belgium_osm.mtx"
	"/export/shared/ufl_sparse/debr/debr.mtx"
)

# parameters
numThreads=28

# Set environment variables
export OMP_PROC_BIND=spread 
export OMP_PLACES=threads
export OMP_NUM_THREADS=${numThreads}


binary="./kokkos/kokkos-kernels/example/buildlib/perf_test/KokkosSparse_spgemm.exe"
outKKDENSE=${numThreads}t_KKDENSE.log
outKKMEM=${numThreads}t_KKMEM.log
outKKSPGEMM=${numThreads}t_KKSPGEMM.log

noOfMatrices=${#mmfPathAArr[@]}
for (( i = 0; i < $noOfMatrices; i++))
do
    mmfPathA=${mmfPathAArr[i]}

	cmdDense="${binary} --openmp ${numThreads} --amtx ${mmfPathA} --algorithm kkdense"
	cmdMem="${binary} --openmp ${numThreads} --amtx ${mmfPathA} --algorithm kkmem"
	cmdSpgemm="${binary} --openmp ${numThreads} --amtx ${mmfPathA}"

	echo "${cmdDense} >> ${outKKDENSE}"
	date
	${cmdDense} >> ${outKKDENSE}

	echo "${cmdMem} >> ${outKKMEM}"
	date
	${cmdMem} >> ${outKKMEM}

	echo "${cmdSpgemm} >> ${outKKSPGEMM}"
	date
	${cmdSpgemm} >> ${outKKSPGEMM}
done    
