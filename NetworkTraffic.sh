#!/bin/bash

export
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/intel/compilers_and_libraries_2017.1.132/linux/ipp/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/mkl/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/tbb/lib/intel64/gcc4.7:/opt/intel/debugger_2017/iga/lib:/opt/intel/debugger_2017/libipt/intel64/lib:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/mpi/intel64/lib:/opt/intel/compilers_and_libraries_2017.1.132/linux/mpi/mic/lib:/homes/mehmetbasaran/.opt/numactl/build/install/lib

###############################################################################

dirPath="./spgemm-bandwidth"

# knl (Knights Landing)
###############################################################################

numThreadsArr=( 68 136 204 272 )
placements=( "68c,1t" "68c,2t" "68c,3t" "68c,3t" )
configs=( 1 0 2 3 )
# configs=( 0 1 2 3 )

###############################################################################

noOfConfigs=${#configs[@]}
for (( c = 0; c < $noOfConfigs; c++ ))
do
	config=${configs[c]}
	numThreads=${numThreadsArr[config]}
	placement=${placements[config]}

    export OMP_NUM_THREADS=${numThreads}
	export KMP_HW_SUBSET=${placement}
	export KMP_AFFINITY=compact,verbose

    outPath="${dirPath}/NetworkTraffic_${placement}"
    cmd="${dirPath}/NetworkTraffic numBlocks=${numThreads}"
    $cmd >> $outPath
done
