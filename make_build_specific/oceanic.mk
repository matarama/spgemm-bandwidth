
# Build settings
################################################################################

build_macros := -DWITH_AVX -DWITH_LIB_NUMA

build_cflags := -no-vec

build_include_paths := -I/opt/intel/vtune_amplifier_xe_2017/include/ \
	-I/homes/mehmetbasaran/.opt/numactl/build/install/include

build_runtime_shared_lib_paths := -Wl,-rpath,/opt/intel/mkl/lib/intel64 \
        -Wl,-rpath,/opt/intel/lib/intel64 \
        -Wl,-rpath,/opt/intel/composer_xe_2013_sp1/lib/intel64 \
        -Wl,-rpath,/opt/intel/composer_xe_2013_sp1/mkl/lib/intel64 \
        -Wl,-rpath,/opt/intel/vtune_amplifier_xe_2017/lib64

build_lib_paths := -L/opt/intel/mkl/lib/intel64 \
    -L/opt/intel/lib/intel64 \
    -L/opt/intel/composer_xe_2013_sp1/lib/intel64 \
    -L/opt/intel/composer_xe_2013_sp1/mkl/lib/intel64 \
    -L/opt/intel/vtune_amplifier_xe_2017/lib64 \
	-L/homes/mehmetbasaran/.opt/numactl/build/install/lib

build_libs := -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lnuma

build_ldflags := -lm

# Compiler options
################################################################################
# icc compiler outputs
# -qopt-report[=6] -qopt-report-phase=vec (vectorization report) 
# -opt-report-phase ipo_inl (lists inlined functions)
# -vec-report [=5] controls the diagnostic information reported by the vectorizer
# -no-vec -no-simd
cc := /opt/intel/bin/icpc
cflags := $(generic_cflags) ${build_cflags} \
          $(generic_include_paths) $(build_include_paths) -mkl=parallel \
          $(generic_macros) $(build_macros)

ld := /opt/intel/bin/icpc
ldflags := $(generic_ldflags) $(build_ldflags) \
           -qopenmp $(generic_lib_paths) $(build_lib_paths) \
           $(build_runtime_shared_lib_paths) -mkl=parallel \
           $(generic_libs) $(build_libs) 
