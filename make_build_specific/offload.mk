
# Build settings
###################################################################################################

ifndef MIC_NO
MIC_NO = 3
endif

# BUILD = offload
BUILD_PATH = $(BUILD_DIR)/$(BUILD)

ARCH_MACROS = -DOFFLOAD_TO_MIC -DMIC_NO=$(MIC_NO)

INC_DIRS = -I$(PROJECT_DIR)/$(SOURCE) \
	-I/opt/intel/composer_xe_2015/mkl/include \
	-I/opt/intel/composer_xe_2015/include

LIB_DIRS = -L/opt/intel/mkl/lib/intel64 \
	-L/opt/intel/lib/intel64 \
	-L/opt/intel/composer_xe_2015/mkl/lib/intel64 \
	-L/opt/intel/composer_xe_2015/lib/intel64

LIB_DIRS_MIC = -L/opt/intel/composer_xe_2015/mkl/lib/mic \
	-L/opt/intel/composer_xe_2015/lib/mic \
	-L/opt/intel/mkl/lib/mic


LIBS = -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core 
	

# Compiler options
###################################################################################################

# icc compiler outputs
# -qopt-report[=6] -qopt-report-phase=vec (vectorization report) 
# -opt-report-phase ipo_inl (lists inlined functions)
# -vec-report [=5] controls the diagnostic information reported by the vectorizer

CC = /opt/intel/bin/icpc
CFLAGS = -c -fopenmp -O3 -std=c++11 $(INC_DIRS) -mkl=parallel $(ARCH_MACROS) $(GENERIC_MACROS) \
		-qopt-report=5 -qopt-report-phase=vec

LD = /opt/intel/bin/icpc 
LDFLAGS_MIC = -offload-option,mic,compiler,"$(LIB_DIRS_MIC) $(LIBS)"
LDFLAGS = -openmp $(LIB_DIRS) -mkl=parallel $(LIBS) -lpthread -lm $(LDFLAGS_MIC)
