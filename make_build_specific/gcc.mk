
# Build settings
################################################################################

build_macros :=

build_include_paths :=

build_runtime_shared_lib_paths :=

build_lib_paths :=

build_libs := -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core

build_ldflags := -lm

# Compiler options
################################################################################

cc := g++
cflags := $(generic_cflags) \
          $(generic_include_paths) $(build_include_paths) \
          $(generic_macros) $(build_macros)

LD := g++
LDFLAGS = $(generic_ldflags) $(build_ldflags) \
           -fopenmp $(generic_lib_paths) $(build_lib_paths) \
           $(build_runtime_shared_lib_paths) -mkl=parallel \
           $(generic_libs) $(build_libs)

