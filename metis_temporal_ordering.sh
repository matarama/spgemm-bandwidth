#!/bin/bash

# For ufl matrices
###############################################################################
graphDir=/home/mbasaran/arc/metis_orderings
mmfPathAArr=(
    # "/arc/shared/data/ufl/2cubes_sphere/2cubes_sphere.mtx"
	# "/arc/shared/data/ufl/cage12/cage12.mtx"
	# "/arc/shared/data/ufl/filter3D/filter3D.mtx"
	# "/arc/shared/data/ufl/mac_econ_fwd500/mac_econ_fwd500.mtx"
	# "/arc/shared/data/ufl/majorbasis/majorbasis.mtx"
	# "/arc/shared/data/ufl/mario002/mario002.mtx"
	# "/arc/shared/data/ufl/mc2depi/mc2depi.mtx"
	# "/arc/shared/data/ufl/offshore/offshore.mtx"
    # "/arc/shared/data/ufl/scircuit/scircuit.mtx"
	# "/arc/shared/data/ufl/tmt_sym/tmt_sym.mtx"
	# "/arc/shared/data/ufl/torso2/torso2.mtx"
	# "/arc/shared/data/ufl/europe_osm/europe_osm.mtx"
	# "/arc/shared/data/ufl/delaunay_n24/delaunay_n24.mtx"
    # "/arc/shared/data/ufl/web-Stanford/web-Stanford.mtx"
    # "/arc/shared/data/ufl/road_usa/road_usa.mtx"
    # "/arc/shared/data/ufl/Freescale1/Freescale1.mtx"
    # "/arc/shared/data/ufl/rajat31/rajat31.mtx"
    # "/arc/shared/data/ufl/asia_osm/asia_osm.mtx"
    # "/arc/shared/data/ufl/germany_osm/germany_osm.mtx"
    # "/arc/shared/data/ufl/NLR/NLR.mtx"
    # "/arc/shared/data/ufl/patents/patents.mtx"
    # "/arc/shared/data/ufl/cit-Patents/cit-Patents.mtx"
    # "/arc/shared/data/ufl/adaptive/adaptive.mtx"
    # "/arc/shared/data/ufl/italy_osm/italy_osm.mtx"
    # "/arc/shared/data/ufl/M6/M6.mtx"
	# "/arc/shared/data/ufl/memchip/memchip.mtx"
	# "/arc/shared/data/ufl/cage14/cage14.mtx"
	"/arc/shared/data/ufl/wb-edu/wb-edu.mtx"
	"/arc/shared/data/ufl/hugebubbles-00020/hugebubbles-00020.mtx"
	"/arc/shared/data/ufl/ldoor/ldoor.mtx"
	"/arc/shared/data/ufl/hugebubbles-00010/hugebubbles-00010.mtx"
	"/arc/shared/data/ufl/hugetrace-00010/hugetrace-00010.mtx"
	"/arc/shared/data/ufl/road_central/road_central.mtx"
	"/arc/shared/data/ufl/gsm_106857/gsm_106857.mtx"
	"/arc/shared/data/ufl/msdoor/msdoor.mtx"
	"/arc/shared/data/ufl/circuit5M_dc/circuit5M_dc.mtx"
	"/arc/shared/data/ufl/AS365/AS365.mtx"
	"/arc/shared/data/ufl/333SP/333SP.mtx"
	"/arc/shared/data/ufl/atmosmodm/atmosmodm.mtx"
	"/arc/shared/data/ufl/hugetrace-00000/hugetrace-00000.mtx"
	"/arc/shared/data/ufl/thermal2/thermal2.mtx"
	"/arc/shared/data/ufl/G3_circuit/G3_circuit.mtx"
	"/arc/shared/data/ufl/CurlCurl_2/CurlCurl_2.mtx"
	"/arc/shared/data/ufl/great-britain_osm/great-britain_osm.mtx"
	"/arc/shared/data/ufl/delaunay_n21/delaunay_n21.mtx"
	"/arc/shared/data/ufl/sls/sls.mtx"
	"/arc/shared/data/ufl/Hamrle3/Hamrle3.mtx"
	"/arc/shared/data/ufl/delaunay_n22/delaunay_n22.mtx"
	"/arc/shared/data/ufl/delaunay_n23/delaunay_n23.mtx"
	"/arc/shared/data/ufl/eu-2005/eu-2005.mtx"
	"/arc/shared/data/ufl/StocF-1465/StocF-1465.mtx"
	"/arc/shared/data/ufl/Transport/Transport.mtx"
	"/arc/shared/data/ufl/venturiLevel3/venturiLevel3.mtx"
	"/arc/shared/data/ufl/ecology1/ecology1.mtx"
	"/arc/shared/data/ufl/roadNet-CA/roadNet-CA.mtx"
	"/arc/shared/data/ufl/webbase-1M/webbase-1M.mtx"
	"/arc/shared/data/ufl/netherlands_osm/netherlands_osm.mtx"
	"/arc/shared/data/ufl/roadNet-TX/roadNet-TX.mtx"
	"/arc/shared/data/ufl/roadNet-PA/roadNet-PA.mtx"
	"/arc/shared/data/ufl/citationCiteseer/citationCiteseer.mtx"
	"/arc/shared/data/ufl/belgium_osm/belgium_osm.mtx"
	"/arc/shared/data/ufl/debr/debr.mtx"
	"/arc/shared/data/ufl/pkustk10/pkustk10.mtx"
	"/arc/shared/data/ufl/Chebyshev4/Chebyshev4.mtx"
	"/arc/shared/data/ufl/amazon-2008/amazon-2008.mtx"
	"/arc/shared/data/ufl/Ga10As10H30/Ga10As10H30.mtx"
	"/arc/shared/data/ufl/pre2/pre2.mtx"
	"/arc/shared/data/ufl/pkustk13/pkustk13.mtx"
	"/arc/shared/data/ufl/hood/hood.mtx"
	"/arc/shared/data/ufl/pwtk/pwtk.mtx"
	"/arc/shared/data/ufl/kkt_power/kkt_power.mtx"
	"/arc/shared/data/ufl/pkustk14/pkustk14.mtx"
	"/arc/shared/data/ufl/F1/F1.mtx"
	"/arc/shared/data/ufl/ML_Laplace/ML_Laplace.mtx"
	"/arc/shared/data/ufl/Fault_639/Fault_639.mtx"
	"/arc/shared/data/ufl/nlpkkt80/nlpkkt80.mtx"
	"/arc/shared/data/ufl/Emilia_923/Emilia_923.mtx"
	"/arc/shared/data/ufl/dielFilterV2real/dielFilterV2real.mtx"
	"/arc/shared/data/ufl/af_shell10/af_shell10.mtx"
	"/arc/shared/data/ufl/Geo_1438/Geo_1438.mtx"
	"/arc/shared/data/ufl/nlpkkt120/nlpkkt120.mtx"
)

# for vertex-count=10
kArr=(
	# 10150 # 2cubes_sphere
	# 13023 # cage12
	# 10644 # filter3D
	# 20650 # mac_econ_fwd500
	# 16000 # majorbasis
	# 38988 # mario002
	# 52583 # mc2depi
	# 25979 # offshore
	# 17100 # scircuit
	# 72672 # tmt_sym
	# 11597 # torso2
	# 5091202 # europe_osm
	# 1677722 # delaunay_n24
	# 28191 # web-Stanford
	# 2394735 # road_usa
	# 342876 # Freescale1
	# 469001 # rajat31
	# 1195076 # asia_osm
	# 1154885 # germany_osm
	# 416377 # NLR
	# 377477 # patents
	# 377477 # cit-Patents
	# 681575 # adaptive
	# 668650 # italy_osm
	# 350178 # M6
	# 270753 # memchip
	# 150579 # cage14
	984573 # wb-edu
	2119812 # hugebubbles-00020
	95221 # ldoor
	1945809 # hugebubbles-00010
	1205745 # hugetrace-00010
	1408182 # road_central
	58945 # gsm_106857
	41587 # msdoor
	352332 # circuit5M_dc
	379928 # AS365
	371282 # 333SP
	148976 # atmosmodm
	458849 # hugetrace-00000
	122805 # thermal2
	158548 # G3_circuit
	80653 # CurlCurl_2
	773383 # great-britain_osm
	209716 # delaunay_n21
	6273 # sls
	144736 # Hamrle3
	419431 # delaunay_n22
	838861 # delaunay_n23
	86267 # eu-2005
	146514 # StocF-1465
	160212 # Transport
	402682 # venturiLevel3
	100000 # ecology1
	197129 # roadNet-CA
	100001 # webbase-1M
	221669 # netherlands_osm
	139339 # roadNet-TX
	26850 # citationCiteseer
	144130 # belgium_osm
	104858 # debr
	8068 # pkustk10
	6813 # Chebyshev4
	73533 # amazon-2008
	11309 # Ga10As10H30
	65904 # pre2
	9490 # pkustk13
	22055 # hood
	21792 # pwtk
	206350 # kkt_power
	15193 # pkustk14
	34380 # F1
	37701 # ML_Laplace
	63881 # Fault_639
	106240 # nlpkkt80
	92314 # Emilia_923
	115746 # dielFilterV2real
	150807 # af_shell10
	143796 # Geo_1438
	354240 # nlpkkt120
	# 1639 # R-MAT_14_2
	# 1639 # R-MAT_14_4
	# 1639 # R-MAT_14_8
	# 1639 # R-MAT_14_16
	# 1639 # R-MAT_14_32
	# 1639 # R-MAT_14_64
	# 3277 # R-MAT_15_2
	# 3277 # R-MAT_15_4
	# 3277 # R-MAT_15_8
	# 3277 # R-MAT_15_16
	# 3277 # R-MAT_15_32
	# 6554 # R-MAT_16_2
	# 6554 # R-MAT_16_4
	# 6554 # R-MAT_16_8
	# 6554 # R-MAT_16_16
	# 6554 # R-MAT_16_32
	# 13108 # R-MAT_17_2
	# 13108 # R-MAT_17_4
	# 13108 # R-MAT_17_8
	# 13108 # R-MAT_17_16
	# 13108 # R-MAT_17_32
	# 26215 # R-MAT_18_2
	# 26215 # R-MAT_18_4
	# 26215 # R-MAT_18_8
	# 26215 # R-MAT_18_16
	# 26215 # R-MAT_18_32
	# 52429 # R-MAT_19_2
	# 52429 # R-MAT_19_4
	# 52429 # R-MAT_19_8
	# 52429 # R-MAT_19_16
	# 52429 # R-MAT_19_32
	# 104858 # R-MAT_20_2
	# 104858 # R-MAT_20_4
	# 104858 # R-MAT_20_8
	# 104858 # R-MAT_20_16
	# 104858 # R-MAT_20_32
	# 209716 # R-MAT_21_2
	# 209716 # R-MAT_21_4
	# 209716 # R-MAT_21_8
	# 209716 # R-MAT_21_16
	# 209716 # R-MAT_21_32
	# 419431 # R-MAT_22_2
	# 419431 # R-MAT_22_4
	# 419431 # R-MAT_22_8
	# 419431 # R-MAT_22_16
	# 3277 # R-MAT_15_2
	# 3277 # R-MAT_15_4
	# 3277 # R-MAT_15_8
	# 3277 # R-MAT_15_16
	# 3277 # R-MAT_15_32
	# 6554 # R-MAT_16_2
	# 6554 # R-MAT_16_4
	# 6554 # R-MAT_16_8
	# 6554 # R-MAT_16_16
	# 6554 # R-MAT_16_32
	# 13108 # R-MAT_17_2
	# 13108 # R-MAT_17_4
	# 13108 # R-MAT_17_8
	# 13108 # R-MAT_17_16
	# 13108 # R-MAT_17_32
	# 26215 # R-MAT_18_2
	# 26215 # R-MAT_18_4
	# 26215 # R-MAT_18_8
	# 26215 # R-MAT_18_16
	# 52429 # R-MAT_19_2
	# 52429 # R-MAT_19_4
	# 52429 # R-MAT_19_8
	# 104858 # R-MAT_20_2
	# 104858 # R-MAT_20_4
)


# for vertex-count=100
kArr=(
	# 1015 # 2cubes_sphere
	# 1303 # cage12
	# 1065 # filter3D
	# 2065 # mac_econ_fwd500
	# 1600 # majorbasis
	# 3899 # mario002
	# 5259 # mc2depi
	# 2598 # offshore
	# 1710 # scircuit
	# 7268 # tmt_sym
	# 1160 # torso2
	# 509121 # europe_osm
	# 167773 # delaunay_n24
	# 2820 # web-Stanford
	# 239474 # road_usa
	# 34288 # Freescale1
	# 46901 # rajat31
	# 119508 # asia_osm
	# 115489 # germany_osm
	# 41638 # NLR
	# 37748 # patents
	# 37748 # cit-Patents
	# 68158 # adaptive
	# 66865 # italy_osm
	# 35018 # M6
	# 27076 # memchip
	# 15058 # cage14
	98458 # wb-edu
	211982 # hugebubbles-00020
	9523 # ldoor
	194581 # hugebubbles-00010
	120575 # hugetrace-00010
	140819 # road_central
	5895 # gsm_106857
	4159 # msdoor
	35234 # circuit5M_dc
	37993 # AS365
	37129 # 333SP
	14898 # atmosmodm
	45885 # hugetrace-00000
	12281 # thermal2
	15855 # G3_circuit
	8066 # CurlCurl_2
	77339 # great-britain_osm
	20972 # delaunay_n21
	628 # sls
	14474 # Hamrle3
	41944 # delaunay_n22
	83887 # delaunay_n23
	8627 # eu-2005
	14652 # StocF-1465
	16022 # Transport
	40269 # venturiLevel3
	10000 # ecology1
	19713 # roadNet-CA
	10001 # webbase-1M
	22167 # netherlands_osm
	13934 # roadNet-TX
	2685 # citationCiteseer
	14413 # belgium_osm
	10486 # debr
	807 # pkustk10
	682 # Chebyshev4
	7354 # amazon-2008
	1131 # Ga10As10H30
	6591 # pre2
	949 # pkustk13
	2206 # hood
	2180 # pwtk
	20635 # kkt_power
	1520 # pkustk14
	3438 # F1
	3771 # ML_Laplace
	6389 # Fault_639
	10624 # nlpkkt80
	9232 # Emilia_923
	11575 # dielFilterV2real
	15081 # af_shell10
	14380 # Geo_1438
	35424 # nlpkkt120
	# 164 # R-MAT_14_2
	# 164 # R-MAT_14_4
	# 164 # R-MAT_14_8
	# 164 # R-MAT_14_16
	# 164 # R-MAT_14_32
	# 164 # R-MAT_14_64
	# 328 # R-MAT_15_2
	# 328 # R-MAT_15_4
	# 328 # R-MAT_15_8
	# 328 # R-MAT_15_16
	# 328 # R-MAT_15_32
	# 656 # R-MAT_16_2
	# 656 # R-MAT_16_4
	# 656 # R-MAT_16_8
	# 656 # R-MAT_16_16
	# 656 # R-MAT_16_32
	# 1311 # R-MAT_17_2
	# 1311 # R-MAT_17_4
	# 1311 # R-MAT_17_8
	# 1311 # R-MAT_17_16
	# 1311 # R-MAT_17_32
	# 2622 # R-MAT_18_2
	# 2622 # R-MAT_18_4
	# 2622 # R-MAT_18_8
	# 2622 # R-MAT_18_16
	# 2622 # R-MAT_18_32
	# 5243 # R-MAT_19_2
	# 5243 # R-MAT_19_4
	# 5243 # R-MAT_19_8
	# 5243 # R-MAT_19_16
	# 5243 # R-MAT_19_32
	# 10486 # R-MAT_20_2
	# 10486 # R-MAT_20_4
	# 10486 # R-MAT_20_8
	# 10486 # R-MAT_20_16
	# 10486 # R-MAT_20_32
	# 20972 # R-MAT_21_2
	# 20972 # R-MAT_21_4
	# 20972 # R-MAT_21_8
	# 20972 # R-MAT_21_16
	# 20972 # R-MAT_21_32
	# 41944 # R-MAT_22_2
	# 41944 # R-MAT_22_4
	# 41944 # R-MAT_22_8
	# 41944 # R-MAT_22_16
	# 328 # R-MAT_15_2
	# 328 # R-MAT_15_4
	# 328 # R-MAT_15_8
	# 328 # R-MAT_15_16
	# 328 # R-MAT_15_32
	# 656 # R-MAT_16_2
	# 656 # R-MAT_16_4
	# 656 # R-MAT_16_8
	# 656 # R-MAT_16_16
	# 656 # R-MAT_16_32
	# 1311 # R-MAT_17_2
	# 1311 # R-MAT_17_4
	# 1311 # R-MAT_17_8
	# 1311 # R-MAT_17_16
	# 1311 # R-MAT_17_32
	# 2622 # R-MAT_18_2
	# 2622 # R-MAT_18_4
	# 2622 # R-MAT_18_8
	# 2622 # R-MAT_18_16
	# 5243 # R-MAT_19_2
	# 5243 # R-MAT_19_4
	# 5243 # R-MAT_19_8
	# 10486 # R-MAT_20_2
	# 10486 # R-MAT_20_4
)

# for vertex-count=1000
kArr=(
	# 102 # 2cubes_sphere
	# 131 # cage12
	# 107 # filter3D
	# 207 # mac_econ_fwd500
	# 160 # majorbasis
	# 390 # mario002
	# 526 # mc2depi
	# 260 # offshore
	# 171 # scircuit
	# 727 # tmt_sym
	# 116 # torso2
	# 50913 # europe_osm
	# 16778 # delaunay_n24
	# 282 # web-Stanford
	# 23948 # road_usa
	# 3429 # Freescale1
	# 4691 # rajat31
	# 11951 # asia_osm
	# 11549 # germany_osm
	# 4164 # NLR
	# 3775 # patents
	# 3775 # cit-Patents
	# 6816 # adaptive
	# 6687 # italy_osm
	# 3502 # M6
	# 2708 # memchip
	# 1506 # cage14
	9846 # wb-edu
	21199 # hugebubbles-00020
	953 # ldoor
	19459 # hugebubbles-00010
	12058 # hugetrace-00010
	14082 # road_central
	590 # gsm_106857
	416 # msdoor
	3524 # circuit5M_dc
	3800 # AS365
	3713 # 333SP
	1490 # atmosmodm
	4589 # hugetrace-00000
	1229 # thermal2
	1586 # G3_circuit
	807 # CurlCurl_2
	7734 # great-britain_osm
	2098 # delaunay_n21
	63 # sls
	1448 # Hamrle3
	4195 # delaunay_n22
	8389 # delaunay_n23
	863 # eu-2005
	1466 # StocF-1465
	1603 # Transport
	4027 # venturiLevel3
	1000 # ecology1
	1972 # roadNet-CA
	1001 # webbase-1M
	2217 # netherlands_osm
	1394 # roadNet-TX
	269 # citationCiteseer
	1442 # belgium_osm
	1049 # debr
	81 # pkustk10
	69 # Chebyshev4
	736 # amazon-2008
	114 # Ga10As10H30
	660 # pre2
	95 # pkustk13
	221 # hood
	218 # pwtk
	2064 # kkt_power
	152 # pkustk14
	344 # F1
	378 # ML_Laplace
	639 # Fault_639
	1063 # nlpkkt80
	924 # Emilia_923
	1158 # dielFilterV2real
	1509 # af_shell10
	1438 # Geo_1438
	3543 # nlpkkt120
	# 17 # R-MAT_14_2
	# 17 # R-MAT_14_4
	# 17 # R-MAT_14_8
	# 17 # R-MAT_14_16
	# 17 # R-MAT_14_32
	# 17 # R-MAT_14_64
	# 33 # R-MAT_15_2
	# 33 # R-MAT_15_4
	# 33 # R-MAT_15_8
	# 33 # R-MAT_15_16
	# 33 # R-MAT_15_32
	# 66 # R-MAT_16_2
	# 66 # R-MAT_16_4
	# 66 # R-MAT_16_8
	# 66 # R-MAT_16_16
	# 66 # R-MAT_16_32
	# 132 # R-MAT_17_2
	# 132 # R-MAT_17_4
	# 132 # R-MAT_17_8
	# 132 # R-MAT_17_16
	# 132 # R-MAT_17_32
	# 263 # R-MAT_18_2
	# 263 # R-MAT_18_4
	# 263 # R-MAT_18_8
	# 263 # R-MAT_18_16
	# 263 # R-MAT_18_32
	# 525 # R-MAT_19_2
	# 525 # R-MAT_19_4
	# 525 # R-MAT_19_8
	# 525 # R-MAT_19_16
	# 525 # R-MAT_19_32
	# 1049 # R-MAT_20_2
	# 1049 # R-MAT_20_4
	# 1049 # R-MAT_20_8
	# 1049 # R-MAT_20_16
	# 1049 # R-MAT_20_32
	# 2098 # R-MAT_21_2
	# 2098 # R-MAT_21_4
	# 2098 # R-MAT_21_8
	# 2098 # R-MAT_21_16
	# 2098 # R-MAT_21_32
	# 4195 # R-MAT_22_2
	# 4195 # R-MAT_22_4
	# 4195 # R-MAT_22_8
	# 4195 # R-MAT_22_16
	# 33 # R-MAT_15_2
	# 33 # R-MAT_15_4
	# 33 # R-MAT_15_8
	# 33 # R-MAT_15_16
	# 33 # R-MAT_15_32
	# 66 # R-MAT_16_2
	# 66 # R-MAT_16_4
	# 66 # R-MAT_16_8
	# 66 # R-MAT_16_16
	# 66 # R-MAT_16_32
	# 132 # R-MAT_17_2
	# 132 # R-MAT_17_4
	# 132 # R-MAT_17_8
	# 132 # R-MAT_17_16
	# 132 # R-MAT_17_32
	# 263 # R-MAT_18_2
	# 263 # R-MAT_18_4
	# 263 # R-MAT_18_8
	# 263 # R-MAT_18_16
	# 525 # R-MAT_19_2
	# 525 # R-MAT_19_4
	# 525 # R-MAT_19_8
	# 1049 # R-MAT_20_2
	# 1049 # R-MAT_20_4
)


# for eudos-renly R-mat matrices
###############################################################################
graphDir=/home/mbasaran/arc/metis_orderings/ER
mmfPathAArr=(
	"/home/mbasaran/arc/ER/R-MAT_14_2.mtx"
	"/home/mbasaran/arc/ER/R-MAT_14_4.mtx"
	"/home/mbasaran/arc/ER/R-MAT_14_8.mtx"
	"/home/mbasaran/arc/ER/R-MAT_14_16.mtx"
	"/home/mbasaran/arc/ER/R-MAT_14_32.mtx"
	"/home/mbasaran/arc/ER/R-MAT_14_64.mtx"
	"/home/mbasaran/arc/ER/R-MAT_15_2.mtx"
	"/home/mbasaran/arc/ER/R-MAT_15_4.mtx"
	"/home/mbasaran/arc/ER/R-MAT_15_8.mtx"
	"/home/mbasaran/arc/ER/R-MAT_15_16.mtx"
	"/home/mbasaran/arc/ER/R-MAT_15_32.mtx"
    "/home/mbasaran/arc/ER/R-MAT_16_2.mtx"
	"/home/mbasaran/arc/ER/R-MAT_16_4.mtx"
	"/home/mbasaran/arc/ER/R-MAT_16_8.mtx"
	"/home/mbasaran/arc/ER/R-MAT_16_16.mtx"
	"/home/mbasaran/arc/ER/R-MAT_16_32.mtx"
    "/home/mbasaran/arc/ER/R-MAT_17_2.mtx"
	"/home/mbasaran/arc/ER/R-MAT_17_4.mtx"
	"/home/mbasaran/arc/ER/R-MAT_17_8.mtx"
	"/home/mbasaran/arc/ER/R-MAT_17_16.mtx"
	"/home/mbasaran/arc/ER/R-MAT_17_32.mtx"
    "/home/mbasaran/arc/ER/R-MAT_18_2.mtx"
	"/home/mbasaran/arc/ER/R-MAT_18_4.mtx"
	"/home/mbasaran/arc/ER/R-MAT_18_8.mtx"
	"/home/mbasaran/arc/ER/R-MAT_18_16.mtx"
	"/home/mbasaran/arc/ER/R-MAT_18_32.mtx"
    "/home/mbasaran/arc/ER/R-MAT_19_2.mtx"
	"/home/mbasaran/arc/ER/R-MAT_19_4.mtx"
	"/home/mbasaran/arc/ER/R-MAT_19_8.mtx"
	"/home/mbasaran/arc/ER/R-MAT_19_16.mtx"
	"/home/mbasaran/arc/ER/R-MAT_19_32.mtx"
    "/home/mbasaran/arc/ER/R-MAT_20_2.mtx"
	"/home/mbasaran/arc/ER/R-MAT_20_4.mtx"
	"/home/mbasaran/arc/ER/R-MAT_20_8.mtx"
	"/home/mbasaran/arc/ER/R-MAT_20_16.mtx"
	"/home/mbasaran/arc/ER/R-MAT_20_32.mtx"
	"/home/mbasaran/arc/ER/R-MAT_21_2.mtx"
	"/home/mbasaran/arc/ER/R-MAT_21_4.mtx"
	"/home/mbasaran/arc/ER/R-MAT_21_8.mtx"
	"/home/mbasaran/arc/ER/R-MAT_21_16.mtx"
	"/home/mbasaran/arc/ER/R-MAT_21_32.mtx"
	"/home/mbasaran/arc/ER/R-MAT_22_2.mtx"
	"/home/mbasaran/arc/ER/R-MAT_22_4.mtx"
	"/home/mbasaran/arc/ER/R-MAT_22_8.mtx"
	"/home/mbasaran/arc/ER/R-MAT_22_16.mtx"
	"/home/mbasaran/arc/ER/R-MAT_22_32.mtx"
)

# for vertex-count=10
kArr=(
	1639 # R-MAT_14_2
	1639 # R-MAT_14_4
	1639 # R-MAT_14_8
	1639 # R-MAT_14_16
	1639 # R-MAT_14_32
	1639 # R-MAT_14_64
	3277 # R-MAT_15_2
	3277 # R-MAT_15_4
	3277 # R-MAT_15_8
	3277 # R-MAT_15_16
	3277 # R-MAT_15_32
	6554 # R-MAT_16_2
	6554 # R-MAT_16_4
	6554 # R-MAT_16_8
	6554 # R-MAT_16_16
	6554 # R-MAT_16_32
	13108 # R-MAT_17_2
	13108 # R-MAT_17_4
	13108 # R-MAT_17_8
	13108 # R-MAT_17_16
	13108 # R-MAT_17_32
	26215 # R-MAT_18_2
	26215 # R-MAT_18_4
	26215 # R-MAT_18_8
	26215 # R-MAT_18_16
	26215 # R-MAT_18_32
	52429 # R-MAT_19_2
	52429 # R-MAT_19_4
	52429 # R-MAT_19_8
	52429 # R-MAT_19_16
	52429 # R-MAT_19_32
	104858 # R-MAT_20_2
	104858 # R-MAT_20_4
	104858 # R-MAT_20_8
	104858 # R-MAT_20_16
	104858 # R-MAT_20_32
	209716 # R-MAT_21_2
	209716 # R-MAT_21_4
	209716 # R-MAT_21_8
	209716 # R-MAT_21_16
	209716 # R-MAT_21_32
	419431 # R-MAT_22_2
	419431 # R-MAT_22_4
	419431 # R-MAT_22_8
	419431 # R-MAT_22_16
)


# for vertex-count=100
kArr=(
	164 # R-MAT_14_2
	164 # R-MAT_14_4
	164 # R-MAT_14_8
	164 # R-MAT_14_16
	164 # R-MAT_14_32
	164 # R-MAT_14_64
	328 # R-MAT_15_2
	328 # R-MAT_15_4
	328 # R-MAT_15_8
	328 # R-MAT_15_16
	328 # R-MAT_15_32
	656 # R-MAT_16_2
	656 # R-MAT_16_4
	656 # R-MAT_16_8
	656 # R-MAT_16_16
	656 # R-MAT_16_32
	1311 # R-MAT_17_2
	1311 # R-MAT_17_4
	1311 # R-MAT_17_8
	1311 # R-MAT_17_16
	1311 # R-MAT_17_32
	2622 # R-MAT_18_2
	2622 # R-MAT_18_4
	2622 # R-MAT_18_8
	2622 # R-MAT_18_16
	2622 # R-MAT_18_32
	5243 # R-MAT_19_2
	5243 # R-MAT_19_4
	5243 # R-MAT_19_8
	5243 # R-MAT_19_16
	5243 # R-MAT_19_32
	10486 # R-MAT_20_2
	10486 # R-MAT_20_4
	10486 # R-MAT_20_8
	10486 # R-MAT_20_16
	10486 # R-MAT_20_32
	20972 # R-MAT_21_2
	20972 # R-MAT_21_4
	20972 # R-MAT_21_8
	20972 # R-MAT_21_16
	20972 # R-MAT_21_32
	41944 # R-MAT_22_2
	41944 # R-MAT_22_4
	41944 # R-MAT_22_8
	41944 # R-MAT_22_16
)

# for vertex-count=1000
kArr=(
	17 # R-MAT_14_2
	17 # R-MAT_14_4
	17 # R-MAT_14_8
	17 # R-MAT_14_16
	17 # R-MAT_14_32
	17 # R-MAT_14_64
	33 # R-MAT_15_2
	33 # R-MAT_15_4
	33 # R-MAT_15_8
	33 # R-MAT_15_16
	33 # R-MAT_15_32
	66 # R-MAT_16_2
	66 # R-MAT_16_4
	66 # R-MAT_16_8
	66 # R-MAT_16_16
	66 # R-MAT_16_32
	132 # R-MAT_17_2
	132 # R-MAT_17_4
	132 # R-MAT_17_8
	132 # R-MAT_17_16
	132 # R-MAT_17_32
	263 # R-MAT_18_2
	263 # R-MAT_18_4
	263 # R-MAT_18_8
	263 # R-MAT_18_16
	263 # R-MAT_18_32
	525 # R-MAT_19_2
	525 # R-MAT_19_4
	525 # R-MAT_19_8
	525 # R-MAT_19_16
	525 # R-MAT_19_32
	1049 # R-MAT_20_2
	1049 # R-MAT_20_4
	1049 # R-MAT_20_8
	1049 # R-MAT_20_16
	1049 # R-MAT_20_32
	2098 # R-MAT_21_2
	2098 # R-MAT_21_4
	2098 # R-MAT_21_8
	2098 # R-MAT_21_16
	2098 # R-MAT_21_32
	4195 # R-MAT_22_2
	4195 # R-MAT_22_4
	4195 # R-MAT_22_8
	4195 # R-MAT_22_16
)

# for grapg500 R-mat matrices
###############################################################################
graphDir=/home/mbasaran/arc/metis_orderings/Graph500
mmfPathAArr=(
	"/home/mbasaran/arc/Graph500/R-MAT_15_2.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_15_4.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_15_8.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_15_16.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_15_32.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_16_2.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_16_4.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_16_8.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_16_16.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_16_32.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_17_2.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_17_4.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_17_8.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_17_16.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_17_32.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_18_2.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_18_4.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_18_8.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_18_16.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_18_32.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_19_2.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_19_4.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_19_8.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_19_16.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_19_32.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_20_2.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_20_4.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_20_8.mtx"
    "/home/mbasaran/arc/Graph500/R-MAT_20_16.mtx"
)

# for vertex-count=10
kArr=(
	3277 # R-MAT_15_2
	3277 # R-MAT_15_4
	3277 # R-MAT_15_8
	3277 # R-MAT_15_16
	3277 # R-MAT_15_32
	6554 # R-MAT_16_2
	6554 # R-MAT_16_4
	6554 # R-MAT_16_8
	6554 # R-MAT_16_16
	6554 # R-MAT_16_32
	13108 # R-MAT_17_2
	13108 # R-MAT_17_4
	13108 # R-MAT_17_8
	13108 # R-MAT_17_16
	13108 # R-MAT_17_32
	26215 # R-MAT_18_2
	26215 # R-MAT_18_4
	26215 # R-MAT_18_8
	26215 # R-MAT_18_16
	52429 # R-MAT_19_2
	52429 # R-MAT_19_4
	52429 # R-MAT_19_8
	104858 # R-MAT_20_2
	104858 # R-MAT_20_4
)


# for vertex-count=100
kArr=(
	328 # R-MAT_15_2
	328 # R-MAT_15_4
	328 # R-MAT_15_8
	328 # R-MAT_15_16
	328 # R-MAT_15_32
	656 # R-MAT_16_2
	656 # R-MAT_16_4
	656 # R-MAT_16_8
	656 # R-MAT_16_16
	656 # R-MAT_16_32
	1311 # R-MAT_17_2
	1311 # R-MAT_17_4
	1311 # R-MAT_17_8
	1311 # R-MAT_17_16
	1311 # R-MAT_17_32
	2622 # R-MAT_18_2
	2622 # R-MAT_18_4
	2622 # R-MAT_18_8
	2622 # R-MAT_18_16
	5243 # R-MAT_19_2
	5243 # R-MAT_19_4
	5243 # R-MAT_19_8
	10486 # R-MAT_20_2
	10486 # R-MAT_20_4
)

# for vertex-count=1000
kArr=(
	33 # R-MAT_15_2
	33 # R-MAT_15_4
	33 # R-MAT_15_8
	33 # R-MAT_15_16
	33 # R-MAT_15_32
	66 # R-MAT_16_2
	66 # R-MAT_16_4
	66 # R-MAT_16_8
	66 # R-MAT_16_16
	66 # R-MAT_16_32
	132 # R-MAT_17_2
	132 # R-MAT_17_4
	132 # R-MAT_17_8
	132 # R-MAT_17_16
	132 # R-MAT_17_32
	263 # R-MAT_18_2
	263 # R-MAT_18_4
	263 # R-MAT_18_8
	263 # R-MAT_18_16
	525 # R-MAT_19_2
	525 # R-MAT_19_4
	525 # R-MAT_19_8
	1049 # R-MAT_20_2
	1049 # R-MAT_20_4
)

###############################################################################

binary=mt-metis-0.6.0/build/Linux-x86_64/bin/mtmetis
graphSuffix="_spatialPartVector_temporal0.graph"

noOfMatrices=${#mmfPathAArr[@]}
for (( i = 0; i < $noOfMatrices; i++))
do
	mmfPathA=${mmfPathAArr[i]}
	mtx=$(basename $mmfPathA)
	temporalGraphPath="${graphDir}/${mtx}${graphSuffix}"
	partVectorPath="${mtx}_temporalPartVector0"
	k=${kArr[i]}
	
	cmd="${binary} -p rb ${temporalGraphPath} ${k} ${partVectorPath}"
	echo "$cmd" >> temporal_ordering.log
	time $cmd >> temporal_ordering.log 2>&1 3>&1
	mv ${partVectorPath} ${graphDir}/
done    

