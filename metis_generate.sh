#!/bin/bash

mmfPathAs=(
	"/arc/shared/data/ufl/144/144.mtx"
	"/arc/shared/data/ufl/2cubes_sphere/2cubes_sphere.mtx"
	"/arc/shared/data/ufl/cage12/cage12.mtx"
	"/arc/shared/data/ufl/conf5_4-8x8-05/conf5_4-8x8-05.mtx"
	"/arc/shared/data/ufl/cop20k_A/cop20k_A.mtx"
	"/arc/shared/data/ufl/cp2k-h2o-.5e7/cp2k-h2o-.5e7.mtx"
	"/arc/shared/data/ufl/cp2k-h2o-e6/cp2k-h2o-e6.mtx"
	"/arc/shared/data/ufl/filter3D/filter3D.mtx"
	"/arc/shared/data/ufl/mac_econ_fwd500/mac_econ_fwd500.mtx"
	"/arc/shared/data/ufl/majorbasis/majorbasis.mtx"
	"/arc/shared/data/ufl/mario002/mario002.mtx"
	"/arc/shared/data/ufl/mc2depi/mc2depi.mtx"
	"/arc/shared/data/ufl/offshore/offshore.mtx"
	"/arc/shared/data/ufl/poisson3Da/poisson3Da.mtx"
	"/arc/shared/data/ufl/scircuit/scircuit.mtx"
	"/arc/shared/data/ufl/tmt_sym/tmt_sym.mtx"
	"/arc/shared/data/ufl/torso2/torso2.mtx"
	"/arc/shared/data/ufl/europe_osm/europe_osm.mtx"
	"/arc/shared/data/ufl/delaunay_n24/delaunay_n24.mtx"
	"/arc/shared/data/ufl/web-Stanford/web-Stanford.mtx"
	"/arc/shared/data/ufl/road_usa/road_usa.mtx"
	"/arc/shared/data/ufl/Freescale1/Freescale1.mtx"
	"/arc/shared/data/ufl/circuit5M_dc/circuit5M_dc.mtx"
	"/arc/shared/data/ufl/rajat31/rajat31.mtx"
	"/arc/shared/data/ufl/asia_osm/asia_osm.mtx"
	"/arc/shared/data/ufl/germany_osm/germany_osm.mtx"
	"/arc/shared/data/ufl/NLR/NLR.mtx"
	"/arc/shared/data/ufl/patents/patents.mtx"
	"/arc/shared/data/ufl/cit-Patents/cit-Patents.mtx"
    "/arc/shared/data/ufl/adaptive/adaptive.mtx"
    "/arc/shared/data/ufl/italy_osm/italy_osm.mtx"
    "/arc/shared/data/ufl/M6/M6.mtx"
	"/arc/shared/data/ufl/memchip/memchip.mtx"
	"/arc/shared/data/ufl/cage14/cage14.mtx"
)

mmfPathBs=(
	"/arc/shared/data/ufl/144/144.mtx"
	"/arc/shared/data/ufl/2cubes_sphere/2cubes_sphere.mtx"
	"/arc/shared/data/ufl/cage12/cage12.mtx"
	"/arc/shared/data/ufl/conf5_4-8x8-05/conf5_4-8x8-05.mtx"
	"/arc/shared/data/ufl/cop20k_A/cop20k_A.mtx"
	"/arc/shared/data/ufl/cp2k-h2o-.5e7/cp2k-h2o-.5e7.mtx"
	"/arc/shared/data/ufl/cp2k-h2o-e6/cp2k-h2o-e6.mtx"
	"/arc/shared/data/ufl/filter3D/filter3D.mtx"
	"/arc/shared/data/ufl/mac_econ_fwd500/mac_econ_fwd500.mtx"
	"/arc/shared/data/ufl/majorbasis/majorbasis.mtx"
	"/arc/shared/data/ufl/mario002/mario002.mtx"
	"/arc/shared/data/ufl/mc2depi/mc2depi.mtx"
	"/arc/shared/data/ufl/offshore/offshore.mtx"
	"/arc/shared/data/ufl/poisson3Da/poisson3Da.mtx"
	"/arc/shared/data/ufl/scircuit/scircuit.mtx"
	"/arc/shared/data/ufl/tmt_sym/tmt_sym.mtx"
	"/arc/shared/data/ufl/torso2/torso2.mtx"
	"/arc/shared/data/ufl/europe_osm/europe_osm.mtx"
	"/arc/shared/data/ufl/delaunay_n24/delaunay_n24.mtx"
	"/arc/shared/data/ufl/web-Stanford/web-Stanford.mtx"
	"/arc/shared/data/ufl/road_usa/road_usa.mtx"
	"/arc/shared/data/ufl/Freescale1/Freescale1.mtx"
	"/arc/shared/data/ufl/circuit5M_dc/circuit5M_dc.mtx"
	"/arc/shared/data/ufl/rajat31/rajat31.mtx"
	"/arc/shared/data/ufl/asia_osm/asia_osm.mtx"
	"/arc/shared/data/ufl/germany_osm/germany_osm.mtx"
	"/arc/shared/data/ufl/NLR/NLR.mtx"
	"/arc/shared/data/ufl/patents/patents.mtx"
	"/arc/shared/data/ufl/cit-Patents/cit-Patents.mtx"
    "/arc/shared/data/ufl/adaptive/adaptive.mtx"
    "/arc/shared/data/ufl/italy_osm/italy_osm.mtx"
    "/arc/shared/data/ufl/M6/M6.mtx"
	"/arc/shared/data/ufl/memchip/memchip.mtx"
	"/arc/shared/data/ufl/cage14/cage14.mtx"
)

outputDirs=(
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
    "~/"
    "~/"
    "~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
	"~/"
)

targettedCacheSizeKB=512 # sizeof(L2)/2
metisBVertexCount=100
binary="$HOME/spgemm/bins/MetisRBP"

logFile="${targettedCacheSizeKB}KB_${metisBVertexCount}.log"
noOfMatrices=${#mmfPathAs[@]}
for (( i = 0; i < $noOfMatrices; i++))
do
	mmfPathA=${mmfPathAs[i]}
	mmfPathB=${mmfPathBs[i]}
	outDir=${outDirss[i]}

    cmd="$binary mmfPathA=$mmfPathA mmfPathB=$mmfPathB targettedCacheSizeKB=$targettedCacheSizeKB outDir=$outDir metisBVertexCount=$metisBVertexCount"

	echo "" >> ${logFile}
	date >> ${logFile}
	$cmd >> ${logFile} 2>&1 3>&1
	echo "" >> ${logFile}

	# move outputs to arc
	nameA=$(basename $mmfPathA)
	nameB=$(basename $mmfPathB)
	mv ~/${nameA}_A_rowPfxSumPerPart0 /arc/user/mbasaran/
	mv ~/${nameA}_A_rowOrder0 /arc/user/mbasaran/
	mv ~/${nameB}_B_rowOrder0 /arc/user/mbasaran/
done

