#!/bin/bash

# rows, columns, nnz
# rowArr=( 1048576 2097152 4194304 8388608 16777216 33554432 )
# nnzPerRowArr=( 2 4 8 16 32 64 128 )
# tempDir=/home/mbasaran
# mmfDir=/arc/user/mbasaran/synthetic-matrices
# bin=build/icc/SyntheticMatrixGenerator

rowArr=( 64 128 )
nnzPerRowArr=( 2 4 8 16 )
tempDir=/home/macbeth/Desktop
mmfDir=input
bin=debug/SyntheticMatrixGenerator

for rows in ${rowArr[@]}
do
	for nnzPerRow in ${nnzPerRowArr[@]}
	do
		mmfPath=${mmfDir}/${rows}_${nnzPerRow}.mtx
		echo -n "Generating ${mmfPath}..."
		$bin outDir=$tempDir mmfPathA=$mmfPath rows=${rows} columns=${rows} nnzPerRow=${nnzPerRow} denseRows=5 denseNnzPerRow=32
		echo " done."
	done
	echo ""
done

