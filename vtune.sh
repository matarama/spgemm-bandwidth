
mmfPathAArr=(
	"/arc/shared/data/ufl/europe_osm/europe_osm.mtx"
	# "/arc/shared/data/ufl/delaunay_n24/delaunay_n24.mtx"
    # "/arc/shared/data/ufl/web-Stanford/web-Stanford.mtx"
    # "/arc/shared/data/ufl/road_usa/road_usa.mtx"
    # "/arc/shared/data/ufl/Freescale1/Freescale1.mtx"
    # "/arc/shared/data/ufl/circuit5M_dc/circuit5M_dc.mtx"
    # "/arc/shared/data/ufl/rajat31/rajat31.mtx"
    # "/arc/shared/data/ufl/asia_osm/asia_osm.mtx"
    # "/arc/shared/data/ufl/germany_osm/germany_osm.mtx"
    # "/arc/shared/data/ufl/NLR/NLR.mtx"
    # "/arc/shared/data/ufl/patents/patents.mtx"
    # "/arc/shared/data/ufl/cit-Patents/cit-Patents.mtx"
    # "/arc/shared/data/ufl/adaptive/adaptive.mtx"
    # "/arc/shared/data/ufl/italy_osm/italy_osm.mtx"
    # "/arc/shared/data/ufl/M6/M6.mtx"
	# "/arc/shared/data/ufl/memchip/memchip.mtx"
	"/arc/shared/data/ufl/cage14/cage14.mtx"
)

numThreadsArr=( 16 64 )
clb=64                   # bytes in cache line
iterations=3
binaries=bins-vtune/*
accSizes=16,64,512

# Select a node to use
cores_per_node=( "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15"
				 "0,1,2,3,18,19,20,21,36,37,38,39,54,55,56,57"
				 "0-15,18-33,36-51,54-69" )

taskset_params=( "0-15"
				 "0-3,18-21,36-39,54-57"
				 "0-15,18-33,36-51,54-69" )

nodeToUseArr=( 0 1 2 )



###############################################################################


for node_to_use in ${nodeToUseArr[@]}
do
	numThreads=${numThreadsArr[node_to_use]}
	cores=${cores_per_node[node_to_use]}
	pc=${taskset_params[node_to_use]}

	export KMP_AFFINITY=granularity=fine,proclist=[$cores],explicit

	noOfMatrices=${#mmfPathAArr[@]}
	for (( i = 0; i < $noOfMatrices; i++))
	do
		mmfPathA=${mmfPathAArr[i]}
		outMmfPath=${numThreads}t_$(basename $mmfPathA)_$(basename $mmfPathA).csv

		for binary in $binaries
		do
			outBinPath=${numThreads}t_$(basename $binary).csv
			outBinMmfPath=${numThreads}t_$(basename $binary)_$(basename $mmfPathA).csv

			cmd="$binary mmfPathA=$mmfPathA numBlocks=$numThreads cacheLineBytes=$clb iterations=$iterations accSizes=$accSizes BSizeKB=1024"

			echo "$cmd"
			date
			echo ""

			/opt/intel/vtune_amplifier_xe/bin64/amplxe-cl -collect general-exploration -knob collect-memory-bandwidth=true -- /usr/bin/taskset -c $pc $cmd

			/opt/intel/vtune_amplifier_xe/bin64/amplxe-cl -collect memory-access -knob analyze-mem-objects=true -knob dram-bandwidth-limits=true  -- /usr/bin/taskset -c $pc $cmd
		done
	done    	
done




# /opt/intel/vtune_amplifier_xe/bin64/amplxe-cl -collect general-exploration -knob collect-memory-bandwidth=true -- taskset -c $pc bins/StaticRowByRow3p_vtune mmfPathA=/arc/shared/data/ufl/europe_osm/europe_osm.mtx numBlocks=$numThreads iterations=5 accSizes=$accSizes > log_europe_osm_ge_${numThreads} 2>&1
# /opt/intel/vtune_amplifier_xe/bin64/amplxe-cl -collect general-exploration -knob collect-memory-bandwidth=true -- taskset -c 54-72 bins/StaticRowByRow3p_vtune mmfPathA=/arc/shared/data/ufl/europe_osm/europe_osm.mtx numBlocks=15 iterations=3 accSizes=16,64,512 > log_europe_osm_ge_15 2>&1

# /opt/intel/vtune_amplifier_xe/bin64/amplxe-cl -collect memory-access -knob analyze-mem-objects=true -knob dram-bandwidth-limits=true -- taskset -c 54-72 bins/StaticRowByRow3p_vtune mmfPathA=/arc/shared/data/ufl/europe_osm/europe_osm.mtx numBlocks=9 iterations=3 accSizes=16,64,512 > log_europe_osm_ma_9 2>&1
# /opt/intel/vtune_amplifier_xe/bin64/amplxe-cl -collect memory-access -knob analyze-mem-objects=true -knob dram-bandwidth-limits=true -- taskset -c 54-72 bins/StaticRowByRow3p_vtune mmfPathA=/arc/shared/data/ufl/europe_osm/europe_osm.mtx numBlocks=15 iterations=3 accSizes=16,64,512 > log_europe_osm_ma_15 2>&1

# /opt/intel/vtune_amplifier_xe/bin64/amplxe-cl -collect concurrency -- taskset -c 54-72 bins/StaticRowByRow3p_vtune mmfPathA=/arc/shared/data/ufl/europe_osm/europe_osm.mtx numBlocks=9 iterations=3 accSizes=16,64,512 > log_europe_osm_c_9 2>&1
# /opt/intel/vtune_amplifier_xe/bin64/amplxe-cl -collect concurrency -- taskset -c 54-72 bins/StaticRowByRow3p_vtune mmfPathA=/arc/shared/data/ufl/europe_osm/europe_osm.mtx numBlocks=15 iterations=3 accSizes=16,64,512 > log_europe_osm_c_15 2>&1

# /opt/intel/vtune_amplifier_xe/bin64/amplxe-cl -report summary -r r000ge -format=csv > r000ge.csv
# /opt/intel/vtune_amplifier_xe/bin64/amplxe-cl -report summary -r r001ge -format=csv > r001ge.csv
# /opt/intel/vtune_amplifier_xe/bin64/amplxe-cl -report summary -r r002macc -format=csv > r002macc.csv
# /opt/intel/vtune_amplifier_xe/bin64/amplxe-cl -report summary -r r003macc -format=csv > r003macc.csv
# /opt/intel/vtune_amplifier_xe/bin64/amplxe-cl -report summary -r r004cc -format=csv > r004cc.csv
# /opt/intel/vtune_amplifier_xe/bin64/amplxe-cl -report summary -r r005cc -format=csv > r005cc.csv
