#!/bin/bash

###############################################################################

mmfPathAArr=(
	"/arc/user/mbasaran/synthetic-matrices/1048576_2.mtx"
    "/arc/user/mbasaran/synthetic-matrices/1048576_8.mtx"
    "/arc/user/mbasaran/synthetic-matrices/1048576_16.mtx"
    "/arc/user/mbasaran/synthetic-matrices/1048576_32.mtx"
    "/arc/user/mbasaran/synthetic-matrices/1048576_8.mtx"
    "/arc/user/mbasaran/synthetic-matrices/1048576_8.mtx"
    "/arc/user/mbasaran/synthetic-matrices/1048576_64.mtx"
    "/arc/user/mbasaran/synthetic-matrices/1048576_64.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_2.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_8.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_16.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_32.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_8.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_8.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_64.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_64.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_2.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_64.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_4.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_64.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_8.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_64.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_16.mtx"
)

mmfPathBArr=(
	"/arc/user/mbasaran/synthetic-matrices/1048576_2.mtx"
    "/arc/user/mbasaran/synthetic-matrices/1048576_64.mtx"
    "/arc/user/mbasaran/synthetic-matrices/1048576_64.mtx"
    "/arc/user/mbasaran/synthetic-matrices/1048576_64.mtx"
    "/arc/user/mbasaran/synthetic-matrices/1048576_2_128_512.mtx"
    "/arc/user/mbasaran/synthetic-matrices/1048576_2_64_512.mtx"
    "/arc/user/mbasaran/synthetic-matrices/1048576_2_128_512.mtx"
    "/arc/user/mbasaran/synthetic-matrices/1048576_2.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_2.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_64.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_64.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_64.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_2_128_512.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_2_64_512.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_2_128_512.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_2.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_64.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_4.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_64.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_8.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_64.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_16.mtx"
    "/arc/user/mbasaran/synthetic-matrices/2097152_64.mtx"
)

numThreadsArr=( 15 )
clb=64                   # bytes in cache line
iterations=5
binaries=bins/*

# Algorithm Specific parameters
rowsPerBucket=32768
hybLess=32768
hybPercent=0.01
fpPartSizeKB=512
spPartSizeKB=1024

# Select a node to use
cores_per_node=( "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17"
                 "18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35"
                 "36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53"
                 "54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71" )

taskset_params=( "0-17"
                 "18-35"
                 "36-53"
                 "54-71" )

node_to_use=3
cores=${cores_per_node[node_to_use]}
pc=${taskset_params[node_to_use]}

export KMP_AFFINITY=granularity=fine,proclist=[$cores],explicit

###############################################################################

noOfMatrices=${#mmfPathAArr[@]}
for numThreads in ${numThreadsArr[@]}
do
    for (( i = 0; i < $noOfMatrices; i++))
    do
        mmfPathA=${mmfPathAArr[i]}
                outMmfPath=$(basename $mmfPathA)_$(basename $mmfPathA).csv
        mmfPathB=${mmfPathBArr[i]}
        outMmfPath=$(basename $mmfPathA)_$(basename $mmfPathB).csv

        for binary in $binaries
        do
            outBinPath=$(basename $binary).csv
            outBinMmfPath=$(basename $binary)_$(basename $mmfPathA)_$(basename $mmfPathB).csv
                        #outBinMmfPath=$(basename $binary)_$(basename $mmfPathA)_$(basename $mmfPathA).csv

            cmd_core="$binary mmfPathA=$mmfPathA mmfPathB=$mmfPathB numBlocks=$numThreads cacheLineBytes=$clb iterations=$iterations"
            cmd_alg="hybLess=$hybLess hyb%=$hybPercent rowsPerBucket=$rowsPerBucket fpPartSizeKB=$fpPartSizeKB spPartSizeKB=$spPartSizeKB"
            cmd="$cmd_core $cmd_alg"

            echo "$cmd"
            date
            echo ""

            echo $cmd | tee -a $outMmfPath $outBinPath $outBinMmfPath > /dev/null
            taskset -c $pc $cmd | tee -a $outBinPath $outBinMmfPath > /dev/null
            echo "" | tee -a $outMmfPath $outBinPath $outBinMmfPath > /dev/null
            echo "" | tee -a $outBinPath $outBinMmfPath > /dev/null
        done
        echo "" | tee -a $outMmfPath > /dev/null
    done    
done
