
// Optimized min-heap implementation for multiway-merge
// -----------------------------------------------------------------------------

template < typename K, typename V, typename I >
MultiwayMerge::MinHeap< K, V, I >::MinHeap()
: size( 0 ), keys( nullptr ), values( nullptr ), indices( nullptr )
{
}

template < typename K, typename V, typename I >
MultiwayMerge::MinHeap< K, V, I >::~MinHeap()
{
	free();
}

template < typename K, typename V, typename I >
void
MultiwayMerge::MinHeap< K, V, I >::init( const K& capacity )
{
	size = 0;
	keys = util::alloc< K >( capacity );
	values = util::alloc< V >( capacity );
	indices = util::alloc< I >( capacity );
}

template < typename K, typename V, typename I >
void
MultiwayMerge::MinHeap< K, V, I >::free()
{
	size = 0;
	util::free( keys );
	util::free( values );
	util::free( indices );
}

template < typename K, typename V, typename I >
void
MultiwayMerge::MinHeap< K, V, I >::insert(
	const K& k, const V& v, const I& i )
{
	ui32 current = size;
	ui32 parent = ( size - 1 ) / 2;
	while ( parent < current && keys[ parent ] > k )
	{
		keys[ current ] = keys[ parent ];
		values[ current ] = values[ parent ];
		indices[ current ] = indices[ parent ];

		current = parent;
		parent = ( parent - 1 ) / 2;
	}

	keys[ current ] = k;
	values[ current ] = v;
	indices[ current ] = i;
	++size;
}

template < typename K, typename V, typename I >
void
MultiwayMerge::MinHeap< K, V, I >::min( K& k_out, V& v_out, I& i_out )
{
	k_out = keys[ 0 ];
	v_out = values[ 0 ];
	i_out = indices[ 0 ];
}

template < typename K, typename V, typename I >
void
MultiwayMerge::MinHeap< K, V, I >::heapify( K& k, V& v, I& i )
{
	if ( i < 0 )
	{
		--size;
		k = keys[ size ];
		v = values[ size ];
		i = indices[ size ];
	}

	ui32 curr = 0;
	ui32 next = 0;
	ui32 left;
	ui32 right;
	bool goon = true;
	do
	{
		left = curr * 2 + 1;
		right = curr * 2 + 2;

		if ( left < size && keys[ left ] < k )
			next = left;
		if ( right < size && keys[ right ] < k &&
		     keys[ right ] <= keys[ left ] )
			next = right;

		if ( curr != next )
		{
			keys[ curr ] = keys[ next ];
            values[ curr ] = values[ next ];
            indices[ curr ] = indices[ next ];
			curr = next;
		}
		else
		{
			goon = false;
		}
	} while ( goon );

	keys[ curr ] = k;
	values[ curr ] = v;
	indices[ curr ] = i;
}

// -----------------------------------------------------------------------------

template < typename K, typename V, typename I >
bool
MultiwayMerge::MinHeap< K, V, I >::checkHeapProperty( const ui32& i )
{
	if ( size <= 1 || i < ( size + 1 ) / 2 )
		return true;

	if ( keys[ i ] < keys[ 2 * i + 1 ] && keys[ i ] < keys[ 2 * i + 2 ] )
		return checkHeapProperty( 2 * i + 1 ) && checkHeapProperty( 2 * i + 2 );

	return false;
}

template < typename K, typename V, typename I >
std::ostream&
MultiwayMerge::MinHeap< K, V, I >::str( std::ostream& out )
{
	for ( ui32 i = 0; i < size; ++i )
		out << i + 1
		    << ". K=" << keys[ i ]
		    << " V=" << values[ i ]
		    << " I=" << indices[ i ] << std::endl;
	out << std::endl;

	return out;
}
