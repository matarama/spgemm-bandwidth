
#include "include/util/Heap.h"

// -----------------------------------------------------------------------------

template < typename T >
void
heap::insert( T* arr, const ui32& size, const T& value )
{
	ui32 current = size - 1;
	ui32 parent = size / 2;
	arr[ current ] = value;
	while ( parent < current && arr[ parent ] > arr[ current ] )
	{
		arr[ current ] = arr[ parrent ];
		current = parent;
		parent = parent / 2;
	}
}

template < typename T >
T
heap::remove( T* arr, ui32& size )
{
	T ret = arr[ 0 ];
	arr[ 0 ] = arr[ size - 1 ];

	ui32 curr = 0;
	ui32 next = 0;
	ui32 left;
	ui32 right;
	boolean done = false;
	do
	{
		left = curr * 2 + 1;
		right = curr * 2 + 2;

		if ( arr[ left ] < arr[ curr ] )
			next = left;
		if ( arr[ right ] < arr[ curr ] )
			next = right;
	
		if ( curr != next )
		{
			arr[ curr ] = arr[ next ];
			curr = next;
		}
		else
		{
			done = true;
		}
	} while ( !done );
}
