
#include <cassert>
#include <set>
#include <unordered_set>

#include "include/util/Spgemm.h"

#include "include/util/Memory.h"

Triplet*
util::preprocessC( Triplets& A, Triplets& B, const ui32& numThreads,
                   double** accumulatorPerThrd,
                   ui64& nnzC_out, ui64& mults_out, ui64* multsPerThrd )
{
	assert( A.colStart() == B.rowStart() );

	A.sortRowMajor( numThreads );
	B.sortRowMajor( numThreads );
	std::shared_ptr< std::vector< glbNnz_t > > pA_rowPtr = A.rowPtr();
	std::shared_ptr< std::vector< glbNnz_t > > pB_rowPtr = B.rowPtr();
	std::vector< glbNnz_t >& A_rowPtr = *pA_rowPtr.get();
	std::vector< glbNnz_t >& B_rowPtr = *pB_rowPtr.get();

	ui64 mults = 0;
	ui64 nnzC = 0;
	glbNnz_t cNnzPtr[ numThreads + 1 ];

	glbNnz_t rowsPerThread = ( A.rows() + numThreads - 1 ) / numThreads;
	glbNnz_t threadRowPtr[ numThreads + 1 ];
	threadRowPtr[ 0 ] = 0;
	for ( ui32 i = 1; i < numThreads; ++i )
		threadRowPtr[ i ] = threadRowPtr[ i - 1 ] + rowsPerThread;
	threadRowPtr[ numThreads ] = A.rows();

	std::vector< std::vector< Triplet > > cTripPerThread(
		numThreads, std::vector< Triplet >() );

	Triplet* cTripletArr = nullptr;
	#pragma omp parallel num_threads( numThreads )
	{
		const int TID = omp_get_thread_num();
		char* hashSet = (char*) accumulatorPerThrd[ TID ];

		const glbInd_t TROW_START = threadRowPtr[ TID ];
		const glbInd_t TROW_END = threadRowPtr[ TID + 1 ];
		std::vector< Triplet >& tCTriplets = cTripPerThread[ TID ];
		ui64 tMults = 0;

		for ( glbInd_t i = TROW_START; i < TROW_END; ++i )
		{
			glbInd_t noOfKeys = 0;
			for ( glbNnz_t n = A_rowPtr[ i ]; n < A_rowPtr[ i + 1 ]; ++n )
			{
				glbInd_t j = A[ n ].j;
				for ( glbNnz_t k = B_rowPtr[ j ]; k < B_rowPtr[ j + 1 ]; ++k )
				{
					if ( hashSet[ B[ k ].j ] != 1 )
					{
						hashSet[ B[ k ].j ] = 1;
						tCTriplets.push_back( Triplet( i, B[ k ].j, 0 ) );
						++noOfKeys;
					}
				}

				tMults += B_rowPtr[ j + 1 ] - B_rowPtr[ j ];
			}

			for ( glbInd_t n = 1; n <= noOfKeys; ++n )
				hashSet[ tCTriplets[ tCTriplets.size() - n ].j ] = 0;
		}

		multsPerThrd[ TID ] = tMults;

		#pragma omp barrier
		#pragma omp single
		{
			cNnzPtr[ 0 ] = 0;
			for ( auto i = 0; i < cTripPerThread.size(); ++i )
			{
				nnzC += cTripPerThread[ i ].size();
				cNnzPtr[ i + 1 ] = nnzC;
				mults += multsPerThrd[ i ];
			}

			cTripletArr = new Triplet[ nnzC ];
		}
		#pragma omp barrier

		ui64 offset = cNnzPtr[ TID ];
		for ( auto i = 0; i < tCTriplets.size(); ++i )
		{
			cTripletArr[ offset + i ] = tCTriplets[ i ];
		}
	}

	nnzC_out = nnzC;
	mults_out = mults;
	return cTripletArr;
}


void
util::generateMultiMapSTL_verbose(
	const std::vector< ui32 >& vAccLengths,
	const glbNnz_t& NO_OF_ROWS, const glbInd_t& NO_OF_COLUMNS,
	glbNnz_t const* const C_rowPtr, glbInd_t const* const C_colInd,
	const ui32& numThreads,
	char* maskIndexPerRow,
	const std::vector< ui64 >& multsPerRow )
{
	std::vector< glbInd_t > threadRowPfxSum( numThreads + 1, 0 );
	glbInd_t rowsPerThread = NO_OF_ROWS / numThreads;
	for ( ui32 i = 0; i < numThreads; ++i )
		threadRowPfxSum[ i + 1 ] = threadRowPfxSum[ i ] + rowsPerThread;
	threadRowPfxSum[ numThreads ] = NO_OF_ROWS;

	#pragma omp parallel num_threads( numThreads )
	{
		ui32 tid = omp_get_thread_num();
		glbInd_t tRowStart = threadRowPfxSum[ tid ];
		glbInd_t tRowEnd = threadRowPfxSum[ tid + 1 ];

		for ( glbInd_t i = tRowStart; i < tRowEnd; ++i )
		{
			bool hasCollision;
			ui32 mark = 0;
			do
			{
				hasCollision = false;
				ui32 mask = vAccLengths[ mark ] - 1;

				std::unordered_set< glbInd_t > colIndSet;
				for ( glbNnz_t n = C_rowPtr[ i ];
				      n < C_rowPtr[ i + 1 ] && !hasCollision;
				      ++n )
				{
					glbInd_t j = C_colInd[ n ];
					if ( colIndSet.find( j & mask ) == colIndSet.end() )
						colIndSet.insert( j & mask );
					else
						hasCollision = true;
				}

				if ( hasCollision )
					++mark;

			} while( hasCollision );
            
			maskIndexPerRow[ i ] = mark;
		}
	}

	ui64 totalMultCount = 0;
	std::vector< ui64 > markRowCounts( vAccLengths.size(), 0 );
	std::vector< ui64 > markRowMultCounts( vAccLengths.size(), 0 );
	for ( ui32 i = 0; i < NO_OF_ROWS; ++i )
	{
		++markRowCounts[ maskIndexPerRow[ i ] ];
		markRowMultCounts[ maskIndexPerRow[ i ] ] += multsPerRow[ i ];
		totalMultCount += multsPerRow[ i ];
	}

	for ( ui32 i = 0; i < markRowCounts.size(); ++i )
	{
		double markRowPct =
			( (double) markRowCounts[ i ] / NO_OF_ROWS ) * 100;
		double markRowMultPct =
			( (double) markRowMultCounts[ i ] / totalMultCount ) * 100;

		std::cout << ( vAccLengths[ i ] * 8 ) / 1024 << "KB,";
		std::cout << markRowCounts[ i ] << "," << markRowPct << ",";
		std::cout << markRowMultCounts[ i ] << "," << markRowMultPct << ",";
	}
}

void
util::generateMultiMapSTL(
	const std::vector< ui32 >& vAccLengths,
	const glbNnz_t& NO_OF_ROWS, const glbInd_t& NO_OF_COLUMNS,
	glbNnz_t const* const C_rowPtr, glbInd_t const* const C_colInd,
	const ui32& numThreads,
	char* maskIndexPerRow )
{
	std::vector< glbInd_t > threadRowPfxSum( numThreads + 1, 0 );
	glbInd_t rowsPerThread = NO_OF_ROWS / numThreads;
	for ( ui32 i = 0; i < numThreads; ++i )
		threadRowPfxSum[ i + 1 ] = threadRowPfxSum[ i ] + rowsPerThread;
	threadRowPfxSum[ numThreads ] = NO_OF_ROWS;

	#pragma omp parallel num_threads( numThreads )
	{
		ui32 tid = omp_get_thread_num();
		glbInd_t tRowStart = threadRowPfxSum[ tid ];
		glbInd_t tRowEnd = threadRowPfxSum[ tid + 1 ];

		for ( glbInd_t i = tRowStart; i < tRowEnd; ++i )
		{
			bool hasCollision;
			ui32 mark = 0;
			do
			{
				hasCollision = false;
				ui32 mask = vAccLengths[ mark ] - 1;

				std::unordered_set< glbInd_t > colIndSet;
				for ( glbNnz_t n = C_rowPtr[ i ];
				      n < C_rowPtr[ i + 1 ] && !hasCollision;
				      ++n )
				{
					glbInd_t j = C_colInd[ n ];
					if ( colIndSet.find( j & mask ) == colIndSet.end() )
						colIndSet.insert( j & mask );
					else
						hasCollision = true;
				}

				if ( hasCollision )
					++mark;

				if ( mark == vAccLengths.back() )
					break;

			} while( hasCollision );
            
			maskIndexPerRow[ i ] = mark;
		}
	}
}

void
util::generateMultiMapLinearTime(
	const std::vector< ui32 >& vAccLengths,
	const glbNnz_t& NO_OF_ROWS, const glbInd_t& NO_OF_COLUMNS,
	glbNnz_t const* const C_rowPtr, glbInd_t const* const C_colInd,
	const ui32& numThreads, fp_t** accumulatorsPerThrd, 
	char* maskIndexPerRow )
{
	std::vector< glbInd_t > threadRowPfxSum( numThreads + 1, 0 );
	glbInd_t rowsPerThread = NO_OF_ROWS / numThreads;
	for ( ui32 i = 0; i < numThreads; ++i )
		threadRowPfxSum[ i + 1 ] = threadRowPfxSum[ i ] + rowsPerThread;
	threadRowPfxSum[ numThreads ] = NO_OF_ROWS;

	#pragma omp parallel num_threads( numThreads )
	{
		ui32 tid = omp_get_thread_num();
		glbInd_t tRowStart = threadRowPfxSum[ tid ];
		glbInd_t tRowEnd = threadRowPfxSum[ tid + 1 ];

		// calculate nnz of C's biggest row
		glbInd_t maxNnz = 0;
		for ( glbInd_t i = tRowStart; i < tRowEnd; ++i )
		{
			const glbInd_t NNZ = C_rowPtr[ i + 1 ] - C_rowPtr[ i ];
			if ( NNZ > maxNnz )
				maxNnz = NNZ;
		}

		glbInd_t* hashSet = (glbInd_t*) accumulatorsPerThrd[ tid ];
		glbInd_t* collisionSet = (glbInd_t*) ( hashSet + NO_OF_COLUMNS );
		glbInd_t* collidedKeys = util::alloc< glbInd_t >( maxNnz );
		for ( glbInd_t i = tRowStart; i < tRowEnd; ++i )
		{
			glbInd_t noOfCollidedKeys = 0;
			for ( glbNnz_t n = C_rowPtr[ i ];
			      n < C_rowPtr[ i + 1 ];
			      ++n, ++noOfCollidedKeys )
				collidedKeys[ noOfCollidedKeys ] = C_colInd[ n ];
	
			int currentMaskIndex = -1;
			do
			{
				++currentMaskIndex;
				glbInd_t currNoOfCollidedKeys = 0;
				ui32 mask = vAccLengths[ currentMaskIndex ] - 1;
				for ( glbInd_t n = 0; n < noOfCollidedKeys; ++n )
				{
					const glbInd_t K = collidedKeys[ n ];
					const glbInd_t HASH_INDEX = K & mask;
					const glbInd_t PREV = hashSet[ HASH_INDEX ];

					if ( PREV == 0 )
					{
						hashSet[ HASH_INDEX ] = K;
					}
					else if ( PREV != K )
					{
						if ( collisionSet[ K ] == 0 )
							collidedKeys[ currNoOfCollidedKeys++ ] = K;

						if ( collisionSet[ PREV ] == 0 )
							collidedKeys[ currNoOfCollidedKeys++ ] = PREV;

						collisionSet[ K ] = K;
						collisionSet[ PREV ] = PREV;
					}
				}

				// Reset hashSet and collisionSet arrays
				for ( glbNnz_t n = C_rowPtr[ i ]; n < C_rowPtr[ i + 1 ]; ++n )
					hashSet[ C_colInd[ n ] & mask ] = 0;

				for ( glbInd_t n = 0; n < noOfCollidedKeys; ++n )
					collisionSet[ collidedKeys[ n ] ] = 0;

				noOfCollidedKeys = currNoOfCollidedKeys;
			}
			while ( noOfCollidedKeys > 0 &&
			        currentMaskIndex < vAccLengths.back() );

			maskIndexPerRow[ i ] = currentMaskIndex;
		}

		util::free( collidedKeys );
	}
}

void
util::generateMultiMapSymbolic(
	const std::vector< ui32 >& vAccLengths,
	const glbNnz_t& NO_OF_ROWS, const glbInd_t& NO_OF_COLUMNS,
	glbNnz_t const* const C_rowPtr, glbInd_t const* const C_colInd,
	const ui32& numThreads, fp_t** accumulatorsPerThrd, 
	char* maskIndexPerRow )
{
	std::vector< glbInd_t > threadRowPfxSum( numThreads + 1, 0 );
	glbInd_t rowsPerThread = NO_OF_ROWS / numThreads;
	for ( ui32 i = 0; i < numThreads; ++i )
		threadRowPfxSum[ i + 1 ] = threadRowPfxSum[ i ] + rowsPerThread;
	threadRowPfxSum[ numThreads ] = NO_OF_ROWS;

	#pragma omp parallel num_threads( numThreads )
	{
		ui32 tid = omp_get_thread_num();
		glbInd_t tRowStart = threadRowPfxSum[ tid ];
		glbInd_t tRowEnd = threadRowPfxSum[ tid + 1 ];
		glbInd_t* hashArr = (glbInd_t*) accumulatorsPerThrd[ tid ];

		for ( glbInd_t i = tRowStart; i < tRowEnd; ++i )
		{
			bool hasCollision;
			int maskIndex = -1;
			do
			{
				hasCollision = false;
				++maskIndex;
				if ( maskIndex == vAccLengths.back() )
					break;

				ui32 mask = vAccLengths[ maskIndex ] - 1;
				glbNnz_t n = C_rowPtr[ i ];
				for ( ; n < C_rowPtr[ i + 1 ] && !hasCollision; ++n )
				{
					glbInd_t j = C_colInd[ n ];
					glbInd_t HASH_INDEX = j & mask;

					if ( hashArr[ HASH_INDEX ] != 0 && 
					     hashArr[ HASH_INDEX ] != j )
						hasCollision = true;
					else
						hashArr[ HASH_INDEX ] = j;
				}

				// Reset hash array
				for ( glbInd_t l = C_rowPtr[ i ]; l < n; ++l )
				{
					glbInd_t j = C_colInd[ l ];
					glbInd_t HASH_INDEX = j & mask;
					hashArr[ HASH_INDEX ] = 0;
				}

			} while( hasCollision );
			
			maskIndexPerRow[ i ] = maskIndex;

			// Reset hash array
			ui32 mask = vAccLengths[ maskIndex ] - 1;
			for ( glbInd_t n = C_rowPtr[ i ]; n < C_rowPtr[ i + 1 ]; ++n )
			{
				glbInd_t j = C_colInd[ n ];
				glbInd_t HASH_INDEX = j & mask;
				hashArr[ HASH_INDEX ] = 0;
			}
		}
	}
}

