
#include "include/util/Memory.h"

#ifdef WITH_LIB_NUMA
#include <numa.h>

void*
allocator::NumaLocal::alloc( const std::size_t& bytes )
{	
	return numa_alloc_local( bytes );
}

void*
allocator::NumaLocal::alloc( const std::size_t& length,
                             const std::size_t& bytesPerEntry )
{
	return alloc( bytesPerEntry * length );
}

void*
allocator::NumaLocal::initAlloc( const std::size_t& bytes )
{
	char* arr = static_cast< char* >( alloc( bytes ) );
	for ( std::size_t i = 0; i < bytes; ++i )
		arr[ i ] = 0;
	return static_cast< void* >( arr );
}

void*
allocator::NumaLocal::initAlloc( const std::size_t& length,
                                 const std::size_t& bytesPerEntry )
{
	return initAlloc( length * bytesPerEntry );
}

void
allocator::NumaLocal::free( void* t,
                            const std::size_t& length,
                            const std::size_t& bytesPerEntry )
{
	numa_free( static_cast< void* >( t ), length * bytesPerEntry );
}

// -----------------------------------------------------------------------------

void*
allocator::NumaInterleaved::alloc( const std::size_t& bytes )
{
	return numa_alloc_interleaved( bytes );
}

void*
allocator::NumaInterleaved::alloc( const std::size_t& length,
                                   const std::size_t& bytesPerEntry )
{
	return alloc( bytesPerEntry * length );
}

void*
allocator::NumaInterleaved::initAlloc( const std::size_t& bytes )
{
	char* arr = static_cast< char* >( alloc( bytes ) );
	for ( std::size_t i = 0; i < bytes; ++i )
		arr[ i ] = 0;
	return static_cast< void* >( arr );
}

void*
allocator::NumaInterleaved::initAlloc( const std::size_t& length,
                                       const std::size_t& bytesPerEntry )
{
	return initAlloc( length * bytesPerEntry );
}

void
allocator::NumaInterleaved::free( void* t,
                                  const std::size_t& length,
                                  const std::size_t& bytesPerEntry )
{
	numa_free( static_cast< void* >( t ), length * bytesPerEntry );
}

#endif /* WITH_LIB_NUMA */

// -----------------------------------------------------------------------------

void*
allocator::Aligned::alloc( const std::size_t& bytes )
{
	void* spc = nullptr;
#ifdef __INTEL_COMPILER
	spc =  _mm_malloc( bytes, ALIGNMENT );
#else
	posix_memalign( (void**) &spc, ALIGNMENT, bytes );
#endif
	return spc;
}

void*
allocator::Aligned::alloc( const std::size_t& length,
                           const std::size_t& bytesPerEntry )
{
	return alloc( bytesPerEntry * length );
}

void*
allocator::Aligned::initAlloc( const std::size_t& bytes )
{
	char* spc = static_cast< char* >( alloc( bytes ) );
	for ( std::size_t i = 0; i < bytes; ++i )
		spc[ i ] = 0;
	return static_cast< void* >( spc );
}

void*
allocator::Aligned::initAlloc( const std::size_t& length,
                               const std::size_t& bytesPerEntry )
{
	return initAlloc( length * bytesPerEntry );
}

void
allocator::Aligned::free( void* t,
                          const std::size_t& length,
                          const std::size_t& bytesPerEntry )
{
#ifdef __INTEL_COMPILER
	_mm_free( t );
#else
	std::free( t );
#endif
}

// -----------------------------------------------------------------------------

#ifdef WITH_HBW_MEMORY
#include <hbwmalloc.h>

void*
allocator::Mcdram::alloc( const std::size_t& bytes )
{
	void* spc = nullptr;
	hbw_posix_memalign( (void**) &spc, ALIGNMENT, bytes );
	return spc;
}

void*
allocator::Mcdram::alloc( const std::size_t& length,
                          const std::size_t& bytesPerEntry )
{
	return alloc( length * bytesPerEntry );
}

void*
allocator::Mcdram::initAlloc( const std::size_t& bytes )
{
	char* spc = static_cast< char* >( alloc( bytes ) );
	for ( std::size_t i = 0; i < bytes; ++i )
		spc[ i ] = 0;
	return static_cast< void* >( spc );
}

void*
allocator::Mcdram::initAlloc( const std::size_t& length,
                              const std::size_t& bytesPerEntry )
{
	return initAlloc( length * bytesPerEntry );
}

void
allocator::Mcdram::free( void* t,
                         const std::size_t& length,
                         const std::size_t& bytesPerEntry )
{
	hbw_free( t );
}

#endif /* WITH_HBW_MEMORY */
