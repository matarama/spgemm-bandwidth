/*
 * Data.cpp
 *
 *  Created on: Apr 24, 2017
 *      Author: memoks
 */

#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <map>
#include <algorithm>
#include <omp.h>

#include "include/util/Data.h"

#include "include/config.h"
#include "include/util/Generic.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
#include "include/decomposition/Parter.h"
#include "include/decomposition/RowSlicer.h"
#include "include/decomposition/RowSlicerLmt.h"
#include "include/decomposition/custom/RowSlicerNnzRowPtr.h"
#include "include/decomposition/RowSlicerStatic.h"
#include "include/decomposition/RowSlicerGreedy.h"
#include "include/decomposition/ColumnSlicer.h"
#include "include/decomposition/ColumnSlicerLmt.h"
#include "include/decomposition/ColumnSlicerStatic.h"
#include "include/decomposition/ColumnSlicerGreedy.h"
#include "include/decomposition/CheckerBoard.h"
#include "include/decomposition/CheckerBoardLmt.h"
#include "include/decomposition/Alg.h"

/*
void
data::prepareJobs(
    Triplets& A, Triplets& B, Triplets C,
    Csr< glbInd_t, fp_t, glbNnz_t >** spmB_out,
    Csr< glbInd_t, fp_t, glbNnz_t >** spmC_out,
    double partSizeC_KB, double partSizeB_KB,
    JobFp*** fpJobs_out, ui32* numFpJobs_out,
    JobSp*** spJobs_out, ui32* numSpJobs_out )
{
	try
	{
		C.sortRowMajor();
		std::shared_ptr< std::vector< glbNnz_t > > pRowPtrC = C.rowPtr();
		std::vector< glbNnz_t > rowPtrC = *pRowPtrC.get();

		ui64 totalNoOfBuckets = 0;
		ui64 totalNoOfSubs = 0;
		ui64 paddedTo = ALIGNMENT / sizeof( fp_t );


		std::vector< nnz_t* > ARowPtrs;
		std::vector< std::shared_ptr< std::vector< glbNnz_t > > > BRowPtrs;
		std::vector< ui64 > bucketRowPtr;
		std::vector< std::vector< bool > > bucketSubMtxBitTable;
		std::vector< std::vector< ui64 > > subMtxWriteLengthPtrPerBucket;
		std::vector< std::vector< ui64 > > subMtxPaddingLengthPerBucket;

		// partition B into submatrices so that
		// their size is no more than given treshold
		// ---------------------------------------------------------------------

		std::vector< Triplets > As;
		std::vector< Triplets > Bs;
		{
			Bs = RowSlicerNnzRowPtr(
				partSizeB_KB, sizeof( fp_t ), sizeof( nnz_t ) ).partition( B );
			std::vector< glbInd_t > _fpBRowPtr;
			for ( auto it = Bs.begin(); it != Bs.end(); ++it )
				_fpBRowPtr.push_back( it->rowStart() );
			_fpBRowPtr.push_back( Bs.back().rowEnd() );

			As = ColumnSlicerStatic( _fpBRowPtr ).partition( A );
		}

		totalNoOfSubs = Bs.size();

		// Partition local C matrix
		// ---------------------------------------------------------------------
		// Partition C matrix into buckets so that size of each bucket will be
		// less than the given treshold. Size of bucket: C.nnz + C.rowPtr

		std::vector< Triplets > cBuckets = RowSlicerNnzRowPtr(
				partSizeC_KB, sizeof( fp_t ), sizeof( nnz_t ) ).partition( C );

		bucketRowPtr.resize( cBuckets.size() + 1, 0 );
		for ( std::size_t i = 0; i < cBuckets.size(); ++i )
			bucketRowPtr[ i ] = cBuckets[ i ].rowStart();
		bucketRowPtr[ cBuckets.size() ] = cBuckets.back().rowEnd();
		totalNoOfBuckets = cBuckets.size();


		JobFp** jobsFp = nullptr;
		JobSp** jobsSp = nullptr;
		#pragma omp parallel
		{
			#pragma omp single
			{
				jobsFp = util::alloc< JobFp* >( totalNoOfSubs );
				jobsSp = util::alloc< JobSp* >( totalNoOfBuckets );
			}
		}

		ARowPtrs.resize( totalNoOfSubs, nullptr );
		BRowPtrs.resize( totalNoOfSubs, nullptr );
		#pragma omp parallel for
		for ( ui32 s = 0; s < totalNoOfSubs; ++s )
		{
			jobsFp[ s ] = util::alloc< JobFp >();
			JobFp& job = *jobsFp[ s ];

			job.A.initDflt();
			job.A.extract( As[ s ] );

			ARowPtrs[ s ] = job.A.spm().getRowPtr();
			BRowPtrs[ s ] = Bs[ s ].rowPtr();
		}

		// calculate bucketSubMtxBitTable, subMtxWriteLengthPerBucket
		// ---------------------------------------------------------------------
		// example bucketSubMtxBitTable; (assuming the settings mentioned above)
		// |-----------+---+---+---+---+---+---+---|
		// |           |      Sub-Matrix Ids       |
		// |-----------+---+---+---+---+---+---+---|
		// | Bucket id | 0 | 1 | 2 | 3 | 4 | 5 | 6 |
		// |-----------+---+---+---+---+---+---+---|
		// |         0 | T | T | T | F | F | F | F |
		// |         1 | T | F | F | T | F | T | T |
		// |         2 | F | F | T | T | F | F | F |
		// |         3 | F | T | T | T | T | F | T |
		// |         4 | T | T | F | F | F | F | T |
		// |         5 | T | T | T | F | T | F | F |
		// |         6 | F | F | F | F | F | T | T |
		// |-----------+---+---+---+---+---+---+---|
		// where T (true) in cell (i, j) indicates that j.th sub-matrix will
		// write into i.th bucket. Similary F (false) means that sub-matrix
		// will not have any results for that bucket.

		// subMtxWriteLengthPerBlock array keeps track of how many entries
		// sub-matrix i will write into a block buffer. Note that a block
		// buffer consists of all of that block's buckets put together.

		bucketSubMtxBitTable.resize( totalNoOfBuckets, std::vector< bool >() );
		subMtxPaddingLengthPerBucket.resize( totalNoOfBuckets,
		                                     std::vector< ui64 >() );
		subMtxWriteLengthPtrPerBucket.resize( totalNoOfBuckets,
		                                      std::vector< ui64 >() );
		for ( std::size_t i = 0; i < totalNoOfBuckets; ++i )
		{
			subMtxWriteLengthPtrPerBucket[ i ].resize( totalNoOfSubs + 1, 0 );
			subMtxPaddingLengthPerBucket[ i ].resize( totalNoOfSubs, 0 );
			bucketSubMtxBitTable[ i ].resize( totalNoOfSubs, false );
		}
		#pragma omp parallel for
		for ( ui32 s = 0; s < totalNoOfSubs; ++s )
		{
			Triplets& A = As[ s ];
			Triplets& B = Bs[ s ];

			nnz_t* rowPtrA = ARowPtrs[ s ];
			std::vector< glbNnz_t >& rowPtrB = *BRowPtrs[ s ].get();

			for ( ui32 b = 0; b < totalNoOfBuckets; ++b )
			{
				glbInd_t bucketRowStart = bucketRowPtr[ b ];
				glbInd_t bucketRowEnd = bucketRowPtr[ b + 1 ];

				glbNnz_t nzStartA = rowPtrA[ bucketRowStart ];
				glbNnz_t nzEndA = rowPtrA[ bucketRowEnd ];

				ui64 lengthCurrBucket = 0;
				for ( glbNnz_t nzA = nzStartA; nzA < nzEndA; ++nzA )
				{
					glbInd_t rowIndB = A[ nzA ].j - A.colStart();
					lengthCurrBucket +=
						rowPtrB[ rowIndB + 1 ] - rowPtrB[ rowIndB ];
				}

				ui64 padding = 0;
				if ( lengthCurrBucket % paddedTo != 0 )
					padding = paddedTo - lengthCurrBucket % paddedTo;

				subMtxWriteLengthPtrPerBucket[ b ][ s + 1 ] =
					lengthCurrBucket + padding;
				subMtxPaddingLengthPerBucket[ b ][ s ] = padding;
				bucketSubMtxBitTable[ b ][ s ] =
					lengthCurrBucket > 0 ? true : false;
			}
		}

		for ( std::size_t b = 0; b < totalNoOfBuckets; ++b )
		{
			std::vector< ui64 >& bucketSubMtxWriteLengthPtr =
				subMtxWriteLengthPtrPerBucket[ b ];
			for ( ui32 s = 0; s < totalNoOfSubs; ++s )
				bucketSubMtxWriteLengthPtr[ s + 1 ] +=
					bucketSubMtxWriteLengthPtr[ s ];
		}

		// preprocess row-index & col-offset per multiplication
		#pragma omp parallel for
		for ( ui32 b = 0; b < totalNoOfBuckets; ++b )
		{
			jobsSp[ b ] = util::alloc< JobSp >();
			JobSp& job = *jobsSp[ b ];

			job.alloc( subMtxWriteLengthPtrPerBucket[ b ].back() );

			glbInd_t bucketRowStart = bucketRowPtr[ b ];
			glbInd_t bucketRowEnd = bucketRowPtr[ b + 1 ];

			for ( ui32 s = 0; s < totalNoOfSubs; ++s )
			{
				if ( !bucketSubMtxBitTable[ b ][ s ] )
					continue;

				ui64 offset = subMtxWriteLengthPtrPerBucket[ b ][ s ];

				Triplets& A = As[ s ];
				Triplets& B = Bs[ s ];

				nnz_t* rowPtrA = ARowPtrs[ s ];
				std::vector< glbNnz_t >& rowPtrB = *BRowPtrs[ s ].get();

				glbNnz_t nzStartA = rowPtrA[ bucketRowStart ];
				glbNnz_t nzEndA = rowPtrA[ bucketRowEnd ];

				opColOffset_t* colOffsets = &job.spColOffsets[ offset ];
				buffRowInd_t* spRowInds = &job.spRowInds[ offset ];

				ui64 k = 0;
				for ( glbNnz_t nzA = nzStartA; nzA < nzEndA; ++nzA )
				{
					const Triplet& tA = A[ nzA ];

					glbInd_t rowIndB = tA.j - A.colStart();
					for ( glbNnz_t nzB = rowPtrB[ rowIndB ];
					      nzB < rowPtrB[ rowIndB + 1 ];
					      ++nzB, ++k )
					{
						const Triplet& tB = B[ nzB ];
						spRowInds[ k ] = tA.i;
						colOffsets[ k ] = static_cast< buffOffset_t >(
							garbage::getIndexFromLeft(
							&C[ 0 ] + rowPtrC[ tA.i ],
							&C[ 0 ] + rowPtrC[ tA.i + 1 ],
							tB.j ) );
					}
				}
			}
		}

		// lastly adjust write address points for first-phase jobs
		#pragma omp parallel for
		for ( ui32 s = 0; s < totalNoOfSubs; ++s )
		{
			JobFp& job = *jobsFp[ s ];
			nnz_t* rowPtrA = job.A.spm().getRowPtr();

			std::vector< ui32 > bucketIds;
			for ( ui32 b = 0; b < totalNoOfBuckets; ++b )
			{
				std::vector< bool > bitV = bucketSubMtxBitTable[ b ];
				if ( bitV[ s ] )
					bucketIds.push_back( b );
			}

			job.lengthWriteTos = bucketIds.size();
			job.bucketIds = util::alloc< ui32 >( job.lengthWriteTos );
			std::copy( bucketIds.begin(), bucketIds.end(), job.bucketIds );
			job.writeTos = util::alloc< fp_t* >( job.lengthWriteTos );
			for ( ui32 b = 0; b < bucketIds.size(); ++b )
			{
				ui32 bucketId = bucketIds[ b ];
				ui64 offset = subMtxWriteLengthPtrPerBucket[ bucketId ][ s ];
				job.writeTos[ b ] = &jobsSp[ bucketId ]->spBuff[ offset ];
			}

			// instead of keeping a whole rowPtr array of A,
			// just keep start and end rows of each bucket
			std::vector< nnz_t > nzStartEnds;
			for ( ui32 b = 0; b < bucketIds.size(); ++b )
			{
				ui32 bucketId = bucketIds[ b ];
				nzStartEnds.push_back(
					rowPtrA[ bucketRowPtr[ bucketId ] ] );
				nzStartEnds.push_back(
					rowPtrA[ bucketRowPtr[ bucketId + 1 ] ] );
			}

			nnz_t* startEndRowsBucket =
				util::alloc< nnz_t >( nzStartEnds.size() );
			std::copy( nzStartEnds.begin(), nzStartEnds.end(),
			           startEndRowsBucket );
			job.A.spm().deleteRowPtr();
			job.A.spm().setRowPtr( startEndRowsBucket );
		}


		Csr< glbInd_t, fp_t, glbNnz_t >* spmC = nullptr;
		Csr< glbInd_t, fp_t, glbNnz_t >* spmB = nullptr;
		#pragma omp parallel
		{
			#pragma omp single
			{
				spmC = util::alloc< Csr< glbInd_t, fp_t, glbNnz_t > >();
				spmC->initDflt();
				spmC->extract( C );

				spmB = util::alloc< Csr< glbInd_t, fp_t, glbNnz_t > >();
				spmB->initDflt();
				spmB->extract( B );
			}
		}
		// outputs
		*fpJobs_out = jobsFp;
		*numFpJobs_out = totalNoOfSubs;
		*spJobs_out = jobsSp;
		*numSpJobs_out = totalNoOfBuckets;
		*spmB_out = spmB;
		*spmC_out = spmC;
	}
	catch( std::bad_alloc& e )
	{
		std::cerr << "std::bad_alloc caught in " << __FILE__;
		std::cerr << " line " << __LINE__;
		std::cerr << " function: " << __FUNCTION__ << std::endl;
		std::cerr << e.what() << std::endl;
		std::cerr.flush();
		exit( EXIT_FAILURE );
	}
}
*/
// ------------------------------------------------------------------------------

void
data::prepareGustafson(
    Triplets& A, Triplets& B,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrA_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrB_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrC_out )
{
	// Generate C triplets
	ui64 rowStartC = 0;
	ui64 colStartC = 0;
	ui64 nnzC = 0;
	ui64 flops = 0;
	Triplet* tripArrC = util::preprocessC(
	    A, B, rowStartC, colStartC, nnzC, flops );
	
	Triplets C( 0, 0, A.rows(), B.columns(), tripArrC, tripArrC + nnzC );

	// output: Generate A, B, C matrices in CSR format
	#pragma omp parallel num_threads( 3 )
	{
		#pragma omp sections
		{
			#pragma omp section
			csrA_out.extract( A );

			#pragma omp section
			csrB_out.extract( B );

			#pragma omp section
			csrC_out.extract( C );
		}
	}

	delete [] tripArrC;
}

void
data::prepareGustafsonMod(
    Triplets& A, Triplets& B,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrA_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrB_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrC_out,
    buffColOffset_t** offset_out, buffNnz_t** offsetPreSum_out )
{
	// Generate C triplets
	ui64 rowStartC = 0;
	ui64 colStartC = 0;
	ui64 nnzC = 0;
	ui64 flops = 0;
	Triplet* tripArrC = util::preprocessC(
	    A, B, rowStartC, colStartC, nnzC, flops );
	Triplets C( 0, 0, A.rows(), B.columns(), tripArrC, tripArrC + nnzC );

	// output: Generate A, B, C matrices in CSR format
	#pragma omp parallel num_threads( 3 )
	{
		#pragma omp sections
		{
			#pragma omp section
			csrA_out.extract( A );

			#pragma omp section
			csrB_out.extract( B );

			#pragma omp section
			csrC_out.extract( C );
		}
	}
	
	#pragma omp parallel num_threads( 1 )
	{
		std::shared_ptr< std::vector< glbNnz_t > > pRowPtrA( A.rowPtr() );
		std::vector< glbNnz_t > rowPtrA = *pRowPtrA.get();
		std::shared_ptr< std::vector< glbNnz_t > > pRowPtrB( B.rowPtr() );
		std::vector< glbNnz_t > rowPtrB = *pRowPtrB.get();
		std::shared_ptr< std::vector< glbNnz_t > > pRowPtrC( C.rowPtr() );
		std::vector< glbNnz_t > rowPtrC = *pRowPtrC.get();

		// calculate offset prefix sum
		buffNnz_t* offsetPreSum = util::newVector< buffNnz_t >( A.rows() + 1 );
		for ( glbInd_t rind = 0; rind < A.rows(); ++rind )
		{
			buffNnz_t multCurrRow = 0;
			for ( glbNnz_t nzIndA = rowPtrA[ rind ];
			      nzIndA < rowPtrA[ rind + 1 ];
			      ++nzIndA )
			{
				glbInd_t jA = A[ nzIndA ].j;

				multCurrRow += rowPtrB[ jA + 1 ] - rowPtrB[ jA ];
			}

			offsetPreSum[ rind + 1 ] = offsetPreSum[ rind ] + multCurrRow;
		}

		// calculate offset array to C
		ui64 offsetLength = offsetPreSum[ A.rows() ];
		buffColOffset_t* offsetArr =
			util::alloc< buffColOffset_t >( offsetLength );
		ui64 k = 0;
		for ( glbNnz_t trpIndA = 0; trpIndA < A.length(); ++trpIndA )
		{
			glbInd_t rowIndC = A[ trpIndA ].i;
			glbInd_t rowIndB = A[ trpIndA ].j;
			for ( glbNnz_t trpIndB = rowPtrB[ rowIndB ];
					trpIndB < rowPtrB[ rowIndB + 1 ];
					++trpIndB )
			{
				Triplet& tB = B[ trpIndB ];

				offsetArr[ k++ ] = static_cast< buffColOffset_t >(
						garbage::getIndexFromLeft(
								&C[ 0 ] + rowPtrC[ rowIndC ],
								&C[ 0 ] + rowPtrC[ rowIndC + 1 ],
								tB.j ) );
			}
		}

		// outputs
		*offset_out = offsetArr;
		*offsetPreSum_out = offsetPreSum;
	}


	// clean up
	delete [] tripArrC;
}

void
data::prepareGustafsonStatic(
    Triplets& A, Triplets& B, ui32 numBlocks,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrA_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrB_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrC_out,
    glbInd_t** rowPtrPartsA_out )
{
	// Generate C triplets
	ui64 rowStartC = 0;
	ui64 colStartC = 0;
	ui64 nnzC = 0;
	ui64 flops = 0;
	Triplet* tripArrC =
		util::preprocessC( A, B, rowStartC, colStartC, nnzC, flops );
	Triplets C( 0, 0, A.rows(), B.columns(), tripArrC, tripArrC + nnzC );

	std::shared_ptr< std::vector< glbNnz_t > > pRowPtrA( nullptr );
	std::shared_ptr< std::vector< glbNnz_t > > pRowPtrC( nullptr );
	std::vector< Triplets > partsA;
	glbInd_t* rowPtrPartsA = nullptr;
	#pragma omp parallel num_threads( numBlocks )
	{
		#pragma omp single
		{
			// output: Generate A, B, C matrices in CSR format
			csrA_out.extract( A );
			csrB_out.extract( B );
			csrC_out.extract( C );

			pRowPtrA = A.rowPtr();
			pRowPtrC = C.rowPtr();

			std::tuple< std::vector< Triplets >, std::vector< buffNnz_t > >
				pair = util::partitionNoConflict( A, B, numBlocks );
			partsA = std::get< 0 >( pair );

			rowPtrPartsA = util::alloc< glbNnz_t >( numBlocks + 1 );
			rowPtrPartsA[ 0 ] = 0;
			for ( std::size_t i = 0; i < partsA.size(); ++i )
				rowPtrPartsA[ i ] = partsA[ i ].rowStart();
			rowPtrPartsA[ numBlocks ] = partsA.back().rowStart() + partsA.back().rows();
		}
	}

	// outputs
	*rowPtrPartsA_out = rowPtrPartsA;

	// clean up
	delete [] tripArrC;
}

void
data::prepareGustafsonModStatic(
    Triplets& A, Triplets& B, ui32 numBlocks,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrA_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrB_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrC_out,
    buffColOffset_t*** offsets_out, glbInd_t** rowPtrPartsA_out )
{
	// Generate C triplets
	ui64 rowStartC = 0;
	ui64 colStartC = 0;
	ui64 nnzC = 0;
	ui64 flops = 0;
	Triplet* tripArrC =
		util::preprocessC( A, B, rowStartC, colStartC, nnzC, flops );
	Triplets C( 0, 0, A.rows(), B.columns(), tripArrC, tripArrC + nnzC );

	std::shared_ptr< std::vector< glbNnz_t > > pRowPtrA( nullptr );
	std::shared_ptr< std::vector< glbNnz_t > > pRowPtrB( nullptr );
	std::shared_ptr< std::vector< glbNnz_t > > pRowPtrC( nullptr );
	std::vector< Triplets > partsA;
	std::vector< buffNnz_t > productPtr;
	buffColOffset_t** offsets = nullptr;
	glbInd_t* rowPtrPartsA = nullptr;

	#pragma omp parallel num_threads( numBlocks )
	{
		#pragma omp single
		{
			// output: Generate A, B, C matrices in CSR format
			csrA_out.extract( A );
			csrB_out.extract( B );
			csrC_out.extract( C );

			A.sortRowMajor();

			pRowPtrA = A.rowPtr();
			pRowPtrB = B.rowPtr();
			pRowPtrC = C.rowPtr();

			std::tuple< std::vector< Triplets >, std::vector< buffNnz_t > >
				pair = util::partitionNoConflict( A, B, numBlocks );
			partsA = std::get< 0 >( pair );
			productPtr = std::get< 1 >( pair );

			rowPtrPartsA = util::alloc< glbNnz_t >( numBlocks + 1 );
			for ( std::size_t i = 0; i < partsA.size(); ++i )
				rowPtrPartsA[ i ] = partsA[ i ].rowStart();
			rowPtrPartsA[ numBlocks ] =
				partsA.back().rowStart() + partsA.back().rows();

			offsets = util::alloc< buffColOffset_t* >( numBlocks );

			A.sortRowMajor();
		}

		std::vector< glbNnz_t > rowPtrA = *pRowPtrA.get();
		std::vector< glbNnz_t > rowPtrB = *pRowPtrB.get();
		std::vector< glbNnz_t > rowPtrC = *pRowPtrC.get();

		ui32 bid = omp_get_thread_num();


		ui64 offsetLength = productPtr[ rowPtrPartsA[ bid + 1 ] ] -
			productPtr[ rowPtrPartsA[ bid ] ];
		buffColOffset_t* offsetArr =
			util::alloc< buffColOffset_t >( offsetLength );
		ui64 k = 0;
		for ( glbInd_t rowIndA = rowPtrPartsA[ bid ];
		      rowIndA < rowPtrPartsA[ bid + 1 ];
		      ++rowIndA )
		{
			for ( glbNnz_t trpIndA = rowPtrA[ rowIndA ];
					trpIndA < rowPtrA[ rowIndA + 1 ];
					++trpIndA )
			{
				Triplet& tA = A[ trpIndA ];
				for ( glbNnz_t trpIndB = rowPtrB[ tA.j ];
				      trpIndB < rowPtrB[ tA.j + 1 ];
				      ++trpIndB )
				{
					Triplet& tB = B[ trpIndB ];


					offsetArr[ k++ ] = static_cast< buffColOffset_t >(
							garbage::getIndexFromLeft(
									&C[ 0 ] + rowPtrC[ tA.i ],
									&C[ 0 ] + rowPtrC[ tA.i + 1 ],
									tB.j ) );
				}

			}
		}

		// outputs
		offsets[ bid ] = offsetArr;

	}

	// outputs
	*offsets_out = offsets;
	*rowPtrPartsA_out = rowPtrPartsA;

	// clean up
	delete [] tripArrC;
}

// local A and C matrices
// -----------------------------------------------------------------------------

void
data::prepareGustafsonStatic(
    Triplets& A, Triplets& B, ui32 numBlocks,
    SubMtx< ind_t, fp_t, nnz_t, Csr >*** csrAs_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrB_out,
    SubMtx< ind_t, fp_t, nnz_t, Csr >*** csrCs_out )
{
	// Generate C triplets
	ui64 rowStartC = 0;
	ui64 colStartC = 0;
	ui64 nnzC = 0;
	ui64 flops = 0;
	Triplet* tripArrC =
		util::preprocessC( A, B, rowStartC, colStartC, nnzC, flops );
	Triplets C( 0, 0, A.rows(), B.columns(), tripArrC, tripArrC + nnzC );

	SubMtx< ind_t, fp_t, nnz_t, Csr >** csrAs = nullptr;
	SubMtx< ind_t, fp_t, nnz_t, Csr >** csrCs = nullptr;

	std::shared_ptr< std::vector< glbNnz_t > > pRowPtrA( nullptr );
	std::shared_ptr< std::vector< glbNnz_t > > pRowPtrC( nullptr );
	std::vector< Triplets > partsA;
	std::vector< glbInd_t > rowPtrPartsA;
	#pragma omp parallel num_threads( numBlocks )
	{
		#pragma omp single
		{
			// output: Generate A, B, C matrices in CSR format
			csrB_out.extract( B );

			pRowPtrA = A.rowPtr();
			pRowPtrC = C.rowPtr();

			std::tuple< std::vector< Triplets >, std::vector< buffNnz_t > >
				pair = util::partitionNoConflict( A, B, numBlocks );
			partsA = std::get< 0 >( pair );

			rowPtrPartsA.resize( numBlocks + 1, 0 );
			for ( std::size_t i = 0; i < partsA.size(); ++i )
				rowPtrPartsA[ i ] = partsA[ i ].rowStart();
			rowPtrPartsA[ numBlocks ] =
				partsA.back().rowStart() + partsA.back().rows();

			csrAs =
				util::alloc< SubMtx< ind_t, fp_t, nnz_t, Csr >* >( numBlocks );
			csrCs =
				util::alloc< SubMtx< ind_t, fp_t, nnz_t, Csr >* >( numBlocks );

			A.sortRowMajor();
			C.sortRowMajor();
		}

		ui32 bid = omp_get_thread_num();

		std::vector< glbNnz_t >& rowPtrA = *pRowPtrA.get();
		std::vector< glbNnz_t >& rowPtrC = *pRowPtrC.get();

		glbInd_t rowStart = rowPtrPartsA[ bid ];
		glbInd_t rowEnd = rowPtrPartsA[ bid + 1 ];
		glbInd_t colStart = 0;
		glbInd_t colEndA = A.columns();
		glbInd_t colEndC = C.columns();

		Triplets threadA(
		    rowStart, colStart, rowEnd - rowStart, colEndA - colStart,
		    &A[ 0 ] + rowPtrA[ rowStart ], &A[ 0 ] + rowPtrA[ rowEnd ] );
		SubMtx< ind_t, fp_t, nnz_t, Csr >* csrA =
			util::alloc< SubMtx< ind_t, fp_t, nnz_t, Csr > >();
		csrA->initDflt();
		csrA->extract( threadA );

		Triplets threadC(
		    rowStart, colStart, rowEnd - rowStart, colEndC - colStart,
		    &C[ 0 ] + rowPtrC[ rowStart ], &C[ 0 ] + rowPtrC[ rowEnd ] );
		SubMtx< ind_t, fp_t, nnz_t, Csr >* csrC =
			util::alloc< SubMtx< ind_t, fp_t, nnz_t, Csr > >();
		csrC->initDflt();
		csrC->extract( threadC );

		// per thread output
		csrAs[ bid ] = csrA;
		csrCs[ bid ] = csrC;
	}

	// outputs
	*csrAs_out = csrAs;
	*csrCs_out = csrCs;

	// clean up
	delete [] tripArrC;
}

void
data::prepareGustafsonModStatic(
    Triplets& A, Triplets& B, ui32 numBlocks,
    SubMtx< ind_t, fp_t, nnz_t, Csr >*** csrAs_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrB_out,
    SubMtx< ind_t, fp_t, nnz_t, Csr >*** csrCs_out,
    buffColOffset_t*** offsets_out )
{
	// Generate C triplets
	ui64 rowStartC = 0;
	ui64 colStartC = 0;
	ui64 nnzC = 0;
	ui64 flops = 0;
	Triplet* tripArrC =
		util::preprocessC( A, B, rowStartC, colStartC, nnzC, flops );
	Triplets C( 0, 0, A.rows(), B.columns(), tripArrC, tripArrC + nnzC );

	std::shared_ptr< std::vector< glbNnz_t > > pRowPtrA( nullptr );
	std::shared_ptr< std::vector< glbNnz_t > > pRowPtrB( nullptr );
	std::shared_ptr< std::vector< glbNnz_t > > pRowPtrC( nullptr );
	std::vector< Triplets > partsA;
	std::vector< glbNnz_t > rowPtrPartsA;
	std::vector< buffNnz_t > productPtr;

	buffColOffset_t** offsets = nullptr;
	SubMtx< ind_t, fp_t, nnz_t, Csr >** csrAs = nullptr;
	SubMtx< ind_t, fp_t, nnz_t, Csr >** csrCs = nullptr;

	#pragma omp parallel num_threads( numBlocks )
	{
		#pragma omp single
		{
			// output: Generate A, B, C matrices in CSR format
			csrB_out.extract( B );

			csrAs =
				util::alloc< SubMtx< ind_t, fp_t, nnz_t, Csr >* >( numBlocks );
			csrCs =
				util::alloc< SubMtx< ind_t, fp_t, nnz_t, Csr >* >( numBlocks );
			offsets = util::alloc< buffColOffset_t* >( numBlocks );

			pRowPtrA = A.rowPtr();
			pRowPtrB = B.rowPtr();
			pRowPtrC = C.rowPtr();

			std::tuple< std::vector< Triplets >, std::vector< buffNnz_t > >
				pair = util::partitionNoConflict( A, B, numBlocks );
			partsA = std::get< 0 >( pair );
			productPtr = std::get< 1 >( pair );

			rowPtrPartsA.resize( numBlocks + 1, 0 );
			for ( std::size_t i = 0; i < partsA.size(); ++i )
				rowPtrPartsA[ i ] = partsA[ i ].rowStart();
			rowPtrPartsA[ numBlocks ] =
				partsA.back().rowStart() + partsA.back().rows();

			A.sortRowMajor();
			C.sortRowMajor();
		}

		std::vector< glbNnz_t > rowPtrA = *pRowPtrA.get();
		std::vector< glbNnz_t > rowPtrB = *pRowPtrB.get();
		std::vector< glbNnz_t > rowPtrC = *pRowPtrC.get();

		ui32 bid = omp_get_thread_num();

		glbInd_t rowStart = rowPtrPartsA[ bid ];
		glbInd_t rowEnd = rowPtrPartsA[ bid + 1 ];
		glbInd_t colStart = 0;
		glbInd_t colEndA = A.columns();
		glbInd_t colEndC = C.columns();

		Triplets threadA( rowStart, colStart,
						  rowEnd - rowStart, colEndA - colStart,
						  &A[ 0 ] + rowPtrA[ rowStart ],
		                  &A[ 0 ] + rowPtrA[ rowEnd ] );
		SubMtx< ind_t, fp_t, nnz_t, Csr >* csrA =
			util::alloc< SubMtx< ind_t, fp_t, nnz_t, Csr > >();
		csrA->initDflt();
		csrA->extract( threadA );

		Triplets threadC( rowStart, colStart,
		                  rowEnd - rowStart, colEndC - colStart,
						  &C[ 0 ] + rowPtrC[ rowStart ],
		                  &C[ 0 ] + rowPtrC[ rowEnd ] );
		SubMtx< ind_t, fp_t, nnz_t, Csr >* csrC =
			util::alloc< SubMtx< ind_t, fp_t, nnz_t, Csr > >();
		csrC->initDflt();
		csrC->extract( threadC );

		ui64 offsetLength = productPtr[ rowPtrPartsA[ bid + 1 ] ] -
			productPtr[ rowPtrPartsA[ bid ] ];
		buffColOffset_t* offsetArr =
			util::alloc< buffColOffset_t >( offsetLength );
		ui64 k = 0;
		for ( glbInd_t rowIndA = rowPtrPartsA[ bid ];
		      rowIndA < rowPtrPartsA[ bid + 1 ];
		      ++rowIndA )
		{
			for ( glbNnz_t trpIndA = rowPtrA[ rowIndA ];
			      trpIndA < rowPtrA[ rowIndA + 1 ];
			      ++trpIndA )
			{
				Triplet& tA = A[ trpIndA ];
				for ( glbNnz_t trpIndB = rowPtrB[ tA.j ];
				      trpIndB < rowPtrB[ tA.j + 1 ];
				      ++trpIndB )
				{
					Triplet& tB = B[ trpIndB ];

					offsetArr[ k++ ] = static_cast< buffColOffset_t >(
							garbage::getIndexFromLeft(
									&C[ 0 ] + rowPtrC[ tA.i ],
									&C[ 0 ] + rowPtrC[ tA.i + 1 ],
									tB.j ) );
				}

			}
		}

		// per thread output
		csrAs[ bid ] = csrA;
		csrCs[ bid ] = csrC;
		offsets[ bid ] = offsetArr;
	}

	// outputs
	*offsets_out = offsets;
	*csrAs_out = csrAs;
	*csrCs_out = csrCs;

	// clean up
	delete [] tripArrC;
}

