/*
 * Debug.cpp
 *
 *  Created on: Jan 3, 2017
 *      Author: memoks
 */

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <execinfo.h>

#include "include/util/Debug.h"

std::ostream&
util::printStackFrames( std::ostream& out )
{
	std::size_t buffSize = 100;
	void* buff[ 100 ];
	std::size_t len = 0;
	char **backtraceSymbols;

	len = backtrace( buff, buffSize );
	backtraceSymbols = backtrace_symbols( buff, len );

	out << "Stack frames: " << std::endl;
	for ( std::size_t i = 0; i < len; ++i )
		std::cerr << backtraceSymbols[ i ] << std::endl;

	return out;
}
