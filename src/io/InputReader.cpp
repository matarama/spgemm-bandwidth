/*
 * InputReader.cpp
 *
 *  Created on: Dec 21, 2016
 *      Author: memoks
 */

#include <cstdio>
#include <cassert>

#include "include/io/InputReader.h"

// -----------------------------------------------------------------------------

void
InputReader::readMMF( const std::string& mmfPath, MMInfo& mmInfo,
					  int* rowCount_out, int* colCount_out,  int* nnz_out,
					  int** is_out, int** js_out, double** nzs_out )
{
	FILE* f = fopen(mmfPath.c_str(), "r");
	assert(f != NULL);

	mmInfo.mm_read( f, rowCount_out, colCount_out,
	                nnz_out, nzs_out, is_out, js_out );
}

// -----------------------------------------------------------------------------
