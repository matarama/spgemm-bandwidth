/*
 * Cli.cpp
 *
 *  Created on: Jan 9, 2017
 *      Author: memoks
 */

#include <sstream>
#include <cassert>

#include "include/io/Cli.h"

const std::string Cli::NUM_BLOCKS = "numBlocks";
const std::string Cli::MMF_PATH_A = "mmfPathA";
const std::string Cli::MMF_PATH_B = "mmfPathB";
const std::string Cli::CACHE_LINE_BYTES = "cacheLineBytes";
const std::string Cli::SP_PART_SIZE_KB = "spPartSizeKB";
const std::string Cli::FP_PART_SIZE_KB = "fpPartSizeKB";
const std::string Cli::ROWS_PER_BUCKET = "rowsPerBucket";
const std::string Cli::ITERATIONS = "iterations";
const std::string Cli::OUTPUT_DIR = "outDir";
const std::string Cli::INPUT_DIR = "inDir";
const std::string Cli::HYBRID_COLUMN_NNZ_LESS = "hybLess";
const std::string Cli::HYBRID_COLUMN_PERCENT = "hyb%";
const std::string Cli::COMPARISON_PATH = "cmpPath";
const std::string Cli::PERMUTE_AB = "permuteAB";
const std::string Cli::TRANSPOSE_B = "transposeB";
const std::string Cli::HYBRID_PARTER_A = "hybA";
const std::string Cli::ROWS = "rows";
const std::string Cli::COLUMNS = "columns";
const std::string Cli::NNZ_PER_ROW = "nnzPerRow";
const std::string Cli::DENSE_ROWS = "denseRows";
const std::string Cli::NNZ_PER_ROW_DENSE = "denseNnzPerRow";
const std::string Cli::HEURISTIC_TRESHOLD = "heuristicTreshold";
const std::string Cli::ACC_SIZE_KB = "accSizeKB";
const std::string Cli::BUFF_LENGTH_MULTIPLIER = "accMultiplier";
const std::string Cli::PHF_ALPHA = "phfAlpha";
const std::string Cli::SHAMT = "shamt";
const std::string Cli::ACC_SIZES = "accSizes";
const std::string Cli::B_SIZE_KB = "BSizeKB";
const std::string Cli::TARGETTED_CACHE_SIZE_KB = "targettedCacheSizeKB";
const std::string Cli::PATH_TO_ROW_ORDERING_A = "pathToRowOrderingA";
const std::string Cli::PATH_TO_ROW_PREFIX_SUM_PER_PART_A = "pathToRowPfxSumPerPartA";
const std::string Cli::PATH_TO_ROW_ORDERING_B = "pathToRowOrderingB";
const std::string Cli::METIS_B_VERTEX_COUNT = "metisBVertexCount";
const std::string Cli::PATH_TO_PART_VECTOR_A = "pathToPartVectorA";
const std::string Cli::PATH_TO_PART_VECTOR_B = "pathToPartVectorB";
const std::string Cli::OUTPUT_PATH = "outputPath";
const std::string Cli::SORT = "sort";
const std::string Cli::NETWORK_L2_UNION_MB = "networkL2UnionMB";

Cli::Cli( const int& argc, char** argv )
: mmfPathA( "" ),
  mmfPathB( "" ),
  numBlocks( 15 ),
  cacheLineBytes( 64 ),
  spPartSizeKB( 0.2 ),
  fpPartSizeKB( 0.2 ),
  rowsPerBucket( 64 ),
  iterations( 50 ),
  outputDir( "" ),
  hybridColumnNnzLe( 0 ),
  hybridColumnNnzPercent( 0 ),
  inputDir( "" ),
  comparisonPath( "" ),
  permuteAB( false ),
  transposeB( false ),
  hybridParterA( true ),
  rows( 0 ),
  columns( 0 ),
  nnzPerRow( 0 ),
  denseRows( 0 ),
  nnzPerRowDense( 0 ),
  heuristicTreshold( 0 ),
  accSizeKB( 64 ),
  buffLengthMultiplier( 1 ),
  phfAlpha( 100 ),
  shamt( false ),
  BSizeKB( 128 ),
  targettedCacheSizeKB( 1024 ),
  pathToRowOrderingA( "" ),
  pathToRowPfxSumPerPartA( "" ),
  pathToRowOrderingB( "" ),
  metisBVertexCount( 10 ),
  pathToPartVectorA( "" ),
  pathToPartVectorB( "" ),
  outputPath( "" ),
  sorted( false ),
  networkL2UnionMB( 32 )
{
	for ( int i = 0; i < argc; ++i )
	{
		std::string param( argv[ i ] );
		if( param.find( '=' ) == std::string::npos )
			continue;

		std::string paramName = param.substr( 0, param.find( '=' ) );
		std::string paramValue = param.substr( param.find( '=' ) + 1 );
		vars[ paramName ] = true;

		std::stringstream converter;
		converter << paramValue;
		if ( paramName == Cli::NUM_BLOCKS )
			converter >> numBlocks;
		else if ( paramName == Cli::MMF_PATH_A )
			converter >> mmfPathA;
		else if ( paramName == Cli::MMF_PATH_B )
			converter >> mmfPathB;
		else if ( paramName == Cli::CACHE_LINE_BYTES )
			converter >> cacheLineBytes;
		else if ( paramName == Cli::FP_PART_SIZE_KB )
			converter >> fpPartSizeKB;
		else if ( paramName == Cli::SP_PART_SIZE_KB )
			converter >> spPartSizeKB;
		else if ( paramName == Cli::ITERATIONS )
			converter >> iterations;
		else if ( paramName == Cli::ROWS_PER_BUCKET )
			converter >> rowsPerBucket;
		else if ( paramName == Cli::OUTPUT_DIR )
			converter >> outputDir;
		else if ( paramName == Cli::HYBRID_COLUMN_NNZ_LESS )
			converter >> hybridColumnNnzLe;
		else if ( paramName == Cli::HYBRID_COLUMN_PERCENT )
			converter >> hybridColumnNnzPercent;
		else if ( paramName == Cli::INPUT_DIR )
			converter >> inputDir;
		else if ( paramName == Cli::COMPARISON_PATH )
			converter >> comparisonPath;
		else if ( paramName == Cli::PERMUTE_AB )
			converter >> permuteAB;
		else if ( paramName == Cli::TRANSPOSE_B )
			converter >> transposeB;
		else if ( paramName == Cli::HYBRID_PARTER_A )
			converter >> hybridParterA;
		else if ( paramName == Cli::ROWS )
			converter >> rows;
		else if ( paramName == Cli::COLUMNS )
			converter >> columns;
		else if ( paramName == Cli::NNZ_PER_ROW )
			converter >> nnzPerRow;
		else if ( paramName == Cli::DENSE_ROWS )
			converter >> denseRows;
		else if ( paramName == Cli::NNZ_PER_ROW_DENSE )
			converter >> nnzPerRowDense;
		else if ( paramName == Cli::HEURISTIC_TRESHOLD )
			converter >> heuristicTreshold;
		else if ( paramName == Cli::ACC_SIZE_KB )
			converter >> accSizeKB;
		else if ( paramName == Cli::BUFF_LENGTH_MULTIPLIER )
			converter >> buffLengthMultiplier;
		else if ( paramName == Cli::PHF_ALPHA )
			converter >> phfAlpha;
		else if ( paramName == Cli::SHAMT )
			converter >> shamt;
		else if ( paramName == Cli::ACC_SIZES )
		{
			std::string s;
			converter >> s;
			std::string temp;
			int t;
			while ( s.find( ',' ) != std::string::npos )
			{
				temp = s.substr( 0, s.find( ',' ) );
				t = atoi( temp.c_str() );
                accSizes.push_back( t );
				s = s.substr( s.find( ',' ) + 1 );
			}
			accSizes.push_back( atoi( s.c_str() ) );

			for ( auto it = accSizes.begin(); it != accSizes.end(); ++it )
				assert( ( *it * 128 ) % 2 == 0 );
		}
		else if ( paramName == Cli::B_SIZE_KB )
			converter >> BSizeKB;
		else if ( paramName == Cli::TARGETTED_CACHE_SIZE_KB )
			converter >> targettedCacheSizeKB;
		else if ( paramName == Cli::PATH_TO_ROW_ORDERING_A )
			converter >> pathToRowOrderingA;
		else if ( paramName == Cli::PATH_TO_ROW_PREFIX_SUM_PER_PART_A )
			converter >> pathToRowPfxSumPerPartA;
		else if ( paramName == Cli::PATH_TO_ROW_ORDERING_B )
			converter >> pathToRowOrderingB;
		else if ( paramName == Cli::METIS_B_VERTEX_COUNT )
			converter >> metisBVertexCount;
		else if ( paramName == Cli::PATH_TO_PART_VECTOR_A )
			converter >> pathToPartVectorA;
		else if ( paramName == Cli::PATH_TO_PART_VECTOR_B )
			converter >> pathToPartVectorB;
		else if ( paramName == Cli::OUTPUT_PATH )
			converter >> outputPath;
		else if ( paramName == Cli::SORT )
			converter >> sorted;
		else if ( paramName == Cli::NETWORK_L2_UNION_MB )
			converter >> networkL2UnionMB;
	}
}

Cli::~Cli()
{
}

std::ostream&
Cli::str( std::ostream& out ) const
{
	out << std::endl;
	out << "CLI parameters" << std::endl;
	out << MMF_PATH_A << " = " << mmfPathA << std::endl;
	out << MMF_PATH_B << " = " << mmfPathB << std::endl;
	out << NUM_BLOCKS << " = " << numBlocks << std::endl;
	out << CACHE_LINE_BYTES << " = " << cacheLineBytes << std::endl;
	out << FP_PART_SIZE_KB << " = " << fpPartSizeKB << std::endl;
	out << SP_PART_SIZE_KB << " = " << spPartSizeKB << std::endl;
	out << ROWS_PER_BUCKET << " = " << rowsPerBucket << std::endl;
	out << ITERATIONS << " = " << iterations << std::endl;
	out << OUTPUT_DIR << " = " << outputDir << std::endl;
	out << INPUT_DIR << " = " << inputDir << std::endl;
	out << HYBRID_COLUMN_NNZ_LESS << " = " << hybridColumnNnzLe << std::endl;
	out << HYBRID_COLUMN_PERCENT << " = "
	    << hybridColumnNnzPercent << std::endl;
	out << COMPARISON_PATH << " = " << comparisonPath << std::endl;
	out << PERMUTE_AB << " = " << permuteAB << std::endl;
	out << TRANSPOSE_B << " = " << transposeB << std::endl;
	out << HYBRID_PARTER_A << " = " << hybridParterA << std::endl;
	out << ROWS << " = " << rows << std::endl;
	out << COLUMNS << " = " << columns << std::endl;
	out << NNZ_PER_ROW << " = " << nnzPerRow << std::endl;
	out << DENSE_ROWS << " = " << denseRows << std::endl;
	out << NNZ_PER_ROW_DENSE << " = " << nnzPerRowDense << std::endl;
	out << HEURISTIC_TRESHOLD << " = " << heuristicTreshold << std::endl;
	out << ACC_SIZE_KB << " = " << accSizeKB << std::endl;
	out << BUFF_LENGTH_MULTIPLIER << " = "
	    << buffLengthMultiplier << std::endl;
	out << PHF_ALPHA << " = " << phfAlpha << std::endl;
	out << SHAMT << " = " << shamt << std::endl;
	out << B_SIZE_KB << " = " << BSizeKB << std::endl;
	out << TARGETTED_CACHE_SIZE_KB << " = " << targettedCacheSizeKB << std::endl;
	out << PATH_TO_ROW_ORDERING_A << " = " << pathToRowOrderingA << std::endl;
	out << PATH_TO_ROW_PREFIX_SUM_PER_PART_A << " = " <<  pathToRowPfxSumPerPartA << std::endl;
	out << PATH_TO_ROW_ORDERING_B << " = " << pathToRowOrderingB << std::endl;
	out << METIS_B_VERTEX_COUNT << " = " << metisBVertexCount << std::endl;
	out << PATH_TO_PART_VECTOR_A << " = " << pathToPartVectorA << std::endl;
	out << PATH_TO_PART_VECTOR_B << " = " << pathToPartVectorB << std::endl;
	out << OUTPUT_PATH << " = " << outputPath << std::endl;
	out << SORT << " = " << sorted << std::endl;
	out << NETWORK_L2_UNION_MB << " = " << networkL2UnionMB << std::endl;
	out << std::endl;

	return out;
}

bool
Cli::isSet( const std::string& s ) const
{
	return vars.find( s ) != vars.end();
}

bool
Cli::isSet( std::initializer_list< std::string > l ) const
{
	bool allSet = true;
	for ( auto it = l.begin(); it != l.end(); ++it )
	{
		bool varSet = isSet( *it );
		allSet &= varSet;

		if ( !varSet )
			std::cout << *it << " not set." << std::endl;
	}

	return allSet;
}
