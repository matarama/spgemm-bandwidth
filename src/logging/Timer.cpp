/*
 * Timer.cpp
 *
 *  Created on: Jan 1, 2017
 *      Author: memoks
 */

#include "include/logging/Timer.h"

Timer::Timer()
: duration( 0.0 ), errorFlag( false )
{
}

Timer::~Timer()
{
}

void
Timer::start()
{
#ifdef __INTEL_COMPILER
	// do a dummy dsecnd() call to improve accuracy of timing
	dsecnd();
	t1 = dsecnd();
#else
	t1 = std::chrono::high_resolution_clock::now();
#endif
}

double
Timer::finish()
{
#ifdef __INTEL_COMPILER
	t2 = dsecnd();
	duration = t2 - t1;
#else
	t2 = std::chrono::high_resolution_clock::now();
	duration = std::chrono::duration_cast
		< std::chrono::nanoseconds >( t2 - t1 ).count();
	duration = duration / 1000000000.0;
#endif
	return duration;
}

double
Timer::getDuration() const
{
	return duration;
}

bool
Timer::getErrorFlag() const
{
	return errorFlag;
}

void
Timer::toggleErrorFlag()
{
	errorFlag = !errorFlag;
}
