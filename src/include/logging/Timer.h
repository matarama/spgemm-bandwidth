/*
 * Timer.h
 *
 *  Created on: Jan 1, 2017
 *      Author: memoks
 */

#ifndef SRC_TIME_TIMER_H_
#define SRC_TIME_TIMER_H_

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <chrono>
#include <stdarg.h>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#include <sys/time.h>
#endif


class Timer
{
private:
#ifdef __INTEL_COMPILER
	double t1;
	double t2;
#else
	std::chrono::high_resolution_clock::time_point t1;
	std::chrono::high_resolution_clock::time_point t2;
#endif
	double duration;
	bool errorFlag;

public:

	Timer();
	virtual	~Timer();

	void start();
	double finish();

	double getDuration() const;
	bool getErrorFlag() const;
	void toggleErrorFlag();
};

static Timer globalTimer;
static int tabIndex = -1;
static Timer genericTimer;

#define HOWLONGBEGIN( message )	                       \
	do {                                               \
		++tabIndex;                                    \
		for ( int i = 0; i < tabIndex; ++i )           \
			std::cout << "\t";                         \
		std::cout << message << "... ";                \
		std::cout.flush();                             \
		genericTimer.start();                          \
	} while ( false )

#define HOWLONGEND()                                   \
	do {                                               \
		--tabIndex;                                    \
		genericTimer.finish();                         \
		std::cout << genericTimer.getDuration()        \
		          << std::endl;                        \
		std::cout.flush();                             \
	} while ( false )

#define MEASURE_SINGLE( func_calls )                   \
	do {                                               \
		globalTimer.start();                           \
		func_calls                                     \
		globalTimer.finish();                          \
		RECORD( globalTimer.getDuration() );           \
} while ( false )

#define MEASURE( iter_count, func_calls )              \
	do {                                               \
		globalTimer.start();                           \
		for ( ui32 i = 0; i < iter_count; ++i )        \
		{                                              \
			func_calls                                 \
		}                                              \
		globalTimer.finish();                          \
} while ( false )

#define TIMER_START()                                  \
	do {                                               \
		globalTimer.start();                           \
	} while( false )

#define TIMER_STOP()                                   \
	do {                                               \
		globalTimer.finish();                          \
	} while( false )

#define TIMER_DURATION()                               \
	globalTimer.getDuration()

#define RECORD( duration )                             \
	do {                                               \
		std::cout << duration << ",";                  \
		std::cout.flush();                             \
	} while( false )

#define RUN( iter_count, func_calls, duration_out )    \
	do {                                               \
		for ( ui32 i = 0; i < 10; ++i )	               \
		{ func_calls /* WARM-UP */ }				   \
		                                               \
		MEASURE( iter_count, func_calls );             \
		duration_out =                                 \
			globalTimer.getDuration() / iter_count;    \
                                                       \
	} while ( false )


#define RRUN( iter_count, func_calls )		           \
	do {                                               \
		double duration;                               \
		RUN( iter_count, func_calls, duration );       \
		RECORD( duration );                            \
	} while ( false )


#define GTRUN( iter_count, flops, func_calls )		   \
	do {                                               \
		double duration;                               \
		RUN( iter_count, func_calls, duration );       \
		RECORD( duration );                            \
		double gflops = ( flops / duration );          \
		gflops /= pow( 2, 30 );                        \
		gflops *= 100;                                 \
        long lgflops = (long) gflops;                  \
		gflops = lgflops / 100.0;                      \
		RECORD( gflops );                              \
	} while ( false )

#define RRUNS( iter_count, stream, func_calls )        \
	do {                                               \
		double duration;                               \
		RUN( iter_count, func_calls, duration );       \
		stream << duration << ",";                     \
	} while ( false )


#endif /* SRC_TIME_TIMER_H_ */
