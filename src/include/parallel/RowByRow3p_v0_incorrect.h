/*
 * RowByRow3p_v0.h
 *
 *  Created on: Aug 15, 2017
 *      Author: memoks
 */

#ifndef SRC_INCLUDE_PARALLEL_ROWBYROW3P_V0_INCORRECT_H_
#define SRC_INCLUDE_PARALLEL_ROWBYROW3P_V0_INCORRECT_H_

#include <string>
#include <iostream>

#include "include/sparse_types.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Csr.h"
#include "include/parallel/RowByRow3p_v0.h"

namespace rbr3p_incorrect
{
namespace v0
{


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_A_read(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_B_read(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_C_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_C_read_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_B_read_C_read_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_B_read_C_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_accumulator_random_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_accumulator_random_read(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_accumulator_random_write_read(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_accumulator_random_write_read_B_read(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_accumulator_random_write_read_C_read_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_accumulator_random_write_read_B_read_C_read_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_no_accumulator(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_no_accumulator_B_read(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_no_accumulator_C_read_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_no_accumulator_B_read_C_read_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_no_first_half(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_no_second_half(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );

}
}

#include "parallel/RowByRow3p_v0_incorrect.cpp"

#endif /* SRC_INCLUDE_PARALLEL_ROWBYROW3P_V0_INCORRECT_H_ */
