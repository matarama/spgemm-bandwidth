/*
 * RowByRow3p_v1.h
 *
 *  Created on: Aug 15, 2017
 *      Author: memoks
 */

#ifndef SRC_INCLUDE_PARALLEL_ROWBYROW3P_V1_H_
#define SRC_INCLUDE_PARALLEL_ROWBYROW3P_V1_H_

#include <string>
#include <iostream>

#include "include/sparse_types.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Csr.h"

namespace rbr3p
{
namespace v1
{

template < typename ind_t, typename fp_t, typename nnz_t >
class Thrd;

// Kernel routines that perform memory allocation inside
// -----------------------------------------------------------------------------

template < typename ind_t >
void
phase0_alloc( Triplets& B, const double& sizeKB,
              ind_t** B_bucketRowPfxSum_out, ui32& noOf_AB_primes_out );

template < typename ind_t, typename fp_t, typename nnz_t >
void
phase1_alloc( const Csr< ind_t, fp_t, nnz_t >& A,
              const Csr< ind_t, fp_t, nnz_t >& B,
              ind_t const* const B_bucketRowPfxSum,
              ui32 const& noOfBuckets,
              Thrd< ind_t, fp_t, nnz_t >** thrds, const ui32& numThreads,
              ui64** B_bucketLengths_out,
              ind_t***allB_bucketsColInd_out,
              fp_t*** allB_bucketNzs );

template < typename ind_t, typename fp_t, typename nnz_t >
void
phase2_alloc( const Csr< ind_t, fp_t, nnz_t >& B,
              Thrd< ind_t, fp_t, nnz_t >** thrds, const ui32& numThreads );

// Kernel routines that don't perform memory allocation inside
// -----------------------------------------------------------------------------

template < typename ind_t >
void
phase0( Triplets& B, const double& sizeKB,
        ind_t* B_bucketRowPfxSum, ui32& noOf_AB_primes_out );

template < typename ind_t, typename fp_t, typename nnz_t >
void
phase1( const Csr< ind_t, fp_t, nnz_t >& A,
        const Csr< ind_t, fp_t, nnz_t >& B,
        ind_t const* const B_bucketRowPfxSum, ui32 const& noOfBuckets,
        Thrd< ind_t, fp_t, nnz_t >** thrds, const ui32& numThreads,
        ui64* B_bucketLengths, ind_t** allB_bucketsColInd,
        fp_t** allB_bucketsNzs );

template < typename ind_t, typename fp_t, typename nnz_t >
void
phase2( const Csr< ind_t, fp_t, nnz_t >& B,
        Thrd< ind_t, fp_t, nnz_t >** thrds, const ui32& numThreads );

template < typename ind_t, typename fp_t, typename nnz_t >
void
phase3( const Csr< ind_t, fp_t, nnz_t >& A,
        const Csr< ind_t, fp_t, nnz_t >& C,
        ui32 const* const masks, char const* const maskIndexPerRow,
        fp_t** accumulatorPerThrd,
        Thrd< ind_t, fp_t, nnz_t >** thrds, const ui32& numThreads );

// -----------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t >
void
prep( Triplets& A, Triplets& B, Triplets& C, const ui32& numThreads,
      Csr< ind_t, fp_t, nnz_t >& csrA,
      Csr< ind_t, fp_t, nnz_t >& csrB,
      Csr< ind_t, fp_t, nnz_t >& csrC,
      Thrd< ind_t, fp_t, nnz_t >*** thrds_out, ind_t** thrdRowPfxSum_out );


template < typename ind_t, typename fp_t, typename nnz_t >
void
stats( Thrd< ind_t, fp_t, nnz_t >** thrds, const ui32& numThreads,
       std::ostream& out, std::ostream& header );

template < typename ind_t, typename fp_t, typename nnz_t >
void
allocCache( Thrd< ind_t, fp_t, nnz_t >** thrds, const ui32& numThreads,
            const ui32& fpCacheCapacity );
}
}

template < typename ind_t, typename fp_t, typename nnz_t >
class rbr3p::v1::Thrd
{
public:
	Thrd();
	~Thrd();
	
	Thrd( const Thrd& other ) = delete;
	Thrd& operator =( const Thrd& other ) = delete;

	void init();
	void free();
	void set( const ind_t& rowStart, const ind_t& rowEnd,
	          const nnz_t& nzStart, const nnz_t& nzEnd );

	std::ostream& str( std::ostream& out ) const;
	std::string str() const;
	friend std::ostream& operator <<
	( std::ostream& out, Thrd& b )
	{ return out << b.str(); }

// private:
	// Threads's row slice
	ind_t A_rowStart;
	ind_t A_rowEnd;
	ind_t A_nzStart;
	ind_t A_nzEnd;

	// phase 0
	ind_t* B_bucketRowPfxSum;
	
	// phase 1
	ui32 p1BucketCount;
	nnz_t* p1A_bucketCounters;
	nnz_t* p1A_bucketStarts;
	nnz_t* p1A_bucketNnzs;
	ind_t** p1A_buckets;

	ui32 p2BucketCount;
	ui32 p2BucketIdStart;
	ind_t** p2A_buckets;
	nnz_t* p2A_bucketNnzs;
	ind_t** p2B_bucketsColInd;
	fp_t** p2B_bucketsNzs;
	ui64* p2B_bucketsNnz;
	
	fp_t** p3A_ptrToBNzs;
	ind_t** p3A_ptrToBColInd;
	ind_t* p3A_BLengths;
	ui64* p3B_lengthPerBucket;
	ui64* p3B_startIndexPerBucket;

	// streaming store buffers
	ui32 fpCacheCapacity;
	fp_t* fpCache;
};


// Kernel routines that use intrinsics
// #############################################################################

namespace rbr3p
{
namespace v1
{

template < typename ind_t, typename fp_t, typename nnz_t >
void
phase3_avx(
	const Csr< ind_t, fp_t, nnz_t >& A, const Csr< ind_t, fp_t, nnz_t >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd,
	Thrd< ind_t, fp_t, nnz_t >** thrds, const ui32& numThreads );

#ifdef WITH_AVX
template < >
void
phase3_avx< int, double, int >(
	const Csr< int, double, int >& A, const Csr< int, double, int >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	double** accumulatorPerThrd,
	Thrd< int, double, int >** thrds, const ui32& numThreads );
#endif

template < typename ind_t, typename fp_t, typename nnz_t >
void
phase3_avx512(
	const Csr< ind_t, fp_t, nnz_t >& A, const Csr< ind_t, fp_t, nnz_t >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd,
	Thrd< ind_t, fp_t, nnz_t >** thrds, const ui32& numThreads );

#ifdef WITH_AVX512
template < >
void
phase3_avx512< int, double, int >(
	const Csr< int, double, int >& A, const Csr< int, double, int >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	double** accumulatorPerThrd,
	Thrd< int, double, int >** thrds, const ui32& numThreads );
#endif

}
}

#include "parallel/RowByRow3p_v1.cpp"

#endif /* SRC_INCLUDE_PARALLcheEL_ROWBYROW3P_V0_H_ */
