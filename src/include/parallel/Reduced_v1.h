
#ifndef SRC_PARALLEL_REDUCED_V1_H_
#define SRC_PARALLEL_REDUCED_V1_H_

#include "include/sparse_types.h"
#include "include/storage_format/Cs.h"

namespace regularized
{

template < typename coffset_t, typename fp_t, typename alength_t,
           typename mem >
struct AB
{
	alength_t* ALengths;
	coffset_t* COffsets;
	fp_t* nzs;

	ui64 ANnz;
	ui64 length;

	AB();
	~AB();

	AB( const AB& other ) = delete;
	AB& operator=( const AB& other ) = delete;

	void init();
	void free();
};



template < bool doAlloc = false,
           typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename B_mem, typename C_mem, typename AB_mem >
void
symbolic(
	const ind_t& B_columns,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	ind_t const* const thrdPtrPfxSum, ui64 const* const thrdMultsPfxSum,
	fp_t** accumulators, const ui32& NUM_THREADS );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
numeric(
	const regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	ind_t const* const thrdPtrPfxSum, ui64 const* const thrdMultsPfxSum,
	const ui32& NUM_THREADS );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
numeric_unrolled8(
	const regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	ind_t const* const thrdPtrPfxSum, ui64 const* const thrdMultsPfxSum,
	const ui32& NUM_THREADS );



template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
numeric2(
	const regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	ind_t const* const thrdPtrPfxSum, ui64 const* const thrdMultsPfxSum,
	const ui32& NUM_THREADS );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
numeric(
	const regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	ind_t const* const thrdPtrPfxSum, ui64 const* const thrdMultsPfxSum,
	const ui32& NUM_THREADS, const ui32& ITERATIONS );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
numeric_unrolled8(
	const regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	ui64 const* const thrdMultsPfxSum,
	nnz_t const* const aSlicePfxSum,
	nnz_t const* const cSlicePfxSum,
	ui32 const* const slicesPerThrdPfxSum,
	const ui32& NUM_THREADS );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
numeric_doubleThread(
	const regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	ui64 const* const thrdMultsPfxSum,
	nnz_t const* const aSlicePfxSum,
	nnz_t const* const cSlicePfxSum,
	ui32 const* const slicesPerThrdPfxSum,
	bool** shareds, const ui32& NUM_THREADS );



template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
numeric_doubleThread(
	const regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	ui64 const* const thrdMultsPfxSum,
	nnz_t const* const aSlicePfxSum,
	nnz_t const* const cSlicePfxSum,
	ui32 const* const slicesPerThrdPfxSum,
	omp_lock_t* const locks, const ui32& NUM_THREADS );



// ----------------------------------------------------------------------------


template < typename dind_t, typename fp_t, typename mem >
struct AB_indirect
{
	dind_t* DIndices;
	fp_t** writeAddresses;
	fp_t* nzs;
	ui64 length;

	AB_indirect();
	~AB_indirect();

	AB_indirect( const AB_indirect& other ) = delete;
	AB_indirect& operator=( const AB_indirect& other ) = delete;

	void init();
	void free();
};

template < bool doAlloc = false,
           typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem, typename AB_mem >
void
symbolic(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	regularized::AB_indirect< ind_t, fp_t, AB_mem >& ab,
	ind_t const* const thrdPtrPfxSum, ui64 const* const thrdMultsPfxSum,
	nnz_t* const thrdCNnzPfxSum,
	fp_t** accumulators, const ui32& NUM_THREADS );



template < typename ind_t, typename fp_t, typename nnz_t, typename AB_mem >
void
numeric(
	const regularized::AB_indirect< ind_t, fp_t, AB_mem >& ab,
	fp_t const* const D, fp_t* const C,
	nnz_t const* const thrdCNnzPfxSum,
	ui64 const* const thrdMultsPfxSum,
	const ui32& NUM_THREADS );

template < typename ind_t, typename fp_t, typename nnz_t, typename AB_mem >
void
numeric(
	const regularized::AB_indirect< ind_t, fp_t, AB_mem >& ab,
	fp_t const* const D, fp_t* const C,
	nnz_t const* const thrdCNnzPfxSum,
	ui64 const* const thrdMultsPfxSum,
	const ui32& NUM_THREADS, const ui32& ITERATIONS );



}

#include "parallel/Reduced_v1.cpp"

#endif /* SRC_PARALLEL_REDUCED_V1_H_ */
