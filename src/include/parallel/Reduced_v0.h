
#ifndef SRC_PARALLEL_REDUCED_V0_H_
#define SRC_PARALLEL_REDUCED_V0_H_

#include "include/sparse_types.h"
#include "include/storage_format/Cs.h"

using namespace sparse_storage;

namespace spmv
{

template < typename zind_t, typename fp_t, typename znnz_t, typename mem >
struct Z
{
	ui64 length;
	ui64 capacity;
	znnz_t* lengths;
	fp_t* nzs;
	zind_t* inds;
	ui32 ssBuffCapacity;

	Z();
	~Z();

	Z( const Z& other ) = delete;
	Z& operator= ( const Z& other ) = delete;

	void init();
	void free();
};


template < typename fp_t >
void
allocate_ss_buffs( fp_t*** ssBuffs_out, const ui32& capacity,
				   const ui32& numThreads );



template < bool doAlloc = false,
           typename ind_t, typename fp_t, typename nnz_t,
           typename zind_t, typename znnz_t,
           typename A_mem, typename B_mem, typename C_mem, typename Z_mem >
void
symbolic(
	const ind_t& B_columns,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	ind_t const* const thrdPtrPfxSum,
	ui64 const* const thrdMultPfxSum,
	const ui32& numThreads, nnz_t* thrdCNnzPfxSum,
	fp_t** accumulators,
	Cs< ind_t, fp_t, nnz_t, C_mem > &C,
	spmv::Z< zind_t, fp_t, znnz_t, Z_mem > &z,
	ind_t* const extraMem );



template < typename fp_t, typename nnz_t,
           typename zind_t, typename znnz_t,
           typename Z_mem >
void
numeric(
	const spmv::Z< zind_t, fp_t, znnz_t, Z_mem >& z,
	fp_t const* const D,
	nnz_t const* const thrdCNnzPfxSum,
	ui64 const* const thrdMultPfxSum,
	const ui32& numThreads,
	fp_t* const C );



template < typename fp_t, typename nnz_t,
           typename zind_t, typename znnz_t,
           typename Z_mem >
void
numeric(
	const spmv::Z< zind_t, fp_t, znnz_t, Z_mem >& data,
	fp_t const* const D,
	nnz_t const* const thrdCNnzPfxSum,
	ui64 const* const thrdMultPfxSum,
	const ui32& NUM_THREADS,
	fp_t* const C,
	fp_t** ssBuffPerThrd, const ui32& SS_BUFF_CAPACITY );



// ----------------------------------------------------------------------------


template < typename zind_t, typename fp_t, typename mem >
struct Z_indirect
{
	ui64 capacity;
	fp_t** writeAddresses;
	fp_t* nzs;
	zind_t* inds;

	Z_indirect();
	~Z_indirect();

	Z_indirect( const Z_indirect& other ) = delete;
	Z_indirect& operator= ( const Z_indirect& other ) = delete;

	void init();
	void free();
};



template < bool doAlloc = false,
           typename ind_t, typename fp_t, typename nnz_t, typename zind_t,
           typename A_mem, typename B_mem, typename C_mem, typename Z_mem >
void
symbolic(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	ind_t const* const thrdPtrPfxSum,
	ui64 const* const thrdMultPfxSum,
	const ui32& numThreads, nnz_t* thrdCNnzPfxSum,
	fp_t** accumulators,
	Cs< ind_t, fp_t, nnz_t, C_mem > &C,
	spmv::Z_indirect< zind_t, fp_t, Z_mem > &z,
	ind_t* const extraMem );



template < typename fp_t, typename nnz_t, typename zind_t,
           typename Z_mem >
void
numeric(
	const Z_indirect< zind_t, fp_t, Z_mem >& data,
	fp_t const* const D,
	nnz_t const* const thrdCNnzPfxSum,
	ui64 const* const thrdMultPfxSum,
	const ui32& NUM_THREADS,
	fp_t* const C );


};


#include "parallel/Reduced_v0.cpp"

#endif /* SRC_PARALLEL_REDUCED_V0_H_ */
