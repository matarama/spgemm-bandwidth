/*
 * RowByRow3p_v0.h
 *
 *  Created on: Aug 15, 2017
 *      Author: memoks
 */

#ifndef SRC_INCLUDE_PARALLEL_ROWBYROWREDUCED_V0_H_
#define SRC_INCLUDE_PARALLEL_ROWBYROWREDUCED_V0_H_

#include <string>
#include <iostream>

#include "include/sparse_types.h"
#include "include/util/Memory.h"
#include "include/data_structure/Triplet.h"

namespace rbr_reduced
{
namespace v0
{

template < typename z_ind_t, typename fp_t, typename z_nnz_t,
           typename z_mem >
class Thrd;
	
template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename z_ind_t, typename z_nnz_t, typename Z_mem >
void
symbolic_alloc(
	const Csc< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csc< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< z_ind_t, fp_t, z_nnz_t, Z_mem >& Z,
	fp_t** accumulatorPerThrd, ind_t const* const thrdRowPfxSum,
	const ui32& numThreads, Thrd< z_ind_t);



template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_prime_mem >
void
symbolic(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_prime_mem >& C,
	fp_t** accumulatorPerThrd, ind_t const* const thrdRowPfxSum, 
	const ui32& numThreads );


	
template < typename ind_t, typename fp_t, typename nnz_t,
           typename C_prime_mem, typename C_mem >
void
numeric(
	const Csr< ind_t, fp_t, nnz_t, C_prime_mem >& C_prime,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t const* const D,
	nnz_t const* const workPfxSum, const ui32& numThreads );
}
}

template < typename z_ind_t, typename fp_t, typename z_nnz_t,
           typename z_mem >
class rbr_reduced::v0::Thrd
{
public:
	Thrd();
	~Thrd();

	Thrd( const Thrd& other ) = delete;
	Thrd& operator =( const Thrd& other ) = delete;

	void init();
	void free();

// private:
	ui64 Z_ptrLength;
	ui64 Z_capacity;
	z_ind_t* Z_colInd;
	fp_t* Z_nzs;
	z_nnz_t* Z_lengths;
};

#include "parallel/RowByRowReduced_v0.cpp"

#endif /* SRC_INCLUDE_PARALLEL_ROWBYROWREDUCED_V0_H_ */
