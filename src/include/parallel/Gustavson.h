
#ifndef SRC_PARALLEL_GUSTAVSON_H_
#define SRC_PARALLEL_GUSTAVSON_H_

#include <iostream>
#include <string>
#include <vector>

#include "include/sparse_types.h"
#include "include/storage_format/Csr.h"
#include "include/storage_format/Cs.h"

using namespace sparse_storage;

namespace rbr
{
namespace gustavson
{

template < bool doAlloc = false, bool isSorted = false,
           typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
symbolic_stl(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd,
	nnz_t const* const thrdRowPfxSum,
	const ui32& numThreads );


/**
 * Uses static partitioning (thrdRowPfxSum)
 */
template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
numeric(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd,
	nnz_t const* const thrdRowPfxSum,
	const ui32& numThreads );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
numeric(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd,
	nnz_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	const ui32& ITERATIONS );


/**
 * Uses OMP's dynamic schedule policy on A matrix rows.
 * Call set_omp_num_threads( no-of-threads ) before this function.
 */
template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
numeric_OMP_dynamic(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd );

/**
 * Uses OMP's dynamic scheduler over METIS (row-slices) partitions
 * (instead of rows).
 */
template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
numeric_METIS_dynamic(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd,
	const ui32& numParts,
	ind_t* partRowPfxSum );


// No accumulator routines
// ----------------------------------------------------------------------------

template < bool doAlloc = false,
           typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
symbolic_stl(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd,
	nnz_t const* const thrdRowPfxSum,
	ind_t** C_offsetsPerThrd,
	const ui32& numThreads );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
numeric(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	nnz_t const* const thrdRowPfxSum,
	ind_t** C_offsetsPerThrd,
	const ui32& numThreads );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
numeric(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	nnz_t const* const thrdRowPfxSum,
	ind_t** C_offsetsPerThrd,
	const ui32& numThreads,
	const ui32& ITERATIONS );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
numeric( const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
		 const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
		 Cs< ind_t, fp_t, nnz_t, C_mem >& C,
		 ind_t** C_offsetsPerThrd,
		 nnz_t const* const aSlicePfxSum,
		 nnz_t const* const cSlicePfxSum,
		 ui32 const* const slicesPerThrdPfxSum,
		 const ui32& numThreads );


// ----------------------------------------------------------------------------

// Routine to count how many B cache lines are touched
template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
count(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	const Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	const ui32& cacheLineBytes,
	glbInd_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	ui64& noOfCacheLinesRead_B,
	ui64& noOfLoads_accumulator,
	ui64& noOfLoads_A,
	ui64& noOfLoads_B,
	ui64& noOfLoads_C,
	ui64& noOfStores_accumulator,
	ui64& noOfStores_C );

// Data preparation routines
// -------------------------------------------------------------------------
template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem >
void
partition(
	Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	ind_t** thrdRowPfxSum_out,
	const ui32& numThreads );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem >
void
prepare(
	Triplets& A, Triplets& B,
	Csr< ind_t, fp_t, nnz_t, A_mem >& A_out,
	Csr< ind_t, fp_t, nnz_t, B_mem >& B_out,
	ind_t const* const thrdRowPfxSum, const ui32& numThreads );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
prepare(
	Triplets& A, Triplets& B, Triplets& C,
	Csr< ind_t, fp_t, nnz_t, A_mem >& A_out,
	Csr< ind_t, fp_t, nnz_t, B_mem >& B_out,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C_out,
	ind_t const* const thrdRowPfxSum, const ui32& numThreads );
}
}


#include "parallel/Gustavson.cpp"

#endif /* SRC_PARALLEL_GUSTAVSON_H_ */
