/*
 * RowByRow3p_v0.h
 *
 *  Created on: Aug 15, 2017
 *      Author: memoks
 */

#ifndef SRC_INCLUDE_PARALLEL_ROWBYROW3P_V2_H_
#define SRC_INCLUDE_PARALLEL_ROWBYROW3P_V2_H_

#include <string>
#include <iostream>

#include "include/sparse_types.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Csr.h"


namespace rbr3p
{
namespace v2
{

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem = DefaultAllocator >
class Thrd;

// Kernel routines
// -----------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
symbolic(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd, ind_t const* const thrdRowPfxSum, 
	const ui32& numThreads, Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
symbolic_preprocessC(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd, ind_t const* const thrdRowPfxSum, 
	const ui32& numThreads, Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
symbolic_alloc(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd, ind_t const* const thrdRowPfxSum,
	const ui32& numThreads, Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem,
           typename B_prime_mem >
void
phase1( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
        const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
        ind_t const* const thrdRowPfxSum, 
        const ui32& numThreads,
        Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem,
           typename B_prime_mem >
void
phase2( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
        const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
        ind_t const* const thrdRowPfxSum, 
        const ui32& numThreads,
        Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
         Csr< ind_t, fp_t, nnz_t, C_mem >& C,
         nnz_t const* const thrdRowPfxSum,
         const ui32& numThreads,
         Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_simd( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
              Csr< ind_t, fp_t, nnz_t, C_mem >& C,
              fp_t** accumulatorPerThrd,
              nnz_t const* const thrdRowPfxSum,
              const ui32& numThreads,
              Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_gustavson( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
                   const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
                   Csr< ind_t, fp_t, nnz_t, C_mem >& C,
                   nnz_t const* const thrdRowPfxSum, const ui32& numThreads,
                   rbr3p::v2::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );

// -----------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem >
void
alloc( const ui32& numThreads,
       const ui32& fpCacheCapacity,
       const ui32& indCacheCapacity,
       const ui32& lengthCacheCapacity,
       rbr3p::v2::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >*** thrds_out );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem >
void
setCacheCapacity( rbr3p::v2::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds,
                  const ui32& numThreads,
                  const ui32& fpCacheCapacity,
                  const ui32& indCacheCapacity,
                  const ui32& lengthCacheCapacity );
}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem >
class rbr3p::v2::Thrd
{
public:
	Thrd();
	~Thrd();
	
	Thrd( const Thrd& other ) = delete;
	Thrd& operator =( const Thrd& other ) = delete;

	void init();
	void free();

	friend std::ostream& operator <<
	( std::ostream& out, Thrd& b )
	{ return out << b.str(); }

// private:
	// B'
	ui64 B_prime_capacity;
	ind_t* B_prime_colInds;
	fp_t* B_prime_nzs;
	nnz_t* B_prime_lengths;

	// Buffers used for streaming stores
	ui32 buffLength;
	ui32 fpCacheCapacity;
	fp_t* fpCache;
	ind_t* indCache;
	ui32 lengthCacheCapacity;
	ind_t* lengthCache;
};

// Kernel routines that use compiler intrinsics
// ############################################################################

#include "parallel/RowByRow3p_v2.cpp"

#endif /* SRC_INCLUDE_PARALLcheEL_ROWBYROW3P_V2_H_ */
