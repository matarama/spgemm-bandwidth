/*
 * RowByRow3p_v0.h
 *
 *  Created on: Aug 15, 2017
 *      Author: memoks
 */

#ifndef SRC_INCLUDE_PARALLEL_ROWBYROW3P_V0_H_
#define SRC_INCLUDE_PARALLEL_ROWBYROW3P_V0_H_

#include <string>
#include <iostream>

#include "include/sparse_types.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Csr.h"


namespace rbr3p
{
namespace v0
{

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem = DefaultAllocator >
class Thrd;

// Kernel routines
// -----------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
symbolic(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd, ind_t const* const thrdRowPfxSum, 
	const ui32& numThreads, Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds,
	char* maskIndices, ui32* masks, const ui32& noOfMasks );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
symbolic_alloc(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd, ind_t const* const thrdRowPfxSum,
	const ui32& numThreads, Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds,
	char* maskIndices, ui32* masks, const ui32& noOfMasks );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem,
           typename B_prime_mem >
void
phase1( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
        const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
        ind_t const* const thrdRowPfxSum, 
        const ui32& numThreads,
        Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem,
           typename B_prime_mem >
void
phase2( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
        const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
        ind_t const* const thrdRowPfxSum, 
        const ui32& numThreads,
        Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem,
           typename B_prime_mem >
void
phase2( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
        Csr< ind_t, fp_t, nnz_t, B_mem > const* const Bs,
        ind_t const* const thrdRowPfxSum,
        const ui32& numThreads,
        rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
gustavson_batched(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd,
	ind_t const* const thrdRowPfxSum, 
	const ui32& numThreads,
	Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds,
	const ui32& batchBufferLength );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
gustavson_batched_1MB(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd,
	ind_t const* const thrdRowPfxSum, 
	const ui32& numThreads,
	Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds,
	const ui32& batchBufferLength,
	ind_t* const rowEndPerThrd );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
gustavson_batched(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks,
	char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd,
	ind_t const* const thrdRowPfxSum, 
	const ui32& numThreads,
	Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds,
	const ui32& batchBufferLength );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
         Csr< ind_t, fp_t, nnz_t, C_mem >& C,
         ui32 const* const masks,
         char const* const maskIndexPerRow,
         fp_t** accumulatorPerThrd,
         nnz_t const* const thrdRowPfxSum,
         const ui32& numThreads,
         Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
p2_p3( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
       const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
       Csr< ind_t, fp_t, nnz_t, C_mem >& C,
       fp_t** accumulatorPerThrd,
       nnz_t const* const thrdRowPfxSum,
       const ui32& numThreads,
       Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
p2_p3_v2( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
          const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
          Csr< ind_t, fp_t, nnz_t, C_mem >& C,
          fp_t** accumulatorPerThrd,
          nnz_t const* const thrdRowPfxSum,
          const ui32& numThreads,
          Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
         Csr< ind_t, fp_t, nnz_t, C_mem >& C,
         fp_t** accumulatorPerThrd,
         nnz_t const* const thrdRowPfxSum,
         const ui32& numThreads,
         Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_incorrect( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
                   Csr< ind_t, fp_t, nnz_t, C_mem >& C,
                   fp_t** accumulatorPerThrd,
                   nnz_t const* const thrdRowPfxSum,
                   const ui32& numThreads,
                   Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );
	
template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_simd( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
              Csr< ind_t, fp_t, nnz_t, C_mem >& C,
              ui32 const* const masks,
              char const* const maskIndexPerRow,
              fp_t** accumulatorPerThrd,
              nnz_t const* const thrdRowPfxSum,
              const ui32& numThreads,
              Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
count( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
       const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
       Csr< ind_t, fp_t, nnz_t, C_mem >& C,
       const ui32& cacheLineBytes,
       ind_t const* const thrdRowPfxSum,
       const ui32& numThreads,
       ui64& noOfCacheLinesRead_B_prime,
       ui64& noOfLoads_accumulator,
       ui64& noOfLoads_A,
       ui64& noOfLoads_B_prime,
       ui64& noOfLoads_C,
       ui64& noOfLoads_masks,
       ui64& noOfStores_accumulator,
       ui64& noOfStores_B_prime,
       ui64& noOfStores_C );



template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
count( const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
       const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
       Cs< ind_t, fp_t, nnz_t, C_mem >& C,
       const ui32& cacheLineBytes,
       ind_t const* const thrdRowPfxSum,
       const ui32& numThreads,
       ui64& noOfCacheLinesRead_B_prime,
       ui64& noOfLoads_accumulator,
       ui64& noOfLoads_A,
       ui64& noOfLoads_B_prime,
       ui64& noOfLoads_C,
       ui64& noOfLoads_masks,
       ui64& noOfStores_accumulator,
       ui64& noOfStores_B_prime,
       ui64& noOfStores_C );


// -----------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem >
void
alloc( const ui32& numThreads,
       const ui32& fpCacheCapacity,
       const ui32& indCacheCapacity,
       const ui32& lengthCacheCapacity,
       rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >*** thrds_out );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem >
void
setCacheCapacity( rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds,
                  const ui32& numThreads,
                  const ui32& fpCacheCapacity,
                  const ui32& indCacheCapacity,
                  const ui32& lengthCacheCapacity );
}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem >
class rbr3p::v0::Thrd
{
public:
	Thrd();
	~Thrd();
	
	Thrd( const Thrd& other ) = delete;
	Thrd& operator =( const Thrd& other ) = delete;

	void init();
	void free();

	friend std::ostream& operator <<
	( std::ostream& out, Thrd& b )
	{ return out << b.str(); }

// private:
	// B'
	ui64 B_prime_capacity;
	ind_t* B_prime_colInds;
	fp_t* B_prime_nzs;
	nnz_t* B_prime_lengths;

	// Buffers used for streaming stores
	ui32 buffLength;
	ui32 fpCacheCapacity;
	fp_t* fpCache;
	ind_t* indCache;
	ui32 lengthCacheCapacity;
	ind_t* lengthCache;
};

// Kernel routines that use compiler intrinsics
// ############################################################################

namespace rbr3p
{
namespace v0
{

#ifdef WITH_AVX
template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_avx_ss(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A, 
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, nnz_t const* const thrdRowPfxSum,
	nnz_t const* const thrdRowPfxSumAligned,
	const ui32& numThreads, Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );
#endif

#ifdef WITH_AVX512
template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_avx512_ss(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, nnz_t const* const thrdRowPfxSum,
	nnz_t const* const thrdRowPfxSumAligned,
	const ui32& numThreads, Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_avx512_sr(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, nnz_t const* const thrdRowPfxSum,
	const ui32& numThreads, Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );
#endif

#ifdef __INTEL_COMPILER
template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
numeric_prefetch(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, nnz_t const* const thrdRowPfxSum,
	const ui32& numThreads, Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds );
#endif

}
}

#include "parallel/RowByRow3p_v0.cpp"

#endif /* SRC_INCLUDE_PARALLcheEL_ROWBYROW3P_V0_H_ */
