
#ifndef SRC_PARALLEL_GUSTAVSONINCORRECT_H_
#define SRC_PARALLEL_GUSTAVSONINCORRECT_H_

#include <iostream>
#include <string>
#include <vector>

#include "include/sparse_types.h"
#include "include/storage_format/Csr.h"

namespace rbr
{
namespace gustavson_incorrect
{
	template < typename ind_t, typename fp_t, typename nnz_t,
			   typename A_mem, typename B_mem, typename C_mem >
	void numeric_acc(
		const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
		const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
		fp_t const* const D,
		Cs< ind_t, fp_t, nnz_t, C_mem >& C,
		fp_t** accumulatorPerThrd,
		nnz_t const* const thrdRowPfxSum,
		const ui32& numThreads );

	template < typename ind_t, typename fp_t, typename nnz_t,
			   typename A_mem, typename B_mem, typename C_mem >
	void numeric_comp(
		const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
		const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
		fp_t const* const D,
		Cs< ind_t, fp_t, nnz_t, C_mem >& C,
		fp_t** accumulatorPerThrd,
		nnz_t const* const thrdRowPfxSum,
		const ui32& numThreads );
}
}



#include "parallel/GustavsonIncorrect.cpp"

#endif /* SRC_PARALLEL_GUSTAVSONINCORRECT_H_ */
