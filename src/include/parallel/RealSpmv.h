
#include "include/sparse_types.h"
#include "include/storage_format/Cs.h"


namespace spmv
{

template < typename ind_t, typename fp_t, typename nnz_t, typename A_mem >
void
yAx( const sparse_storage::Cs< ind_t, fp_t, nnz_t, A_mem >& A,
     fp_t const* const x, fp_t* const y,
     ind_t const* const thrdRowPfxSum, const ui32& NUM_THREADS );


template < typename ind_t, typename fp_t, typename nnz_t, typename A_mem >
void
yAx( const sparse_storage::Cs< ind_t, fp_t, nnz_t, A_mem >& A,
     fp_t const* const x, fp_t* const y,
     fp_t** writeAddresses,
     ind_t const* const thrdRowPfxSum, const ui32& NUM_THREADS );


}

#include "parallel/RealSpmv.cpp"
