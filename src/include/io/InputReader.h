/*
 * InputReader.h
 *
 *  Created on: Dec 21, 2016
 *      Author: memoks
 */

#ifndef SRC_INCLUDE_IO_INPUTREADER_H_
#define SRC_INCLUDE_IO_INPUTREADER_H_

#include <memory>
#include <string>

#include "include/io/MMInfo.h"

class InputReader
{
public:
	static void readMMF(
		const std::string& mmfPath, MMInfo& mmInfo,
		int* rowCount_out, int* colCount_out,  int* nnz_out,
		int** is_out, int** js_out, double** nzs_out );
};


#endif /* SRC_INCLUDE_IO_INPUTREADER_H_ */
