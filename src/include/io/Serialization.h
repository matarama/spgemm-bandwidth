/*
 * Serialization.h
 *
 *  Created on: Oct 21, 2016
 *      Author: memoks
 */

#ifndef SRC_IO_SERIALIZATION_H_
#define SRC_IO_SERIALIZATION_H_

#include <map>
#include <iostream>

#include "include/config.h"

// Short for serialization
namespace srl
{
class Metadata;

std::istream&
readId( std::istream& in, void* addressOfObj );

};

class srl::Metadata
{
public:
	std::map< PTR_TYPE, void* > idPtrMap;
};


#endif /* SRC_IO_SERIALIZATION_H_ */
