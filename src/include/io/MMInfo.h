/*
 * MMInfo2.h
 *
 *  Created on: Dec 22, 2016
 *      Author: memoks
 */

#ifndef SRC_IO_MMINFO_H_
#define SRC_IO_MMINFO_H_

#include <iostream>
#include <string>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

typedef char MM_typecode[4];

struct MMInfo
{
	int isMatrix;
	int isSparse;
	int isCoordinate;
	int isDense;
	int isArray;
	int isComplex;
	int isReal;
	int isPattern;
	int isInteger;
	int isSymmetric;
	int isGeneral;
	int isSkew;
	int isHermitian;

	MM_typecode code;

	MMInfo();
	MMInfo(MM_typecode typecode);
	virtual ~MMInfo();
	void init(MM_typecode typecode);

	char *mm_typecode_to_str(MM_typecode matcode);

	int mm_read_banner(FILE *f, MM_typecode *matcode);
	int mm_read_mtx_crd_size(FILE *f, int *M, int *N, int *nz);
	int mm_read_mtx_array_size(FILE *f, int *M, int *N);

	int mm_write_banner(FILE *f, MM_typecode matcode);
	int mm_write_mtx_crd_size(FILE *f, int M, int N, int nz);
	int mm_write_mtx_array_size(FILE *f, int M, int N);

	int mm_write_mtx_crd(
		char fname[], int M, int N, int nz, int I[], int J[],
		double val[], MM_typecode matcode);
	int mm_read_mtx_crd_data(
		FILE *f, int M, int N, int nz, int I[], int J[],
		double val[], MM_typecode matcode);
	int mm_read_mtx_crd_entry(
		FILE *f, int *I, int *J, double *real, double *img,
		MM_typecode matcode);

	int mm_read(
		FILE* f, int* rowCount_out, int* colCount_out,
		int* nnz_out, double** values_out,
		int** rowInds_out, int** colInds_out);
};

#endif /* SRC_IO_MMINFO_H_ */
