/*
 * Cli.h
 *
 *  Created on: Jan 9, 2017
 *      Author: memoks
 */

#ifndef SRC_IO_CLI_H_
#define SRC_IO_CLI_H_

#include <iostream>
#include <string>
#include <map>
#include <initializer_list>
#include <vector>

#include "include/sparse_types.h"

class Cli
{
public:
	Cli( const int& argc, char** argv );
	~Cli();

	static const std::string MMF_PATH_A;
	static const std::string MMF_PATH_B;
	static const std::string NUM_BLOCKS;
	static const std::string CACHE_LINE_BYTES;
	static const std::string SP_PART_SIZE_KB;
	static const std::string FP_PART_SIZE_KB;
	static const std::string ROWS_PER_BUCKET;
	static const std::string ITERATIONS;
	static const std::string OUTPUT_DIR;
	static const std::string HYBRID_COLUMN_NNZ_LESS;
	static const std::string HYBRID_COLUMN_PERCENT;
	static const std::string INPUT_DIR;
    static const std::string COMPARISON_PATH;
    static const std::string PERMUTE_AB;
    static const std::string TRANSPOSE_B;
    static const std::string HYBRID_PARTER_A;
    static const std::string ROWS;
    static const std::string COLUMNS;
    static const std::string NNZ_PER_ROW;
    static const std::string DENSE_ROWS;
    static const std::string NNZ_PER_ROW_DENSE;
    static const std::string HEURISTIC_TRESHOLD;
    static const std::string ACC_SIZE_KB;
    static const std::string BUFF_LENGTH_MULTIPLIER;
    static const std::string PHF_ALPHA;
    static const std::string SHAMT;
    static const std::string ACC_SIZES;
	static const std::string B_SIZE_KB;
	static const std::string TARGETTED_CACHE_SIZE_KB;
	static const std::string PATH_TO_ROW_ORDERING_A;
	static const std::string PATH_TO_ROW_PREFIX_SUM_PER_PART_A;
	static const std::string PATH_TO_ROW_ORDERING_B;
	static const std::string METIS_B_VERTEX_COUNT;
	static const std::string PATH_TO_PART_VECTOR_A;
	static const std::string PATH_TO_PART_VECTOR_B;
	static const std::string OUTPUT_PATH;
	static const std::string SORT;
	static const std::string NETWORK_L2_UNION_MB;

	std::ostream& str( std::ostream& out ) const;
	bool isSet( const std::string& s ) const;
	bool isSet( std::initializer_list< std::string > l ) const;

	std::string getMmfPathA() const { return mmfPathA; }
	std::string getMmfPathB() const { return mmfPathB; }
	ui32 getNumBlocks() const { return numBlocks; }
	ui32 getCacheLineBytes() const { return cacheLineBytes; }
	double getFpPartSizeKB() const { return fpPartSizeKB; }
	double getSpPartSizeKB() const { return spPartSizeKB; }
	ui32 getRowsPerBucket() const { return rowsPerBucket; }
	ui32 getIterations() const { return iterations; }
	double getHybridColumnNnzLe() const { return hybridColumnNnzLe; }
	double getHybridColumnNnzPercent() const { return hybridColumnNnzPercent; }
	std::string getOutputDir() const { return outputDir; }
	std::string getInputDir() const { return inputDir; }
	std::string getComparisonPath() const { return comparisonPath; }
	bool getPermuteAB() const { return permuteAB; }
	bool getTransposeB() const { return transposeB; }
	bool getHybridParterA() const { return hybridParterA; }
	ui64 getRows() const { return rows; }
	ui64 getColumns() const { return columns; }
	ui64 getNnzPerRow() const { return nnzPerRow; }
	ui64 getDenseRows() const { return denseRows; }
	ui64 getNnzPerRowDense() const { return nnzPerRowDense; }
	double getHeuristicTreshold() const { return heuristicTreshold; }
	double getAccSizeKB() const { return accSizeKB; }
	double getBuffLengthMultiplier() const { return buffLengthMultiplier; }
	ui32 getPhfAlpha() const { return phfAlpha; }
	bool getShamt() const { return shamt; }
	std::vector< ui32 > getAccSizes() { return accSizes; }
	double getBSizeKB() const { return BSizeKB; }
	double getTargettedCacheSizeKB() const { return targettedCacheSizeKB; }
	std::string getPathToRowOrderingA() const { return pathToRowOrderingA; }
	std::string getPathToRowPfxSumPerPartA() const
	{ return pathToRowPfxSumPerPartA; }
	std::string getPathToRowOrderingB() const { return pathToRowOrderingB; }
	int getMetisBVertexCount() const { return metisBVertexCount; }
	std::string getPathToPartVectorA() const { return pathToPartVectorA; }
	std::string getPathToPartVectorB() const { return pathToPartVectorB; }
	std::string getOutputPath() const { return outputPath; }
	bool isSorted() const { return sorted; }
	double getNetworkL2UnionMB() const { return networkL2UnionMB; }

private:
	std::string mmfPathA;
	std::string mmfPathB;
	ui32 numBlocks;
	ui32 cacheLineBytes;
	double spPartSizeKB;
	double fpPartSizeKB;
	ui32 rowsPerBucket;
	ui32 iterations;
	std::string outputDir;
	double hybridColumnNnzLe;
	double hybridColumnNnzPercent;
	std::string inputDir;
	std::string comparisonPath;
	bool permuteAB;
	bool transposeB;
	bool hybridParterA;
	ui64 rows;
	ui64 columns;
	ui64 nnzPerRow;
	ui64 denseRows;
	ui64 nnzPerRowDense;
	double heuristicTreshold;
	double accSizeKB;
	double buffLengthMultiplier;
	ui32 phfAlpha;
	bool shamt;
	std::vector< ui32 > accSizes;
	double BSizeKB;
	double targettedCacheSizeKB;
	std::string pathToRowOrderingA;
	std::string pathToRowPfxSumPerPartA;
	std::string pathToRowOrderingB;
	int metisBVertexCount;
	std::string pathToPartVectorA;
	std::string pathToPartVectorB;
	std::string outputPath;
	bool sorted;
	double networkL2UnionMB;

	std::map< std::string, bool > vars;
};

#endif /* SRC_IO_CLI_H_ */
