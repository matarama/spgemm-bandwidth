/*
 * Csr.h
 *
 *  Created on: Dec 20, 2016
 *      Author: memoks
 */

#ifndef SRC_DATA_STRUCTURE_CSR_H_
#define SRC_DATA_STRUCTURE_CSR_H_

#include <string>
#include <iostream>

#include "include/sparse_types.h"
#include "include/data_structure/Triplet.h"
#include "include/util/Memory.h"

template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory = DefaultAllocator >
class Csr
{
public:
	Csr();
	Csr( Triplets& triplets );
	~Csr();

	Csr( const Csr& other );
	Csr& operator =( const Csr& other );

	template < typename MemoryOther >
	void copy( const Csr< ind_t, fp_t, nnz_t, MemoryOther >& other,
	           ind_t const* const thrdRowPfxSum, const ui32& numThreads );

	void extract( Triplets& triplets );
	/**
	 * Extracting csr while respecting first touch.
	 */
	void extract( Triplets& triplets,
	              ind_t const* const thrdRowPfxSum,
	              const ui32& numThreads );
	void zero();
	bool isNull() const;
	void init();
	void free();
	void reset();

	// Print & Stats
	std::ostream& stats( std::ostream& out ) const;
	std::ostream& plot( std::ostream& out ) const;
	std::string stats() const;
	std::string plot() const;
	bool isSame( fp_t const* const res );

// private:
	glbInd_t rowCount;
	glbInd_t columnCount;
	nnz_t nnz;
	nnz_t* rowPtr;
	ind_t* colInd;
	fp_t* nzs;

private:
	void _copy( const Csr& other );
};

#include "storage_format/Csr.cpp"

//namespace std
//{
//template < >
//void
//swap( Csr& lhs, Csr& rhs )
//{ lhs.swap( rhs ); }
//}

#endif /* SRC_DATA_STRUCTURE_CSR_H_ */
