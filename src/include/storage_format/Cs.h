
#ifndef SRC_STORAGE_FORMAT_CS_H_
#define SRC_STORAGE_FORMAT_CS_H_

#include "include/sparse_types.h"
#include "include/data_structure/Triplet.h"
#include "include/util/Memory.h"

namespace sparse_storage
{

template < typename ind_t, typename fp_t, typename nnz_t, typename mem >
struct Cs
{
	ui64 length;
	nnz_t* ptr;
	ind_t* inds;
	fp_t* nzs;

	Cs();
	~Cs();

	Cs( const Cs& other ) = delete;
	Cs& operator=( const Cs& other ) = delete;

	void init();
	void free();
	nnz_t nnz() const;
	void reset();
};



template < typename find_t, typename ffp_t, typename fnnz_t, typename fmem,
           typename tind_t, typename tfp_t, typename tnnz_t, typename tmem >
void
copy( const Cs< find_t, ffp_t, fnnz_t, fmem >& from,
      Cs< tind_t, tfp_t, tnnz_t, tmem >& to );



template < typename ind_t, typename fp_t, typename nnz_t, typename mem >
void
convertToCsr( Triplets& from, Cs< ind_t, fp_t, nnz_t, mem >& to );



template < typename ind_t, typename fp_t, typename nnz_t, typename mem >
void
convertToCsc( Triplets& from, Cs< ind_t, fp_t, nnz_t, mem >& to );


}



#include "storage_format/Cs.cpp"

#endif /* SRC_STORAGE_FORMAT_CS_H_ */
