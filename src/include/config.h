/*
 * config.h
 *
 *  Created on: Dec 19, 2016
 *      Author: memoks
 */


#ifndef SRC_INCLUDE_CONFIG_H_
#define SRC_INCLUDE_CONFIG_H_

// Some data types and alignment rules
// for flexibility (float, double, etc...)
// --------------------------------------------------------

#define PTR_SIZE 8 // Bytes
#define PTR_TYPE ui64

// Compilation flag that is passed by makefile
// (for code not to break)
// --------------------------------------------------------

#define _XSTR( value ) _STR( value )
#define _STR( value ) #value

#define RESTRICT __restrict__
#define REGISTER register

// Static buffer sizes
// --------------------------------------------------------

#define BUFFER_SIZE_RESULT 8192 // 8 KBs
#define BUFFER_SIZE_TRACER 67108864	 // 64 MBs
#define L1_STREAM_BUFFER_SIZE 8 // 64 Byte
#define L1_SS_DEFAULT_BUFFER_SIZE 8 // 64 Byte
#define L1_SR_DEFAULT_BUFFER_SIZE 8 // 64 Byte

#ifdef __INTEL_COMPILER

#define PREFETCH_T0( addr, nrOfBytesAhead ) \
	_mm_prefetch( ( (char *)(addr) ) + nrOfBytesAhead, _MM_HINT_T0 )

#ifndef FETCH_DIST
#define FETCH_DIST 128
#endif

#endif

#endif /* SRC_INCLUDE_CONFIG_H_ */
