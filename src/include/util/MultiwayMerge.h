
namespace MultiwayMerge
{

// Optimized min-heap implementation for multiway-merge
// ---------------------------------------------------------------------
// Deletion is postponed until next insertion so that heapify is called
// only 1 time instead of 2. Therefore there isn't extractMin function.
// Since deletion only happens at first element, heapify( i=-1 ) will
// delete values[ 0 ].
template < typename K, typename V, typename I >
struct MinHeap
{
	K size;
	K* keys;
	V* values;
	I* indices;

	MinHeap();
	~MinHeap();
	void init( const K& capacity );
	void free();

	MinHeap( const MinHeap& other ) = delete;
	MinHeap& operator=( const MinHeap& other ) = delete;

	inline void insert( const K& k, const V& v, const I& i );
	inline void min( K& k_out, V& v_out, I& i_out );

	/**
	 * ASSUMPTION: I should be a type that can take negative values.
	 * When i is negative, first element in heap is deleted.
	 */
	inline void heapify( K& k, V& v, I& i );
	std::ostream& str( std::ostream& out );
	bool checkHeapProperty( const ui32& i = 0 );
};

};

#include "util/MultiwayMerge.cpp"
