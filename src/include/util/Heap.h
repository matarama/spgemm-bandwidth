
#ifndef SRC_INCLUDE_UTIL_HEAP_H_
#define SRC_INCLUDE_UTIL_HEAP_H_

/*
 * Heap.h
 *
 *  Created on: Apr 2, 2018
 *      Author: macbeth
 */

#include "include/sparse_types.h"

namespace heap
{
	template < typename T >
	void insert( T* arr, const ui32& size, const T& value );

	template < typename T >
	T remove( T* arr, ui32& size );
}

#endif /* SRC_INCLUDE_UTIL_HEAP_H_ */
