/*
 * Generic.h
 *
 *  Created on: Jan 3, 2017
 *      Author: memoks
 */

#ifndef SRC_INCLUDE_UTIL_MEMORY_H_
#define SRC_INCLUDE_UTIL_MEMORY_H_

#ifdef WITH_LIB_NUMA
#include <numa.h>
#endif

#ifdef WITH_HBW_MEMORY
#include <hbwmalloc.h>
#endif

#include <string>
#include <iostream>

#include "include/config.h"
#include "include/sparse_types.h"

// Cache alignment (32 or 64 bytes)
#define ALIGNMENT 64

namespace allocator
{
#ifdef WITH_LIB_NUMA
class NumaLocal
{
public:
	static void* alloc( const std::size_t& bytes );
	static void* alloc( const std::size_t& length,
	                    const std::size_t& bytesPerEntry );
	static void* initAlloc( const std::size_t& bytes );
	static void* initAlloc( const std::size_t& length,
	                        const std::size_t& bytesPerEntry );
	static void free( void* t,
	                  const std::size_t& length = 1,
	                  const std::size_t& bytesPerEntry = 1 );
};

class NumaInterleaved
{
public:
	static void* alloc( const std::size_t& bytes );
	static void* alloc( const std::size_t& length,
	                    const std::size_t& bytesPerEntry );
	static void* initAlloc( const std::size_t& bytes );
	static void* initAlloc( const std::size_t& length,
	                        const std::size_t& bytesPerEntry );
	static void free( void* t,
	                  const std::size_t& length,
	                  const std::size_t& bytesPerEntry );
};
#endif /* WITH_LIB_NUMA */

class Aligned
{
  public:
	static void* alloc( const std::size_t& bytes );
	static void* alloc( const std::size_t& length,
	                    const std::size_t& bytesPerEntry );
	static void* initAlloc( const std::size_t& bytes );
	static void* initAlloc( const std::size_t& length,
	                        const std::size_t& bytesPerEntry );
	static void free( void* t,
	                  const std::size_t& length = 0,
	                  const std::size_t& bytesPerEntry = 0 );
};

#ifdef WITH_HBW_MEMORY
class Mcdram
{
  public:
	static void* alloc( const std::size_t& bytes );
	static void* alloc( const std::size_t& length,
	                    const std::size_t& bytesPerEntry );
	static void* initAlloc( const std::size_t& bytes );
	static void* initAlloc( const std::size_t& length,
	                        const std::size_t& bytesPerEntry );
	static void free( void* t,
	                  const std::size_t& length = 0,
	                  const std::size_t& bytesPerEntry = 0 );
};
#endif /* WITH_HBW_MEMORY */

}

using DefaultAllocator = allocator::Aligned;

namespace util
{
template < typename D >
D* alloc( const std::size_t& length = 1 );

template < typename D >
void free( D* arr, const std::size_t& length = 1 );

/* template < typename D > */
/* D* alignedMemAlloc( const std::size_t& length ); */

/* template < typename D > */
/* void alignedMemFree( D* arr ); */

/* template < typename D > */
/* D* numaAllocInterleaved( const std::size_t& length ); */

/* template < typename D > */
/* D* numaAllocLocal( const std::size_t& length = 1 ); */

/* template < typename D > */
/* void numaFree( D* ptr, const std::size_t& length = 1 ); */

/* template < typename D > */
/* D* hbwAlloc( const std::size_t& length = 1 ); */

/* template < typename D > */
/* void hbwFree( D* ptr ); */
};

// -----------------------------------------------------------------------------

template < typename D >
D*
util::alloc( const std::size_t& length )
{
	return static_cast< D* >( DefaultAllocator::alloc( length, sizeof( D ) ) );
}

template < typename D >
void
util::free( D* arr, const std::size_t& length )
{
	DefaultAllocator::free( arr, length );
}

/* template < typename D > */
/* D* */
/* util::alignedMemAlloc( const std::size_t& length ) */
/* { */
/* 	D* arr = NULL; */

/* #ifdef __INTEL_COMPILER */
/* 	arr = ( D* ) _mm_malloc( sizeof( D ) * length, ALIGNMENT ); */
/* #else */
/* 	posix_memalign( (void**) &arr, ALIGNMENT, sizeof( D ) * length ); */
/* #endif */

/* 	return arr; */
/* } */

/* template < typename D > */
/* void */
/* util::alignedMemFree( D* arr ) */
/* { */
/* #ifdef __INTEL_COMPILER */
/* 	_mm_free( arr ); */
/* #else */
/* 	std::free( arr ); */
/* #endif */
/* } */

/* template < typename D > */
/* D* */
/* util::numaAllocInterleaved( const std::size_t& length ) */
/* { */
/* #ifdef WITH_LIB_NUMA */
/* 	return (D*) numa_alloc_interleaved( sizeof( D ) * length ); */
/* #else */
/* 	return alignedMemAlloc< D >( length ); */
/* #endif */
/* } */

/* template < typename D > */
/* D* */
/* util::numaAllocLocal( const std::size_t& length ) */
/* { */
/* #ifdef WITH_LIB_NUMA */
/* 	return (D*) numa_alloc_local( sizeof( D ) * length ); */
/* #else */
/* 	return alignedMemAlloc< D >( length ); */
/* #endif */
/* } */

/* template < typename D > */
/* void */
/* util::numaFree( D* ptr, const std::size_t& length ) */
/* { */
/* #ifdef WITH_LIB_NUMA */
/* 	numa_free( (void*) ptr, sizeof( D ) * length ); */
/* #else */
/* 	util::alignedMemFree( ptr ); */
/* #endif */
/* } */

/* template < typename D > */
/* D* */
/* util::hbwAlloc( const std::size_t& length ) */
/* { */
/* 	D* arr = nullptr; */
/* #ifdef WITH_HBW_MEMORY */
/* 	hbw_posix_memalign( (void**) &arr, ALIGNMENT, sizeof( D ) * length ); */
/* #else */
/* 	arr = util::alloc< D >( length ); */
/* #endif */

/* 	return arr; */
/* } */

/* template < typename D > */
/* void */
/* util::hbwFree( D* ptr ) */
/* { */
/* #ifdef WITH_HBW_MEMORY */
/* 	hbw_free( ptr ); */
/* #else */
/* 	util::free( ptr ); */
/* #endif */
/* } */


#endif /* SRC_INCLUDE_UTIL_MEMORY_H_ */
