/*
 * DataPrep.h
 *
 *  Created on: Jan 3, 2017
 *      Author: memoks
 */

#ifndef SRC_UTIL_DATA_H_
#define SRC_UTIL_DATA_H_

#include <tuple>
#include <vector>

#include "include/config.h"
#include "include/sparse_types.h"
#include "include/data_structure/Triplet.h"
#include "include/data_structure/SubMtx.h"
#include "include/parallel/Job.h"
#include "include/parallel/OuterProduct_v2.h"
#include "include/parallel/HybridProduct_v0.h"

namespace data
{
/*
void
prepareJobs(
    Triplets& A, Triplets& B, Triplets C,
    Csr< glbInd_t, fp_t, glbNnz_t >** spmB_out,
    Csr< glbInd_t, fp_t, glbNnz_t >** spmC_out,
    double partSizeC_KB, double partSizeB_KB,
    JobFp*** fpJobs_out, ui32* numFpJobs_out,
    JobSp*** spJobs_out, ui32* numSpJobs_out );
*/
// -----------------------------------------------------------------------------

void
prepareGustafson(
    Triplets& A, Triplets& B,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrA_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrB_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrC_out );

void
prepareGustafsonMod(
    Triplets& A, Triplets& B,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrA_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrB_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrC_out,
    buffColOffset_t** offset_out,
    buffNnz_t** offsetPreSum_out );

void
prepareGustafsonStatic(
    Triplets& A, Triplets& B, ui32 numBlocks,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrA_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrB_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrC_out,
    glbInd_t** rowPtrPartsA_out );

void
prepareGustafsonModStatic(
    Triplets& A, Triplets& B, ui32 numBlocks,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrA_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrB_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrC_out,
    buffColOffset_t*** offsets_out,
    glbInd_t** rowPtrPartsA_out );

// local A and C matrices
// -----------------------------------------------------------------------------

void
prepareGustafsonStatic(
    Triplets& A, Triplets& B, ui32 numBlocks,
    SubMtx< ind_t, fp_t, nnz_t, Csr >***  csrAs_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrB_out,
    SubMtx< ind_t, fp_t, nnz_t, Csr >*** csrCs_out );

void
prepareGustafsonModStatic(
    Triplets& A, Triplets& B, ui32 numBlocks,
    SubMtx< ind_t, fp_t, nnz_t, Csr >*** csrAs_out,
    Csr< glbInd_t, fp_t, glbNnz_t >& csrB_out,
    SubMtx< ind_t, fp_t, nnz_t, Csr >*** csrCs_out,
    buffColOffset_t*** offsets_out );

};

#endif /* SRC_UTIL_DATA_H_ */
