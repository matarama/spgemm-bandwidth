/*
 * IO.h
 *
 *  Created on: Jan 3, 2017
 *      Author: memoks
 */

#ifndef SRC_INCLUDE_UTIL_IO_H_
#define SRC_INCLUDE_UTIL_IO_H_

#include <cstdio>
#include <iostream>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <string>

#include "include/config.h"
#include "include/sparse_types.h"
#include "include/logging/StackTraceException.h"

namespace util
{
template < typename FP >
FP* readResult( const std::string& path );

template < typename FP >
void writeResult( const std::string& path, FP const* const r, const ui64 length );
};

template < typename FP >
FP*
util::readResult( const std::string& path )
{
	std::ifstream in( path, std::ifstream::binary );
	ui64 length = 0;
	in.read( reinterpret_cast< char* >( &length ), sizeof( length ) );
	FP* r = util::alloc< FP >( length );
	in.read( reinterpret_cast< char* >( r ), sizeof( *r ) * length );
	in.close();

	return r;
}

template < typename FP >
void
util::writeResult( const std::string& path,
             FP const* const r, const ui64 length )
{
	std::ofstream out( path, std::ofstream::binary );
	out.write( reinterpret_cast< char const* >( &length ), sizeof( length ) );
	out.write( reinterpret_cast< char const* >( r ), sizeof( *r ) * length );
	out.close();
}

#endif /* SRC_INCLUDE_UTIL_IO_H_ */
