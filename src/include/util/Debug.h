/*
 * Debug.h
 *
 *  Created on: Jan 3, 2017
 *      Author: memoks
 */

#ifndef SRC_INCLUDE_UTIL_DEBUG_H_
#define SRC_INCLUDE_UTIL_DEBUG_H_

#include <iostream>

namespace util
{
extern std::ostream& printStackFrames( std::ostream& out );
};


#endif /* SRC_INCLUDE_UTIL_DEBUG_H_ */
