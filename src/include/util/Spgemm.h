/*
 * DataPrep.h
 *
 *  Created on: Jan 3, 2017
 *      Author: memoks
 */

#ifndef SRC_UTIL_SPGEMM_H_
#define SRC_UTIL_SPGEMM_H_

#include <tuple>
#include <vector>

#include "include/sparse_types.h"
#include "include/data_structure/Triplet.h"
#include "include/util/Memory.h"

namespace util
{

template < typename ind_t >
void
partition( Triplets& A, Triplets& B,
           ind_t** thrdRowPfxSum_out,
           std::vector< ui64 >& multPfxSumPerThrd,
           const ui32& numThreads );

template < typename ind_t >
void
partition(
	const glbNnz_t* firstPtr, Triplets& first, const bool& row,
	const glbNnz_t* secondPtr,
	const ui32& numThreads,
	ind_t* thrdPtrPfxSum, ui64* thrdMultPfxSum, ui64* multsPfxSum );

template < typename ind_t >
void
partitionColumnWise(
	Triplets& A, Triplets& B, const ui32& numThreads,
	ind_t* thrdPtrPfxSum, ui64* thrdMultPfxSum, ui64* multsPfxSum );

template < typename ind_t >
void
partitionRowWise(
	Triplets& A, Triplets& B, const ui32& numThreads,
	ind_t* thrdPtrPfxSum, ui64* thrdMultPfxSum, ui64* multsPfxSum );

	
template < typename ind_t, typename nnz_t >
void
align( ind_t const* const thrdRowPfxSum, const ui32& numThreads,
       nnz_t const* const C_rowPtr, const ui32& alignLength,
       ind_t** thrdRowPfxSumAligned_out );

Triplet*
preprocessC( Triplets& A, Triplets& B, const ui32& numThreads,
             double** accumulatorPerThrd,
             ui64& nnzC_out, ui64& multiplicationCount_out,
             ui64* multiplicationCountPerThrd_out );

void
generateMultiMapSTL_verbose(
	const std::vector< ui32 >& vAccLengths,
	const glbNnz_t& NO_OF_ROWS, const glbInd_t& NO_OF_COLUMNS,
	glbNnz_t const* const C_rowPtr, glbInd_t const* const C_colInd,
	const ui32& numThreads,
	char* maskIndexPerRow, const std::vector< ui64 >& multsPerRow );

void
generateMultiMapSTL(
	const std::vector< ui32 >& vAccLengths,
	const glbNnz_t& NO_OF_ROWS, const glbInd_t& NO_OF_COLUMNS,
	glbNnz_t const* const C_rowPtr, glbInd_t const* const C_colInd,
	const ui32& numThreads,
	char* maskIndexPerRow );

void
generateMultiMapLinearTime(
	const std::vector< ui32 >& vAccLengths,
	const glbNnz_t& NO_OF_ROWS, const glbInd_t& NO_OF_COLUMNS,
	glbNnz_t const* const C_rowPtr, glbInd_t const* const C_colInd,
	const ui32& numThreads, fp_t** accumulatorsPerThrd, 
	char* maskIndexPerRow );

void
generateMultiMapSymbolic(
	const std::vector< ui32 >& vAccLengths,
	const glbNnz_t& NO_OF_ROWS, const glbInd_t& NO_OF_COLUMNS,
	glbNnz_t const* const C_rowPtr, glbInd_t const* const C_colInd,
	const ui32& numThreads, fp_t** accumulatorsPerThrd, 
	char* maskIndexPerRow );

template < typename ind_t >
void
partitionRowWise(
	Triplets& A, const ui32& numThreads, ind_t* thrdRowPfxSum );

};

template < typename ind_t >
void
util::partition( Triplets& A, Triplets& B,
                 ind_t** thrdRowPfxSum_out,
                 std::vector< ui64 >& multPfxSumPerThrd,
                 const ui32& numThreads )
{
	multPfxSumPerThrd.push_back( 0 );
	ind_t* thrdRowPfxSum = nullptr;
	#pragma omp parallel
	#pragma omp master
	thrdRowPfxSum = util::alloc< ind_t >( numThreads + 1 );

	A.sortRowMajor();
	std::shared_ptr< std::vector< glbNnz_t > > pA_rowPtr( A.rowPtr() );
	std::vector< glbNnz_t >& A_rowPtr = *pA_rowPtr.get();
	std::shared_ptr< std::vector< glbNnz_t > > pB_rowPtr( B.rowPtr() );
	std::vector< glbNnz_t >& B_rowPtr = *pB_rowPtr.get();

	ui64 multCount = 0;
	for ( std::size_t nA = 0; nA < A.length(); ++nA )
	{
		glbInd_t jA = A[ nA ].j;
		multCount += B_rowPtr[ jA + 1 ] - B_rowPtr[ jA ];
	}

	ui64 multPerThrd = ( multCount + numThreads - 1 ) / numThreads;
	ui64 limit = multPerThrd;
	ui64 currMults = 0;
	ui64 prevMults = 0;
	ui32 t = 0;
	for ( glbInd_t r = 0; r < A.rows(); ++r )
	{
		prevMults = currMults;
		for ( glbNnz_t nA = A_rowPtr[ r ]; nA < A_rowPtr[ r + 1 ]; ++nA )
		{
			glbInd_t jA = A[ nA ].j;
			currMults += B_rowPtr[ jA + 1 ] - B_rowPtr[ jA ];
		}

		if ( currMults >= limit )
		{
			limit += multPerThrd;
			++t;
			thrdRowPfxSum[ t ] = r;
			multPfxSumPerThrd.push_back( prevMults );
		}
	}

	thrdRowPfxSum[ 0 ] = 0;
	thrdRowPfxSum[ numThreads ] = A.rows();
	*thrdRowPfxSum_out = thrdRowPfxSum;

	if ( multPfxSumPerThrd.size() == numThreads )
		multPfxSumPerThrd.push_back( multCount );
	else
		multPfxSumPerThrd[ numThreads ] = multCount;
}

// TODO split triplets into 3 separate arrays
template < typename ind_t >
void
util::partition(
	const glbNnz_t* firstPtr, Triplets& first, const bool& row,
	const glbNnz_t* secondPtr,
	const ui32& numThreads,
	ind_t* thrdPtrPfxSum, ui64* thrdMultPfxSum, ui64* multsPfxSum )
{
	ind_t length;
	ui64 multCount = 0;
	if ( row )
	{
		for ( ind_t i = 0; i < first.rows() + 1; ++i )
			multsPfxSum[ i ] = 0;

		for ( std::size_t n = 0; n < first.length(); ++n )
		{
			glbInd_t l = first[ n ].i;
			multCount += secondPtr[ l + 1 ] - secondPtr[ l ];
			multsPfxSum[ l + 1 ] += secondPtr[ l + 1 ] - secondPtr[ l ];;
		}

		for ( ind_t i = 0; i < first.rows(); ++i )
			multsPfxSum[ i + 1 ] += multsPfxSum[ i ];
	}
	else
	{
		for ( ind_t i = 0; i < first.columns() + 1; ++i )
			multsPfxSum[ i ] = 0;

		for ( std::size_t n = 0; n < first.length(); ++n )
		{
			glbInd_t l = first[ n ].j;
			multCount += secondPtr[ l + 1 ] - secondPtr[ l ];
			multsPfxSum[ l + 1 ] += secondPtr[ l + 1 ] - secondPtr[ l ];
		}

		for ( ind_t i = 0; i < first.columns(); ++i )
			multsPfxSum[ i + 1 ] += multsPfxSum[ i ];
	}

	thrdPtrPfxSum[ 0 ] = 0;
	ui64 multPerThrd = ( multCount + numThreads - 1 ) / numThreads;
	ui64 limit = multPerThrd;
	ui64 currMults = 0;
	ui64 prevMults = 0;
	ui32 t = 0;
	if ( row )
	{
		for ( glbInd_t r = 0; r < first.rows(); ++r )
		{
			prevMults = currMults;
			for ( glbNnz_t n = firstPtr[ r ]; n < firstPtr[ r + 1 ]; ++n )
			{
				glbInd_t l = first[ n ].j;
				currMults += secondPtr[ l + 1 ] - secondPtr[ l ];
			}

			if ( currMults >= limit )
			{
				limit += multPerThrd;
				++t;
				thrdPtrPfxSum[ t ] = r;
			}
		}

		thrdPtrPfxSum[ numThreads ] = first.rows();
	}
	else
	{
		for ( glbInd_t c = 0; c < first.columns(); ++c )
		{
			prevMults = currMults;
			for ( glbNnz_t n = firstPtr[ c ]; n < firstPtr[ c + 1 ]; ++n )
			{
				glbInd_t l = first[ n ].i;
				currMults += secondPtr[ l + 1 ] - secondPtr[ l ];
			}

			if ( currMults >= limit )
			{
				limit += multPerThrd;
				++t;
				thrdPtrPfxSum[ t ] = c;
			}
		}

		thrdPtrPfxSum[ numThreads ] = first.columns();
	}

	
	thrdMultPfxSum[ 0 ] = 0;
	for ( ui32 t = 0; t < numThreads; ++t )
		thrdMultPfxSum[ t + 1 ] = multsPfxSum[ thrdPtrPfxSum[ t + 1 ] ];

	thrdMultPfxSum[ numThreads ] = multCount;
}



template < typename ind_t >
void
util::partitionColumnWise(
	Triplets& A, Triplets& B, const ui32& numThreads,
	ind_t* thrdColPfxSum, ui64* thrdMultsPfxSum, ui64* colMultsPfxSum )
{
	std::shared_ptr< std::vector< glbNnz_t > > pBColPtr = B.colPtr();
	std::shared_ptr< std::vector< glbNnz_t > > pAColPtr = A.colPtr();
	std::vector< glbNnz_t >& BColPtr = *pBColPtr.get();
	std::vector< glbNnz_t >& AColPtr = *pAColPtr.get();
	B.sortColMajorStd();
	A.sortColMajorStd();

	for ( ind_t cB = 0; cB < B.columns() + 1; ++cB )
		colMultsPfxSum[ cB ] = 0;

	for ( nnz_t nB = 0; nB < B.length(); ++nB )
	{
		const ind_t JB = B[ nB ].j;
		const ind_t IB = B[ nB ].i;
		colMultsPfxSum[ JB + 1 ] += AColPtr[ IB + 1 ] - AColPtr[ IB ];
	}

	for ( ind_t cB = 0; cB < B.columns(); ++cB )
		colMultsPfxSum[ cB + 1 ] += colMultsPfxSum[ cB ];

	// partition B into column-slices of roughly equal flops
	thrdColPfxSum[ 0 ] = 0;
	thrdMultsPfxSum[ 0 ] = 0;
	ui32 t = 1;
	const ui64 MULTS = colMultsPfxSum[ B.columns() ];
	const ui64 TARGET = ( MULTS + numThreads - 1 ) / numThreads;
	ui64 limit = TARGET;
	for ( ind_t cB = 0; cB < B.columns(); ++cB )
	{
		if ( limit < colMultsPfxSum[ cB ] )
		{
			thrdColPfxSum[ t ] = cB;
			thrdMultsPfxSum[ t ] = colMultsPfxSum[ cB ];
			++t;
			limit = TARGET * t;
		}
	}

	for ( ui32 i = t; i <= numThreads; ++i )
	{
		thrdColPfxSum[ i ] = B.columns();
		thrdMultsPfxSum[ i ] = MULTS;
	}
}



template < typename ind_t >
void
util::partitionRowWise(
	Triplets& A, Triplets& B, const ui32& numThreads,
	ind_t* thrdRowPfxSum, ui64* thrdMultsPfxSum, ui64* rowMultsPfxSum )
{
	std::shared_ptr< std::vector< glbNnz_t > > pBRowPtr = B.rowPtr();
	std::shared_ptr< std::vector< glbNnz_t > > pARowPtr = A.rowPtr();
	std::vector< glbNnz_t >& BRowPtr = *pBRowPtr.get();
	std::vector< glbNnz_t >& ARowPtr = *pARowPtr.get();
	B.sortRowMajorStd();
	A.sortRowMajorStd();

	for ( ind_t ra = 0; ra < A.rows() + 1; ++ra )
		rowMultsPfxSum[ ra ] = 0;

	for ( nnz_t na = 0; na < A.length(); ++na )
	{
		const ind_t IA = A[ na ].i;
		const ind_t JA = A[ na ].j;
		rowMultsPfxSum[ IA + 1 ] += BRowPtr[ JA + 1 ] - BRowPtr[ JA ];
	}

	for ( ind_t ra = 0; ra < A.rows(); ++ra )
		rowMultsPfxSum[ ra + 1 ] += rowMultsPfxSum[ ra ];

	// partition A into row-slices of roughly equal flops
	thrdRowPfxSum[ 0 ] = 0;
	thrdMultsPfxSum[ 0 ] = 0;
	ui32 t = 1;
	const ui64 MULTS = rowMultsPfxSum[ B.columns() ];
	const ui64 TARGET = ( MULTS + numThreads - 1 ) / numThreads;
	ui64 limit = TARGET;
	for ( ind_t ra = 0; ra < A.rows(); ++ra )
	{
		if ( limit < rowMultsPfxSum[ ra ] )
		{
			thrdRowPfxSum[ t ] = ra;
			thrdMultsPfxSum[ t ] = rowMultsPfxSum[ ra ];
			++t;
			limit = TARGET * t;
		}
	}

	for ( ui32 i = t; i <= numThreads; ++i )
	{
		thrdRowPfxSum[ i ] = A.rows();
		thrdMultsPfxSum[ i ] = MULTS;
	}
}

template < typename ind_t >
void
util::partitionRowWise(
	Triplets& A, const ui32& numThreads, ind_t* thrdRowPfxSum )
{
	std::shared_ptr< std::vector< glbNnz_t > > pARowPtr = A.rowPtr();
	std::vector< glbNnz_t >& ARowPtr = *pARowPtr.get();
	A.sortRowMajorStd();

	// partition A into row-slices of roughly equal flops
	thrdRowPfxSum[ 0 ] = 0;
	ui32 t = 1;
	const ui64 MULTS = A.length();
	const ui64 TARGET = ( MULTS + numThreads - 1 ) / numThreads;
	ui64 limit = TARGET;
	for ( ind_t ra = 0; ra < A.rows(); ++ra )
	{
		if ( limit < ARowPtr[ ra ] )
		{
			thrdRowPfxSum[ t ] = ra;
			++t;
			limit = TARGET * t;
		}
	}

	for ( ui32 i = t; i <= numThreads; ++i )
		thrdRowPfxSum[ i ] = A.rows();
}


template < typename ind_t, typename nnz_t >
void
util::align( ind_t const* const thrdRowPfxSum, const ui32& numThreads,
             nnz_t const* const C_rowPtr, const ui32& alignLength,
             ind_t** thrdRowPfxSumAligned_out )
{
	ind_t* thrdRowPfxSumAligned = util::alloc< ind_t >( numThreads );
	for ( ui32 t = 0; t < numThreads; ++t )
	{
		ind_t rowStart = thrdRowPfxSum[ t ];
		while ( C_rowPtr[ rowStart ] % alignLength )
			++rowStart;
		thrdRowPfxSumAligned[ t ] = rowStart;
	}

	*thrdRowPfxSumAligned_out = thrdRowPfxSumAligned;
}

#endif /* SRC_UTIL_SPGEMM_H_ */
