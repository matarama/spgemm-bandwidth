/*
 * Generic.h
 *
 *  Created on: Jan 3, 2017
 *      Author: memoks
 */

#ifndef SRC_INCLUDE_UTIL_GENERIC_H_
#define SRC_INCLUDE_UTIL_GENERIC_H_

#include <vector>
#include <algorithm>
#include <iostream>

#include "include/config.h"
#include "include/sparse_types.h"
#include "include/util/Memory.h"

namespace util
{
extern const double MAX_ERROR;

template < typename FP >
void reset( FP* const vec, const size_t& length );

template < typename FP >
FP* generateRandomVector( const size_t& length );

template < typename FP >
FP* newVector( const size_t& length );

template < typename FP >
bool isSame( const FP* const v1, const FP* const v2,
             const size_t& length, const double& e = MAX_ERROR );

template < typename T >
void printHistogramExponential( std::ostream& out, std::ostream& header,
                                std::vector< T > data );

template < typename T >
void printHistogramLinear( std::ostream& out, std::ostream& header,
                           std::vector< T > data );

template < typename index, typename value >
class CustomComp
{
public:
	CustomComp( const std::vector< value >& values )
	: values( values )
	{
	}

	~CustomComp() { }

	bool
	operator ()( const index& left, const index& right )
	{
		return values[ left ] < values[ right ];
	}

private:
	std::vector< value > values;
};

template < typename T >
ui32 lower_bound( const T& value, T const* const arr, const ui32& length );

template < typename cmp_t >
void quicksort( cmp_t* cmpPtr, const ui64& p, const ui64& r );

template < typename cmp_t >
ui64 partition( cmp_t* cmpPtr, const ui64& p, const ui64& r );

template < typename cmp_t, typename value_t >
void quicksort( cmp_t* cmpPtr, const ui64& p, const ui64& r,
                value_t* valuePtr );

template < typename cmp_t, typename value_t >
ui64 partition( cmp_t* cmpPtr, const ui64& p, const ui64& r,
                value_t* valuePtr );

template < typename t >
void swap( t& l, t& r );


template < typename cmp_t, typename value1_t, typename value2_t >
void quicksort( cmp_t* cmpPtr, const ui64& p, const ui64& r,
                value1_t* value1Ptr, value2_t* value2Ptr );

template < typename cmp_t, typename value1_t, typename value2_t >
ui64 partition( cmp_t* cmpPtr, const ui64& p, const ui64& r,
                value1_t* value1Ptr, value2_t* value2Ptr );

}

// -----------------------------------------------------------------------------

template < typename FP >
void
util::reset( FP* const vec, const size_t& length )
{
	for ( size_t i = 0; i < length; ++i )
		vec[ i ] = (FP) 0;
}

template < typename FP >
FP*
util::generateRandomVector( const size_t& length )
{
	FP* arr = alloc< FP >( length );
	std::generate_n( arr, length, std::rand );
	return arr;
}

template < typename FP >
FP*
util::newVector( const size_t& length )
{
	FP* arr = alloc< FP >( length );
	reset( arr, length );
	return arr;
}

template < typename FP >
bool
util::isSame( const FP* const v1, const FP* const v2,
              const size_t& length, const double& e )
{
	double total1 = 0, total2 = 0;
	bool isSame = true;
	for ( size_t i = 0; i < length; ++i )
	{
		/* std::cout << "isSame->" << i << std::endl; */
		/* std::cout << "v1: " << v1[ i ] << std::endl; */
		/* std::cout << "v2: " << v2[ i ] << std::endl; */
		total1 += v1[ i ];
		total2 += v2[ i ];
		if ( v1[ i ] - v2[ i ] > e || v1[ i ] - v2[ i ] < -e )
		{
#ifdef DEBUG            
			std::cout << std::endl;
			std::cout << i << ".\t"
			          << v1[ i ] << "\t"
			          << v2[ i ] << "\t = "
			          << v1[ i ] - v2[ i ];
#endif
			isSame = false;
		}
	}

#ifdef DEBUG
	if ( !isSame )
	{
		std::cout << std::endl;
		std::cout << "total1=" << total1 << std::endl;
		std::cout << "total2=" << total2 << std::endl;
	}	
#endif

	return isSame;
}

template < typename T >
void
util::printHistogramExponential( std::ostream& out, std::ostream& header,
                                 std::vector< T > data )
{
	ui64 lessThanArr[ 28 ] = { 0 };
	for ( std::size_t i = 0; i < data.size(); ++i )
	{
		if ( data[ i ] <= 0 )
			++lessThanArr[ 0 ];
		else if ( data[ i ] < 2 )
			++lessThanArr[ 1 ];
		else if ( data[ i ] < 4 )
			++lessThanArr[ 2 ];
		else if ( data[ i ] < 8 )
			++lessThanArr[ 3 ];
		else if ( data[ i ] < 16 )
			++lessThanArr[ 4 ];
		else if ( data[ i ] < 32 )
			++lessThanArr[ 5 ];
		else if ( data[ i ] < 64 )
			++lessThanArr[ 6 ];
		else if ( data[ i ] < 128 )
			++lessThanArr[ 7 ];
		else if ( data[ i ] < 256 )
			++lessThanArr[ 8 ];
		else if ( data[ i ] < 512 )
			++lessThanArr[ 9 ];
		else if ( data[ i ] < 1024 )
			++lessThanArr[ 10 ];
		else if ( data[ i ] < 2048 )
			++lessThanArr[ 11 ];
		else if ( data[ i ] < 4096 )
			++lessThanArr[ 12 ];
		else if ( data[ i ] < 8192 )
			++lessThanArr[ 13 ];
		else if ( data[ i ] < 16384 )
			++lessThanArr[ 14 ];
		else if ( data[ i ] < 32768 )
			++lessThanArr[ 15 ];
		else if ( data[ i ] < 65536 )
			++lessThanArr[ 16 ];
		else if ( data[ i ] < 131072 )
			++lessThanArr[ 17 ];
		else if ( data[ i ] < 262144 )
			++lessThanArr[ 18 ];
		else if ( data[ i ] < 524288 )
			++lessThanArr[ 19 ];
		else if ( data[ i ] < 1048576 )
			++lessThanArr[ 20 ];
		else if ( data[ i ] < 2097152 )
			++lessThanArr[ 21 ];
		else if ( data[ i ] < 4194304 )
			++lessThanArr[ 22 ];
		else if ( data[ i ] < 8388608 )
			++lessThanArr[ 23 ];
		else if ( data[ i ] < 16777216 )
			++lessThanArr[ 24 ];
		else if ( data[ i ] < 33554432 )
			++lessThanArr[ 25 ];
		else if ( data[ i ] < 67108864 )
			++lessThanArr[ 26 ];
		else
			++lessThanArr[ 27 ];
	}

	for ( int i = 0; i < 28; ++i )
		out << lessThanArr[ i ] << ",";

	header << "<= 0, < 2,< 4,< 8,< 16,< 32,< 64,< 128,< 256,< 512,"
	       << "< 1K,< 2K,< 4K,< 8K,< 16K,< 32K,< 64K,< 128K,< 256K,"
	       << "< 512K,< 1M,< 2M,< 4M,< 8M,< 16M,< 32M,< 64M, > 64M,";
}

template < typename T >
void
util::printHistogramLinear( std::ostream& out, std::ostream& header,
                            std::vector< T > data )
{
	ui64 lessThanArr[ 21 ] = { 0 };
	for ( std::size_t i = 0; i < data.size(); ++i )
	{
		if ( data[ i ] <= 0 )
			++lessThanArr[ 0 ];
		else if ( data[ i ] <= 1 )
			++lessThanArr[ 1 ];
		else if ( data[ i ] <= 2 )
			++lessThanArr[ 2 ];
		else if ( data[ i ] <= 3 )
			++lessThanArr[ 3 ];
		else if ( data[ i ] <= 4 )
			++lessThanArr[ 4 ];
		else if ( data[ i ] <= 5 )
			++lessThanArr[ 5 ];
		else if ( data[ i ] <= 6 )
			++lessThanArr[ 6 ];
		else if ( data[ i ] <= 7 )
			++lessThanArr[ 7 ];
		else if ( data[ i ] <= 8 )
			++lessThanArr[ 8 ];
		else if ( data[ i ] <= 9 )
			++lessThanArr[ 9 ];
		else if ( data[ i ] <= 10 )
			++lessThanArr[ 10 ];
		else if ( data[ i ] <= 11 )
			++lessThanArr[ 11 ];
		else if ( data[ i ] <= 12 )
			++lessThanArr[ 12 ];
		else if ( data[ i ] <= 13 )
			++lessThanArr[ 13 ];
		else if ( data[ i ] <= 14 )
			++lessThanArr[ 14 ];
		else if ( data[ i ] <= 15 )
			++lessThanArr[ 15 ];
		else if ( data[ i ] <= 16 )
			++lessThanArr[ 16 ];
		else if ( data[ i ] <= 32 )
			++lessThanArr[ 17 ];
		else if ( data[ i ] <= 64 )
			++lessThanArr[ 18 ];
		else if ( data[ i ] <= 128 )
			++lessThanArr[ 19 ];
		else
			++lessThanArr[ 20 ];
	}

	for ( int i = 0; i < 21; ++i )
		out << lessThanArr[ i ] << ",";

	header << "<= 0, <= 1,<= 2,<= 3,<= 4,<= 5,<= 6,<= 7,<= 8,"
	       << "<= 9,<= 10,<= 11,<= 12,<= 13,<= 14,<= 15,<= 16,"
	       << "<= 32,<= 64,<= 128,> 128,";
}

template < typename T >
ui32
util::lower_bound( const T& value, T const* const arr, const ui32& length )
{
	ui32 left = 0;
	ui32 right = length;
	ui32 mid;
	do
	{
		mid = ( left + right ) / 2;
		if ( value < arr[ mid ] )
			right = mid;
		else if ( value > arr[ mid ] )
			left = mid;
		else
			return mid;

	} while ( right - left > 1 );

	return left;
}

template < typename cmp_t >
void
util::quicksort( cmp_t* cmpPtr, const ui64& p, const ui64& r )
{
	if ( p < r )
	{
		ui64 q = partition( cmpPtr, p, r );
		quicksort( cmpPtr, p, q );
		quicksort( cmpPtr, q + 1, r );
	}
}

template < typename cmp_t >
ui64
util::partition( cmp_t* cmpPtr, const ui64& p, const ui64& r )
{
	cmp_t x = cmpPtr[ p ];
	ui64 i = p;
	for ( ui64 j = p + 1; j < r; ++j )
	{
		if ( cmpPtr[ j ] <= x )
		{
			++i;
			swap( cmpPtr[ i ], cmpPtr[ j ] );
		}
	}

	swap( cmpPtr[ p ], cmpPtr[ i ] );
	return i;
}


template < typename cmp_t, typename value_t >
void
util::quicksort( cmp_t* cmpPtr, const ui64& p, const ui64& r,
                 value_t* valuePtr )
{
	if ( p < r )
	{
		ui64 q = partition( cmpPtr, p, r, valuePtr );
		quicksort( cmpPtr, p, q, valuePtr );
		quicksort( cmpPtr, q + 1, r, valuePtr );
	}
}

template < typename cmp_t, typename value_t >
ui64
util::partition( cmp_t* cmpPtr, const ui64& p, const ui64& r,
                 value_t* valuePtr )
{
	cmp_t x = cmpPtr[ p ];
	ui64 i = p;
	for ( ui64 j = p + 1; j < r; ++j )
	{
		if ( cmpPtr[ j ] <= x )
		{
			++i;
			swap( cmpPtr[ i ], cmpPtr[ j ] );
			swap( valuePtr[ i ], valuePtr[ j ] );
		}
	}

	swap( cmpPtr[ p ], cmpPtr[ i ] );
	swap( valuePtr[ p ], valuePtr[ i ] );
	return i;
}

template < typename cmp_t, typename value1_t, typename value2_t >
void
util::quicksort( cmp_t* cmpPtr, const ui64& p, const ui64& r,
                 value1_t* value1Ptr, value2_t* value2Ptr )
{
	if ( p < r )
	{
		ui64 q = partition( cmpPtr, p, r, value1Ptr, value2Ptr );
		quicksort( cmpPtr, p, q, value1Ptr, value2Ptr );
		quicksort( cmpPtr, q + 1, r, value1Ptr, value2Ptr );
	}
}

template < typename cmp_t, typename value1_t, typename value2_t >
ui64
util::partition( cmp_t* cmpPtr, const ui64& p, const ui64& r,
                 value1_t* value1Ptr, value2_t* value2Ptr )
{
	cmp_t x = cmpPtr[ p ];
	ui64 i = p;
	for ( ui64 j = p + 1; j < r; ++j )
	{
		if ( cmpPtr[ j ] <= x )
		{
			++i;
			swap( cmpPtr[ i ], cmpPtr[ j ] );
			swap( value1Ptr[ i ], value1Ptr[ j ] );
			swap( value2Ptr[ i ], value2Ptr[ j ] );
		}
	}

	swap( cmpPtr[ p ], cmpPtr[ i ] );
	swap( value1Ptr[ p ], value1Ptr[ i ] );
	swap( value2Ptr[ p ], value2Ptr[ i ] );
	return i;
}

template < typename t >
void
util::swap( t& l, t& r )
{
	t temp = l;
	l = r;
	r = temp;
}

#endif /* SRC_INCLUDE_WRAPPER_UTIL_H_ */
