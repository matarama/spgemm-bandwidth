
#ifndef SRC_DATA_STRUCTURE_QUINTET_H_
#define SRC_DATA_STRUCTURE_QUINTET_H_

#include <iostream>
#include <string>
#include <memory>
#include <vector>
#include <map>
#include <omp.h>

#include "include/sparse_types.h"
#include "include/util/Generic.h"

struct Triplet
{
public:
	trpFp_t nz; // non-zero
	glbInd_t i; // row (used for sorting)
	glbInd_t j; // column (used for sorting)

	Triplet();
	Triplet( glbInd_t i, glbInd_t j, trpFp_t nz );

	Triplet( const Triplet& other );
	~Triplet();
	Triplet& operator =( const Triplet& other );

	bool operator ==( const Triplet& other ) const;

	static bool cmpRowMajor( const Triplet& l, const Triplet& r );
	static bool cmpColMajor( const Triplet& l, const Triplet& r );
	static Triplet* array( const int& nnz,
						   const int* const is,
						   const int* const js,
						   const double* const nzs );

	static std::unique_ptr< Triplet[] >
	generate( const ui64& rowCount, const ui64& colCount,
	          const ui64& nnzPerRow );
	static std::unique_ptr< Triplet[] >
	generate( const ui64& rowCount, const ui64& colCount,
	          const ui64& nnzPerRow,
	          const ui64& denseRowCount, const ui64& nnzPerRowDense);
	static void matrixMarketForm(
		Triplet* trpArr,
		const ui64& rows, const ui64& columns, const ui64& nnz,
		const std::string& header, const std::string& mmfPath );

	std::ostream& str( std::ostream& out ) const;
	std::string str() const;
	friend std::ostream& operator <<
	( std::ostream& out, const Triplet& quintet )
	{ return out << quintet.str(); }

private:
	void clear();
	void copy( const Triplet& other );
};


class Triplets
{
public:
	Triplets();
	Triplets( glbInd_t iStart, glbInd_t jStart,
			  glbInd_t iLength, glbInd_t jLength );
	Triplets( glbInd_t iStart, glbInd_t jStart,
			  glbInd_t iLength, glbInd_t jLenght,
			  Triplet* begin, Triplet* end );

	Triplets( const Triplets& other );
	~Triplets();
	Triplets& operator =( const Triplets& other );

	Triplet& operator []( std::size_t i );
	const Triplet& operator []( std::size_t i ) const;
	Triplet& back();
	const Triplet& back() const;

	void addToI( long i );
	void addToJ( long j );
	void addToIJ( long i, long j );
	void remapI( const std::vector< glbInd_t >& iVec );
	void remapJ( const std::vector< glbInd_t >& jVec );
	void remapIJ( const std::vector< glbInd_t >& iVec,
				  const std::vector< glbInd_t >& jVec );
	void transpose();
	void permuteRandom();
	bool isSkewedRow() const;
    bool isSkewedColumn() const;

	void sortRowMajorStd();
	void sortRowMajor( ui32 numThreads = 1 );
	void sortColMajorStd();
	void sortColMajor( ui32 numThreads = 1 );

	template < typename v, typename ind_t >
	void sortRowBy( v* valuePerRow,
	                ind_t const* const thrdRowPfxSum,
	                const ui32& numThreads );

	void sanityCheck() const;

	std::shared_ptr< std::vector< glbInd_t > > nnzPerRow() const;
	std::shared_ptr< std::vector< glbNnz_t > > rowPtr() const;
	std::shared_ptr< std::vector< glbInd_t > > nnzPerCol() const;
	std::shared_ptr< std::vector< glbNnz_t > > colPtr() const;
//	ind_t* nnzPerRow() const;
//	ui64* rowPtr() const;
//	ind_t* nnzPerCol() const;
//	ui64* colPtr() const;

	// Getter & Setter
	Triplet* begin();
	Triplet* end();
	Triplet* cbegin() const;
	Triplet* cend() const;
	std::size_t length() const;
	bool isEmpty() const;
	glbInd_t rows() const;
	glbInd_t columns() const;
	glbInd_t rowStart() const;
	glbInd_t colStart() const;
	glbInd_t rowEnd() const;
	glbInd_t colEnd() const;
	double KBs_CSR( const ui32& bytesInInd_t,
	                const ui32& bytesInFp_t,
	                const ui32& bytesInNnz_t ) const;
	double MBs_CSR( const ui32& bytesInInd_t,
	                const ui32& bytesInFp_t,
	                const ui32& bytesInNnz_t ) const;

	void setRowStart( glbInd_t rowStart );
	void setColStart( glbInd_t colStart );
	void setRowCount( glbInd_t rowCount );
	void setColCount( glbInd_t colCount );
	void setBegin( Triplet* begin );
	void setEnd( Triplet* end );

	// Print & Stats
	std::string summary() const;
	std::string stats() const;
	std::string str() const;
	std::ostream& summary( std::ostream& out ) const;
	std::ostream& stats( std::ostream& out ) const;
	void stats( std::ostream& stats, std::ostream& header ) const;
	std::ostream& str( std::ostream& out ) const;
	friend std::ostream& operator <<
	( std::ostream& out, const Triplets& triplets )
	{ return triplets.str( out ); }


	static void permuteRandom( Triplets& A, Triplets& B );
	static void updateRowColumnIndices( Triplets& A, Triplets& B,
	                                    const std::vector< int >& A_rowOrder,
	                                    const std::vector< int >& B_rowOrder );

	static std::shared_ptr< std::vector< ui64 > >
	calculateMultsPerRow( const Triplets& A, const Triplets& B );

	static std::shared_ptr< std::vector< double > >
	calculateCompressionRatioPerRow(
		const std::vector< ui64 >& multCountPerRow, const Triplets& C );

	static std::shared_ptr< std::vector< double > >
	calculateCompressionRatioPerRow(
		const Triplets& A, const Triplets& B, const Triplets& C );

	static std::shared_ptr< std::vector< glbInd_t > >
	calculateMultsPerCNonZero(
		const Triplets& A, const Triplets& B, const Triplets& C );

private:

	enum CacheId { NNZ_PER_ROW, ROW_PTR, NNZ_PER_COL, COL_PTR };

	// TODO assumption. Triplets should be stored in array
	// or RandomAccessIterator type.
	Triplet* _begin;
	Triplet* _end;
	glbInd_t _iStart;
	glbInd_t _jStart;
	glbInd_t _iLength;
	glbInd_t _jLength;

	// cache
	mutable std::map< CacheId, std::weak_ptr< std::vector< glbInd_t > > > cacheNnzPer;
	mutable std::map< CacheId, std::weak_ptr< std::vector< glbNnz_t > > > cachePtr;

	void clear();
	void copy( const Triplets& other );
	bool isSortedRowMajor() const;
	bool isSortedColMajor() const;
};


template < typename v, typename ind_t >
void
Triplets::sortRowBy( v* valuePerRow,
                     ind_t const* const thrdRowPfxSum,
                     const ui32& numThreads )
{
	sortRowMajorStd();
	std::shared_ptr< std::vector< glbNnz_t > > pRowPtr( rowPtr() );
	std::vector< glbNnz_t >& rowPtr = *pRowPtr.get();
	std::vector< glbInd_t > reverseMap;
	for ( glbInd_t i = 0; i < rows(); ++i )
		reverseMap.push_back( i );
	std::vector< glbInd_t > ordering( reverseMap.size(), 0 );

	#pragma omp parallel num_threads( numThreads )
	{
		const ui32 ID = omp_get_thread_num();
		const ind_t ROW_START = thrdRowPfxSum[ ID ];
		const ind_t ROW_END = thrdRowPfxSum[ ID + 1 ];

		util::quicksort( valuePerRow, ROW_START, ROW_END, &reverseMap[ 0 ] );
		// util::quicksort( valuePerRow, ROW_START, ROW_END );

		for ( ind_t i = ROW_START; i < ROW_END; ++i )
			ordering[ reverseMap[ i ] ] = i;
	}

	remapI( ordering );
	sortRowMajor();
}

namespace garbage
{
	std::size_t
	getIndexFromLeft( Triplet* begin, Triplet* end, glbInd_t j );
}

#endif /* SRC_DATA_STRUCTURE_QUINTET_H_ */
