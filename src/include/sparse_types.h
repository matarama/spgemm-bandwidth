/*
 * custom_types.h
 *
 *  Created on: Apr 8, 2017
 *      Author: memoks
 */

#ifndef SRC_INCLUDE_SPARSE_TYPES_H_
#define SRC_INCLUDE_SPARSE_TYPES_H_

#include <cstdlib>

// Basic types
// --------------------------------------------------------
typedef size_t ui64;
typedef unsigned int ui32;
typedef unsigned short ui16;
typedef unsigned char ui8;

typedef double fp64;
typedef float fp32;


// spgemm intermediate data length
// --------------------------------------------------------
typedef ui64 buffNnz_t;
typedef ui32 buffOffset_t;

typedef ui32 buffRowInd_t;
typedef ui32 buffColInd_t;
typedef ui32 buffColOffset_t;


// Row & column indices for global matrix
// --------------------------------------------------------
typedef int glbInd_t;


// global matrix non-zero count
// --------------------------------------------------------
typedef int glbNnz_t;


// OuterProduct data structure types
// --------------------------------------------------------
typedef ui32 opRowInd_t;
typedef ui32 opColOffset_t;
typedef ui16 opBucketId_t;

// triplets
// --------------------------------------------------------
typedef double trpFp_t;


// types for sub-matrices
// --------------------------------------------------------
typedef glbInd_t ind_t;
typedef double fp_t;
typedef glbNnz_t nnz_t;

#endif /* SRC_INCLUDE_SPARSE_TYPES_H_ */
