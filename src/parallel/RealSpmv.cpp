

template < typename ind_t, typename fp_t, typename nnz_t, typename A_mem >
void
spmv::yAx( const sparse_storage::Cs< ind_t, fp_t, nnz_t, A_mem >& A,
           fp_t const* const x,
           fp_t* const y,
           ind_t const* const thrdRowPfxSum, const ui32& NUM_THREADS )
{
	#pragma omp parallel num_threads( NUM_THREADS )
	{
		const ui32 ID = omp_get_thread_num();
		const ind_t ROW_START = thrdRowPfxSum[ ID ];
		const ind_t ROW_END = thrdRowPfxSum[ ID + 1 ];

		for ( ind_t i = ROW_START; i < ROW_END; ++i )
		{
			fp_t res = 0.0;
			for ( nnz_t n = A.ptr[ i ]; n < A.ptr[ i + 1 ]; ++n )
				res += A.nzs[ n ] * x[ A.inds[ n ] ];
			y[ i ] = res;
		}
	}
}



template < typename ind_t, typename fp_t, typename nnz_t, typename A_mem >
void
spmv::yAx( const sparse_storage::Cs< ind_t, fp_t, nnz_t, A_mem >& A,
           fp_t const* const x,
           fp_t* const y,
           fp_t** writeAddresses,
           ind_t const* const thrdRowPfxSum, const ui32& NUM_THREADS )
{
	#pragma omp parallel num_threads( NUM_THREADS )
	{
		const ui32 ID = omp_get_thread_num();
		const ind_t ROW_START = thrdRowPfxSum[ ID ];
		const ind_t ROW_END = thrdRowPfxSum[ ID + 1 ];

		for ( ind_t i = ROW_START; i < ROW_END; ++i )
			y[ i ] = 0;

		for ( nnz_t n = A.ptr[ ROW_START ]; n < A.ptr[ ROW_END ]; ++n )
			*writeAddresses[ n ] += x[ A.inds[ n ] ] * A.nzs[ n ];
	}
}
