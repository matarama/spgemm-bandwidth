
#include <omp.h>
#include <immintrin.h>

#include "include/util/Generic.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"

// Kernel routines that perform memory allocation inside
// -----------------------------------------------------------------------------

template < typename ind_t >
void
rbr3p::v1::phase0_alloc( Triplets& B, const double& sizeKB,
                         ind_t** B_bucketRowPfxSum_out, ui32& noOfBuckets )
{
	// calculate total size of B
	const double sizeB = sizeKB * 1024;
	double B_totalSizeKB = B.length() * ( sizeof( ind_t ) + sizeof( fp_t ) ) +
		( B.rows() + 1 ) * ( sizeof( nnz_t ) );

	noOfBuckets = ( B_totalSizeKB + sizeB - 1 ) / sizeB;
	ind_t* B_bucketRowPfxSum = nullptr;
	#pragma omp parallel
	#pragma omp single
	{
		RowSlicerGreedy rsg( noOfBuckets );
		std::vector< Triplets > vTrps = rsg.partition( B );
		
		B_bucketRowPfxSum = util::alloc< ind_t >( noOfBuckets + 1 );
		B_bucketRowPfxSum[ 0 ] = 0;
		for ( std::size_t i = 0; i < vTrps.size(); ++i )
			B_bucketRowPfxSum[ i + 1 ] = vTrps[ i ].rowEnd();
	}

	*B_bucketRowPfxSum_out = B_bucketRowPfxSum;
}

template < typename ind_t, typename fp_t, typename nnz_t >
void
rbr3p::v1::phase1_alloc(
	const Csr< ind_t, fp_t, nnz_t >& A, const Csr< ind_t, fp_t, nnz_t >& B,
	ind_t const* const B_bucketRowPfxSum, ui32 const& noOfBuckets,
	Thrd< ind_t, fp_t, nnz_t >** thrds, const ui32& numThreads,
	ui64** B_bucketLengths_out,
	ind_t*** allB_bucketsColInd_out, fp_t*** allB_bucketsNzs_out )
{
	const ui32 MIN_BUCKET_COUNT_P2 = noOfBuckets / numThreads;
	const ui32 LEFTOVER_BUCKETS_P2 = noOfBuckets % numThreads;
	ui64* B_bucketLengths = util::newVector< ui64 >( noOfBuckets );
	ind_t** allB_bucketsColInd = util::alloc< ind_t* > ( noOfBuckets );
	fp_t** allB_bucketsNzs = util::alloc< fp_t* >( noOfBuckets );
	#pragma omp parallel
	{
		// Calculate p2 bucket count
		ui32 _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];
		t->p2BucketCount = MIN_BUCKET_COUNT_P2;
		if ( _id < LEFTOVER_BUCKETS_P2 )
			++t->p2BucketCount;

		const nnz_t A_NZ_START = t->A_nzStart;
		const nnz_t A_NZ_END = t->A_nzEnd;

		t->p1BucketCount = noOfBuckets;
		t->p1A_bucketCounters = util::alloc< nnz_t >( noOfBuckets );
		t->p1A_bucketStarts = util::newVector< nnz_t >( noOfBuckets );
		t->p1A_bucketNnzs = util::newVector< nnz_t >( noOfBuckets );

		t->p3A_ptrToBNzs = util::alloc< fp_t* >( A_NZ_END - A_NZ_START );
		t->p3A_ptrToBColInd = util::alloc< ind_t* >( A_NZ_END - A_NZ_START );
		t->p3A_BLengths = util::alloc< ind_t >( A_NZ_END - A_NZ_START );
		t->p3B_lengthPerBucket = util::newVector< ui64 >( noOfBuckets );
		t->p3B_startIndexPerBucket = util::alloc< ui64 >( noOfBuckets );

		// ---------------------------------------------------------------------
		ui64 _overallBBucketLengthPhase3 = 0;
		for ( nnz_t nA = A_NZ_START; nA < A_NZ_END; ++nA )
		{
			ind_t jA = A.colInd[ nA ];

			// calculate how much A entries a thread will write to each bucket
			ui32 bucketId = util::lower_bound(
				jA, B_bucketRowPfxSum, noOfBuckets );
			++t->p1A_bucketNnzs[ bucketId ];

			// calculate how much B entries a thread will read from each bucket
			// (in phase 3)
			t->p3B_lengthPerBucket[ bucketId ] +=
				B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
			_overallBBucketLengthPhase3 += B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
		}

		// ---------------------------------------------------------------------
		#pragma omp barrier
		#pragma omp single
		{
			// Calculate from where each thread will start
			// writing into A buckets
			thrds[ 0 ]->p2BucketIdStart = 0;
			for ( ui32 b = 0; b < noOfBuckets; ++b )
				thrds[ 0 ]->p1A_bucketStarts[ b ] = 0;

			for ( ui32 t = 1; t < numThreads; ++t )
			{
				for ( ui32 b = 0; b < noOfBuckets; ++b )
				{
					thrds[ t ]->p1A_bucketStarts[ b ] =
						thrds[ t - 1 ]->p1A_bucketStarts[ b ] +
						thrds[ t - 1 ]->p1A_bucketNnzs[ b ];
				}

				thrds[ t ]->p2BucketIdStart =
					thrds[ t - 1 ]->p2BucketIdStart +
					thrds[ t - 1 ]->p2BucketCount;
			}

			// calculate from where each thread will start reading from
			// (in phase 3) for each B bucket
			// TODO make a single preprocessing step?
			for ( ui32 b = 0; b < noOfBuckets; ++b )
				thrds[ 0 ]->p3B_startIndexPerBucket[ b ] = 0;

			for ( ui32 t = 1; t < numThreads; ++t )
			{
				for ( ui32 b = 0; b < noOfBuckets; ++b )
				{
					thrds[ t ]->p3B_startIndexPerBucket[ b ] =
						thrds[ t - 1 ]->p3B_startIndexPerBucket[ b ] +
						thrds[ t - 1 ]->p3B_lengthPerBucket[ b ];
				}
			}
		}

		// Allocate A buckets as a single array for phase 2
		// ---------------------------------------------------------------------
		t->p2A_bucketNnzs = util::alloc< nnz_t >( t->p2BucketCount );
		t->p2A_buckets = util::alloc< ind_t* >( t->p2BucketCount );
		Thrd< ind_t, fp_t, nnz_t >* tLast = thrds[ numThreads - 1 ];
		nnz_t totalLen = 0;
		for ( ui32 b = 0; b < t->p2BucketCount; ++b )
		{
			t->p2A_bucketNnzs[ b ] =
				tLast->p1A_bucketStarts[ b + t->p2BucketIdStart ] +
				tLast->p1A_bucketNnzs[ b + t->p2BucketIdStart ];
			totalLen += t->p2A_bucketNnzs[ b ];
		}

		t->p2A_buckets[ 0 ] = util::alloc< ind_t >( totalLen );
		nnz_t lenSoFar = 0;
		for ( ui32 b = 0; b < t->p2BucketCount - 1; ++b )
		{
			lenSoFar += 
				tLast->p1A_bucketStarts[ b + t->p2BucketIdStart ] +
				tLast->p1A_bucketNnzs[ b + t->p2BucketIdStart ];

			t->p2A_buckets[ b + 1 ] = t->p2A_buckets[ 0 ] + lenSoFar;
		}

		// Calculate B bucket length per thread
		ui64 _totalBBucketLength = 0;
		for ( ui32 b = 0; b < t->p2BucketCount; ++b )
		{
			ui32 bucketId = b + t->p2BucketIdStart;
			ui64 bucketLength = 0;
			for ( ui32 tid = 0; tid < numThreads; ++tid )
				bucketLength += thrds[ tid ]->p3B_lengthPerBucket[ bucketId ];

			B_bucketLengths[ bucketId ] = bucketLength;
			_totalBBucketLength += bucketLength;
		}

		// allocate p2 B buckets

		// ---------------------------------------------------------------------
		#pragma omp barrier
		// ---------------------------------------------------------------------

		// Allocate & Set A bucket pointers for phase 1
		t->p1A_buckets = util::alloc< ind_t* >( t->p1BucketCount );
		ui32 k = 0;
		for ( ui32 tid = 0; tid < numThreads; ++tid )
		{
			for ( ui32 b = 0; b < thrds[ tid ]->p2BucketCount; ++b, ++k )
			{
				t->p1A_buckets[ k ] = thrds[ tid ]->p2A_buckets[ b ];
			}
		}

		// ---------------------------------------------------------------------
		#pragma omp barrier
		// ---------------------------------------------------------------------

		// allocate handles per B bucket (for current thread to be used in p2)
		// ---------------------------------------------------------------------
		t->p2B_bucketsColInd = util::alloc< ind_t* >( t->p2BucketCount );
		t->p2B_bucketsNzs = util::alloc< fp_t* >( t->p2BucketCount );
		t->p2B_bucketsNnz = util::alloc< ui64 >( t->p2BucketCount );

		// allocate B buckets continuously (with respect to p2) for each thrd
		// ---------------------------------------------------------------------
		t->p2B_bucketsColInd[ 0 ] =
			util::alloc< ind_t >( _totalBBucketLength );
		t->p2B_bucketsNzs[ 0 ] =
			util::alloc< fp_t >( _totalBBucketLength );

		ui64 lengthSoFar = 0;
		for ( ui32 b = 0; b < t->p2BucketCount; ++b )
		{
			t->p2B_bucketsColInd[ b ] =
				t->p2B_bucketsColInd[ 0 ] + lengthSoFar;
			t->p2B_bucketsNzs[ b ] = t->p2B_bucketsNzs[ 0 ] + lengthSoFar;
			t->p2B_bucketsNnz[ b ] = B_bucketLengths[ t->p2BucketIdStart + b ];
			lengthSoFar += t->p2B_bucketsNnz[ b ];

			// set up global B bucket pointers
			ui32 bucketId = t->p2BucketIdStart + b;
			allB_bucketsColInd[ bucketId ] = t->p2B_bucketsColInd[ b ];
			allB_bucketsNzs[ bucketId ] = t->p2B_bucketsNzs[ b ];
		}

		// ---------------------------------------------------------------------
		#pragma omp barrier
		// ---------------------------------------------------------------------

		for ( ui32 b = 0; b < noOfBuckets; ++b )
			t->p1A_bucketCounters[ b ] = t->p1A_bucketStarts[ b ];

		// Here lengthPerBucket array is overwritten (will not be used again)
		// and used as a counter
		for ( ui32 b = 0; b < noOfBuckets; ++b )
			t->p3B_lengthPerBucket[ b ] = t->p3B_startIndexPerBucket[ b ];

		// Fill in A buckets & original A matrix pointers
		for ( nnz_t nA = A_NZ_START; nA < A_NZ_END; ++nA )
		{
			ind_t jA = A.colInd[ nA ];
			ui32 bucketId = util::lower_bound(
				jA, B_bucketRowPfxSum, noOfBuckets );

			ind_t* A_bucket = t->p1A_buckets[ bucketId ];
			nnz_t& counter = t->p1A_bucketCounters[ bucketId ];
			A_bucket[ counter ] = jA;
			++counter;

			nnz_t n = nA - A_NZ_START;

			t->p3A_ptrToBNzs[ n ] = allB_bucketsNzs[ bucketId ] +
				t->p3B_lengthPerBucket[ bucketId ];
			t->p3A_ptrToBColInd[ n ] = allB_bucketsColInd[ bucketId ] +
				t->p3B_lengthPerBucket[ bucketId ];
			t->p3A_BLengths[ n ] = B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
			t->p3B_lengthPerBucket[ bucketId ] +=
				B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
		}
	}

	*B_bucketLengths_out = B_bucketLengths;
	*allB_bucketsColInd_out = allB_bucketsColInd;
	*allB_bucketsNzs_out = allB_bucketsNzs;
}

// Kernel routines that don't perform memory allocation inside
// -----------------------------------------------------------------------------

template < typename ind_t >
void
rbr3p::v1::phase0( Triplets& B, const double& sizeKB,
                   ind_t* B_bucketRowPfxSum, ui32& noOfBuckets )
{
	// calculate total size of B
	const double sizeB = sizeKB * 1024;
	double B_totalSizeKB = B.length() *
		( sizeof( ind_t ) + sizeof( fp_t ) ) +
		( B.rows() + 1 ) * ( sizeof( nnz_t ) );

	noOfBuckets = ( B_totalSizeKB + sizeB - 1 ) / sizeB;
	#pragma omp parallel
	#pragma omp single
	{
		RowSlicerGreedy rsg( noOfBuckets );
		std::vector< Triplets > vTrps = rsg.partition( B );
		
		B_bucketRowPfxSum[ 0 ] = 0;
		for ( std::size_t i = 0; i < vTrps.size(); ++i )
			B_bucketRowPfxSum[ i + 1 ] = vTrps[ i ].rowEnd();
	}
}

template < typename ind_t, typename fp_t, typename nnz_t >
void
rbr3p::v1::phase1(
	const Csr< ind_t, fp_t, nnz_t >& A, const Csr< ind_t, fp_t, nnz_t >& B,
	ind_t const* const B_bucketRowPfxSum, ui32 const& noOfBuckets,
	Thrd< ind_t, fp_t, nnz_t >** thrds, const ui32& numThreads,
	ui64* B_bucketLengths,
	ind_t** allB_bucketsColInd, fp_t** allB_bucketsNzs )
{
	const ui32 MIN_BUCKET_COUNT_P2 = noOfBuckets / numThreads;
	const ui32 LEFTOVER_BUCKETS_P2 = noOfBuckets % numThreads;

	// ui64* B_bucketLengths = util::newVector< ui64 >( noOfBuckets );
	// ind_t** allB_bucketsColInd = util::alloc< ind_t* > ( noOfBuckets );
	// fp_t** allB_bucketsNzs = util::alloc< fp_t* >( noOfBuckets );

	for ( ui32 b = 0; b < noOfBuckets; ++b )
		B_bucketLengths[ b ] = 0;
	#pragma omp parallel
	{
		// Calculate p2 bucket count
		ui32 _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];
		t->p2BucketCount = MIN_BUCKET_COUNT_P2;
		if ( _id < LEFTOVER_BUCKETS_P2 )
			++t->p2BucketCount;

		const nnz_t A_NZ_START = t->A_nzStart;
		const nnz_t A_NZ_END = t->A_nzEnd;

		// t->p1BucketCount = noOfBuckets;
		// t->p1A_bucketCounters = util::alloc< nnz_t >( noOfBuckets );
		// t->p1A_bucketStarts = util::newVector< nnz_t >( noOfBuckets );
		// t->p1A_bucketNnzs = util::newVector< nnz_t >( noOfBuckets );

		// t->p3A_ptrToBNzs = util::alloc< fp_t* >( A_NZ_END - A_NZ_START );
		// t->p3A_ptrToBColInd = util::alloc< ind_t* >( A_NZ_END - A_NZ_START );
		// t->p3A_BLengths = util::alloc< ind_t >( A_NZ_END - A_NZ_START );
		// t->p3B_lengthPerBucket = util::newVector< ui64 >( noOfBuckets );
		// t->p3B_startIndexPerBucket = util::alloc< ui64 >( noOfBuckets );

		for ( ui32 b = 0; b < noOfBuckets; ++b )
		{
			t->p1A_bucketStarts[ b ] = 0;
			t->p1A_bucketNnzs[ b ] = 0;
			t->p3B_lengthPerBucket[ b ] = 0;
		}

		// ---------------------------------------------------------------------
		ui64 _overallBBucketLengthPhase3 = 0;
		for ( nnz_t nA = A_NZ_START; nA < A_NZ_END; ++nA )
		{
			ind_t jA = A.colInd[ nA ];

			// calculate how much A entries a thread will write to each bucket
			ui32 bucketId = util::lower_bound(
				jA, B_bucketRowPfxSum, noOfBuckets );
			++t->p1A_bucketNnzs[ bucketId ];

			// calculate how much B entries a thread will read from each bucket
			// (in phase 3)
			t->p3B_lengthPerBucket[ bucketId ] +=
				B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
			_overallBBucketLengthPhase3 += B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
		}

		// ---------------------------------------------------------------------
		#pragma omp barrier
		#pragma omp single
		{
			// Calculate from where each thread will start
			// writing into A buckets
			thrds[ 0 ]->p2BucketIdStart = 0;
			for ( ui32 b = 0; b < noOfBuckets; ++b )
				thrds[ 0 ]->p1A_bucketStarts[ b ] = 0;

			for ( ui32 t = 1; t < numThreads; ++t )
			{
				for ( ui32 b = 0; b < noOfBuckets; ++b )
				{
					thrds[ t ]->p1A_bucketStarts[ b ] =
						thrds[ t - 1 ]->p1A_bucketStarts[ b ] +
						thrds[ t - 1 ]->p1A_bucketNnzs[ b ];
				}

				thrds[ t ]->p2BucketIdStart =
					thrds[ t - 1 ]->p2BucketIdStart +
					thrds[ t - 1 ]->p2BucketCount;
			}

			// calculate from where each thread will start reading from
			// (in phase 3) for each B bucket
			// TODO make a single preprocessing step?
			for ( ui32 b = 0; b < noOfBuckets; ++b )
				thrds[ 0 ]->p3B_startIndexPerBucket[ b ] = 0;

			for ( ui32 t = 1; t < numThreads; ++t )
			{
				for ( ui32 b = 0; b < noOfBuckets; ++b )
				{
					thrds[ t ]->p3B_startIndexPerBucket[ b ] =
						thrds[ t - 1 ]->p3B_startIndexPerBucket[ b ] +
						thrds[ t - 1 ]->p3B_lengthPerBucket[ b ];
				}
			}
		}

		// Allocate A buckets as a single array for phase 2
		// ---------------------------------------------------------------------
		// t->p2A_bucketNnzs = util::alloc< nnz_t >( t->p2BucketCount );
		// t->p2A_buckets = util::alloc< ind_t* >( t->p2BucketCount );
		Thrd< ind_t, fp_t, nnz_t >* tLast = thrds[ numThreads - 1 ];
		nnz_t totalLen = 0;
		for ( ui32 b = 0; b < t->p2BucketCount; ++b )
		{
			t->p2A_bucketNnzs[ b ] =
				tLast->p1A_bucketStarts[ b + t->p2BucketIdStart ] +
				tLast->p1A_bucketNnzs[ b + t->p2BucketIdStart ];
			totalLen += t->p2A_bucketNnzs[ b ];
		}

		// t->p2A_buckets[ 0 ] = util::alloc< ind_t >( totalLen );
		nnz_t lenSoFar = 0;
		for ( ui32 b = 0; b < t->p2BucketCount - 1; ++b )
		{
			lenSoFar += 
				tLast->p1A_bucketStarts[ b + t->p2BucketIdStart ] +
				tLast->p1A_bucketNnzs[ b + t->p2BucketIdStart ];

			t->p2A_buckets[ b + 1 ] = t->p2A_buckets[ 0 ] + lenSoFar;
		}

		// Calculate B bucket length per thread
		ui64 _totalBBucketLength = 0;
		for ( ui32 b = 0; b < t->p2BucketCount; ++b )
		{
			ui32 bucketId = b + t->p2BucketIdStart;
			ui64 bucketLength = 0;
			for ( ui32 tid = 0; tid < numThreads; ++tid )
				bucketLength += thrds[ tid ]->p3B_lengthPerBucket[ bucketId ];

			B_bucketLengths[ bucketId ] = bucketLength;
			_totalBBucketLength += bucketLength;
		}

		// allocate p2 B buckets

		// ---------------------------------------------------------------------
		#pragma omp barrier
		// ---------------------------------------------------------------------

		// Allocate & Set A bucket pointers for phase 1
		// t->p1A_buckets = util::alloc< ind_t* >( t->p1BucketCount );
		ui32 k = 0;
		for ( ui32 tid = 0; tid < numThreads; ++tid )
		{
			for ( ui32 b = 0; b < thrds[ tid ]->p2BucketCount; ++b, ++k )
			{
				t->p1A_buckets[ k ] = thrds[ tid ]->p2A_buckets[ b ];
			}
		}

		// ---------------------------------------------------------------------
		#pragma omp barrier
		// ---------------------------------------------------------------------

		// allocate handles per B bucket (for current thread to be used in p2)
		// ---------------------------------------------------------------------
		// t->p2B_bucketsColInd = util::alloc< ind_t* >( t->p2BucketCount );
		// t->p2B_bucketsNzs = util::alloc< fp_t* >( t->p2BucketCount );
		// t->p2B_bucketsNnz = util::alloc< ui64 >( t->p2BucketCount );

		// allocate B buckets continuously (with respect to p2) for each thrd
		// ---------------------------------------------------------------------
		// t->p2B_bucketsColInd[ 0 ] =
		// 	util::alloc< ind_t >( _totalBBucketLength );
		// t->p2B_bucketsNzs[ 0 ] =
		// 	util::alloc< fp_t >( _totalBBucketLength );

		ui64 lengthSoFar = 0;
		for ( ui32 b = 0; b < t->p2BucketCount; ++b )
		{
			t->p2B_bucketsColInd[ b ] =
				t->p2B_bucketsColInd[ 0 ] + lengthSoFar;
			t->p2B_bucketsNzs[ b ] = t->p2B_bucketsNzs[ 0 ] + lengthSoFar;
			t->p2B_bucketsNnz[ b ] = B_bucketLengths[ t->p2BucketIdStart + b ];
			lengthSoFar += t->p2B_bucketsNnz[ b ];

			// set up global B bucket pointers
			ui32 bucketId = t->p2BucketIdStart + b;
			allB_bucketsColInd[ bucketId ] = t->p2B_bucketsColInd[ b ];
			allB_bucketsNzs[ bucketId ] = t->p2B_bucketsNzs[ b ];
		}

		// ---------------------------------------------------------------------
		#pragma omp barrier
		// ---------------------------------------------------------------------

		for ( ui32 b = 0; b < noOfBuckets; ++b )
			t->p1A_bucketCounters[ b ] = t->p1A_bucketStarts[ b ];

		// Here lengthPerBucket array is overwritten (will not be used again)
		// and used as a counter
		for ( ui32 b = 0; b < noOfBuckets; ++b )
			t->p3B_lengthPerBucket[ b ] = t->p3B_startIndexPerBucket[ b ];

		// Fill in A buckets & original A matrix pointers
		for ( nnz_t nA = A_NZ_START; nA < A_NZ_END; ++nA )
		{
			ind_t jA = A.colInd[ nA ];
			ui32 bucketId = util::lower_bound(
				jA, B_bucketRowPfxSum, noOfBuckets );

			ind_t* A_bucket = t->p1A_buckets[ bucketId ];
			nnz_t& counter = t->p1A_bucketCounters[ bucketId ];
			A_bucket[ counter ] = jA;
			++counter;

			nnz_t n = nA - A_NZ_START;

			t->p3A_ptrToBNzs[ n ] = allB_bucketsNzs[ bucketId ] +
				t->p3B_lengthPerBucket[ bucketId ];
			t->p3A_ptrToBColInd[ n ] = allB_bucketsColInd[ bucketId ] +
				t->p3B_lengthPerBucket[ bucketId ];
			t->p3A_BLengths[ n ] = B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
			t->p3B_lengthPerBucket[ bucketId ] +=
				B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t >
void
rbr3p::v1::phase2( const Csr< ind_t, fp_t, nnz_t >& B,
                   Thrd< ind_t, fp_t, nnz_t >** thrds, const ui32& numThreads )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		// B buckets are allocated in phase 1, we need to just fill them
		// ---------------------------------------------------------------------
		for ( ui32 b = 0; b < t->p2BucketCount; ++b )
		{
			ind_t* B_colInd = t->p2B_bucketsColInd[ b ];
			fp_t* B_nzs = t->p2B_bucketsNzs[ b ];
			ind_t const* const A_colInds = t->p2A_buckets[ b ];
			const nnz_t A_LEN = t->p2A_bucketNnzs[ b ];
			ui64 k = 0;
			for ( nnz_t nA = 0; nA < A_LEN; ++nA )
			{
				const ind_t jA = A_colInds[ nA ];
				for ( nnz_t nB = B.rowPtr[ jA ];
				      nB < B.rowPtr[ jA + 1 ];
				      ++nB, ++k )
				{
					B_colInd[ k ] = B.colInd[ nB ];
					B_nzs[ k ] = B.nzs[ nB ];
				}
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t >
void
rbr3p::v1::phase3(
	const Csr< ind_t, fp_t, nnz_t >& A, const Csr< ind_t, fp_t, nnz_t >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd,
	Thrd< ind_t, fp_t, nnz_t >** thrds, const ui32& numThreads )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];
		fp_t* acc = accumulatorPerThrd[ _id ];

		const ind_t A_ROW_START = t->A_rowStart;
		const ind_t A_ROW_END = t->A_rowEnd;
		const nnz_t A_NZ_OFFSET = t->A_nzStart;
		nnz_t const* const A_rowPtr = A.rowPtr;
		fp_t const* const* const ptrToBNzs = t->p3A_ptrToBNzs;
		ind_t const* const* const ptrToBColInd = t->p3A_ptrToBColInd;
		ind_t const* const BLengths = t->p3A_BLengths;
		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
		{
			const ui32 mask = masks[ maskIndexPerRow[ r ] ];
			for ( nnz_t nA = A_rowPtr[ r ]; nA < A_rowPtr[ r + 1 ]; ++nA )
			{
				const nnz_t n = nA - A_NZ_OFFSET;
				const fp_t fA = A.nzs[ nA ];

				ind_t const* const toBColInd = ptrToBColInd[ n ];
				fp_t const* const toBNzs = ptrToBNzs[ n ];
				for ( ind_t i = 0; i < BLengths[ n ]; ++i )
				{
					const ind_t hashIndex = toBColInd[ i ] & mask;
					acc[ hashIndex ] += toBNzs[ i ] * fA;
				}
			}

			for ( nnz_t nC = C.rowPtr[ r ]; nC < C.rowPtr[ r + 1 ]; ++nC  )
			{
				const ind_t hashIndex = C.colInd[ nC ] & mask;
				C.nzs[ nC ] = acc[ hashIndex ];
				acc[ hashIndex ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t >
void
rbr3p::v1::phase3_avx(
	const Csr< ind_t, fp_t, nnz_t >& A, const Csr< ind_t, fp_t, nnz_t >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd,
	Thrd< ind_t, fp_t, nnz_t >** thrds, const ui32& numThreads )
{	
	std::cout << "no implementation of rbr3p::v1::phase3_avx"
	          << " for given types"
	          << std::endl;
}

#ifdef WITH_AVX
template < >
void
rbr3p::v1::phase3_avx< int, double, int >(
	const Csr< int, double, int >& A, const Csr< int, double, int >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	double** accumulatorPerThrd,
	Thrd< int, double, int >** thrds, const ui32& numThreads )
{
	using ind_t = int;
	using fp_t = double;
	using nnz_t = int;

	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];
		fp_t* acc = accumulatorPerThrd[ _id ];

		const ind_t A_ROW_START = t->A_rowStart;
		const ind_t A_ROW_END = t->A_rowEnd;
		const nnz_t A_NZ_OFFSET = t->A_nzStart;
		nnz_t const* const A_rowPtr = A.rowPtr;
		fp_t const* const* const ptrToBNzs = t->p3A_ptrToBNzs;
		ind_t const* const* const ptrToBColInd = t->p3A_ptrToBColInd;
		ind_t const* const BLengths = t->p3A_BLengths;
		const ui32 FP_CACHE_CAPACITY = t->fpCacheCapacity;
		ui32 fpCacheSize = 0;
		fp_t* fpCache = t->fpCache;
		fp_t* C_nzsPtr = C.nzs + C.rowPtr[ A_ROW_START ];
		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
		{
			const ui32 mask = masks[ maskIndexPerRow[ r ] ];
			for ( nnz_t nA = A_rowPtr[ r ]; nA < A_rowPtr[ r + 1 ]; ++nA )
			{
				const nnz_t n = nA - A_NZ_OFFSET;
				const fp_t fA = A.nzs[ nA ];

				ind_t const* const toBColInd = ptrToBColInd[ n ];
				fp_t const* const toBNzs = ptrToBNzs[ n ];
				for ( ind_t i = 0; i < BLengths[ n ]; ++i )
				{
					const ind_t hashIndex = toBColInd[ i ] & mask;
					acc[ hashIndex ] += toBNzs[ i ] * fA;
				}
			}

			for ( nnz_t nC = C.rowPtr[ r ];
			      nC < C.rowPtr[ r + 1 ];
			      ++nC, ++fpCacheSize )
			{
				if ( fpCacheSize >= FP_CACHE_CAPACITY )
				{
					for ( ui32 s = 0; s < fpCacheSize; s += 4 )
					{
						fp_t* from = fpCache + s;
						fp_t* to = C_nzsPtr + s;
						_mm256_stream_pd( to, _mm256_load_pd( from ) );
					}

					C_nzsPtr = C_nzsPtr + fpCacheSize;
					fpCacheSize = 0;
				}

				const ind_t hashIndex = C.colInd[ nC ] & mask;
				fpCache[ fpCacheSize ] = acc[ hashIndex ];
				acc[ hashIndex ] = 0;
			}
		}

		// leftover stream loop
		for ( ui32 i = 0; i < fpCacheSize; ++i )
			C_nzsPtr[ i ] = fpCache[ i ];
	}
}
#endif

template < typename ind_t, typename fp_t, typename nnz_t >
void
rbr3p::v1::phase3_avx512(
	const Csr< ind_t, fp_t, nnz_t >& A, const Csr< ind_t, fp_t, nnz_t >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd,
	Thrd< ind_t, fp_t, nnz_t >** thrds, const ui32& numThreads )
{	
	std::cout << "no implementation of rbr3p::v1::phase3_avx512"
	          << " for given types"
	          << std::endl;
}

#ifdef WITH_AVX512
template < >
void
rbr3p::v1::phase3_avx512< int, double, int >(
	const Csr< int, double, int >& A, const Csr< int, double, int >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	double** accumulatorPerThrd,
	Thrd< int, double, int >** thrds, const ui32& numThreads )
{
	using ind_t = int;
	using fp_t = double;
	using nnz_t = int;

	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];
		fp_t* acc = accumulatorPerThrd[ _id ];

		const ind_t A_ROW_START = t->A_rowStart;
		const ind_t A_ROW_END = t->A_rowEnd;
		const nnz_t A_NZ_OFFSET = t->A_nzStart;
		nnz_t const* const A_rowPtr = A.rowPtr;
		fp_t const* const* const ptrToBNzs = t->p3A_ptrToBNzs;
		ind_t const* const* const ptrToBColInd = t->p3A_ptrToBColInd;
		ind_t const* const BLengths = t->p3A_BLengths;
		const ui32 FP_CACHE_CAPACITY = t->fpCacheCapacity;
		ui32 fpCacheSize = 0;
		fp_t* fpCache = t->fpCache;
		fp_t* C_nzsPtr = C.nzs + C.rowPtr[ A_ROW_START ];
		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
		{
			const ui32 mask = masks[ maskIndexPerRow[ r ] ];
			for ( nnz_t nA = A_rowPtr[ r ]; nA < A_rowPtr[ r + 1 ]; ++nA )
			{
				const nnz_t n = nA - A_NZ_OFFSET;
				const fp_t fA = A.nzs[ nA ];

				ind_t const* const toBColInd = ptrToBColInd[ n ];
				fp_t const* const toBNzs = ptrToBNzs[ n ];
				for ( ind_t i = 0; i < BLengths[ n ]; ++i )
				{
					const ind_t hashIndex = toBColInd[ i ] & mask;
					acc[ hashIndex ] += toBNzs[ i ] * fA;
				}
			}

			for ( nnz_t nC = C.rowPtr[ r ];
			      nC < C.rowPtr[ r + 1 ];
			      ++nC, ++fpCacheSize )
			{
				if ( fpCacheSize >= FP_CACHE_CAPACITY )
				{
					for ( ui32 s = 0; s < fpCacheSize; s += 4 )
					{
						fp_t* from = fpCache + s;
						fp_t* to = C_nzsPtr + s;
						_mm512_stream_pd( to, _mm512_load_pd( from ) );
					}

					C_nzsPtr = C_nzsPtr + fpCacheSize;
					fpCacheSize = 0;
				}

				const ind_t hashIndex = C.colInd[ nC ] & mask;
				fpCache[ fpCacheSize ] = acc[ hashIndex ];
				acc[ hashIndex ] = 0;
			}
		}

		// leftover stream loop
		for ( ui32 i = 0; i < fpCacheSize; ++i )
			C_nzsPtr[ i ] = fpCache[ i ];
	}
}
#endif

// Data preparation
// -----------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t >
void
rbr3p::v1::prep( Triplets& A, Triplets& B, Triplets& C, const ui32& numThreads,
                 Csr< ind_t, fp_t, nnz_t >& csrA,
                 Csr< ind_t, fp_t, nnz_t >& csrB,
                 Csr< ind_t, fp_t, nnz_t >& csrC,
                 Thrd< ind_t, fp_t, nnz_t >*** thrds_out,
                 ind_t** thrdRowPfxSum_out )
{
	ind_t* thrdRowPfxSum = nullptr;
	bool* streamOverflowIndicatorPerRow = nullptr;
	std::vector< Triplets > As;
	Thrd< ind_t, fp_t, nnz_t >** thrds = nullptr;
	#pragma omp parallel num_threads( numThreads )
	{
		#pragma omp sections
		{
			#pragma omp section
			{
				csrA.extract( A );
			}

			#pragma omp section
			{
				csrB.extract( B );
			}

			#pragma omp section
			{
				csrC.extract( C );
			}
		}

		// ---------------------------------------------------------------------
        #pragma omp barrier
		// ---------------------------------------------------------------------
		
		#pragma omp single
		{
			thrds = util::alloc< Thrd< ind_t, fp_t, nnz_t >* >( numThreads );

			auto pair = util::partitionNoConflictAligned( A, B, C, numThreads );
			// auto pair = util::partitionNoConflict( A, B, numThreads );
			As = std::get< 0 >( pair );

			thrdRowPfxSum = util::alloc< ind_t >( numThreads + 1 );
			thrdRowPfxSum[ 0 ] = 0;
			for ( std::size_t i = 0; i < As.size(); ++i )
				thrdRowPfxSum[ i + 1 ] = As[ i ].rowEnd();
		}

		// ---------------------------------------------------------------------
		#pragma omp barrier
		// ---------------------------------------------------------------------

		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t >* _thrd =
			util::alloc< Thrd< ind_t, fp_t, nnz_t > >();
		_thrd->init();
		_thrd->set( As[ _id ].rowStart(), As[ _id ].rowEnd(),
		            csrA.rowPtr[ As[ _id ].rowStart() ],
		            csrA.rowPtr[ As[ _id ].rowEnd() ] );
		thrds[ _id ] = _thrd;
	}

	*thrds_out = thrds;
	*thrdRowPfxSum_out = thrdRowPfxSum;
}

template < typename ind_t, typename fp_t, typename nnz_t >
void
rbr3p::v1::allocCache(
	Thrd< ind_t, fp_t, nnz_t >** thrds, const ui32& numThreads,
	const ui32& fpCacheCapacity )
{
	#pragma omp parallel num_threads( numThreads )
	{
		ui32 _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		// TODO: Why segmentation fault here??!?!
		if ( t->fpCacheCapacity > 0 )
			util::free( t->fpCache );

		t->fpCacheCapacity = fpCacheCapacity;
		t->fpCache = util::newVector< fp_t >( fpCacheCapacity );
	}
}

// -----------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t >
rbr3p::v1::Thrd< ind_t, fp_t, nnz_t >::Thrd()
: A_rowStart( 0 ), A_rowEnd( 0 ), A_nzStart( 0 ), A_nzEnd( 0 ),
  B_bucketRowPfxSum( nullptr ),
  p1BucketCount( 0 ),
  p1A_bucketCounters( nullptr ), p1A_bucketStarts( nullptr ),
  p1A_bucketNnzs( nullptr ), p1A_buckets( nullptr ),
  p2BucketCount( 0 ), p2BucketIdStart( 0 ),
  p2A_buckets( nullptr ), p2A_bucketNnzs( nullptr ),
  p3A_ptrToBNzs( nullptr ), p3A_ptrToBColInd( nullptr ),
  p3A_BLengths( nullptr ), fpCache( nullptr ), fpCacheCapacity( 0 )
{
}

template < typename ind_t, typename fp_t, typename nnz_t >
rbr3p::v1::Thrd< ind_t, fp_t, nnz_t >::~Thrd()
{
	free();
}

template < typename ind_t, typename fp_t, typename nnz_t >
void
rbr3p::v1::Thrd< ind_t, fp_t, nnz_t >::init()
{
	A_rowStart = 0;
	A_rowEnd = 0;
	A_nzStart = 0;
	A_nzEnd = 0;

	B_bucketRowPfxSum = nullptr;

	p1BucketCount = 0;
	p1A_bucketCounters = nullptr;
	p1A_bucketStarts = nullptr;
	p1A_bucketNnzs = nullptr;
	p1A_buckets = nullptr;

	p2BucketCount = 0;
	p2BucketIdStart = 0;
	p2A_buckets = nullptr;
	p2A_bucketNnzs = nullptr;
	p2B_bucketsColInd = nullptr;
	p2B_bucketsNzs = nullptr;
	p2B_bucketsNnz = nullptr;

	p3A_ptrToBNzs = nullptr;
	p3A_ptrToBColInd = nullptr;
	p3A_BLengths = nullptr;
	p3B_lengthPerBucket = nullptr;
	p3B_startIndexPerBucket = nullptr;

	fpCacheCapacity = 0;
	fpCache = nullptr;
}

template < typename ind_t, typename fp_t, typename nnz_t >
void
rbr3p::v1::Thrd< ind_t, fp_t, nnz_t >::free()
{
	if ( p1BucketCount > 0 )
	{
		util::free( p1A_bucketCounters );
		util::free( p1A_bucketStarts );
		util::free( p1A_bucketNnzs );
		util::free( p1A_buckets );
		p1BucketCount = 0;
	}

	if ( p2BucketCount > 0 )
	{
		util::free( p2A_buckets[ 0 ] );
		util::free( p2A_buckets );
		util::free( p2A_bucketNnzs );

		util::free( p2B_bucketsColInd[ 0 ] );
		util::free( p2B_bucketsColInd );
		util::free( p2B_bucketsNzs[ 0 ] );
		util::free( p2B_bucketsNzs );
		util::free( p2B_bucketsNnz );
		p2BucketCount = 0;
	}

	if ( p3A_ptrToBNzs != nullptr ) util::free( p3A_ptrToBNzs );
	if ( p3A_ptrToBColInd != nullptr ) util::free( p3A_ptrToBColInd );
	if ( p3A_BLengths != nullptr ) util::free( p3A_BLengths );
	if ( p3B_lengthPerBucket != nullptr ) util::free( p3B_lengthPerBucket );
	if ( p3B_startIndexPerBucket != nullptr )
		util::free( p3B_startIndexPerBucket );

	if ( fpCacheCapacity > 0 )
		util::free( fpCache );
	fpCacheCapacity = 0;
}

template < typename ind_t, typename fp_t, typename nnz_t >
void
rbr3p::v1::Thrd< ind_t, fp_t, nnz_t >::set(
	const ind_t& rowStart, const ind_t& rowEnd,
	const nnz_t& nzStart, const nnz_t& nzEnd )
{
	A_rowStart = rowStart;
	A_rowEnd = rowEnd;
	A_nzStart = nzStart;
	A_nzEnd = nzEnd;

	// std::cout << "A_rowStart=" << A_rowStart
	//           << " A_rowEnd=" << A_rowEnd << std::endl;
	// std::cout << "A_nzStart=" << A_nzStart
	//           << " A_nzEnd=" << A_nzEnd << std::endl;
}

template < typename ind_t, typename fp_t, typename nnz_t >
std::ostream&
rbr3p::v1::Thrd< ind_t, fp_t, nnz_t >::str( std::ostream& out ) const
{
	return out;
}

template < typename ind_t, typename fp_t, typename nnz_t >
std::string
rbr3p::v1::Thrd< ind_t, fp_t, nnz_t >::str() const
{
	std::stringstream ss;
	str( ss );
	return ss.str();
}

// -----------------------------------------------------------------------------
