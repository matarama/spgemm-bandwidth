
#include <vector>
#include <cstdlib>
#include <omp.h>
#include <immintrin.h>
#include <cassert>

#include "include/util/Memory.h"
#include "include/util/Spgemm.h"

// RowByRow3p per thread data
// -----------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem >
rbr3p::v2::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >::Thrd()
: B_prime_capacity( 0 ), B_prime_colInds( nullptr ),
  B_prime_nzs( nullptr ), B_prime_lengths( nullptr ),
  fpCacheCapacity( 64 ), fpCache( nullptr ), indCache( nullptr ),
  lengthCacheCapacity( 0 ), lengthCache( nullptr )
{
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem >
rbr3p::v2::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >::~Thrd()
{
	free();
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem >
void
rbr3p::v2::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >::init()
{
	B_prime_capacity = 0;
	B_prime_colInds = nullptr;
	B_prime_nzs = nullptr;
	B_prime_lengths = nullptr;

	fpCacheCapacity = 0;
	fpCache = nullptr;
	indCache = nullptr;
	lengthCacheCapacity = 0;
	lengthCache = nullptr;
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem >
void
rbr3p::v2::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >::free()
{
	if ( B_prime_capacity > 0 )
	{
		if ( B_prime_colInds != nullptr )
			B_prime_mem::free( static_cast< void* >( B_prime_colInds ),
			                   B_prime_capacity, sizeof( ind_t ) );

		if ( B_prime_nzs != nullptr )
			B_prime_mem::free( static_cast< void* >( B_prime_nzs ),
			                   B_prime_capacity, sizeof( fp_t ) );

		if ( B_prime_lengths != nullptr )
			B_prime_mem::free( static_cast< void* >( B_prime_lengths ),
			                   B_prime_capacity, sizeof( nnz_t ) );
	}

	if ( fpCache != nullptr ) util::free( fpCache );
	if ( indCache != nullptr ) util::free( indCache );
	if ( lengthCache != nullptr ) util::free( lengthCache );

	init();
}

// Data preparation
// -----------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem >
void
rbr3p::v2::alloc(
	const ui32& numThreads,
	const ui32& fpCacheCapacity,
	const ui32& indCacheCapacity,
	const ui32& lengthCacheCapacity,
	rbr3p::v2::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >*** thrds_out )
{
	Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds = nullptr;	
	#pragma omp parallel num_threads( numThreads )
	{
		#pragma omp single
		thrds = util::alloc< Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* >(
			numThreads );

		const int ID = omp_get_thread_num();
		thrds[ ID ] = util::alloc< Thrd< ind_t, fp_t, nnz_t, B_prime_mem > >();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ ID ];
		t->init();

		t->fpCacheCapacity = fpCacheCapacity;
		t->lengthCacheCapacity = lengthCacheCapacity;
		t->fpCache = util::alloc< fp_t >( t->fpCacheCapacity );
		t->indCache = util::alloc< ind_t >( t->fpCacheCapacity );
		t->lengthCache = util::alloc< ind_t >( t->lengthCacheCapacity );
	}

	*thrds_out = thrds;
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem >
void
rbr3p::v2::setCacheCapacity(
	rbr3p::v2::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds,
	const ui32& numThreads,
	const ui32& fpCacheCapacity,
	const ui32& indCacheCapacity,
	const ui32& lengthCacheCapacity )
{
	#pragma omp parallel num_threads( numThreads )
	{
		int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];

		t->fpCacheCapacity = fpCacheCapacity;
		t->lengthCacheCapacity = lengthCacheCapacity;
		// t->indCacheCapacity = indCacheCapacity;
	}
}

// Kernel routines for first run
// -----------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v2::symbolic_alloc(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd, ind_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	rbr3p::v2::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	assert( sizeof( fp_t ) > sizeof( ind_t ) );
	nnz_t perThrdCNnz[ numThreads + 1 ];
	#pragma omp parallel num_threads( numThreads )
	{
		const int _ID = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _ID ];
		const ind_t A_ROW_START = thrdRowPfxSum[ _ID ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _ID + 1 ];
		const nnz_t A_NZ_START = A.rowPtr[ A_ROW_START ];
		const nnz_t A_NZ_END = A.rowPtr[ A_ROW_END ];

		#pragma omp single
		{
			C.rowPtr = static_cast< nnz_t* >(
				C_mem::alloc( A.rowCount + 1, sizeof( nnz_t ) ) );
			C.rowPtr[ 0 ] = 0;
		}

		fp_t* acc = accumulatorPerThrd[ _ID ];
		ind_t* colIndSet = (ind_t*) t->B_prime_nzs;
		char* lookupArr = (char*) acc;
		ind_t* C_indArr = ( (ind_t*) acc ) + B.columnCount;

		// Preprocess partial C matrix
		// ---------------------------------------------------------------------
		ui64 k = 0;
		nnz_t _C_nnz = 0;
		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
		{
			ind_t currRowNnz = 0;
			for ( nnz_t nA = A.rowPtr[ r ]; nA < A.rowPtr[ r + 1 ]; ++nA )
			{
				const ind_t jA = A.colInd[ nA ];
				for ( nnz_t nB = B.rowPtr[ jA ];
				      nB < B.rowPtr[ jA + 1 ];
				      ++nB, ++k )
				{
					const ind_t jB = B.colInd[ nB ];
					const ind_t hashedAddress = jB >> 3;
					const ind_t bitMask = ( 1 << ( jB & 7 ) );

					if ( ( lookupArr[ hashedAddress ] & bitMask ) == 0 )
					{
						lookupArr[ hashedAddress ] |= bitMask;
						C_indArr[ jB ] = currRowNnz;
						t->B_prime_colInds[ k ] = currRowNnz;
						colIndSet[ currRowNnz ] = jB;
						++currRowNnz;
					}
					else
					{
						t->B_prime_colInds[ k ] = C_indArr[ jB ];
					}
				}
			}

			// reset lookup bit vector
			for ( ind_t j = 0; j < currRowNnz; ++j )
			{
				const ind_t jC = colIndSet[ j ];
				const ind_t hashedAddress = jC >> 3;
				lookupArr[ hashedAddress ] = 0;
			}

			_C_nnz += currRowNnz;
			C.rowPtr[ r + 1 ] = _C_nnz;
			colIndSet = colIndSet + currRowNnz;
		}

		C.rowPtr[ A_ROW_END ] = 0;
		perThrdCNnz[ _ID + 1 ] = _C_nnz;

		// allocate C non-zero and col-index arrays
		// ---------------------------------------------------------------------
		#pragma omp barrier
		#pragma omp single
		{
			perThrdCNnz[ 0 ] = 0;
			for ( ui32 t = 0; t < numThreads; ++t )
				perThrdCNnz[ t + 1 ] += perThrdCNnz[ t ];

			C.rowCount = A.rowCount;
			C.nnz = perThrdCNnz[ numThreads ];
			C.rowPtr[ C.rowCount ] = C.nnz;
			C.nzs = static_cast< fp_t* >(
				C_mem::alloc( C.nnz, sizeof( fp_t ) ) );
			C.colInd = static_cast< ind_t* >(
				C_mem::alloc( C.nnz, sizeof( ind_t ) ) );
		}
		// ---------------------------------------------------------------------

		// add offset to C row-ptr
		const nnz_t C_NZ_BEGIN = perThrdCNnz[ _ID ];
		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
			C.rowPtr[ r ] += C_NZ_BEGIN;

		// fill col-index array
		ind_t* _temp = (ind_t*) t->B_prime_nzs;
		const nnz_t C_NZ_END = perThrdCNnz[ _ID + 1 ];
		for ( nnz_t n = C_NZ_BEGIN, k = 0; n < C_NZ_END; ++n, ++k )
			C.colInd[ n ] = _temp[ k ];

		// Generate B'
		// ---------------------------------------------------------------------
		// Fill in B' length-arr, index-arr, nonzero-arr
		// (this is the actual phase2)
		k = 0;
		for ( nnz_t n = A_NZ_START; n < A_NZ_END; ++n )
		{
			const ind_t jA = A.colInd[ n ];
			t->B_prime_lengths[ n - A_NZ_START ] =
				B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
			for ( nnz_t nB = B.rowPtr[ jA ];
			      nB < B.rowPtr[ jA + 1 ];
			      ++nB, ++k )
			{
				// t->B_prime_colInds[ k ] = B.colInd[ nB ];
				t->B_prime_nzs[ k ] = B.nzs[ nB ];
			}
		}

		// Small padding for vectorization etc.
		// Occupying at most the size of a cache line (generally 64 bytes)
		for ( ; k < t->B_prime_capacity; ++k )
		{
			t->B_prime_colInds[ k ] = t->B_prime_colInds[ k - 1 ];
			t->B_prime_nzs[ k ] = 0;
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v2::symbolic(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd, ind_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	rbr3p::v2::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	assert( sizeof( fp_t ) > sizeof( ind_t ) );
	C.rowPtr[ 0 ] = 0;
	nnz_t perThrdCNnz[ numThreads + 1 ];
	#pragma omp parallel num_threads( numThreads )
	{
		const int _ID = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _ID ];
		const ind_t A_ROW_START = thrdRowPfxSum[ _ID ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _ID + 1 ];
		const nnz_t A_NZ_START = A.rowPtr[ A_ROW_START ];
		const nnz_t A_NZ_END = A.rowPtr[ A_ROW_END ];

		fp_t* acc = accumulatorPerThrd[ _ID ];
		ind_t* colIndSet = (ind_t*) t->B_prime_nzs;
		ind_t* C_lookup = ( (ind_t*) acc ) + B.columnCount;

		// Preprocess partial C matrix
		// ---------------------------------------------------------------------
		ui64 k = 0;
		nnz_t _C_nnz = 0;
		const ind_t BIT_MASK = ( 1 << 31 );
		const ind_t REVERSE_BIT_MASK = ~BIT_MASK;
		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
		{
			ind_t currRowNnz = 0;
			for ( nnz_t nA = A.rowPtr[ r ]; nA < A.rowPtr[ r + 1 ]; ++nA )
			{
				const ind_t jA = A.colInd[ nA ];
				for ( nnz_t nB = B.rowPtr[ jA ];
				      nB < B.rowPtr[ jA + 1 ];
				      ++nB, ++k )
				{
					const ind_t jB = B.colInd[ nB ];
					if ( ( C_lookup[ jB ] ) == 0 )
					{
						C_lookup[ jB ] = currRowNnz | BIT_MASK; 
						colIndSet[ currRowNnz ] = jB;
						++currRowNnz;
					}

					t->B_prime_colInds[ k ] = C_lookup[ jB ] & REVERSE_BIT_MASK;
				}
			}

			// reset lookup bit vector
			for ( ind_t j = 0; j < currRowNnz; ++j )
			{
				const ind_t jC = colIndSet[ j ];
				C_lookup[ jC ] = 0;
			}

			_C_nnz += currRowNnz;
			C.rowPtr[ r + 1 ] = _C_nnz;
			colIndSet = colIndSet + currRowNnz;
		}

		C.rowPtr[ A_ROW_END ] = 0;
		perThrdCNnz[ _ID + 1 ] = _C_nnz;

		// allocate C non-zero and col-index arrays
		// ---------------------------------------------------------------------
		#pragma omp barrier
		#pragma omp single
		{
			perThrdCNnz[ 0 ] = 0;
			for ( ui32 t = 0; t < numThreads; ++t )
				perThrdCNnz[ t + 1 ] += perThrdCNnz[ t ];

			C.rowCount = A.rowCount;
			C.nnz = perThrdCNnz[ numThreads ];
			C.rowPtr[ C.rowCount ] = C.nnz;
		}
		// ---------------------------------------------------------------------

		// add offset to C row-ptr
		const nnz_t C_NZ_BEGIN = perThrdCNnz[ _ID ];
		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
			C.rowPtr[ r ] += C_NZ_BEGIN;

		// fill col-index array
		ind_t* _temp = (ind_t*) t->B_prime_nzs;
		const nnz_t C_NZ_END = perThrdCNnz[ _ID + 1 ];
		for ( nnz_t n = C_NZ_BEGIN, k = 0; n < C_NZ_END; ++n, ++k )
			C.colInd[ n ] = _temp[ k ];

		// Generate B'
		// ---------------------------------------------------------------------
		// Fill in B' length-arr, index-arr, nonzero-arr
		// (this is the actual phase2)
		k = 0;
		for ( nnz_t n = A_NZ_START; n < A_NZ_END; ++n )
		{
			const ind_t jA = A.colInd[ n ];
			t->B_prime_lengths[ n - A_NZ_START ] =
				B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
			for ( nnz_t nB = B.rowPtr[ jA ];
			      nB < B.rowPtr[ jA + 1 ];
			      ++nB, ++k )
			{
				// t->B_prime_colInds[ k ] = B.colInd[ nB ];
				t->B_prime_nzs[ k ] = B.nzs[ nB ];
			}
		}

		// Small padding for vectorization etc.
		// Occupying at most the size of a cache line (generally 64 bytes)
		for ( ; k < t->B_prime_capacity; ++k )
		{
			t->B_prime_colInds[ k ] = t->B_prime_colInds[ k - 1 ];
			t->B_prime_nzs[ k ] = 0;
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v2::symbolic_preprocessC(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd, ind_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	rbr3p::v2::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	C.rowPtr[ 0 ] = 0;
	nnz_t perThrdCNnz[ numThreads + 1 ];
	#pragma omp parallel num_threads( numThreads )
	{
		const int _ID = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _ID ];
		const ind_t A_ROW_START = thrdRowPfxSum[ _ID ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _ID + 1 ];
		const nnz_t A_NZ_START = A.rowPtr[ A_ROW_START ];
		const nnz_t A_NZ_END = A.rowPtr[ A_ROW_END ];

		fp_t* acc = accumulatorPerThrd[ _ID ];
		ind_t* colIndSet = (ind_t*) t->B_prime_nzs;
		char* lookupArr = (char*) acc;

		// Preprocess partial C matrix
		// ---------------------------------------------------------------------
		ui64 k = 0;
		nnz_t _C_nnz = 0;
		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
		{
			ind_t currRowNnz = 0;
			for ( nnz_t nA = A.rowPtr[ r ]; nA < A.rowPtr[ r + 1 ]; ++nA )
			{
				const ind_t jA = A.colInd[ nA ];
				for ( nnz_t nB = B.rowPtr[ jA ];
				      nB < B.rowPtr[ jA + 1 ];
				      ++nB, ++k )
				{
					const ind_t jB = B.colInd[ nB ];
					const ind_t hashedAddress = jB >> 3;
					const ind_t bitMask = ( 1 << ( jB & 7 ) );

					if ( ( lookupArr[ hashedAddress ] & bitMask ) == 0 )
					{
						lookupArr[ hashedAddress ] |= bitMask;
						colIndSet[ currRowNnz ] = jB;
						++currRowNnz;
					}
				}
			}

			// reset lookup bit vector
			for ( ind_t j = 0; j < currRowNnz; ++j )
			{
				const ind_t jC = colIndSet[ j ];
				const ind_t hashedAddress = jC >> 3;
				lookupArr[ hashedAddress ] = 0;
			}

			_C_nnz += currRowNnz;
			C.rowPtr[ r + 1 ] = _C_nnz;
			colIndSet = colIndSet + currRowNnz;
		}

		C.rowPtr[ A_ROW_END ] = 0;
		perThrdCNnz[ _ID + 1 ] = _C_nnz;

		// allocate C non-zero and col-index arrays
		// ---------------------------------------------------------------------
		#pragma omp barrier
		#pragma omp single
		{
			perThrdCNnz[ 0 ] = 0;
			for ( ui32 t = 0; t < numThreads; ++t )
				perThrdCNnz[ t + 1 ] += perThrdCNnz[ t ];

			C.rowCount = A.rowCount;
			C.nnz = perThrdCNnz[ numThreads ];
			C.rowPtr[ C.rowCount ] = C.nnz;
		}
		// ---------------------------------------------------------------------

		// add offset to C row-ptr
		const nnz_t C_NZ_BEGIN = perThrdCNnz[ _ID ];
		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
			C.rowPtr[ r ] += C_NZ_BEGIN;

		// fill col-index array
		ind_t* _temp = (ind_t*) t->B_prime_nzs;
		const nnz_t C_NZ_END = perThrdCNnz[ _ID + 1 ];
		for ( nnz_t n = C_NZ_BEGIN, k = 0; n < C_NZ_END; ++n, ++k )
			C.colInd[ n ] = _temp[ k ];
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem,
           typename B_prime_mem >
void
rbr3p::v2::phase1(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	ind_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	rbr3p::v2::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];
		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		const nnz_t A_NZ_START = A.rowPtr[ A_ROW_START ];
		const nnz_t A_NZ_END = A.rowPtr[ A_ROW_END ];

		ui64 B_prime_capacity = 0;
		for ( nnz_t n = A_NZ_START; n < A_NZ_END; ++n )
		{
			const ind_t c = A.colInd[ n ];
			B_prime_capacity += B.rowPtr[ c + 1 ] - B.rowPtr[ c ];
		}

		t->B_prime_capacity = B_prime_capacity;

		// Padding for vectorization issues
		t->B_prime_capacity += 16 - B_prime_capacity % 16;

		// Allocate B' matrix
		t->B_prime_colInds = static_cast< ind_t* >(
			B_prime_mem::alloc( t->B_prime_capacity, sizeof( ind_t ) ) );
		t->B_prime_nzs = static_cast< fp_t* >(
			B_prime_mem::alloc( t->B_prime_capacity, sizeof( fp_t ) ) );
		t->B_prime_lengths = static_cast< nnz_t* >(
			B_prime_mem::alloc( A_NZ_END - A_NZ_START, sizeof( nnz_t ) ) );
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v2::numeric(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	nnz_t const* const thrdRowPfxSum, const ui32& numThreads,
	rbr3p::v2::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
	
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];

		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		nnz_t const* const __restrict__ A_rowPtr = A.rowPtr + A_ROW_START;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		ind_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr + A_ROW_START;
		ind_t* const __restrict__ C_colInds = C.colInd;

		const ind_t ROWS = A_ROW_END - A_ROW_START;
		const nnz_t A_NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			fp_t* __restrict__ C_nzs = C.nzs + C_rowPtr[ i ];
			const ind_t C_CURR_ROW_LEN = C_rowPtr[ i + 1 ] - C_rowPtr[ i ];
			for ( k = 0; k < C_CURR_ROW_LEN; ++k )
				C_nzs[ k ] = 0.0;

			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const nnz_t K = k - A_NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					C_nzs[ B_colInds[ m ] ] += fA * B_nzs[ m ];
				}
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v2::numeric(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	nnz_t const* const thrdRowPfxSum,
	nnz_t const* const thrdRowPfxSumAligned, const ui32& numThreads,
	rbr3p::v2::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )	
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];

		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		nnz_t const* const __restrict__ A_rowPtr = A.rowPtr + A_ROW_START;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		ind_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr + A_ROW_START;
		ind_t* const __restrict__ C_colInds = C.colInd;

		const ind_t ROWS = A_ROW_END - A_ROW_START;
		const nnz_t A_NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			fp_t* __restrict__ C_nzs = C.nzs + C_rowPtr[ i ];
			const ind_t C_CURR_ROW_LEN = C_rowPtr[ i + 1 ] - C_rowPtr[ i ];
			for ( k = 0; k < C_CURR_ROW_LEN; ++k )
				C_nzs[ k ] = 0.0;

			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const nnz_t K = k - A_NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					C_nzs[ B_colInds[ m ] ] += fA * B_nzs[ m ];
				}
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v2::numeric_simd(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	nnz_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	rbr3p::v2::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];

		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		nnz_t const* const __restrict__ A_rowPtr = A.rowPtr + A_ROW_START;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		ind_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr + A_ROW_START;
		ind_t* const __restrict__ C_colInds = C.colInd;

		const ind_t ROWS = A_ROW_END - A_ROW_START;
		const nnz_t A_NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			fp_t* __restrict__ C_nzs = C.nzs + C_rowPtr[ i ];
			const ind_t C_CURR_ROW_LEN = C_rowPtr[ i + 1 ] - C_rowPtr[ i ];
			#pragma simd
			for ( k = 0; k < C_CURR_ROW_LEN; ++k )
				C_nzs[ k ] = 0.0;

			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const nnz_t K = k - A_NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					C_nzs[ B_colInds[ m ] ] += fA * B_nzs[ m ];
				}
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v2::numeric_gustavson(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	nnz_t const* const thrdRowPfxSum, const ui32& numThreads,
	rbr3p::v2::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
	
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];

		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		nnz_t const* const __restrict__ A_rowPtr = A.rowPtr + A_ROW_START;
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		ind_t const* const __restrict__ B_rowPtr = B.rowPtr;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = B.nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr + A_ROW_START;
		ind_t* const __restrict__ C_colInds = C.colInd;

		const ind_t ROWS = A_ROW_END - A_ROW_START;
		const nnz_t A_NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			fp_t* __restrict__ C_nzs = C.nzs + C_rowPtr[ i ];
			const ind_t C_CURR_ROW_LEN = C_rowPtr[ i + 1 ] - C_rowPtr[ i ];
			for ( k = 0; k < C_CURR_ROW_LEN; ++k )
				C_nzs[ k ] = 0.0;

			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const ind_t jA = A_colInds[ k ];

				for ( l = B_rowPtr[ jA ]; l < B_rowPtr[ jA + 1 ]; ++l, ++m )
				{
					C_nzs[ B_colInds[ m ] ] += fA * B_nzs[ l ];
				}
			}
		}
	}
}
