
#include "include/parallel/RowByRow3p_v1_incorrect.h"

void
rbr3p::v1::CAB_phase1( const Csr< glbInd_t, fp_t, glbNnz_t >& A,
                       glbInd_t const* const B_L2_rowPtr,
                       Thrd** thrds, const ui32& numThreads )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd* t = thrds[ _id ];

		// Fill in A_prime matrices
		for ( glbInd_t r = t->A_rowStart; r < t->A_rowEnd; ++r )
		{
			for ( glbNnz_t n = A.rowPtr[ r ]; n < A.rowPtr[ r + 1 ]; ++n )
			{
				glbInd_t jA = A.colInd[ n ];
				fp_t fA = A.nzs[ n ];
				ui32 i = util::lower_bound( jA, B_L2_rowPtr, t->totalABCount );

				glbNnz_t writeOffset = t->A_primeStarts[ i ];
				glbNnz_t& writeIndex = t->A_primeCounters[ i ];
				t->allA_primes[ i ]->nzs[ writeOffset + writeIndex ] = fA;

				++writeIndex;
			}
		}

		for ( ui32 i = 0; i < t->totalABCount; ++i )
			t->A_primeCounters[ i ] = 0;		
	}
}

void
rbr3p::v1::CAB_phase2(
	const Csr< glbInd_t, fp_t, glbNnz_t >& B,
	Thrd** thrds, const ui32& numThreads )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd* t = thrds[ _id ];

		for ( ui32 i = 0; i < t->myABCount; ++i )
		{
			Coo< glbInd_t, fp_t, glbNnz_t >& A_prime = t->myA_primes[ i ];
			Csr< glbInd_t, fp_t, ui64 >& B_prime = t->myB_primes[ i ];
		
			// Fill B'
			ui64 k = 0;
			for ( glbInd_t nA = 0; nA < A_prime._nnz; ++nA )
			{
				const glbInd_t iA = A_prime.is[ nA ];
				const glbInd_t jA = A_prime.js[ nA ];

				const glbNnz_t B_NZ_START = B.rowPtr[ jA ];
				const glbNnz_t B_NZ_END = B.rowPtr[ jA + 1 ];

				for ( glbNnz_t nB = B_NZ_START; nB < B_NZ_END; ++nB, ++k )
					B_prime.nzs[ k ] = B.nzs[ nB ];
			}
		}
		
	}
}

void
rbr3p::v1::CAB_phase3(
	const Csr< glbInd_t, fp_t, glbNnz_t >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, Thrd** thrds, const ui32& numThreads )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd* t = thrds[ _id ];
		fp_t* acc = accumulatorPerThrd[ _id ];
		const ui32 TOTAL_AB_COUNT = t->totalABCount;
		const glbInd_t A_ROW_START = t->A_rowStart;
		const glbInd_t A_ROW_END = t->A_rowEnd;
        
		for ( ui32 ab = 0; ab < TOTAL_AB_COUNT; ++ab )
			t->A_primeCounters[ ab ] = t->A_primeStarts[ ab ];

		for ( glbInd_t r = A_ROW_START; r < A_ROW_END; ++r )
		{
			const ui32 mask = masks[ maskIndexPerRow[ r ] ];
			for ( ui32 ab = 0; ab < TOTAL_AB_COUNT; ++ab )
			{
				const Coo< glbInd_t, fp_t, glbNnz_t >& Ap =
					*t->allA_primes[ ab ];
				const Csr< glbInd_t, fp_t, ui64 >& Bp =
					*t->allB_primes[ ab ];

				glbNnz_t& currAIndex = t->A_primeCounters[ ab ];
				while ( currAIndex < Ap.nnz() && Ap.is[ currAIndex ] == r )
				{
					fp_t fA = Ap.nzs[ currAIndex ];
					for ( ui64 nB = Bp.rowPtr[ currAIndex ];
					      nB < Bp.rowPtr[ currAIndex + 1 ];
					      ++nB )
					{
						const glbInd_t hashIndex = Bp.colInd[ nB ] & mask;
						acc[ hashIndex ] += fA * Bp.nzs[ nB ];
					}

					++currAIndex;
				}
			}

			// TODO use streaming stores into C
			for ( glbNnz_t nC = C.rowPtr[ r ]; nC < C.rowPtr[ r + 1 ]; ++nC  )
			{
				const glbInd_t hashIndex = C.colInd[ nC ] & mask;
				C.nzs[ nC ] = acc[ hashIndex ];
				acc[ hashIndex ] = 0;
			}
		}
	}
}

