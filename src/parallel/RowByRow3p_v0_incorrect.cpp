
#include <vector>
#include <cstdlib>
#include <omp.h>
#include <immintrin.h>
#include <cassert>

#include "include/util/Memory.h"
#include "include/util/Spgemm.h"

#define CACHE_WINDOW 63

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p_incorrect::v0::numeric_A_read(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		rbr3p::v0::Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = &A.rowPtr[ t->A_rowStart ];
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		nnz_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + t->A_rowStart;
		const ind_t ROWS = t->A_rowEnd - t->A_rowStart;
		const glbNnz_t NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k & CACHE_WINDOW ];
				const glbNnz_t K = k - NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ B_colInds[ m ] & mask ] += fA * B_nzs[ m ];
				}
			}
            
			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k ] & mask;
				C_nzs[ k ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p_incorrect::v0::numeric_B_read(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		rbr3p::v0::Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = &A.rowPtr[ t->A_rowStart ];
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		nnz_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + t->A_rowStart;
		const ind_t ROWS = t->A_rowEnd - t->A_rowStart;
		const glbNnz_t NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const glbNnz_t K = k - NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ B_colInds[ m & CACHE_WINDOW ] & mask ] +=
						fA * B_nzs[ m & CACHE_WINDOW ];
				}
			}
            
			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k ] & mask;
				C_nzs[ k ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p_incorrect::v0::numeric_C_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		rbr3p::v0::Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = &A.rowPtr[ t->A_rowStart ];
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		nnz_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + t->A_rowStart;
		const ind_t ROWS = t->A_rowEnd - t->A_rowStart;
		const glbNnz_t NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const glbNnz_t K = k - NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ B_colInds[ m ] & mask ] += fA * B_nzs[ m ];
				}
			}
            
			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k ] & mask;
				C_nzs[ k & CACHE_WINDOW ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p_incorrect::v0::numeric_C_read_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		rbr3p::v0::Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = &A.rowPtr[ t->A_rowStart ];
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		nnz_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + t->A_rowStart;
		const ind_t ROWS = t->A_rowEnd - t->A_rowStart;
		const glbNnz_t NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const glbNnz_t K = k - NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ B_colInds[ m ] & mask ] += fA * B_nzs[ m ];
				}
			}
            
			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k & CACHE_WINDOW ] & mask;
				C_nzs[ k & CACHE_WINDOW ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p_incorrect::v0::numeric_B_read_C_read_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		rbr3p::v0::Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = &A.rowPtr[ t->A_rowStart ];
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		nnz_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + t->A_rowStart;
		const ind_t ROWS = t->A_rowEnd - t->A_rowStart;
		const glbNnz_t NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const glbNnz_t K = k - NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ B_colInds[ m & CACHE_WINDOW ] & mask ] +=
						fA * B_nzs[ m & CACHE_WINDOW ];
				}
			}
            
			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k & CACHE_WINDOW ] & mask;
				C_nzs[ k & CACHE_WINDOW ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p_incorrect::v0::numeric_B_read_C_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		rbr3p::v0::Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = &A.rowPtr[ t->A_rowStart ];
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		nnz_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + t->A_rowStart;
		const ind_t ROWS = t->A_rowEnd - t->A_rowStart;
		const glbNnz_t NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const glbNnz_t K = k - NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ B_colInds[ m & CACHE_WINDOW ] & mask ] +=
						fA * B_nzs[ m & CACHE_WINDOW ];
				}
			}
            
			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k ] & mask;
				C_nzs[ k & CACHE_WINDOW ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p_incorrect::v0::numeric_accumulator_random_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		rbr3p::v0::Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = &A.rowPtr[ t->A_rowStart ];
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		nnz_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + t->A_rowStart;
		const ind_t ROWS = t->A_rowEnd - t->A_rowStart;
		const glbNnz_t NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const glbNnz_t K = k - NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ ( B_colInds[ m ] & mask ) & CACHE_WINDOW ] +=
						fA * B_nzs[ m ];
				}
			}
            
			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k ] & mask;
				C_nzs[ k ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p_incorrect::v0::numeric_accumulator_random_read(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		rbr3p::v0::Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = &A.rowPtr[ t->A_rowStart ];
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		nnz_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + t->A_rowStart;
		const ind_t ROWS = t->A_rowEnd - t->A_rowStart;
		const glbNnz_t NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const glbNnz_t K = k - NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ B_colInds[ m ] & mask ] += fA * B_nzs[ m ];
				}
			}
            
			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex =
					( C_colInds[ k ] & mask ) & CACHE_WINDOW;
				C_nzs[ k ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p_incorrect::v0::numeric_accumulator_random_write_read(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		rbr3p::v0::Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = &A.rowPtr[ t->A_rowStart ];
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		nnz_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + t->A_rowStart;
		const ind_t ROWS = t->A_rowEnd - t->A_rowStart;
		const glbNnz_t NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const glbNnz_t K = k - NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ ( B_colInds[ m ] & mask ) & CACHE_WINDOW ] +=
						fA * B_nzs[ m ];
				}
			}
            
			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex =
					( C_colInds[ k ] & mask ) & CACHE_WINDOW;
				C_nzs[ k ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p_incorrect::v0::numeric_accumulator_random_write_read_B_read(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		rbr3p::v0::Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = &A.rowPtr[ t->A_rowStart ];
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		nnz_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + t->A_rowStart;
		const ind_t ROWS = t->A_rowEnd - t->A_rowStart;
		const glbNnz_t NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const glbNnz_t K = k - NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ ( B_colInds[ m & CACHE_WINDOW ] & mask ) & CACHE_WINDOW ] +=
						fA * B_nzs[ m & CACHE_WINDOW ];
				}
			}
            
			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex =
					( C_colInds[ k ] & mask ) & CACHE_WINDOW;
				C_nzs[ k ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p_incorrect::v0::numeric_accumulator_random_write_read_C_read_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		rbr3p::v0::Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = &A.rowPtr[ t->A_rowStart ];
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		nnz_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + t->A_rowStart;
		const ind_t ROWS = t->A_rowEnd - t->A_rowStart;
		const glbNnz_t NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const glbNnz_t K = k - NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ ( B_colInds[ m ] & mask ) & CACHE_WINDOW ] +=
						fA * B_nzs[ m ];
				}
			}
            
			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex =
					( C_colInds[ k & CACHE_WINDOW ] & mask ) & CACHE_WINDOW;
				C_nzs[ k & CACHE_WINDOW ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p_incorrect::v0::numeric_accumulator_random_write_read_B_read_C_read_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		rbr3p::v0::Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = &A.rowPtr[ t->A_rowStart ];
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		nnz_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + t->A_rowStart;
		const ind_t ROWS = t->A_rowEnd - t->A_rowStart;
		const glbNnz_t NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const glbNnz_t K = k - NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ ( B_colInds[ m & CACHE_WINDOW ] & mask ) & CACHE_WINDOW ] +=
						fA * B_nzs[ m & CACHE_WINDOW ];
				}
			}
            
			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex =
					( C_colInds[ k & CACHE_WINDOW ] & mask ) & CACHE_WINDOW;
				C_nzs[ k & CACHE_WINDOW ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p_incorrect::v0::numeric_no_accumulator(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		rbr3p::v0::Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = &A.rowPtr[ t->A_rowStart ];
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		nnz_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + t->A_rowStart;
		const ind_t ROWS = t->A_rowEnd - t->A_rowStart;
		const glbNnz_t NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		fp_t temp = 0.0;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const glbNnz_t K = k - NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					temp += ( B_colInds[ m ] & mask ) + fA * B_nzs[ m ];
				}
			}
            
			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k ] & mask;
				C_nzs[ k ] = temp + hashIndex;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p_incorrect::v0::numeric_no_accumulator_B_read(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		rbr3p::v0::Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = &A.rowPtr[ t->A_rowStart ];
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		nnz_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + t->A_rowStart;
		const ind_t ROWS = t->A_rowEnd - t->A_rowStart;
		const glbNnz_t NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		fp_t temp = 0.0;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const glbNnz_t K = k - NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					temp += ( B_colInds[ m & CACHE_WINDOW ] & mask ) +
						fA * B_nzs[ m & CACHE_WINDOW ];
				}
			}
            
			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k ] & mask;
				C_nzs[ k ] = temp + hashIndex;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p_incorrect::v0::numeric_no_accumulator_C_read_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		rbr3p::v0::Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = &A.rowPtr[ t->A_rowStart ];
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		nnz_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + t->A_rowStart;
		const ind_t ROWS = t->A_rowEnd - t->A_rowStart;
		const glbNnz_t NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		fp_t temp = 0.0;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const glbNnz_t K = k - NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					temp += ( B_colInds[ m ] & mask ) + fA * B_nzs[ m ];
				}
			}
            
			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k & CACHE_WINDOW ] & mask;
				C_nzs[ k & CACHE_WINDOW ] = temp + hashIndex;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p_incorrect::v0::numeric_no_accumulator_B_read_C_read_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		rbr3p::v0::Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = &A.rowPtr[ t->A_rowStart ];
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		nnz_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + t->A_rowStart;
		const ind_t ROWS = t->A_rowEnd - t->A_rowStart;
		const glbNnz_t NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		fp_t temp = 0.0;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const glbNnz_t K = k - NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					temp += ( B_colInds[ m & CACHE_WINDOW ] & mask ) +
						fA * B_nzs[ m & CACHE_WINDOW ];
				}
			}
            
			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k & CACHE_WINDOW ] & mask;
				C_nzs[ k & CACHE_WINDOW ] = temp + hashIndex;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p_incorrect::v0::numeric_no_first_half(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		rbr3p::v0::Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = &A.rowPtr[ t->A_rowStart ];
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		nnz_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + t->A_rowStart;
		const ind_t ROWS = t->A_rowEnd - t->A_rowStart;
		const glbNnz_t NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			// for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			// {
			// 	fA = A_nzs[ k ];
			// 	const glbNnz_t K = k - NZ_START;
			// 	for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
			// 	{
			// 		accumulator[ B_colInds[ m ] & mask ] += fA * B_nzs[ m ];
			// 	}
			// }
            
			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k ] & mask;
				C_nzs[ k ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p_incorrect::v0::numeric_no_second_half(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		rbr3p::v0::Thrd< ind_t, fp_t, nnz_t >* t = thrds[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = &A.rowPtr[ t->A_rowStart ];
		ind_t const* const __restrict__ A_colInds = A.colInd;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		nnz_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + t->A_rowStart;
		const ind_t ROWS = t->A_rowEnd - t->A_rowStart;
		const glbNnz_t NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const glbNnz_t K = k - NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ B_colInds[ m ] & mask ] += fA * B_nzs[ m ];
				}
			}
            
			// const ind_t C_rowInd = i;
			// for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			// {
			// 	const ind_t hashIndex = C_colInds[ k ] & mask;
			// 	C_nzs[ k ] = accumulator[ hashIndex ];
			// 	accumulator[ hashIndex ] = 0;
			// }
		}
	}
}

