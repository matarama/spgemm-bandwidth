
#include <iostream>
#include <bitset>

#include "include/util/Memory.h"

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_prime_mem >
void
rbr_reduced::v0::symbolic_alloc(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_prime_mem >& C,
	fp_t** accumulatorPerThrd, ind_t const* const thrdRowPfxSum,
	const ui32& numThreads )
{
	const ind_t BIT_MASK = ( 1 << 31 );
	const ind_t REVERSE_BIT_MASK = ~BIT_MASK;

	nnz_t perThrdCNnz[ numThreads + 1 ];
	#pragma omp parallel num_threads( numThreads )
	{
		const ui32 ID = omp_get_thread_num();
		fp_t* acc = accumulatorPerThrd[ ID ];
		
		char* lookupArr = (char*) acc;
		ind_t* colIndSet = ((ind_t*) acc) + B.columnCount;

		const ui32 A_ROW_START = thrdRowPfxSum[ ID ];
		const ui32 A_ROW_END = thrdRowPfxSum[ ID + 1 ];

		// Preprocess C.rowPtr
		// ---------------------------------------------------------------------
		#pragma omp single
		{
			C.rowPtr = static_cast< nnz_t* >(
				C_prime_mem::alloc( A.rowCount + 1, sizeof( nnz_t ) ) );
			C.rowPtr[ 0 ] = 0;
		}

		glbNnz_t _C_nnz = 0;
		for ( glbInd_t r = A_ROW_START; r < A_ROW_END; ++r )
		{
			glbInd_t currRowNnz = 0;
			for ( glbNnz_t nA = A.rowPtr[ r ]; nA < A.rowPtr[ r + 1 ]; ++nA )
			{
				const glbInd_t jA = A.colInd[ nA ];
				for ( glbNnz_t nB = B.rowPtr[ jA ];
				      nB < B.rowPtr[ jA + 1 ];
				      ++nB )
				{
					const glbInd_t jB = B.colInd[ nB ];
					const ind_t hashedAddress = jB >> 3;
					const ind_t bitMask = ( 1 << ( jB & 7 ) );
                    
					if ( ( lookupArr[ hashedAddress ] & bitMask ) == 0 )
					{
						colIndSet[ currRowNnz ] = jB;
						lookupArr[ hashedAddress ] |= bitMask;
						++currRowNnz;
					}
				}
			}

			for ( nnz_t n = 0; n < currRowNnz; ++n )
			{
				const glbInd_t jC = colIndSet[ n ];
				const ind_t hashedAddress = jC >> 3;
				lookupArr[ hashedAddress ] = 0;
				colIndSet[ n ] = 0;
			}

			_C_nnz += currRowNnz;
			C.rowPtr[ r + 1 ] = _C_nnz;
		}

		perThrdCNnz[ ID + 1 ] = _C_nnz;
		#pragma omp barrier
		#pragma omp single
		{
			perThrdCNnz[ 0 ] = 0;
			for ( ui32 t = 0; t < numThreads; ++t )
				perThrdCNnz[ t + 1 ] += perThrdCNnz[ t ];

			C.nnz = perThrdCNnz[ numThreads ];
			C.rowCount = A.rowCount;
			C.columnCount = B.columnCount;

			C.nzs = static_cast< fp_t* >(
				C_prime_mem::alloc( C.nnz, sizeof( fp_t ) ) );
			C.colInd = static_cast< ind_t* >(
				C_prime_mem::alloc( C.nnz, sizeof( ind_t ) ) );
		}

		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
			C.rowPtr[ r + 1 ] += perThrdCNnz[ ID ];

		#pragma omp barrier

		// preprocess C.colInd and C.nzs
		// ---------------------------------------------------------------------
		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
		{
			const nnz_t A_NZ_END = A.rowPtr[ r + 1 ];
			ind_t* C_colIndsCurrRow = C.colInd + C.rowPtr[ r ];
			fp_t* C_nzsCurrRow = C.nzs + C.rowPtr[ r ];
			ind_t currRowNnz = 0;
			for ( nnz_t nA = A.rowPtr[ r ]; nA < A_NZ_END; ++nA )
			{
				const ind_t JA = A.colInd[ nA ];
				const ind_t FA = A.nzs[ nA ];

				const nnz_t B_NZ_END = B.rowPtr[ JA + 1 ];
				for ( nnz_t nB = B.rowPtr[ JA ]; nB < B_NZ_END; ++nB )
				{
					const ind_t JB = B.colInd[ nB ];
					if ( colIndSet[ JB ] == 0 )
					{
						C_colIndsCurrRow[ currRowNnz ] = JB;
						C_nzsCurrRow[ currRowNnz ] = FA * B.nzs[ nB ];
						colIndSet[ JB ] = currRowNnz | BIT_MASK;
						++currRowNnz;
					}
					else
					{
						const ind_t index = colIndSet[ JB ] & REVERSE_BIT_MASK;
						C_nzsCurrRow[ index ] += FA * B.nzs[ nB ];
					}
				}
			}

			const nnz_t C_NZ_END = C.rowPtr[ r + 1 ];
			for ( nnz_t nC = C.rowPtr[ r ]; nC < C.rowPtr[ r + 1 ]; ++nC )
			{
				colIndSet[ C.colInd[ nC ] ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_prime_mem >
void
rbr_reduced::v0::symbolic(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_prime_mem >& C,
	fp_t** accumulatorPerThrd, ind_t const* const thrdRowPfxSum,
	const ui32& numThreads )
{
	const ind_t BIT_MASK = ( 1 << 31 );
	const ind_t REVERSE_BIT_MASK = ~BIT_MASK;

	C.rowPtr[ 0 ] = 0;
	nnz_t perThrdCNnz[ numThreads + 1 ];
	#pragma omp parallel num_threads( numThreads )
	{
		const ui32 ID = omp_get_thread_num();
		fp_t* acc = accumulatorPerThrd[ ID ];
		
		char* lookupArr = (char*) acc;
		ind_t* colIndSet = ((ind_t*) acc) + B.columnCount;

		const ui32 A_ROW_START = thrdRowPfxSum[ ID ];
		const ui32 A_ROW_END = thrdRowPfxSum[ ID + 1 ];

		// Preprocess C.rowPtr
		// ---------------------------------------------------------------------
		glbNnz_t _C_nnz = 0;
		for ( glbInd_t r = A_ROW_START; r < A_ROW_END; ++r )
		{
			glbInd_t currRowNnz = 0;
			for ( glbNnz_t nA = A.rowPtr[ r ]; nA < A.rowPtr[ r + 1 ]; ++nA )
			{
				const glbInd_t jA = A.colInd[ nA ];
				for ( glbNnz_t nB = B.rowPtr[ jA ];
				      nB < B.rowPtr[ jA + 1 ];
				      ++nB )
				{
					const glbInd_t jB = B.colInd[ nB ];
					const ind_t hashedAddress = jB >> 3;
					const ind_t bitMask = ( 1 << ( jB & 7 ) );
                    
					if ( ( lookupArr[ hashedAddress ] & bitMask ) == 0 )
					{
						colIndSet[ currRowNnz ] = jB;
						lookupArr[ hashedAddress ] |= bitMask;
						++currRowNnz;
					}
				}
			}

			for ( nnz_t n = 0; n < currRowNnz; ++n )
			{
				const glbInd_t jC = colIndSet[ n ];
				const ind_t hashedAddress = jC >> 3;
				lookupArr[ hashedAddress ] = 0;
				colIndSet[ n ] = 0;
			}

			_C_nnz += currRowNnz;
			C.rowPtr[ r + 1 ] = _C_nnz;
		}

		perThrdCNnz[ ID + 1 ] = _C_nnz;
		#pragma omp barrier
		#pragma omp single
		{
			perThrdCNnz[ 0 ] = 0;
			for ( ui32 t = 0; t < numThreads; ++t )
				perThrdCNnz[ t + 1 ] += perThrdCNnz[ t ];

			C.nnz = perThrdCNnz[ numThreads ];
			C.rowCount = A.rowCount;
			C.columnCount = B.columnCount;
		}

		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
			C.rowPtr[ r + 1 ] += perThrdCNnz[ ID ];

		#pragma omp barrier

		// preprocess C.colInd and C.nzs
		// ---------------------------------------------------------------------
		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
		{
			const nnz_t A_NZ_END = A.rowPtr[ r + 1 ];
			ind_t* C_colIndsCurrRow = C.colInd + C.rowPtr[ r ];
			fp_t* C_nzsCurrRow = C.nzs + C.rowPtr[ r ];
			ind_t currRowNnz = 0;
			for ( nnz_t nA = A.rowPtr[ r ]; nA < A_NZ_END; ++nA )
			{
				const ind_t JA = A.colInd[ nA ];
				const ind_t FA = A.nzs[ nA ];

				const nnz_t B_NZ_END = B.rowPtr[ JA + 1 ];
				for ( nnz_t nB = B.rowPtr[ JA ]; nB < B_NZ_END; ++nB )
				{
					const ind_t JB = B.colInd[ nB ];
					if ( colIndSet[ JB ] == 0 )
					{
						C_colIndsCurrRow[ currRowNnz ] = JB;
						C_nzsCurrRow[ currRowNnz ] = FA * B.nzs[ nB ];
						colIndSet[ JB ] = currRowNnz | BIT_MASK;
						++currRowNnz;
					}
					else
					{
						const ind_t index = colIndSet[ JB ] & REVERSE_BIT_MASK;
						C_nzsCurrRow[ index ] += FA * B.nzs[ nB ];
					}
				}
			}

			const nnz_t C_NZ_END = C.rowPtr[ r + 1 ];
			for ( nnz_t nC = C.rowPtr[ r ]; nC < C.rowPtr[ r + 1 ]; ++nC )
			{
				colIndSet[ C.colInd[ nC ] ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename C_prime_mem, typename C_mem >
void
rbr_reduced::v0::numeric(
	const Csr< ind_t, fp_t, nnz_t, C_prime_mem >& C_prime,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t const* const D,
	nnz_t const* const workPfxSum, const ui32& numThreads )
{
	fp_t const* const X = C_prime.nzs;
	ind_t const* const IND = C_prime.colInd;
	fp_t* const Y = C.nzs;
	#pragma omp parallel num_threads( numThreads )
	{
		const ui32 ID = omp_get_thread_num();
		const nnz_t BEGIN = workPfxSum[ ID ];
		const nnz_t END = workPfxSum[ ID + 1 ];

		for ( nnz_t i = BEGIN; i < END; ++i )
			Y[ i ] = X[ i ] * D[ IND[ i ] ];
	}
}

template < typename z_ind_t, typename fp_t, typename z_nnz_t,
           typename z_mem >
rbr_reduced::v0::Thrd< z_ind_t, fp_t, z_nnz_t, z_mem >::Thrd()
: Z_ptrLength( 0 ), Z_capacity( 0 ),
  Z_colInd( nullptr ), Z_nzs( nullptr ), Z_lengths( nullptr )
{
}

template < typename z_ind_t, typename fp_t, typename z_nnz_t,
           typename z_mem >
rbr_reduced::v0::Thrd< z_ind_t, fp_t, z_nnz_t, z_mem >::~Thrd()
{
	free();
}

template < typename z_ind_t, typename fp_t, typename z_nnz_t,
           typename z_mem >
void
rbr_reduced::v0::Thrd< z_ind_t, fp_t, z_nnz_t, z_mem >::init()
{
	Z_ptrLength = 0;
	Z_capacity = 0;
	Z_colInd = nullptr;
	Z_nzs = nullptr;
	Z_lengths = nullptr;
}

template < typename z_ind_t, typename fp_t, typename z_nnz_t,
           typename z_mem >
void
rbr_reduced::v0::Thrd< z_ind_t, fp_t, z_nnz_t, z_mem >::free()
{
	if ( Z_colInd != nullptr )
		z_mem::free( (void*) Z_colInd, Z_capacity, sizeof( z_ind_t ) );
	if ( Z_nzs != nullptr )
		z_mem::free( (void*) Z_nzs, Z_capacity, sizeof( fp_t ) );
	if ( Z_length != nullptr )
		z_mem::free( (void*) Z_lengths, Z_ptrLength, sizeof( z_nnz_t ) );

	Z_ptrLength = 0;
	Z_capacity = 0;
	Z_colInd = nullptr;
	Z_nzs = nullptr;
	Z_lengths = nullptr;
}
