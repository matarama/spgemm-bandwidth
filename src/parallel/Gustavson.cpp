
#include <iostream>
#include <vector>
#include <bitset>
#include "immintrin.h"

#include "include/util/Memory.h"

template < bool doAlloc, bool isSorted,
           typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson::symbolic_stl(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd, nnz_t const* const thrdRowPfxSum,
	const ui32& numThreads )
{
	nnz_t perThrdCNnz[ numThreads + 1 ];
	#pragma omp parallel num_threads( numThreads )
	{
		const ui32 ID = omp_get_thread_num();
		fp_t* acc = accumulatorPerThrd[ ID ];

		std::vector< ind_t > colIndSet;
		char* lookupArr = (char*) acc;

		const ind_t A_ROW_START = thrdRowPfxSum[ ID ];
		const ind_t A_ROW_END = thrdRowPfxSum[ ID + 1 ];

		#pragma omp single
		{
			if ( doAlloc )
			{
				C.ptr = static_cast< nnz_t* >(
					C_mem::alloc( A.length + 1, sizeof( nnz_t ) ) );
			}

			C.length = A.length;
			C.ptr[ 0 ] = 0;
		}

		nnz_t _C_nnz = 0;
		for ( ind_t a = A_ROW_START; a < A_ROW_END; ++a )
		{
			ind_t currRowNnz = 0;
			for ( glbNnz_t na = A.ptr[ a ]; na < A.ptr[ a+ 1 ]; ++na )
			{
				const ind_t JA = A.inds[ na ];
				for ( ind_t nb = B.ptr[ JA ]; nb < B.ptr[ JA + 1 ]; ++nb )
				{
					const ind_t JB = B.inds[ nb ];
					const ind_t HASH_ADDRESS = JB >> 3;
					const ind_t BIT_MASK = ( 1 << ( JB & 7 ) );

					if ( ( lookupArr[ HASH_ADDRESS ] & BIT_MASK ) == 0 )
					{
						lookupArr[ HASH_ADDRESS ] |= BIT_MASK;
						colIndSet.push_back( JB );
						++currRowNnz;
					}
				}
			}

			if ( isSorted )
				std::sort( &colIndSet[ _C_nnz ],
				           &colIndSet[ _C_nnz + currRowNnz ] );

			for ( nnz_t n = 0; n < currRowNnz; ++n )
			{
				const glbInd_t iC = colIndSet[ _C_nnz + n ];
				const ind_t HASH_ADDRESS = iC >> 3;
				lookupArr[ HASH_ADDRESS ] = 0;
			}

			_C_nnz += currRowNnz;
			C.ptr[ a + 1 ] = _C_nnz;
		}
		perThrdCNnz[ ID + 1 ] = _C_nnz;

		#pragma omp barrier
		#pragma omp single
		{
			perThrdCNnz[ 0 ] = 0;
			for ( ui32 t = 0; t < numThreads; ++t )
				perThrdCNnz[ t + 1 ] += perThrdCNnz[ t ];

			if ( doAlloc )
			{
				const nnz_t C_NNZ = perThrdCNnz[ numThreads ];
				C.nzs = static_cast< fp_t* >(
					C_mem::alloc( C_NNZ, sizeof( fp_t ) ) );
				C.inds = static_cast< ind_t* >(
					C_mem::alloc( C_NNZ, sizeof( ind_t ) ) );
			}
		}

		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
			C.ptr[ r + 1 ] += perThrdCNnz[ ID ];

		ind_t* _C_colInd = C.inds + perThrdCNnz[ ID ];
		nnz_t n = 0;
		for( nnz_t n = 0; n < _C_nnz; ++n )
			_C_colInd[ n ] = colIndSet[ n ];
	}
}



template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson::numeric(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd,
	nnz_t const* const thrdRowPfxSum,
	const ui32& numThreads )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const ui32 tid = omp_get_thread_num();
		fp_t* tacc = accumulatorPerThrd[ tid ];

		for ( ind_t r = thrdRowPfxSum[ tid ];
		      r < thrdRowPfxSum[ tid + 1 ];
		      ++r )
		{
			for ( nnz_t k = A.ptr[ r ]; k < A.ptr[ r + 1 ]; ++k )
			{
				const ind_t JA = A.inds[ k ];
				const fp_t FA = A.nzs[ k ] * D[ JA ];
				for ( nnz_t l = B.ptr[ JA ]; l < B.ptr[ JA + 1 ]; ++l )
				{
					const ind_t JB = B.inds[ l ];
					tacc[ JB ] += FA * B.nzs[ l ];
				}
			}

			for ( nnz_t k = C.ptr[ r ]; k < C.ptr[ r + 1 ]; ++k )
			{
				const ind_t JC = C.inds[ k ];
				C.nzs[ k ] = tacc[ JC ];
				tacc[ JC ] = 0;
			}
		}
	}
}



template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson::numeric(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd,
	nnz_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	const ui32& ITERATIONS )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const ui32 tid = omp_get_thread_num();
		fp_t* tacc = accumulatorPerThrd[ tid ];

		for ( ui32 w = 0; w < ITERATIONS; ++w )
		{
			for ( ind_t r = thrdRowPfxSum[ tid ];
				  r < thrdRowPfxSum[ tid + 1 ];
				  ++r )
			{
				for ( nnz_t k = A.ptr[ r ]; k < A.ptr[ r + 1 ]; ++k )
				{
					const ind_t JA = A.inds[ k ];
					const fp_t FA = A.nzs[ k ] * D[ JA ];
					for ( nnz_t l = B.ptr[ JA ]; l < B.ptr[ JA + 1 ]; ++l )
					{
						const ind_t JB = B.inds[ l ];
						tacc[ JB ] += FA * B.nzs[ l ];
					}
				}

				for ( nnz_t k = C.ptr[ r ]; k < C.ptr[ r + 1 ]; ++k )
				{
					const ind_t JC = C.inds[ k ];
					C.nzs[ k ] = tacc[ JC ];
					tacc[ JC ] = 0;
				}
			}

			#pragma omp barrier
		}
	}
}


// -----------------------------------------------------------------------------

template < bool doAlloc,
           typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson::symbolic_stl(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd,
	nnz_t const* const thrdRowPfxSum,
	ind_t** C_offsetsPerThrd,
	const ui32& numThreads )
{
	nnz_t perThrdCNnz[ numThreads + 1 ];
	#pragma omp parallel num_threads( numThreads )
	{
		const ui32 ID = omp_get_thread_num();
		fp_t* acc = accumulatorPerThrd[ ID ];
		ind_t* offsets = C_offsetsPerThrd[ ID ];

		std::vector< ind_t > colIndSet;
		ind_t* lookupArr = (ind_t*) acc;

		const ui32 A_ROW_START = thrdRowPfxSum[ ID ];
		const ui32 A_ROW_END = thrdRowPfxSum[ ID + 1 ];

		#pragma omp single
		{
			if ( doAlloc )
			{
				C.ptr = static_cast< nnz_t* >(
					C_mem::alloc( A.length + 1, sizeof( nnz_t ) ) );
			}

			C.length = A.length;
			C.ptr[ 0 ] = 0;
		}

		ui64 o = 0;
		glbNnz_t _C_nnz = 0;
		const ind_t BIT_MASK = ( 1 << 31 );
		const ind_t REVERSE_BIT_MASK = ~BIT_MASK;
		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
		{
			ind_t currRowNnz = 0;
			for ( nnz_t na = A.ptr[ r ]; na < A.ptr[ r + 1 ]; ++na )
			{
				const ind_t JA = A.inds[ na ];
				for ( nnz_t nB = B.ptr[ JA ]; nB < B.ptr[ JA + 1 ]; ++nB, ++o )
				{
					const glbInd_t JB = B.inds[ nB ];

					if ( ( lookupArr[ JB ] ) == 0 )
					{
						lookupArr[ JB ] = currRowNnz | BIT_MASK;
						colIndSet.push_back( JB );
						++currRowNnz;
					}

					offsets[ o ] = _C_nnz + lookupArr[ JB ] & REVERSE_BIT_MASK;
				}
			}

			for ( nnz_t n = 0; n < currRowNnz; ++n )
			{
				const glbInd_t JC = colIndSet[ _C_nnz + n ];
				lookupArr[ JC ] = 0;
			}

			_C_nnz += currRowNnz;
			C.ptr[ r + 1 ] = _C_nnz;
		}

		perThrdCNnz[ ID + 1 ] = _C_nnz;
		#pragma omp barrier
		#pragma omp single
		{
			perThrdCNnz[ 0 ] = 0;
			for ( ui32 t = 0; t < numThreads; ++t )
				perThrdCNnz[ t + 1 ] += perThrdCNnz[ t ];

			if ( doAlloc )
			{
				const nnz_t C_NNZ = perThrdCNnz[ numThreads ];
				C.nzs = static_cast< fp_t* >(
					C_mem::alloc( C_NNZ, sizeof( fp_t ) ) );
				C.inds = static_cast< ind_t* >(
					C_mem::alloc( C_NNZ, sizeof( ind_t ) ) );
			}
		}

		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
			C.ptr[ r + 1 ] += perThrdCNnz[ ID ];

		for ( ui64 u = 0; u < o; ++u )
			offsets[ u ] += perThrdCNnz[ ID ];

		ind_t* _C_colInd = C.inds + perThrdCNnz[ ID ];
		nnz_t n = 0;
		for( nnz_t n = 0; n < _C_nnz; ++n )
			_C_colInd[ n ] = colIndSet[ n ];
	}
}


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson::numeric( const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
                         const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
                         Cs< ind_t, fp_t, nnz_t, C_mem >& C,
                         nnz_t const* const thrdRowPfxSum,
                         ind_t** C_offsetsPerThrd,
                         const ui32& numThreads )
{
	#pragma omp parallel num_threads( numThreads )
	// for ( ui32 tid = 0; tid < numThreads; ++tid )
	{
		const ui32 tid = omp_get_thread_num();
		const ind_t ROW_START = thrdRowPfxSum[ tid ];
		const ind_t ROW_END = thrdRowPfxSum[ tid + 1 ];

		const nnz_t C_NZ_START = C.ptr[ ROW_START ];
		const nnz_t C_NZ_END = C.ptr[ ROW_END ];
		for ( nnz_t cn = C_NZ_END - 1; cn >= C_NZ_START; --cn )
			C.nzs[ cn ] = 0;

		nnz_t A_NZ_START = A.ptr[ ROW_START ];
		nnz_t A_NZ_END = A.ptr[ ROW_END ];
		ind_t const* const offsets = C_offsetsPerThrd[ tid ];
		ui64 o = 0;
		for ( nnz_t na = A_NZ_START; na < A_NZ_END; ++na )
		{
			const fp_t FA = A.nzs[ na ];
			const ind_t JA = A.inds[ na ];

			for ( nnz_t l = B.ptr[ JA ]; l < B.ptr[ JA + 1 ]; ++l, ++o )
				C.nzs[ offsets[ o ] ] += FA * B.nzs[ l ];
		}
	}
}



template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson::numeric( const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
                         const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
                         Cs< ind_t, fp_t, nnz_t, C_mem >& C,
                         nnz_t const* const thrdRowPfxSum,
                         ind_t** C_offsetsPerThrd,
                         const ui32& numThreads,
						 const ui32& ITERATIONS )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const ui32 tid = omp_get_thread_num();
		const ind_t ROW_START = thrdRowPfxSum[ tid ];
		const ind_t ROW_END = thrdRowPfxSum[ tid + 1 ];

		const nnz_t C_NZ_START = C.ptr[ ROW_START ];
		const nnz_t C_NZ_END = C.ptr[ ROW_END ];

		for ( ui32 w = 0; w < ITERATIONS; ++w )
		{
			for ( nnz_t cn = C_NZ_END - 1; cn >= C_NZ_START; --cn )
				C.nzs[ cn ] = 0;

			nnz_t A_NZ_START = A.ptr[ ROW_START ];
			nnz_t A_NZ_END = A.ptr[ ROW_END ];
			ind_t const* const offsets = C_offsetsPerThrd[ tid ];
			ui64 o = 0;
			for ( nnz_t na = A_NZ_START; na < A_NZ_END; ++na )
			{
				const fp_t FA = A.nzs[ na ];
				const ind_t JA = A.inds[ na ];

				for ( nnz_t l = B.ptr[ JA ]; l < B.ptr[ JA + 1 ]; ++l, ++o )
					C.nzs[ offsets[ o ] ] += FA * B.nzs[ l ];
			}

			#pragma omp barrier
		}
	}
}


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson::numeric( const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
                         const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
                         Cs< ind_t, fp_t, nnz_t, C_mem >& C,
						 ind_t** C_offsetsPerThrd,
						 nnz_t const* const aSlicePfxSum,
						 nnz_t const* const cSlicePfxSum,
						 ui32 const* const slicesPerThrdPfxSum,
                         const ui32& numThreads )
{
	#pragma omp parallel num_threads( numThreads )
	// for ( ui32 ID = 0; ID < numThreads; ++ID )
	{
		const ui32 ID = omp_get_thread_num();
		ind_t const* const offsets = C_offsetsPerThrd[ ID ];
		ui64 o = 0;
		for ( ui32 s = slicesPerThrdPfxSum[ ID ];
			  s < slicesPerThrdPfxSum[ ID + 1 ];
			  ++s )
		{
			for ( nnz_t nc = cSlicePfxSum[ s + 1 ] - 1;
				  nc >= cSlicePfxSum[ s ];
				  --nc )
				C.nzs[ nc ] = 0;

			for ( nnz_t na = aSlicePfxSum[ s ];
				  na < aSlicePfxSum[ s + 1 ];
				  ++na )
			{
				const fp_t FA = A.nzs[ na ];
				const ind_t JA = A.inds[ na ];

				for ( nnz_t l = B.ptr[ JA ]; l < B.ptr[ JA + 1 ]; ++l, ++o )
					C.nzs[ offsets[ o ] ] += FA * B.nzs[ l ];
			}
		}
	}
}



// -----------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson::numeric_OMP_dynamic(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThread )
{
	nnz_t const* const __restrict__ rowPtrA = A.ptr;
	ind_t const* const __restrict__ colIndsA = A.inds;
	fp_t const* const __restrict__ nzsA = A.nzs;

	nnz_t const* const __restrict__ rowPtrB = B.ptr;
	ind_t const* const __restrict__ colIndsB = B.inds;
	fp_t const* const __restrict__ nzsB = B.nzs;

	nnz_t const* const __restrict__ rowPtrC = C.ptr;
	ind_t* const __restrict__ colIndsC = C.inds;
	fp_t* const __restrict__ nzsC = C.nzs;

	const ind_t A_ROW_COUNT = A.length;

	ind_t i;
	ind_t jA;
	ind_t jB;
	nnz_t k;
	nnz_t kend;
	nnz_t l;
	nnz_t lend;
	nnz_t mend;
	fp_t fA;

	#pragma omp parallel for \
		schedule( dynamic ) \
		private( i, jA, jB, k, l, kend, lend, mend, fA )
	for ( ind_t i = 0; i < A_ROW_COUNT; ++i )
	{
		const ui32 tid = omp_get_thread_num();
		fp_t* tacc = accumulatorPerThread[ tid ];

		mend = rowPtrA[ i + 1 ];
		for ( k = rowPtrA[ i ]; k < mend; ++k )
		{
			jA = colIndsA[ k ];
			fA = nzsA[ k ];
			lend = rowPtrB[ jA + 1 ];
			for ( l = rowPtrB[ jA ]; l < lend; ++l )
			{
				jB = colIndsB[ l ];
				tacc[ jB ] += fA * nzsB[ l ];
			}
		}

		for ( glbNnz_t k = rowPtrC[ i ]; k < rowPtrC[ i + 1 ]; ++k )
		{
			const glbInd_t j = colIndsC[ k ];
			nzsC[ k ] = tacc[ j ];
			tacc[ j ] = 0;
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson::numeric_METIS_dynamic(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd,
	const ui32& numParts, ind_t* partRowPfxSum )
{
	nnz_t const* const __restrict__ rowPtrA = A.ptr;
	ind_t const* const __restrict__ colIndsA = A.inds;
	fp_t const* const __restrict__ nzsA = A.nzs;

	nnz_t const* const __restrict__ rowPtrB = B.ptr;
	ind_t const* const __restrict__ colIndsB = B.inds;
	fp_t const* const __restrict__ nzsB = B.nzs;

	nnz_t const* const __restrict__ rowPtrC = C.ptr;
	ind_t* const __restrict__ colIndsC = C.inds;
	fp_t* const __restrict__ nzsC = C.nzs;

	const ind_t A_ROW_COUNT = A.length;

	ind_t i;
	ind_t jA;
	ind_t jB;
	nnz_t k;
	nnz_t kend;
	nnz_t l;
	nnz_t lend;
	nnz_t mend;
	fp_t fA;

	#pragma omp parallel for \
		schedule( dynamic ) \
		private( i, jA, jB, k, l, kend, lend, mend, fA )
	for ( ind_t p = 0; p < numParts; ++p )
	{
		const ind_t ROW_START = partRowPfxSum[ p ];
		const ind_t ROW_END = partRowPfxSum[ p + 1 ];
		for ( i = ROW_START; i < ROW_END; ++i )
		{
			const ui32 tid = omp_get_thread_num();
			fp_t* tacc = accumulatorPerThrd[ tid ];

			mend = rowPtrA[ i + 1 ];
			for ( k = rowPtrA[ i ]; k < mend; ++k )
			{
				jA = colIndsA[ k ];
				fA = nzsA[ k ];
				lend = rowPtrB[ jA + 1 ];
				for ( l = rowPtrB[ jA ]; l < lend; ++l )
				{
					jB = colIndsB[ l ];
					tacc[ jB ] += fA * nzsB[ l ];
				}
			}

			for ( glbNnz_t k = rowPtrC[ i ]; k < rowPtrC[ i + 1 ]; ++k )
			{
				const glbInd_t j = colIndsC[ k ];
				nzsC[ k ] = tacc[ j ];
				tacc[ j ] = 0;
			}
		}
	}
}


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson::count( const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
                       const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
                       const Cs< ind_t, fp_t, nnz_t, C_mem >& C,
                       const ui32& cacheLineBytes,
                       glbInd_t const* const thrdRowPfxSum,
                       const ui32& numThreads,
                       ui64& noOfCacheLinesRead_B,
                       ui64& noOfLoads_accumulator,
                       ui64& noOfLoads_A,
                       ui64& noOfLoads_B,
                       ui64& noOfLoads_C,
                       ui64& noOfStores_accumulator,
                       ui64& noOfStores_C )
{
	nnz_t const* const __restrict__ rowPtrA = A.ptr;
	ind_t const* const __restrict__ colIndsA = A.inds;

	nnz_t const* const __restrict__ rowPtrB = B.ptr;
	ind_t const* const __restrict__ colIndsB = B.inds;

	nnz_t const* const __restrict__ rowPtrC = C.ptr;
	ind_t const* const __restrict__ colIndsC = C.inds;

	const ui64 FP_LENGTH = ( cacheLineBytes / sizeof( fp_t ) );
	const ui64 IND_LENGTH = ( cacheLineBytes / sizeof( glbInd_t ) );
	const ui64 NNZ_LENGTH = ( cacheLineBytes / sizeof( glbNnz_t ) );

	std::vector< ui64 > vNoOfMultiplications( numThreads, 0 );
	std::vector< ui64 > vNoOfCacheLinesRead_B( numThreads, 0 );
	#pragma omp parallel num_threads( numThreads )
	{
		ui64 _noOfMultiplications = 0;
		ui64 _noOfCacheLinesRead_B = 0;
		int _id = omp_get_thread_num();
		for ( glbInd_t i = thrdRowPfxSum[ _id ];
		      i < thrdRowPfxSum[ _id + 1 ];
		      ++i )
		{
			for ( nnz_t k = rowPtrA[ i ]; k < rowPtrA[ i + 1 ]; ++k )
			{
				ind_t jA = colIndsA[ k ];
				_noOfMultiplications += rowPtrB[ jA + 1 ] - rowPtrB[ jA ];

				// no of B cache lines read
				// -------------------------------------------------------------
				int fpAlignedStart =
					rowPtrB[ jA ] - ( rowPtrB[ jA ] % FP_LENGTH );
				int fpAlignedEnd = rowPtrB[ jA + 1 ] % FP_LENGTH;
				if ( fpAlignedEnd > 0 )
					fpAlignedEnd = FP_LENGTH - fpAlignedEnd;
				fpAlignedEnd = rowPtrB[ jA + 1 ] + fpAlignedEnd;
				_noOfCacheLinesRead_B +=
					( fpAlignedEnd - fpAlignedStart ) / FP_LENGTH;

				int indAlignedStart =
					rowPtrB[ jA ] - ( rowPtrB[ jA ] % IND_LENGTH );
				int indAlignedEnd = rowPtrB[ jA + 1 ] % IND_LENGTH;
				if ( indAlignedEnd > 0 )
					indAlignedEnd = IND_LENGTH - indAlignedEnd;
				indAlignedEnd = rowPtrB[ jA + 1 ] + indAlignedEnd;
				_noOfCacheLinesRead_B +=
					( indAlignedEnd - indAlignedStart ) / IND_LENGTH;

				if ( jA % NNZ_LENGTH == NNZ_LENGTH - 1 )
					_noOfCacheLinesRead_B += 2;
				else
					_noOfCacheLinesRead_B += 1;
			}
		}

		vNoOfMultiplications[ _id ] = _noOfMultiplications;
		vNoOfCacheLinesRead_B[ _id ] = _noOfCacheLinesRead_B;
	}

	ui64 totalCacheLineReads_B = 0;
	ui64 totalMultiplications = 0;
	for ( std::size_t i = 0; i < vNoOfMultiplications.size(); ++i )
	{
		totalCacheLineReads_B += vNoOfCacheLinesRead_B[ i ];
		totalMultiplications += vNoOfMultiplications[ i ];
	}

	// outputs
	noOfCacheLinesRead_B = totalCacheLineReads_B;
	noOfLoads_accumulator = totalMultiplications;
	noOfLoads_A = 2 * A.ptr[ A.length ] + A.length + 1;
	noOfLoads_B = 2 * totalMultiplications + 2 * A.ptr[ A.length ];
	noOfLoads_C = 2 * C.ptr[ C.length ] + C.length * 2;
	noOfStores_accumulator = totalMultiplications;
	noOfStores_C = C.ptr[ C.length ];
}

// Data Preparation
// -----------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem >
void
rbr::gustavson::partition(
	Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	ind_t** thrdRowPfxSum_out, const ui32& numThreads )
{
	ind_t* thrdRowPfxSum = nullptr;
	#pragma omp parallel
	#pragma omp master
	thrdRowPfxSum = util::alloc< ind_t >( numThreads + 1 );

	ui64 totalMults = 0;
	for ( nnz_t nA = 0; nA < A.nnz; ++nA )
	{
		const ind_t jA = A.colInd[ nA ];
		totalMults += B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
	}

	ui64 multPerThrd = ( totalMults + numThreads - 1 ) / numThreads;
	ui64 limit = multPerThrd;
	ui64 currMults = 0;
	ui32 t = 0;
	for ( ind_t r = 0; r < A.rowCount; ++r )
	{
		for ( nnz_t nA = A.rowPtr[ r ]; nA < A.rowPtr[ r + 1 ]; ++nA )
		{
			const ind_t jA = A.colInd[ nA ];
			currMults += B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
		}

		if ( currMults >= limit )
		{
			limit += multPerThrd;
			++t;
			thrdRowPfxSum[ t ] = r;
		}
	}

	*thrdRowPfxSum_out = thrdRowPfxSum;
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem >
void
rbr::gustavson::prepare( Triplets& A, Triplets& B,
                         Csr< ind_t, fp_t, nnz_t, A_mem >& A_out,
                         Csr< ind_t, fp_t, nnz_t, B_mem >& B_out,
                         ind_t const* const thrdRowPfxSum,
                         const ui32& numThreads )
{
	// Assign each thread get rougly equal B non-zeros
	// (to respect the first touch policy)
	// -------------------------------------------------------------------------
	std::shared_ptr< std::vector< glbNnz_t > > pB_rowPtr( B.rowPtr() );
	std::vector< glbNnz_t >& B_rowPtr = *pB_rowPtr.get();

	ind_t thrdBRowPfxSum[ numThreads + 1 ];
	thrdBRowPfxSum[ 0 ] = 0;
	nnz_t nnzPerThrd = ( B.length() + numThreads - 1 ) / numThreads;
	ui32 currThrd = 0;
	nnz_t limit = nnzPerThrd;
	for ( ind_t r = 0; r < B.rows(); ++r )
	{
		if ( B_rowPtr[ r ] > limit )
		{
			thrdBRowPfxSum[ currThrd + 1 ] = r;
			limit += nnzPerThrd;
		}
	}
	thrdBRowPfxSum[ numThreads ] = B.rows();
	// -------------------------------------------------------------------------

	A_out.extract( A, thrdRowPfxSum, numThreads );
	B_out.extract( B, thrdBRowPfxSum, numThreads );
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson::prepare( Triplets& A, Triplets& B, Triplets& C,
                         Csr< ind_t, fp_t, nnz_t, A_mem >& A_out,
                         Csr< ind_t, fp_t, nnz_t, B_mem >& B_out,
                         Csr< ind_t, fp_t, nnz_t, C_mem >& C_out,
                         ind_t const* const thrdRowPfxSum,
                         const ui32& numThreads )
{
	prepare( A, B, A_out, B_out, thrdRowPfxSum, numThreads );
	C_out.extract( C, thrdRowPfxSum, numThreads );
}
