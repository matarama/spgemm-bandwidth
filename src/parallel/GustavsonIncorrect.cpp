
template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson_incorrect::numeric_A_read(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThread,
	const ui32& numThreads,
	nnz_t const* const threadRowPfxSum )
{
	nnz_t const* const __restrict__ rowPtrA = A.rowPtr;
	ind_t const* const __restrict__ colIndsA = A.colInd;
	fp_t const* const __restrict__ nzsA = A.nzs;

	nnz_t const* const __restrict__ rowPtrB = B.rowPtr;
	ind_t const* const __restrict__ colIndsB = B.colInd;
	fp_t const* const __restrict__ nzsB = B.nzs;

	nnz_t const* const __restrict__ rowPtrC = C.rowPtr;
	ind_t const* const __restrict__ colIndsC = C.colInd;
	fp_t* const __restrict__ nzsC = C.nzs;

	ind_t i;
	ind_t jA;
	ind_t jB;
	nnz_t k;
	nnz_t kend;
	nnz_t l;
	nnz_t lend;
	nnz_t mend;
	fp_t fA;
	#pragma omp parallel num_threads( numThreads ) \
		private( i, jA, jB, k, l, kend, lend, mend, fA )
	{
		const ui32 tid = omp_get_thread_num();
		fp_t* acc = accumulatorPerThread[ tid ];

		glbInd_t rend = threadRowPfxSum[ tid + 1 ];
		for ( glbInd_t i = threadRowPfxSum[ tid ]; i < rend; ++i )
		{
			mend = rowPtrA[ i + 1 ];
			for ( k = rowPtrA[ i ]; k < mend; ++k )
			{
				jA = colIndsA[ k ];
				fA = nzsA[ k & 255 ];
				lend = rowPtrB[ jA + 1 ];
				for ( l = rowPtrB[ jA ]; l < lend; ++l )
				{
					jB = colIndsB[ l ];
					acc[ jB ] += fA * nzsB[ l ];
				}
			}

			kend = rowPtrC[ i + 1 ];
			k = rowPtrC[ i ];

			const ind_t* keys = &colIndsC[ k ];
			const ind_t noOfKeys = kend - k;
			fp_t* to = &nzsC[ k ];
			for ( ind_t i = 0; i < noOfKeys; ++i )
			{
				to[ i ] = acc[ keys[ i ] ];
				acc[ keys[ i ] ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson_incorrect::numeric_B_read(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThread,
	const ui32& numThreads,
	nnz_t const* const threadRowPfxSum )
{
	nnz_t const* const __restrict__ rowPtrA = A.rowPtr;
	ind_t const* const __restrict__ colIndsA = A.colInd;
	fp_t const* const __restrict__ nzsA = A.nzs;

	nnz_t const* const __restrict__ rowPtrB = B.rowPtr;
	ind_t const* const __restrict__ colIndsB = B.colInd;
	fp_t const* const __restrict__ nzsB = B.nzs;

	nnz_t const* const __restrict__ rowPtrC = C.rowPtr;
	ind_t const* const __restrict__ colIndsC = C.colInd;
	fp_t* const __restrict__ nzsC = C.nzs;

	ind_t i;
	ind_t jA;
	ind_t jB;
	nnz_t k;
	nnz_t kend;
	nnz_t l;
	nnz_t lend;
	nnz_t mend;
	fp_t fA;
	#pragma omp parallel num_threads( numThreads ) \
		private( i, jA, jB, k, l, kend, lend, mend, fA )
	{
		const ui32 tid = omp_get_thread_num();
		fp_t* acc = accumulatorPerThread[ tid ];

		glbInd_t rend = threadRowPfxSum[ tid + 1 ];
		for ( glbInd_t i = threadRowPfxSum[ tid ]; i < rend; ++i )
		{
			mend = rowPtrA[ i + 1 ];
			for ( k = rowPtrA[ i ]; k < mend; ++k )
			{
				jA = colIndsA[ k ];
				fA = nzsA[ k ];
				lend = rowPtrB[ jA + 1 ];
				for ( l = rowPtrB[ jA ]; l < lend; ++l )
				{
					jB = colIndsB[ l & 255 ];
					acc[ jB ] += fA * nzsB[ l & 255 ];
				}
			}

			kend = rowPtrC[ i + 1 ];
			k = rowPtrC[ i ];

			const ind_t* keys = &colIndsC[ k ];
			const ind_t noOfKeys = kend - k;
			fp_t* to = &nzsC[ k ];
			for ( ind_t i = 0; i < noOfKeys; ++i )
			{
				to[ i ] = acc[ keys[ i ] ];
				acc[ keys[ i ] ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson_incorrect::numeric_B_read2(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThread,
	const ui32& numThreads,
	nnz_t const* const threadRowPfxSum,
	glbInd_t const* const cutOffRowIndAPerThread )
{
	nnz_t const* const __restrict__ rowPtrA = A.rowPtr;
	ind_t const* const __restrict__ colIndsA = A.colInd;
	fp_t const* const __restrict__ nzsA = A.nzs;

	nnz_t const* const __restrict__ rowPtrB = B.rowPtr;
	ind_t const* const __restrict__ colIndsB = B.colInd;
	fp_t const* const __restrict__ nzsB = B.nzs;

	nnz_t const* const __restrict__ rowPtrC = C.rowPtr;
	ind_t const* const __restrict__ colIndsC = C.colInd;
	fp_t* const __restrict__ nzsC = C.nzs;

	ind_t i;
	ind_t jA;
	ind_t jB;
	nnz_t k;
	nnz_t kend;
	nnz_t l;
	nnz_t lend;
	nnz_t mend;
	fp_t fA;
	#pragma omp parallel num_threads( numThreads ) \
		private( i, jA, jB, k, l, kend, lend, mend, fA )
	{
		const ui32 tid = omp_get_thread_num();
		fp_t* acc = accumulatorPerThread[ tid ];
		glbInd_t cutOffRowIndA = cutOffRowIndAPerThread[ tid ];

		glbNnz_t counterNnzB = rowPtrB[ threadRowPfxSum[ tid ] ];
		glbInd_t rend = threadRowPfxSum[ tid + 1 ];
		for ( glbInd_t i = threadRowPfxSum[ tid ]; i < cutOffRowIndA; ++i )
		{
			mend = rowPtrA[ i + 1 ];
			for ( k = rowPtrA[ i ]; k < mend; ++k )
			{
				jA = colIndsA[ k ];
				fA = nzsA[ k ];
				lend = rowPtrB[ jA + 1 ];
				for ( l = rowPtrB[ jA ]; l < lend; ++l, ++counterNnzB )
				{
					jB = colIndsB[ counterNnzB ];
					acc[ jB ] += fA * nzsB[ counterNnzB ];
				}
			}

			kend = rowPtrC[ i + 1 ];
			k = rowPtrC[ i ];
			const ind_t* keys = &colIndsC[ k ];
			const ind_t noOfKeys = kend - k;
			fp_t* to = &nzsC[ k ];
			for ( ind_t i = 0; i < noOfKeys; ++i )
			{
				to[ i ] = acc[ keys[ i ] ];
				acc[ keys[ i ] ] = 0;
			}
		}
		
		for ( glbInd_t i = cutOffRowIndA; i < rend; ++i )
		{
			mend = rowPtrA[ i + 1 ];
			for ( k = rowPtrA[ i ]; k < mend; ++k )
			{
				jA = colIndsA[ k ];
				fA = nzsA[ k ];
				lend = rowPtrB[ jA + 1 ];
				for ( l = rowPtrB[ jA ]; l < lend; ++l )
				{
					jB = colIndsB[ l & 255 ];
					acc[ jB ] += fA * nzsB[ l & 255 ];
				}
			}

			kend = rowPtrC[ i + 1 ];
			k = rowPtrC[ i ];
			const ind_t* keys = &colIndsC[ k ];
			const ind_t noOfKeys = kend - k;
			fp_t* to = &nzsC[ k ];
			for ( ind_t i = 0; i < noOfKeys; ++i )
			{
				to[ i ] = acc[ keys[ i ] ];
				acc[ keys[ i ] ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson_incorrect::numeric_accumulator_random_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThread,
	const ui32& numThreads,
	nnz_t const* const threadRowPfxSum )
{
	nnz_t const* const __restrict__ rowPtrA = A.rowPtr;
	ind_t const* const __restrict__ colIndsA = A.colInd;
	fp_t const* const __restrict__ nzsA = A.nzs;

	nnz_t const* const __restrict__ rowPtrB = B.rowPtr;
	ind_t const* const __restrict__ colIndsB = B.colInd;
	fp_t const* const __restrict__ nzsB = B.nzs;

	nnz_t const* const __restrict__ rowPtrC = C.rowPtr;
	ind_t const* const __restrict__ colIndsC = C.colInd;
	fp_t* const __restrict__ nzsC = C.nzs;

	ind_t i;
	ind_t jA;
	ind_t jB;
	nnz_t k;
	nnz_t kend;
	nnz_t l;
	nnz_t lend;
	nnz_t mend;
	fp_t fA;
	#pragma omp parallel num_threads( numThreads ) \
		private( i, jA, jB, k, l, kend, lend, mend, fA )
	{
		const ui32 tid = omp_get_thread_num();
		fp_t* acc = accumulatorPerThread[ tid ];

		glbInd_t rend = threadRowPfxSum[ tid + 1 ];
		for ( glbInd_t i = threadRowPfxSum[ tid ]; i < rend; ++i )
		{
			mend = rowPtrA[ i + 1 ];
			for ( k = rowPtrA[ i ]; k < mend; ++k )
			{
				jA = colIndsA[ k ];
				fA = nzsA[ k ];
				lend = rowPtrB[ jA + 1 ];
				for ( l = rowPtrB[ jA ]; l < lend; ++l )
				{
					jB = colIndsB[ l ];
					acc[ jB & 255 ] += fA * nzsB[ l ];
				}
			}

			kend = rowPtrC[ i + 1 ];
			k = rowPtrC[ i ];

			const ind_t* keys = &colIndsC[ k ];
			const ind_t noOfKeys = kend - k;
			fp_t* to = &nzsC[ k ];
			for ( ind_t i = 0; i < noOfKeys; ++i )
			{
				to[ i ] = acc[ keys[ i ] ];
				acc[ keys[ i ] ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson_incorrect::numeric_accumulator_random_read(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThread,
	const ui32& numThreads,
	nnz_t const* const threadRowPfxSum )
{
	nnz_t const* const __restrict__ rowPtrA = A.rowPtr;
	ind_t const* const __restrict__ colIndsA = A.colInd;
	fp_t const* const __restrict__ nzsA = A.nzs;

	nnz_t const* const __restrict__ rowPtrB = B.rowPtr;
	ind_t const* const __restrict__ colIndsB = B.colInd;
	fp_t const* const __restrict__ nzsB = B.nzs;

	nnz_t const* const __restrict__ rowPtrC = C.rowPtr;
	ind_t const* const __restrict__ colIndsC = C.colInd;
	fp_t* const __restrict__ nzsC = C.nzs;

	ind_t i;
	ind_t jA;
	ind_t jB;
	nnz_t k;
	nnz_t kend;
	nnz_t l;
	nnz_t lend;
	nnz_t mend;
	fp_t fA;
	#pragma omp parallel num_threads( numThreads ) \
		private( i, jA, jB, k, l, kend, lend, mend, fA )
	{
		const ui32 tid = omp_get_thread_num();
		fp_t* acc = accumulatorPerThread[ tid ];

		glbInd_t rend = threadRowPfxSum[ tid + 1 ];
		for ( glbInd_t i = threadRowPfxSum[ tid ]; i < rend; ++i )
		{
			mend = rowPtrA[ i + 1 ];
			for ( k = rowPtrA[ i ]; k < mend; ++k )
			{
				jA = colIndsA[ k ];
				fA = nzsA[ k ];
				lend = rowPtrB[ jA + 1 ];
				for ( l = rowPtrB[ jA ]; l < lend; ++l )
				{
					jB = colIndsB[ l ];
					acc[ jB ] += fA * nzsB[ l ];
				}
			}

			kend = rowPtrC[ i + 1 ];
			k = rowPtrC[ i ];

			const ind_t* keys = &colIndsC[ k ];
			const ind_t noOfKeys = kend - k;
			fp_t* to = &nzsC[ k ];
			for ( ind_t i = 0; i < noOfKeys; ++i )
			{
				to[ i ] = acc[ keys[ i ] & 255 ];
				acc[ keys[ i ] & 255 ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson_incorrect::numeric_accumulator_random_read_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThread,
	const ui32& numThreads,
	nnz_t const* const threadRowPfxSum )
{
	nnz_t const* const __restrict__ rowPtrA = A.rowPtr;
	ind_t const* const __restrict__ colIndsA = A.colInd;
	fp_t const* const __restrict__ nzsA = A.nzs;

	nnz_t const* const __restrict__ rowPtrB = B.rowPtr;
	ind_t const* const __restrict__ colIndsB = B.colInd;
	fp_t const* const __restrict__ nzsB = B.nzs;

	nnz_t const* const __restrict__ rowPtrC = C.rowPtr;
	ind_t const* const __restrict__ colIndsC = C.colInd;
	fp_t* const __restrict__ nzsC = C.nzs;

	ind_t i;
	ind_t jA;
	ind_t jB;
	nnz_t k;
	nnz_t kend;
	nnz_t l;
	nnz_t lend;
	nnz_t mend;
	fp_t fA;
	#pragma omp parallel num_threads( numThreads ) \
		private( i, jA, jB, k, l, kend, lend, mend, fA )
	{
		const ui32 tid = omp_get_thread_num();
		fp_t* acc = accumulatorPerThread[ tid ];

		glbInd_t rend = threadRowPfxSum[ tid + 1 ];
		for ( glbInd_t i = threadRowPfxSum[ tid ]; i < rend; ++i )
		{
			mend = rowPtrA[ i + 1 ];
			for ( k = rowPtrA[ i ]; k < mend; ++k )
			{
				jA = colIndsA[ k ];
				fA = nzsA[ k ];
				lend = rowPtrB[ jA + 1 ];
				for ( l = rowPtrB[ jA ]; l < lend; ++l )
				{
					jB = colIndsB[ l ];
					acc[ jB & 255 ] += fA * nzsB[ l ];
				}
			}

			kend = rowPtrC[ i + 1 ];
			k = rowPtrC[ i ];

			const ind_t* keys = &colIndsC[ k ];
			const ind_t noOfKeys = kend - k;
			fp_t* to = &nzsC[ k ];
			for ( ind_t i = 0; i < noOfKeys; ++i )
			{
				to[ i ] = acc[ keys[ i ] & 255 ];
				acc[ keys[ i ] & 255 ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson_incorrect::numeric_accumulator_random_read_write_B_read(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThread,
	const ui32& numThreads,
	nnz_t const* const threadRowPfxSum )
{
	nnz_t const* const __restrict__ rowPtrA = A.rowPtr;
	ind_t const* const __restrict__ colIndsA = A.colInd;
	fp_t const* const __restrict__ nzsA = A.nzs;

	nnz_t const* const __restrict__ rowPtrB = B.rowPtr;
	ind_t const* const __restrict__ colIndsB = B.colInd;
	fp_t const* const __restrict__ nzsB = B.nzs;

	nnz_t const* const __restrict__ rowPtrC = C.rowPtr;
	ind_t const* const __restrict__ colIndsC = C.colInd;
	fp_t* const __restrict__ nzsC = C.nzs;

	ind_t i;
	ind_t jA;
	ind_t jB;
	nnz_t k;
	nnz_t kend;
	nnz_t l;
	nnz_t lend;
	nnz_t mend;
	fp_t fA;
	#pragma omp parallel num_threads( numThreads ) \
		private( i, jA, jB, k, l, kend, lend, mend, fA )
	{
		const ui32 tid = omp_get_thread_num();
		fp_t* acc = accumulatorPerThread[ tid ];

		glbInd_t rend = threadRowPfxSum[ tid + 1 ];
		for ( glbInd_t i = threadRowPfxSum[ tid ]; i < rend; ++i )
		{
			mend = rowPtrA[ i + 1 ];
			for ( k = rowPtrA[ i ]; k < mend; ++k )
			{
				jA = colIndsA[ k ];
				fA = nzsA[ k ];
				lend = rowPtrB[ jA + 1 ];
				for ( l = rowPtrB[ jA ]; l < lend; ++l )
				{
					jB = colIndsB[ l & 255 ];
					acc[ jB & 255 ] += fA * nzsB[ l & 255 ];
				}
			}

			kend = rowPtrC[ i + 1 ];
			k = rowPtrC[ i ];
			const ind_t* keys = &colIndsC[ k ];
			const ind_t noOfKeys = kend - k;
			fp_t* to = &nzsC[ k ];
			for ( ind_t i = 0; i < noOfKeys; ++i )
			{
				to[ i ] = acc[ keys[ i ] & 255 ];
				acc[ keys[ i ] & 255 ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson_incorrect::numeric_accumulator_random_read_write_B_read2(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThread,
	const ui32& numThreads,
	nnz_t const* const threadRowPfxSum,
	glbInd_t const* const cutOffRowIndAPerThread )
{
	nnz_t const* const __restrict__ rowPtrA = A.rowPtr;
	ind_t const* const __restrict__ colIndsA = A.colInd;
	fp_t const* const __restrict__ nzsA = A.nzs;

	nnz_t const* const __restrict__ rowPtrB = B.rowPtr;
	ind_t const* const __restrict__ colIndsB = B.colInd;
	fp_t const* const __restrict__ nzsB = B.nzs;

	nnz_t const* const __restrict__ rowPtrC = C.rowPtr;
	ind_t const* const __restrict__ colIndsC = C.colInd;
	fp_t* const __restrict__ nzsC = C.nzs;

	ind_t i;
	ind_t jA;
	ind_t jB;
	nnz_t k;
	nnz_t kend;
	nnz_t l;
	nnz_t lend;
	nnz_t mend;
	fp_t fA;
	#pragma omp parallel num_threads( numThreads ) \
		private( i, jA, jB, k, l, kend, lend, mend, fA )
	{
		const ui32 tid = omp_get_thread_num();
		fp_t* acc = accumulatorPerThread[ tid ];
		glbInd_t cutOffRowIndA = cutOffRowIndAPerThread[ tid ];

		glbNnz_t counterNnzB = rowPtrB[ threadRowPfxSum[ tid ] ];
		glbInd_t rend = threadRowPfxSum[ tid + 1 ];
		for ( glbInd_t i = threadRowPfxSum[ tid ]; i < cutOffRowIndA; ++i )
		{
			mend = rowPtrA[ i + 1 ];
			for ( k = rowPtrA[ i ]; k < mend; ++k )
			{
				jA = colIndsA[ k ];
				fA = nzsA[ k ];
				lend = rowPtrB[ jA + 1 ];
				for ( l = rowPtrB[ jA ]; l < lend; ++l, ++counterNnzB )
				{
					jB = colIndsB[ counterNnzB ];
					acc[ jB & 255 ] += fA * nzsB[ counterNnzB ];
				}
			}

			kend = rowPtrC[ i + 1 ];
			k = rowPtrC[ i ];
			const ind_t* keys = &colIndsC[ k ];
			const ind_t noOfKeys = kend - k;
			fp_t* to = &nzsC[ k ];
			for ( ind_t i = 0; i < noOfKeys; ++i )
			{
				to[ i ] = acc[ keys[ i ] & 255 ];
				acc[ keys[ i ] & 255 ] = 0;
			}
		}
		
		for ( glbInd_t i = cutOffRowIndA; i < rend; ++i )
		{
			mend = rowPtrA[ i + 1 ];
			for ( k = rowPtrA[ i ]; k < mend; ++k )
			{
				jA = colIndsA[ k ];
				fA = nzsA[ k ];
				lend = rowPtrB[ jA + 1 ];
				for ( l = rowPtrB[ jA ]; l < lend; ++l )
				{
					jB = colIndsB[ l & 255 ];
					acc[ jB & 255 ] += fA * nzsB[ l & 255 ];
				}
			}

			kend = rowPtrC[ i + 1 ];
			k = rowPtrC[ i ];
			const ind_t* keys = &colIndsC[ k ];
			const ind_t noOfKeys = kend - k;
			fp_t* to = &nzsC[ k ];
			for ( ind_t i = 0; i < noOfKeys; ++i )
			{
				to[ i ] = acc[ keys[ i ] & 255 ];
				acc[ keys[ i ] & 255 ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson_incorrect::numeric_accumulator_no_write(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThread,
	const ui32& numThreads,
	nnz_t const* const threadRowPfxSum )
{
	nnz_t const* const __restrict__ rowPtrA = A.rowPtr;
	ind_t const* const __restrict__ colIndsA = A.colInd;
	fp_t const* const __restrict__ nzsA = A.nzs;

	nnz_t const* const __restrict__ rowPtrB = B.rowPtr;
	ind_t const* const __restrict__ colIndsB = B.colInd;
	fp_t const* const __restrict__ nzsB = B.nzs;

	nnz_t const* const __restrict__ rowPtrC = C.rowPtr;
	ind_t const* const __restrict__ colIndsC = C.colInd;
	fp_t* const __restrict__ nzsC = C.nzs;

	ind_t i;
	ind_t jA;
	ind_t jB;
	nnz_t k;
	nnz_t kend;
	nnz_t l;
	nnz_t lend;
	nnz_t mend;
	fp_t fA;
	#pragma omp parallel num_threads( numThreads ) \
		private( i, jA, jB, k, l, kend, lend, mend, fA )
	{
		const ui32 tid = omp_get_thread_num();
		fp_t* acc = accumulatorPerThread[ tid ];

		fp_t temp = 0;
		glbInd_t rend = threadRowPfxSum[ tid + 1 ];
		for ( glbInd_t i = threadRowPfxSum[ tid ]; i < rend; ++i )
		{
			mend = rowPtrA[ i + 1 ];
			for ( k = rowPtrA[ i ]; k < mend; ++k )
			{
				jA = colIndsA[ k ];
				fA = nzsA[ k ];
				lend = rowPtrB[ jA + 1 ];
				for ( l = rowPtrB[ jA ]; l < lend; ++l )
				{
					jB = colIndsB[ l ];
					temp += fA * nzsB[ l ] + jB;
				}
			}

			kend = rowPtrC[ i + 1 ];
			k = rowPtrC[ i ];
			const ind_t* keys = &colIndsC[ k ];
			const ind_t noOfKeys = kend - k;
			fp_t* to = &nzsC[ k ];
			for ( ind_t i = 0; i < noOfKeys; ++i )
			{
				to[ i ] = temp + keys[ i ];
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson_incorrect::numeric_accumulator_no_write_B_read(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThread,
	const ui32& numThreads,
	nnz_t const* const threadRowPfxSum )
{
	nnz_t const* const __restrict__ rowPtrA = A.rowPtr;
	ind_t const* const __restrict__ colIndsA = A.colInd;
	fp_t const* const __restrict__ nzsA = A.nzs;

	nnz_t const* const __restrict__ rowPtrB = B.rowPtr;
	ind_t const* const __restrict__ colIndsB = B.colInd;
	fp_t const* const __restrict__ nzsB = B.nzs;

	nnz_t const* const __restrict__ rowPtrC = C.rowPtr;
	ind_t const* const __restrict__ colIndsC = C.colInd;
	fp_t* const __restrict__ nzsC = C.nzs;

	ind_t i;
	ind_t jA;
	ind_t jB;
	nnz_t k;
	nnz_t kend;
	nnz_t l;
	nnz_t lend;
	nnz_t mend;
	fp_t fA;
	#pragma omp parallel num_threads( numThreads ) \
		private( i, jA, jB, k, l, kend, lend, mend, fA )
	{
		const ui32 tid = omp_get_thread_num();
		fp_t* acc = accumulatorPerThread[ tid ];

		fp_t temp = 0;
		glbInd_t rend = threadRowPfxSum[ tid + 1 ];
		for ( glbInd_t i = threadRowPfxSum[ tid ]; i < rend; ++i )
		{
			mend = rowPtrA[ i + 1 ];
			for ( k = rowPtrA[ i ]; k < mend; ++k )
			{
				jA = colIndsA[ k ];
				fA = nzsA[ k ];
				lend = rowPtrB[ jA + 1 ];
				for ( l = rowPtrB[ jA ]; l < lend; ++l )
				{
					jB = colIndsB[ l & 255 ];
					temp += fA * nzsB[ l & 255 ];
				}
			}

			kend = rowPtrC[ i + 1 ];
			k = rowPtrC[ i ];
			// tacc.flush( &colIndsC[ k ], kend - k, &nzsC[ k ] );
			const ind_t* keys = &colIndsC[ k ];
			const ind_t noOfKeys = kend - k;
			fp_t* to = &nzsC[ k ];
			for ( ind_t i = 0; i < noOfKeys; ++i )
			{
				to[ i ] = temp + keys[ i ];
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr::gustavson_incorrect::numeric_accumulator_no_write_B_read2(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThread,
	const ui32& numThreads,
	nnz_t const* const threadRowPfxSum,
	glbInd_t const* const cutOffRowIndAPerThread )
{
	nnz_t const* const __restrict__ rowPtrA = A.rowPtr;
	ind_t const* const __restrict__ colIndsA = A.colInd;
	fp_t const* const __restrict__ nzsA = A.nzs;

	nnz_t const* const __restrict__ rowPtrB = B.rowPtr;
	ind_t const* const __restrict__ colIndsB = B.colInd;
	fp_t const* const __restrict__ nzsB = B.nzs;

	nnz_t const* const __restrict__ rowPtrC = C.rowPtr;
	ind_t const* const __restrict__ colIndsC = C.colInd;
	fp_t* const __restrict__ nzsC = C.nzs;

	ind_t i;
	ind_t jA;
	ind_t jB;
	nnz_t k;
	nnz_t kend;
	nnz_t l;
	nnz_t lend;
	nnz_t mend;
	fp_t fA;
	#pragma omp parallel num_threads( numThreads ) \
		private( i, jA, jB, k, l, kend, lend, mend, fA )
	{
		const ui32 tid = omp_get_thread_num();
		fp_t* acc = accumulatorPerThread[ tid ];
		glbInd_t cutOffRowIndA = cutOffRowIndAPerThread[ tid ];

		glbNnz_t counterNnzB = rowPtrB[ threadRowPfxSum[ tid ] ];
		glbInd_t rend = threadRowPfxSum[ tid + 1 ];
		fp_t temp = 0;
		for ( glbInd_t i = threadRowPfxSum[ tid ]; i < cutOffRowIndA; ++i )
		{
			mend = rowPtrA[ i + 1 ];
			for ( k = rowPtrA[ i ]; k < mend; ++k )
			{
				jA = colIndsA[ k ];
				fA = nzsA[ k ];
				lend = rowPtrB[ jA + 1 ];
				for ( l = rowPtrB[ jA ]; l < lend; ++l, ++counterNnzB )
				{
					jB = colIndsB[ counterNnzB ];
					temp += fA * nzsB[ counterNnzB ];
					// acc[ jB & 255 ] += fA * nzsB[ counterNnzB ];
				}
			}

			kend = rowPtrC[ i + 1 ];
			k = rowPtrC[ i ];
			const ind_t* keys = &colIndsC[ k ];
			const ind_t noOfKeys = kend - k;
			fp_t* to = &nzsC[ k ];
			for ( ind_t i = 0; i < noOfKeys; ++i )
			{
				to[ i ] = temp + keys[ i ];
				// to[ i ] = acc[ keys[ i ] & 255 ];
				// acc[ keys[ i ] & 255 ] = 0;
			}
		}
		
		for ( glbInd_t i = cutOffRowIndA; i < rend; ++i )
		{
			mend = rowPtrA[ i + 1 ];
			for ( k = rowPtrA[ i ]; k < mend; ++k )
			{
				jA = colIndsA[ k ];
				fA = nzsA[ k ];
				lend = rowPtrB[ jA + 1 ];
				for ( l = rowPtrB[ jA ]; l < lend; ++l )
				{
					jB = colIndsB[ l & 255 ];
					temp += fA * nzsB[ l & 255 ];
					// acc[ jB & 255 ] += fA * nzsB[ l & 255 ];
				}
			}

			kend = rowPtrC[ i + 1 ];
			k = rowPtrC[ i ];
			const ind_t* keys = &colIndsC[ k ];
			const ind_t noOfKeys = kend - k;
			fp_t* to = &nzsC[ k ];
			for ( ind_t i = 0; i < noOfKeys; ++i )
			{
				to[ i ] = temp + keys[ i ];
				// to[ i ] = acc[ keys[ i ] & 255 ];
				// acc[ keys[ i ] & 255 ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem >
ind_t*
rbr::gustavson_incorrect::findCutOffsA(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	ind_t* threadRowPfxSum,
	const ui32 numThreads )
{
	ind_t* cutOffs = util::alloc< ind_t >( numThreads );
	#pragma omp parallel num_threads( numThreads )
	{
		const ui32 tid = omp_get_thread_num();
		const nnz_t nnzSpanB =
			B.rowPtr[ threadRowPfxSum[ tid + 1 ] ] -
			B.rowPtr[ threadRowPfxSum[ tid ] ];

		bool goon = true;
		nnz_t nnzCountB = 0;
		ind_t cutOff = 0;
		for ( ind_t i = threadRowPfxSum[ tid ];
		      i < threadRowPfxSum[ tid + 1 ] && goon;
		      ++i )
		{
			for ( ind_t n = A.rowPtr[ i ]; n < A.rowPtr[ i + 1 ] && goon; ++n )
			{
				ind_t j = A.colInd[ n ];
				ind_t nnzB = B.rowPtr[ j + 1 ] - B.rowPtr[ j ];
				nnzCountB += nnzB;

				if ( nnzCountB > nnzSpanB )
					goon = false;
			}
			cutOff = i;
		}

		cutOffs[ tid ] = cutOff;
	}

	return cutOffs;
}
