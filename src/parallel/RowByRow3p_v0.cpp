
#include <vector>
#include <cstdlib>
#include <omp.h>
#include <immintrin.h>
#include <cassert>

#include "include/util/Memory.h"
#include "include/util/Spgemm.h"

// RowByRow3p per thread data
// -----------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem >
rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >::Thrd()
: B_prime_capacity( 0 ), B_prime_colInds( nullptr ),
  B_prime_nzs( nullptr ), B_prime_lengths( nullptr ),
  fpCacheCapacity( 64 ), fpCache( nullptr ), indCache( nullptr ),
  lengthCacheCapacity( 0 ), lengthCache( nullptr )
{
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem >
rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >::~Thrd()
{
	free();
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem >
void
rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >::init()
{
	B_prime_capacity = 0;
	B_prime_colInds = nullptr;
	B_prime_nzs = nullptr;
	B_prime_lengths = nullptr;

	fpCacheCapacity = 0;
	fpCache = nullptr;
	indCache = nullptr;
	lengthCacheCapacity = 0;
	lengthCache = nullptr;
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem >
void
rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >::free()
{
	if ( B_prime_capacity > 0 )
	{
		if ( B_prime_colInds != nullptr )
			B_prime_mem::free( static_cast< void* >( B_prime_colInds ),
			                   B_prime_capacity, sizeof( ind_t ) );

		if ( B_prime_nzs != nullptr )
			B_prime_mem::free( static_cast< void* >( B_prime_nzs ),
			                   B_prime_capacity, sizeof( fp_t ) );

		if ( B_prime_lengths != nullptr )
			B_prime_mem::free( static_cast< void* >( B_prime_lengths ),
			                   B_prime_capacity, sizeof( nnz_t ) );
	}

	if ( fpCache != nullptr ) util::free( fpCache );
	if ( indCache != nullptr ) util::free( indCache );
	if ( lengthCache != nullptr ) util::free( lengthCache );

	init();
}

// Data preparation
// -----------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem >
void
rbr3p::v0::alloc(
	const ui32& numThreads,
	const ui32& fpCacheCapacity,
	const ui32& indCacheCapacity,
	const ui32& lengthCacheCapacity,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >*** thrds_out )
{
	Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds = nullptr;	
	#pragma omp parallel num_threads( numThreads )
	{
		#pragma omp single
		thrds = util::alloc< Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* >(
			numThreads );

		const int ID = omp_get_thread_num();
		thrds[ ID ] = util::alloc< Thrd< ind_t, fp_t, nnz_t, B_prime_mem > >();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ ID ];
		t->init();

		t->fpCacheCapacity = fpCacheCapacity;
		t->lengthCacheCapacity = lengthCacheCapacity;
		t->fpCache = util::alloc< fp_t >( t->fpCacheCapacity );
		t->indCache = util::alloc< ind_t >( t->fpCacheCapacity );
		t->lengthCache = util::alloc< ind_t >( t->lengthCacheCapacity );
	}

	*thrds_out = thrds;
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename B_prime_mem >
void
rbr3p::v0::setCacheCapacity(
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds,
	const ui32& numThreads,
	const ui32& fpCacheCapacity,
	const ui32& indCacheCapacity,
	const ui32& lengthCacheCapacity )
{
	#pragma omp parallel num_threads( numThreads )
	{
		int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];

		t->fpCacheCapacity = fpCacheCapacity;
		t->lengthCacheCapacity = lengthCacheCapacity;
		// t->indCacheCapacity = indCacheCapacity;
	}
}

// Kernel routines for first run
// -----------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v0::symbolic_alloc(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd, ind_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds,
	char* maskIndices, ui32* masks, const ui32& noOfMasks )
{
	nnz_t perThrdCNnz[ numThreads + 1 ];
	#pragma omp parallel num_threads( numThreads )
	{
		const int _ID = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _ID ];
		const ind_t A_ROW_START = thrdRowPfxSum[ _ID ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _ID + 1 ];
		const nnz_t A_NZ_START = A.rowPtr[ A_ROW_START ];
		const nnz_t A_NZ_END = A.rowPtr[ A_ROW_END ];

		#pragma omp single
		{
			C.rowPtr = static_cast< nnz_t* >(
				C_mem::alloc( A.rowCount + 1, sizeof( nnz_t ) ) );
			C.rowPtr[ 0 ] = 0;
		}

		fp_t* acc = accumulatorPerThrd[ _ID ];
		ind_t* colIndSet = (ind_t*) t->B_prime_colInds;
		char* lookupArr = (char*) acc;

		// Preprocess partial C matrix
		// ---------------------------------------------------------------------
		nnz_t _C_nnz = 0;
		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
		{
			ind_t currRowNnz = 0;
			for ( nnz_t nA = A.rowPtr[ r ]; nA < A.rowPtr[ r + 1 ]; ++nA )
			{
				const ind_t jA = A.colInd[ nA ];
				for ( nnz_t nB = B.rowPtr[ jA ]; nB < B.rowPtr[ jA + 1 ]; ++nB )
				{
					const ind_t jB = B.colInd[ nB ];
					const ind_t hashedAddress = jB >> 3;
					const ind_t bitMask = ( 1 << ( jB & 7 ) );

					if ( ( lookupArr[ hashedAddress ] & bitMask ) == 0 )
					{
						lookupArr[ hashedAddress ] |= bitMask;
						colIndSet[ currRowNnz ] = jB;
						++currRowNnz;
					}
				}
			}

			// reset lookup bit vector
			for ( ind_t j = 0; j < currRowNnz; ++j )
			{
				const ind_t jC = colIndSet[ j ];
				const ind_t hashedAddress = jC >> 3;
				lookupArr[ hashedAddress ] = 0;
			}

			_C_nnz += currRowNnz;
			C.rowPtr[ r + 1 ] = _C_nnz;
			colIndSet = colIndSet + currRowNnz;
		}

		C.rowPtr[ A_ROW_END ] = 0;
		perThrdCNnz[ _ID + 1 ] = _C_nnz;

		// allocate C non-zero and col-index arrays
		// ---------------------------------------------------------------------
		#pragma omp barrier
		#pragma omp single
		{
			perThrdCNnz[ 0 ] = 0;
			for ( ui32 t = 0; t < numThreads; ++t )
				perThrdCNnz[ t + 1 ] += perThrdCNnz[ t ];

			C.rowCount = A.rowCount;
			C.nnz = perThrdCNnz[ numThreads ];
			C.rowPtr[ C.rowCount ] = C.nnz;
			C.nzs = static_cast< fp_t* >(
				C_mem::alloc( C.nnz, sizeof( fp_t ) ) );
			C.colInd = static_cast< ind_t* >(
				C_mem::alloc( C.nnz, sizeof( ind_t ) ) );
		}
		// ---------------------------------------------------------------------

		// add offset to C row-ptr
		const nnz_t C_NZ_BEGIN = perThrdCNnz[ _ID ];
		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
			C.rowPtr[ r ] += C_NZ_BEGIN;

		// fill col-index array
		const nnz_t C_NZ_END = perThrdCNnz[ _ID + 1 ];
		for ( nnz_t n = C_NZ_BEGIN, k = 0; n < C_NZ_END; ++n, ++k )
			C.colInd[ n ] = t->B_prime_colInds[ k ];

		// decide which hash function (bit-mask) to use for each row of C
		// ---------------------------------------------------------------------
		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
		{
			const nnz_t C_NZ_START = C.rowPtr[ r ];
			const nnz_t C_NZ_END = C.rowPtr[ r + 1 ];
			ui32 maskIndex = 0;
			while ( maskIndex < noOfMasks )
			{
				const ui32 MASK = masks[ maskIndex ];
				if ( C_NZ_END - C_NZ_START < MASK )
				{
					bool collided = false;
					nnz_t c;
					for ( c = C_NZ_START; ( c < C_NZ_END ) && !collided; ++c )
					{
						const ind_t jC = C.colInd[ c ] & MASK;
						const ind_t hashedAddress = jC >> 3;
						const ind_t bitMask = ( 1 << ( jC & 7 ) );
						collided = lookupArr[ hashedAddress ] & bitMask;
						lookupArr[ hashedAddress ] |= bitMask;
					}

					for ( nnz_t c2 = C_NZ_START; c2 < c; ++c2 )
					{
						const ind_t jC = C.colInd[ c2 ] & MASK;
						const ind_t hashedAddress = jC >> 3;
						lookupArr[ hashedAddress ] = 0;
					}

					if ( !collided )
						break;
				}
				++maskIndex;
			}
			maskIndices[ r ] = maskIndex;
		}

		// Generate B'
		// ---------------------------------------------------------------------
		// Fill in B' length-arr, index-arr, nonzero-arr
		// (this is the actual phase2)
		ui64 k = 0;
		for ( nnz_t n = A_NZ_START; n < A_NZ_END; ++n )
		{
			const ind_t jA = A.colInd[ n ];
			t->B_prime_lengths[ n - A_NZ_START ] =
				B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
			for ( nnz_t nB = B.rowPtr[ jA ];
			      nB < B.rowPtr[ jA + 1 ];
			      ++nB, ++k )
			{
				t->B_prime_colInds[ k ] = B.colInd[ nB ];
				t->B_prime_nzs[ k ] = B.nzs[ nB ];
			}
		}

		// Small padding for vectorization etc.
		// Occupying at most the size of a cache line (generally 64 bytes)
		for ( ; k < t->B_prime_capacity; ++k )
		{
			t->B_prime_colInds[ k ] = t->B_prime_colInds[ k - 1 ];
			t->B_prime_nzs[ k ] = 0;
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v0::symbolic(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd, ind_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds,
	char* maskIndices, ui32* masks, const ui32& noOfMasks )
{
	nnz_t perThrdCNnz[ numThreads + 1 ];
	C.rowPtr[ 0 ] = 0;
	#pragma omp parallel num_threads( numThreads )
	{
		const int _ID = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _ID ];
		const ind_t A_ROW_START = thrdRowPfxSum[ _ID ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _ID + 1 ];
		const nnz_t A_NZ_START = A.rowPtr[ A_ROW_START ];
		const nnz_t A_NZ_END = A.rowPtr[ A_ROW_END ];

		fp_t* acc = accumulatorPerThrd[ _ID ];
		ind_t* colIndSet = (ind_t*) t->B_prime_colInds;
		char* lookupArr = (char*) acc;

		// Preprocess partial C matrix
		// ---------------------------------------------------------------------
		nnz_t _C_nnz = 0;
		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
		{
			ind_t currRowNnz = 0;
			for ( nnz_t nA = A.rowPtr[ r ]; nA < A.rowPtr[ r + 1 ]; ++nA )
			{
				const ind_t jA = A.colInd[ nA ];
				for ( nnz_t nB = B.rowPtr[ jA ]; nB < B.rowPtr[ jA + 1 ]; ++nB )
				{
					const ind_t jB = B.colInd[ nB ];
					const ind_t hashedAddress = jB >> 3;
					const ind_t bitMask = ( 1 << ( jB & 7 ) );

					if ( ( lookupArr[ hashedAddress ] & bitMask ) == 0 )
					{
						lookupArr[ hashedAddress ] |= bitMask;
						colIndSet[ currRowNnz ] = jB;
						++currRowNnz;
					}
				}
			}

			// reset lookup bit vector
			for ( ind_t j = 0; j < currRowNnz; ++j )
			{
				const ind_t jC = colIndSet[ j ];
				const ind_t hashedAddress = jC >> 3;
				lookupArr[ hashedAddress ] = 0;
			}

			_C_nnz += currRowNnz;
			C.rowPtr[ r + 1 ] = _C_nnz;
			colIndSet = colIndSet + currRowNnz;
		}

		C.rowPtr[ A_ROW_END ] = 0;
		perThrdCNnz[ _ID + 1 ] = _C_nnz;

		// allocate C non-zero and col-index arrays
		// ---------------------------------------------------------------------
		#pragma omp barrier
		#pragma omp single
		{
			perThrdCNnz[ 0 ] = 0;
			for ( ui32 t = 0; t < numThreads; ++t )
				perThrdCNnz[ t + 1 ] += perThrdCNnz[ t ];

			C.rowCount = A.rowCount;
			C.nnz = perThrdCNnz[ numThreads ];
			C.rowPtr[ C.rowCount ] = C.nnz;
		}
		// ---------------------------------------------------------------------

		// add offset to C row-ptr
		const nnz_t C_NZ_BEGIN = perThrdCNnz[ _ID ];
		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
			C.rowPtr[ r ] += C_NZ_BEGIN;

		// fill col-index array
		const nnz_t C_NZ_END = perThrdCNnz[ _ID + 1 ];
		for ( nnz_t n = C_NZ_BEGIN, k = 0; n < C_NZ_END; ++n, ++k )
			C.colInd[ n ] = t->B_prime_colInds[ k ];

		// decide which hash function (bit-mask) to use for each row of C
		// ---------------------------------------------------------------------
		for ( ind_t r = A_ROW_START; r < A_ROW_END; ++r )
		{
			const nnz_t C_NZ_START = C.rowPtr[ r ];
			const nnz_t C_NZ_END = C.rowPtr[ r + 1 ];
			ui32 maskIndex = 0;
			while ( maskIndex < noOfMasks )
			{
				const ui32 MASK = masks[ maskIndex ];
				if ( C_NZ_END - C_NZ_START < MASK )
				{
					bool collided = false;
					nnz_t c;
					for ( c = C_NZ_START; ( c < C_NZ_END ) && !collided; ++c )
					{
						const ind_t jC = C.colInd[ c ] & MASK;
						const ind_t hashedAddress = jC >> 3;
						const ind_t bitMask = ( 1 << ( jC & 7 ) );
						collided = lookupArr[ hashedAddress ] & bitMask;
						lookupArr[ hashedAddress ] |= bitMask;
					}

					for ( nnz_t c2 = C_NZ_START; c2 < c; ++c2 )
					{
						const ind_t jC = C.colInd[ c2 ] & MASK;
						const ind_t hashedAddress = jC >> 3;
						lookupArr[ hashedAddress ] = 0;
					}

					if ( !collided )
						break;
				}
				++maskIndex;
			}
			maskIndices[ r ] = maskIndex;
		}

		// Generate B'
		// ---------------------------------------------------------------------
		// Fill in B' length-arr, index-arr, nonzero-arr
		// (this is the actual phase2)
		ui64 k = 0;
		for ( nnz_t n = A_NZ_START; n < A_NZ_END; ++n )
		{
			const ind_t jA = A.colInd[ n ];
			t->B_prime_lengths[ n - A_NZ_START ] =
				B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
			for ( nnz_t nB = B.rowPtr[ jA ];
			      nB < B.rowPtr[ jA + 1 ];
			      ++nB, ++k )
			{
				t->B_prime_colInds[ k ] = B.colInd[ nB ];
				t->B_prime_nzs[ k ] = B.nzs[ nB ];
			}
		}

		// Small padding for vectorization etc.
		// Occupying at most the size of a cache line (generally 64 bytes)
		for ( ; k < t->B_prime_capacity; ++k )
		{
			t->B_prime_colInds[ k ] = t->B_prime_colInds[ k - 1 ];
			t->B_prime_nzs[ k ] = 0;
		}
	}
}


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem,
           typename B_prime_mem >
void
rbr3p::v0::phase1(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	ind_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];
		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		const nnz_t A_NZ_START = A.rowPtr[ A_ROW_START ];
		const nnz_t A_NZ_END = A.rowPtr[ A_ROW_END ];

		ui64 B_prime_capacity = 0;
		for ( nnz_t n = A_NZ_START; n < A_NZ_END; ++n )
		{
			const ind_t c = A.colInd[ n ];
			B_prime_capacity += B.rowPtr[ c + 1 ] - B.rowPtr[ c ];
		}

		t->B_prime_capacity = B_prime_capacity;

		// Padding for vectorization issues
		t->B_prime_capacity += 16 - B_prime_capacity % 16;

		// Allocate B' matrix
		t->B_prime_colInds = static_cast< ind_t* >(
			B_prime_mem::alloc( t->B_prime_capacity, sizeof( ind_t ) ) );
		t->B_prime_nzs = static_cast< fp_t* >(
			B_prime_mem::alloc( t->B_prime_capacity, sizeof( fp_t ) ) );
		t->B_prime_lengths = static_cast< nnz_t* >(
			B_prime_mem::alloc( A_NZ_END - A_NZ_START, sizeof( nnz_t ) ) );
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem,
           typename B_prime_mem >
void
rbr3p::v0::phase2(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	ind_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];

		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		const nnz_t A_NZ_START = A.rowPtr[ A_ROW_START ];
		const nnz_t A_PRIME_NNZ = A.rowPtr[ A_ROW_END ] - A_NZ_START;
		ind_t const* const A_primeJs = A.colInd + A_NZ_START;

		ui64 k = 0;
		for ( nnz_t nA = 0; nA < A_PRIME_NNZ; ++nA )
		{
			const ind_t jA = A_primeJs[ nA ];
			t->B_prime_lengths[ nA ] = B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
			for ( nnz_t nB = B.rowPtr[ jA ];
			      nB < B.rowPtr[ jA + 1 ];
			      ++nB, ++k )
			{
				t->B_prime_colInds[ k ] = B.colInd[ nB ];
				t->B_prime_nzs[ k ] = B.nzs[ nB ];
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem,
           typename B_prime_mem >
void
rbr3p::v0::phase2(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, B_mem > const* const Bs,
	ind_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];
		const Csr< ind_t, fp_t, nnz_t, B_mem >& B = Bs[ _id ];

		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		const nnz_t A_NZ_START = A.rowPtr[ A_ROW_START ];
		const nnz_t A_PRIME_NNZ = A.rowPtr[ A_ROW_END ] - A_NZ_START;
		ind_t const* const A_primeJs = A.colInd + A_NZ_START;

		ui64 k = 0;
		for ( nnz_t nA = 0; nA < A_PRIME_NNZ; ++nA )
		{
			const ind_t jA = A_primeJs[ nA ];
			t->B_prime_lengths[ nA ] = B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
			for ( nnz_t nB = B.rowPtr[ jA ];
			      nB < B.rowPtr[ jA + 1 ];
			      ++nB, ++k )
			{
				t->B_prime_colInds[ k ] = B.colInd[ nB ];
				t->B_prime_nzs[ k ] = B.nzs[ nB ];
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v0::gustavson_batched(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd,
	ind_t const* const thrdRowPfxSum, 
	const ui32& numThreads,
	Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds,
	const ui32& batchBufferLength )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];

		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		const nnz_t A_NZ_START = A.rowPtr[ A_ROW_START ];
		const nnz_t A_PRIME_NNZ = A.rowPtr[ A_ROW_END ] - A_NZ_START;

		fp_t* const acc = accumulatorPerThrd[ _id ];

		nnz_t const* const A_rowPtr = A.rowPtr;
		ind_t const* const A_colInd = A.colInd;
		fp_t const* const A_nzs = A.nzs;
		
		nnz_t const* const B_rowPtr = B.rowPtr;
		ind_t const* const B_colInd = B.colInd;
		fp_t const* const B_nzs = B.nzs;

		nnz_t const* const C_rowPtr = C.rowPtr;
		ind_t const* const C_colInd = C.colInd;
		fp_t* const C_nzs = C.nzs;

		ind_t r = A_ROW_START;
		while ( r < A_ROW_END )
		{
			const ind_t rStart = r;
			ui32 bb = 0;
			for ( ; r < A_ROW_END && bb < batchBufferLength; ++r )
			{
				for ( nnz_t nA = A_rowPtr[ r ]; nA < A_rowPtr[ r + 1 ]; ++nA )
				{
					const ind_t jA = A_colInd[ nA ];
					for ( nnz_t nB = B_rowPtr[ jA ];
					      nB < B_rowPtr[ jA + 1 ];
					      ++nB, ++bb )
					{
						t->B_prime_nzs[ bb ] = B_nzs[ nB ];
						t->B_prime_colInds[ bb ] = B_colInd[ nB ];
					}
				}
			}

			ind_t* B_colInd_buff = t->B_prime_colInds;
			fp_t* B_nzs_buff = t->B_prime_nzs;
			for ( ind_t ri = rStart; ri < r; ++ri )
			{
				for ( nnz_t nA = A_rowPtr[ ri ]; nA < A_rowPtr[ ri + 1 ]; ++nA )
				{
					const fp_t fA = A_nzs[ nA ];
					const ind_t jA = A_colInd[ nA ];

					const nnz_t B_ROW_LEN = B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
					for ( nnz_t nB = 0; nB < B_ROW_LEN; ++nB )
					{
						const ind_t jB = B_colInd_buff[ nB ];
						const fp_t fB = B_nzs_buff[ nB ];
						acc[ jB ] += fA * fB;
					}

					B_colInd_buff = B_colInd_buff + B_ROW_LEN;
					B_nzs_buff = B_nzs_buff + B_ROW_LEN;
				}

				for ( nnz_t nC = C_rowPtr[ ri ]; nC < C_rowPtr[ ri + 1 ]; ++nC )
				{
					const ind_t jC = C_colInd[ nC ];
					C_nzs[ nC ] = acc[ jC ];
					acc[ jC ] = 0;
				}
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v0::gustavson_batched_1MB(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd,
	ind_t const* const thrdRowPfxSum, 
	const ui32& numThreads,
	Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds,
	const ui32& batchBufferLength,
	ind_t* const rowEndPerThrd )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];

		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		const nnz_t A_NZ_START = A.rowPtr[ A_ROW_START ];
		const nnz_t A_PRIME_NNZ = A.rowPtr[ A_ROW_END ] - A_NZ_START;

		fp_t* const acc = accumulatorPerThrd[ _id ];

		nnz_t const* const A_rowPtr = A.rowPtr;
		ind_t const* const A_colInd = A.colInd;
		fp_t const* const A_nzs = A.nzs;
		
		nnz_t const* const B_rowPtr = B.rowPtr;
		ind_t const* const B_colInd = B.colInd;
		fp_t const* const B_nzs = B.nzs;

		nnz_t const* const C_rowPtr = C.rowPtr;
		ind_t const* const C_colInd = C.colInd;
		fp_t* const C_nzs = C.nzs;

		ind_t r = A_ROW_START;
		// while ( r < A_ROW_END )
		{
			const ind_t rStart = r;
			ui32 bb = 0;
			for ( ; r < A_ROW_END && bb < batchBufferLength; ++r )
			{
				for ( nnz_t nA = A_rowPtr[ r ]; nA < A_rowPtr[ r + 1 ]; ++nA )
				{
					const ind_t jA = A_colInd[ nA ];
					for ( nnz_t nB = B_rowPtr[ jA ];
					      nB < B_rowPtr[ jA + 1 ];
					      ++nB, ++bb )
					{
						t->B_prime_nzs[ bb ] = B_nzs[ nB ];
						t->B_prime_colInds[ bb ] = B_colInd[ nB ];
					}
				}
			}

			ind_t* B_colInd_buff = t->B_prime_colInds;
			fp_t* B_nzs_buff = t->B_prime_nzs;
			for ( ind_t ri = rStart; ri < r; ++ri )
			{
				for ( nnz_t nA = A_rowPtr[ ri ]; nA < A_rowPtr[ ri + 1 ]; ++nA )
				{
					const fp_t fA = A_nzs[ nA ];
					const ind_t jA = A_colInd[ nA ];

					const nnz_t B_ROW_LEN = B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
					for ( nnz_t nB = 0; nB < B_ROW_LEN; ++nB )
					{
						const ind_t jB = B_colInd_buff[ nB ];
						const fp_t fB = B_nzs_buff[ nB ];
						acc[ jB ] += fA * fB;
					}

					B_colInd_buff = B_colInd_buff + B_ROW_LEN;
					B_nzs_buff = B_nzs_buff + B_ROW_LEN;
				}

				for ( nnz_t nC = C_rowPtr[ ri ]; nC < C_rowPtr[ ri + 1 ]; ++nC )
				{
					const ind_t jC = C_colInd[ nC ];
					C_nzs[ nC ] = acc[ jC ];
					acc[ jC ] = 0;
				}
			}
		}

		rowEndPerThrd[ _id ] = r;
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v0::gustavson_batched(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd,
	ind_t const* const thrdRowPfxSum, 
	const ui32& numThreads,
	Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds,
	const ui32& batchBufferLength )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];

		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		const nnz_t A_NZ_START = A.rowPtr[ A_ROW_START ];
		const nnz_t A_PRIME_NNZ = A.rowPtr[ A_ROW_END ] - A_NZ_START;

		fp_t* const acc = accumulatorPerThrd[ _id ];

		nnz_t const* const A_rowPtr = A.rowPtr;
		ind_t const* const A_colInd = A.colInd;
		fp_t const* const A_nzs = A.nzs;
		
		nnz_t const* const B_rowPtr = B.rowPtr;
		ind_t const* const B_colInd = B.colInd;
		fp_t const* const B_nzs = B.nzs;

		nnz_t const* const C_rowPtr = C.rowPtr;
		ind_t const* const C_colInd = C.colInd;
		fp_t* const C_nzs = C.nzs;

		ind_t r = A_ROW_START;
		while ( r < A_ROW_END )
		{
			const ind_t rStart = r;
			ui32 bb = 0;
			for ( ; r < A_ROW_END && bb < batchBufferLength; ++r )
			{
				for ( nnz_t nA = A_rowPtr[ r ]; nA < A_rowPtr[ r + 1 ]; ++nA )
				{
					const ind_t jA = A_colInd[ nA ];
					for ( nnz_t nB = B_rowPtr[ jA ];
					      nB < B_rowPtr[ jA + 1 ];
					      ++nB, ++bb )
					{
						t->B_prime_nzs[ bb ] = B_nzs[ nB ];
						t->B_prime_colInds[ bb ] = B_colInd[ nB ];
					}
				}
			}

			ind_t* B_colInd_buff = t->B_prime_colInds;
			fp_t* B_nzs_buff = t->B_prime_nzs;
			for ( ind_t ri = rStart; ri < r; ++ri )
			{
				const ui32 MASK = masks[ maskIndexPerRow[ ri ] ];
				for ( nnz_t nA = A_rowPtr[ ri ]; nA < A_rowPtr[ ri + 1 ]; ++nA )
				{
					const fp_t fA = A_nzs[ nA ];
					const ind_t jA = A_colInd[ nA ];

					const nnz_t B_ROW_LEN = B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
					for ( nnz_t nB = 0; nB < B_ROW_LEN; ++nB )
					{
						const ind_t jB = B_colInd_buff[ nB ];
						const fp_t fB = B_nzs_buff[ nB ];
						acc[ jB & MASK ] += fA * fB;
					}

					B_colInd_buff = B_colInd_buff + B_ROW_LEN;
					B_nzs_buff = B_nzs_buff + B_ROW_LEN;
				}

				for ( nnz_t nC = C_rowPtr[ ri ]; nC < C_rowPtr[ ri + 1 ]; ++nC )
				{
					const ind_t jC = C_colInd[ nC ] & MASK;
					C_nzs[ nC ] = acc[ jC ];
					acc[ jC ] = 0;
				}
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v0::numeric(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, nnz_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
	
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];

		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		nnz_t const* const __restrict__ A_rowPtr = A.rowPtr + A_ROW_START;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		ind_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr + A_ROW_START;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* const __restrict__ accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + A_ROW_START;
		const ind_t ROWS = A_ROW_END - A_ROW_START;
		const nnz_t A_NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const nnz_t K = k - A_NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ B_colInds[ m ] & mask ] += fA * B_nzs[ m ];
				}
			}

			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k ] & mask;
				C_nzs[ k ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v0::numeric(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd, nnz_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
	
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];

		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		nnz_t const* const __restrict__ A_rowPtr = A.rowPtr + A_ROW_START;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		ind_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr + A_ROW_START;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* const __restrict__ accumulator = accumulatorPerThrd[ _id ];

		const ind_t ROWS = A_ROW_END - A_ROW_START;
		const nnz_t A_NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const nnz_t K = k - A_NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ B_colInds[ m ] ] += fA * B_nzs[ m ];
				}
			}

			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k ];
				C_nzs[ k ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v0::p2_p3( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
                  const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
                  Csr< ind_t, fp_t, nnz_t, C_mem >& C,
                  fp_t** accumulatorPerThrd,
                  nnz_t const* const thrdRowPfxSum,
                  const ui32& numThreads,
                  Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];

		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		const nnz_t A_NZ_START = A.rowPtr[ A_ROW_START ];
		
		// Phase 2
		// ---------------------------------------------------------------------
		const nnz_t A_PRIME_NNZ = A.rowPtr[ A_ROW_END ] - A_NZ_START;
		ind_t const* const A_primeJs = A.colInd + A_NZ_START;

		ui64 k = 0;
		for ( nnz_t nA = 0; nA < A_PRIME_NNZ; ++nA )
		{
			const ind_t jA = A_primeJs[ nA ];
			t->B_prime_lengths[ nA ] = B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
			for ( nnz_t nB = B.rowPtr[ jA ];
			      nB < B.rowPtr[ jA + 1 ];
			      ++nB, ++k )
			{
				t->B_prime_colInds[ k ] = B.colInd[ nB ];
				t->B_prime_nzs[ k ] = B.nzs[ nB ];
			}
		}

		// Phase 3
		// ---------------------------------------------------------------------
		nnz_t const* const __restrict__ A_rowPtr = A.rowPtr + A_ROW_START;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		ind_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr + A_ROW_START;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* const __restrict__ accumulator = accumulatorPerThrd[ _id ];
		const ind_t ROWS = A_ROW_END - A_ROW_START;

		nnz_t x, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			for ( x = A_rowPtr[ i ]; x < A_rowPtr[ i + 1 ]; ++x )
			{
				fA = A_nzs[ x ];
				const nnz_t K = x - A_NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ B_colInds[ m ] ] += fA * B_nzs[ m ];
				}
			}

			const ind_t C_rowInd = i;
			for ( x = C_rowPtr[ C_rowInd ]; x < C_rowPtr[ C_rowInd + 1 ]; ++x )
			{
				const ind_t hashIndex = C_colInds[ x ];
				C_nzs[ x ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v0::p2_p3_v2( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
                     const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
                     Csr< ind_t, fp_t, nnz_t, C_mem >& C,
                     fp_t** accumulatorPerThrd,
                     nnz_t const* const thrdRowPfxSum,
                     const ui32& numThreads,
                     Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];

		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		const nnz_t A_NZ_START = A.rowPtr[ A_ROW_START ];
		
		// Phase 2
		// ---------------------------------------------------------------------
		const nnz_t A_PRIME_NNZ = A.rowPtr[ A_ROW_END ] - A_NZ_START;
		ind_t const* const A_primeJs = A.colInd + A_NZ_START;
		fp_t const* const A_primeNzs = A.nzs + A_NZ_START;

		ui64 k = 0;
		for ( nnz_t nA = 0; nA < A_PRIME_NNZ; ++nA )
		{
			const fp_t fA = A_primeNzs[ nA ];
			const ind_t jA = A_primeJs[ nA ];
			t->B_prime_lengths[ nA ] = B.rowPtr[ jA + 1 ] - B.rowPtr[ jA ];
			for ( nnz_t nB = B.rowPtr[ jA ];
			      nB < B.rowPtr[ jA + 1 ];
			      ++nB, ++k )
			{
				t->B_prime_colInds[ k ] = B.colInd[ nB ];
				t->B_prime_nzs[ k ] = B.nzs[ nB ] * fA;
			}
		}

		// Phase 3
		// ---------------------------------------------------------------------
		nnz_t const* const __restrict__ A_rowPtr = A.rowPtr + A_ROW_START;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		ind_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr + A_ROW_START;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* const __restrict__ accumulator = accumulatorPerThrd[ _id ];
		const ind_t ROWS = A_ROW_END - A_ROW_START;

		nnz_t x, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			for ( x = A_rowPtr[ i ]; x < A_rowPtr[ i + 1 ]; ++x )
			{
				const nnz_t K = x - A_NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ B_colInds[ m ] ] += B_nzs[ m ];
				}
			}

			const ind_t C_rowInd = i;
			for ( x = C_rowPtr[ C_rowInd ]; x < C_rowPtr[ C_rowInd + 1 ]; ++x )
			{
				const ind_t hashIndex = C_colInds[ x ];
				C_nzs[ x ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
		
	}

}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v0::numeric_incorrect(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	fp_t** accumulatorPerThrd, nnz_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
	
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];

		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		nnz_t const* const __restrict__ A_rowPtr = A.rowPtr + A_ROW_START;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		ind_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr + A_ROW_START;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* const __restrict__ accumulator = accumulatorPerThrd[ _id ];

		const ind_t ROWS = A_ROW_END - A_ROW_START;
		const nnz_t A_NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = 63;
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const nnz_t K = k - A_NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ B_colInds[ m ] & mask ] += fA * B_nzs[ m ];
				}
			}

			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k ] & mask;
				C_nzs[ k ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v0::numeric_simd(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, nnz_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];

		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		nnz_t const* const __restrict__ A_rowPtr = A.rowPtr + A_ROW_START;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		ind_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr + A_ROW_START;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* const __restrict__ accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + A_ROW_START;
		const ind_t ROWS = A_ROW_END - A_ROW_START;
		const nnz_t A_NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const nnz_t K = k - A_NZ_START;
				#pragma simd
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ B_colInds[ m ] & mask ] += fA * B_nzs[ m ];
				}
			}

			const ind_t C_rowInd = i;
			#pragma simd
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k ] & mask;
				C_nzs[ k ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr3p::v0::count( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
                  const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
                  Csr< ind_t, fp_t, nnz_t, C_mem >& C,
                  const ui32& cacheLineBytes,
                  ind_t const* const thrdRowPfxSum,
                  const ui32& numThreads,
                  ui64& noOfCacheLinesRead_B_prime,
                  ui64& noOfLoads_accumulator,
                  ui64& noOfLoads_A,
                  ui64& noOfLoads_B_prime,
                  ui64& noOfLoads_C,
                  ui64& noOfLoads_masks,
                  ui64& noOfStores_accumulator,
                  ui64& noOfStores_B_prime,
                  ui64& noOfStores_C )
{
	nnz_t const* const __restrict__ rowPtrA = A.rowPtr;
	ind_t const* const __restrict__ colIndsA = A.colInd;

	nnz_t const* const __restrict__ rowPtrB = B.rowPtr;
	ind_t const* const __restrict__ colIndsB = B.colInd;

	nnz_t const* const __restrict__ rowPtrC = C.rowPtr;
	ind_t const* const __restrict__ colIndsC = C.colInd;

	const ui64 FP_LENGTH = ( cacheLineBytes / sizeof( fp_t ) );
	const ui64 IND_LENGTH = ( cacheLineBytes / sizeof( ind_t ) );
	const ui64 NNZ_LENGTH = ( cacheLineBytes / sizeof( nnz_t ) );

	std::vector< ui64 > vNoOfMultiplications( numThreads, 0 );
	std::vector< ui64 > vNoOfCacheLinesRead_B( numThreads, 0 );
	#pragma omp parallel num_threads( numThreads )
	{
		ui64 _noOfMultiplications = 0;
		ui64 _noOfCacheLinesRead_B = 0;
		int _id = omp_get_thread_num();
		for ( ind_t i = thrdRowPfxSum[ _id ];
		      i < thrdRowPfxSum[ _id + 1 ];
		      ++i )
		{
			for ( nnz_t k = rowPtrA[ i ]; k < rowPtrA[ i + 1 ]; ++k )
			{
				ind_t jA = colIndsA[ k ];
				_noOfMultiplications += rowPtrB[ jA + 1 ] - rowPtrB[ jA ];
			}
		}

		vNoOfMultiplications[ _id ] = _noOfMultiplications;
		vNoOfCacheLinesRead_B[ _id ] = _noOfCacheLinesRead_B;
	}

	ui64 totalCacheLineReads_B_prime = 0;
	ui64 totalMultiplications = 0;
	for ( std::size_t i = 0; i < vNoOfMultiplications.size(); ++i )
	{
		totalMultiplications += vNoOfMultiplications[ i ];
	}
	totalCacheLineReads_B_prime =
		totalMultiplications / FP_LENGTH +
		totalMultiplications / IND_LENGTH +
		A.nnz / NNZ_LENGTH;

	// outputs
	noOfCacheLinesRead_B_prime = totalCacheLineReads_B_prime;
	noOfLoads_accumulator = totalMultiplications;
	noOfLoads_A = 2 * A.nnz + A.rowCount + 1;
	noOfLoads_B_prime = 2 * totalMultiplications + A.nnz;
	noOfLoads_C = 2 * C.nnz + 2 * C.rowCount;
	noOfLoads_masks = C.rowCount;
	noOfStores_accumulator = totalMultiplications;
	noOfStores_B_prime = totalMultiplications * 2 + A.nnz;
	noOfStores_C = C.nnz;
}


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem >
void
rbr3p::v0::count( const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
                  const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
                  Cs< ind_t, fp_t, nnz_t, C_mem >& C,
                  const ui32& cacheLineBytes,
                  ind_t const* const thrdRowPfxSum,
                  const ui32& numThreads,
                  ui64& noOfCacheLinesRead_B_prime,
                  ui64& noOfLoads_accumulator,
                  ui64& noOfLoads_A,
                  ui64& noOfLoads_B_prime,
                  ui64& noOfLoads_C,
                  ui64& noOfLoads_masks,
                  ui64& noOfStores_accumulator,
                  ui64& noOfStores_B_prime,
                  ui64& noOfStores_C )
{
	nnz_t const* const __restrict__ rowPtrA = A.ptr;
	ind_t const* const __restrict__ colIndsA = A.inds;

	nnz_t const* const __restrict__ rowPtrB = B.ptr;
	ind_t const* const __restrict__ colIndsB = B.inds;

	nnz_t const* const __restrict__ rowPtrC = C.ptr;
	ind_t const* const __restrict__ colIndsC = C.inds;

	const ui64 FP_LENGTH = ( cacheLineBytes / sizeof( fp_t ) );
	const ui64 IND_LENGTH = ( cacheLineBytes / sizeof( ind_t ) );
	const ui64 NNZ_LENGTH = ( cacheLineBytes / sizeof( nnz_t ) );

	std::vector< ui64 > vNoOfMultiplications( numThreads, 0 );
	std::vector< ui64 > vNoOfCacheLinesRead_B( numThreads, 0 );
	#pragma omp parallel num_threads( numThreads )
	{
		ui64 _noOfMultiplications = 0;
		ui64 _noOfCacheLinesRead_B = 0;
		int _id = omp_get_thread_num();
		for ( ind_t i = thrdRowPfxSum[ _id ];
		      i < thrdRowPfxSum[ _id + 1 ];
		      ++i )
		{
			for ( nnz_t k = rowPtrA[ i ]; k < rowPtrA[ i + 1 ]; ++k )
			{
				ind_t jA = colIndsA[ k ];
				_noOfMultiplications += rowPtrB[ jA + 1 ] - rowPtrB[ jA ];
			}
		}

		vNoOfMultiplications[ _id ] = _noOfMultiplications;
		vNoOfCacheLinesRead_B[ _id ] = _noOfCacheLinesRead_B;
	}

	ui64 totalCacheLineReads_B_prime = 0;
	ui64 totalMultiplications = 0;
	for ( std::size_t i = 0; i < vNoOfMultiplications.size(); ++i )
	{
		totalMultiplications += vNoOfMultiplications[ i ];
	}
	totalCacheLineReads_B_prime =
		totalMultiplications / FP_LENGTH +
		totalMultiplications / IND_LENGTH +
		A.ptr[ A.length ] / NNZ_LENGTH;

	// outputs
	noOfCacheLinesRead_B_prime = totalCacheLineReads_B_prime;
	noOfLoads_accumulator = totalMultiplications;
	noOfLoads_A = 2 * A.ptr[ A.length ] + A.length + 1;
	noOfLoads_B_prime = 2 * totalMultiplications + A.ptr[ A.length ];
	noOfLoads_C = 2 * C.ptr[ C.length ] + 2 * C.length;
	noOfLoads_masks = C.length;
	noOfStores_accumulator = totalMultiplications;
	noOfStores_B_prime = totalMultiplications * 2 + A.ptr[ A.length ];
	noOfStores_C = C.ptr[ C.length ];
}



// Kernel routines that use compiler intrinsics
// ############################################################################

#ifdef WITH_AVX
template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v0::numeric_avx_ss(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A, 
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, nnz_t const* const thrdRowPfxSum,
	nnz_t const* const thrdRowPfxSumAligned,
	const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];
		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		const ind_t A_ROW_START_ALIGNED = thrdRowPfxSumAligned[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = A.rowPtr + A_ROW_START;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		ind_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr + A_ROW_START;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* const accumulator = accumulatorPerThrd[ _id ];
		char const* const myMaskInds = maskIndexPerRow + A_ROW_START;

		const nnz_t A_NZ_START = A_rowPtr[ 0 ];
		nnz_t k, l, m = 0;
		fp_t fA;
		const ind_t PEEL_ROWS = A_ROW_START_ALIGNED - A_ROW_START;
		for ( ind_t i = 0; i < PEEL_ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const nnz_t K = k - A_NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ B_colInds[ m ] & mask ] += fA * B_nzs[ m ];
				}
			}

			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k ] & mask;
				C_nzs[ k ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}

		const ui32 FP_CACHE_CAPACITY = t->fpCacheCapacity;
		ui32 fpCacheSize = 0;
		fp_t* fpCache = t->fpCache;
		fp_t* C_nzsPtr = C_nzs + C.rowPtr[ A_ROW_START_ALIGNED ];
		const ind_t ROWS = A_ROW_END - A_ROW_START;
		for ( ind_t i = PEEL_ROWS; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const nnz_t K = k - A_NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ B_colInds[ m ] & mask ] += fA * B_nzs[ m ];
				}
			}

			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ];
			      k < C_rowPtr[ C_rowInd + 1 ];
			      ++k, ++fpCacheSize )
			{
				if ( fpCacheSize >= FP_CACHE_CAPACITY )
				{
					for ( ui32 s = 0; s < fpCacheSize; s += 4 )
					{
						fp_t* from = fpCache + s;
						fp_t* to = C_nzsPtr + s;
						_mm256_stream_pd( to, _mm256_load_pd( from ) );
					}

					C_nzsPtr = C_nzsPtr + fpCacheSize;
					fpCacheSize = 0;
				}

				const ind_t hashIndex = C_colInds[ k ] & mask;
				fpCache[ fpCacheSize ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}

		// leftover stream loop
		for ( ui32 i = 0; i < fpCacheSize; ++i )
			C_nzsPtr[ i ] = fpCache[ i ];
	}
}
#endif

#ifdef WITH_AVX512
template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v0::numeric_avx512_ss(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, nnz_t const* const thrdRowPfxSum,
	nnz_t const* const thrdRowPfxSumAligned,
	const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];
		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];
		const ind_t A_ROW_START_ALIGNED = thrdRowPfxSumAligned[ _id ];

		nnz_t const* const __restrict__ A_rowPtr = A.rowPtr + A_ROW_START;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		ind_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr + A_ROW_START;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* const accumulator = accumulatorPerThrd[ _id ];
		char const* const myMaskInds = maskIndexPerRow + A_ROW_START;

		const ind_t PEEL_ROWS = A_ROW_START_ALIGNED - A_ROW_START;
		const nnz_t A_NZ_START = A_rowPtr[ 0 ];
		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < PEEL_ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const nnz_t K = k - A_NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ B_colInds[ m ] & mask ] += fA * B_nzs[ m ];
				}
			}

			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k ] & mask;
				C_nzs[ k ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}

		const ui32 FP_CACHE_CAPACITY = t->fpCacheCapacity;
		ui32 fpCacheSize = 0;
		fp_t* fpCache = t->fpCache;
		fp_t* C_nzsPtr = C_nzs + C.rowPtr[ A_ROW_START_ALIGNED ];
		const ind_t ROWS = A_ROW_END - A_ROW_START;
		for ( ind_t i = PEEL_ROWS; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const nnz_t K = k - A_NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					accumulator[ B_colInds[ m ] & mask ] += fA * B_nzs[ m ];
				}
			}

			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ];
			      k < C_rowPtr[ C_rowInd + 1 ];
			      ++k, ++fpCacheSize )
			{
				if ( fpCacheSize >= FP_CACHE_CAPACITY )
				{
					for ( ui32 s = 0; s < fpCacheSize; s += 8 )
					{
						fp_t* from = fpCache + s;
						fp_t* to = C_nzsPtr + s;
						_mm512_stream_pd( to, _mm512_load_pd( from ) );
					}

					C_nzsPtr = C_nzsPtr + fpCacheSize;
					fpCacheSize = 0;
				}

				const ind_t hashIndex = C_colInds[ k ] & mask;
				fpCache[ fpCacheSize ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}

		// leftover stream loop
		for ( ui32 i = 0; i < fpCacheSize; ++i )
			C_nzsPtr[ i ] = fpCache[ i ];
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v0::numeric_avx512_sr(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, nnz_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];

		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];

		nnz_t const* const __restrict__ A_rowPtr = A.rowPtr + A_ROW_START;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		ind_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr + A_ROW_START;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* const __restrict__ accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + A_ROW_START;
		const ind_t ROWS = A_ROW_END - A_ROW_START;
		const nnz_t A_NZ_START = A_rowPtr[ 0 ];

		ind_t* const indCache = t->indCache;
		const ui32 IND_CACHE_CAPACITY = t->fpCacheCapacity;
		nnz_t k, l, m = 0;
		ui32 ind = IND_CACHE_CAPACITY;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				fA = A_nzs[ k ];
				const nnz_t K = k - A_NZ_START;
				for ( l = 0; l < B_lengths[ K ]; ++l, ++m, ++ind )
				{
					// __m512i _mm512_stream_load_si512 (void const* mem_addr)
					// void _mm512_stream_si512 (void* mem_addr, __m512i a)
					// void _mm512_storeu_si512 (void* mem_addr, __m512i a)
					if ( ind >= IND_CACHE_CAPACITY )
					{
						ui32 LIMIT = IND_CACHE_CAPACITY;
						if ( t->B_prime_capacity < m + IND_CACHE_CAPACITY )
							LIMIT = t->B_prime_capacity - m;
						for ( ui32 x = 0; x < LIMIT; x += 16 )
						{
							ind_t const* const from = B_colInds + m + x;
							ind_t* const to = indCache + x;
							_mm512_storeu_si512( to, _mm512_stream_load_si512( from ) );
							// for ( ui32 y = 0; y < 16; ++y ) { to[ y ] = from[ y ]; }
						}

						ind = 0;
					}

					// accumulator[ B_colInds[ m ] & mask ] += fA * B_nzs[ m ];
					accumulator[ indCache[ ind ] & mask ] += fA * B_nzs[ m ];
				}
			}

			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k ] & mask;
				C_nzs[ k ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}
#endif

// __m512d _mm512_load_pd (void const* mem_addr)
// __m512d _mm512_i64gather_pd (__m512i vindex, void const* base_addr, int scale)
// __m512d _mm512_i32gather_pd (__m256i vindex, void const* base_addr, int scale)
// __m512d _mm512_mask_load_pd (__m512d src, __mmask8 k, void const* mem_addr)
// void _mm512_store_pd (void* mem_addr, __m512d a)
// void _mm512_mask_store_pd (void* mem_addr, __mmask8 k, __m512d a)
// void _mm512_stream_pd (void* mem_addr, __m512d a)
// __m512i _mm512_stream_load_si512 (void const* mem_addr)
// void _mm512_stream_ps (void* mem_addr, __m512 a)

#ifdef __INTEL_COMPILER
template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename C_mem,
           typename B_prime_mem >
void
rbr3p::v0::numeric_prefetch(
	const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
	Csr< ind_t, fp_t, nnz_t, C_mem >& C,
	ui32 const* const masks, char const* const maskIndexPerRow,
	fp_t** accumulatorPerThrd, nnz_t const* const thrdRowPfxSum,
	const ui32& numThreads,
	rbr3p::v0::Thrd< ind_t, fp_t, nnz_t, B_prime_mem >** thrds )
{
	#pragma omp parallel num_threads( numThreads )
	{
		const int _id = omp_get_thread_num();
		Thrd< ind_t, fp_t, nnz_t, B_prime_mem >* t = thrds[ _id ];

		const ind_t A_ROW_START = thrdRowPfxSum[ _id ];
		const ind_t A_ROW_END = thrdRowPfxSum[ _id + 1 ];

		nnz_t const* const __restrict__ A_rowPtr = A.rowPtr + A_ROW_START;
		fp_t const* const __restrict__ A_nzs = A.nzs;

		ind_t const* const __restrict__ B_lengths = t->B_prime_lengths;
		ind_t const* const __restrict__ B_colInds = t->B_prime_colInds;
		fp_t const* const __restrict__ B_nzs = t->B_prime_nzs;

		nnz_t const* const __restrict__ C_rowPtr = C.rowPtr + A_ROW_START;
		ind_t* const __restrict__ C_colInds = C.colInd;
		fp_t* const __restrict__ C_nzs = C.nzs;

		fp_t* const __restrict__ accumulator = accumulatorPerThrd[ _id ];

		char const* const myMaskInds = maskIndexPerRow + A_ROW_START;
		const ind_t ROWS = A_ROW_END - A_ROW_START;
		const nnz_t A_NZ_START = A_rowPtr[ 0 ];

		nnz_t k, l, m = 0;
		fp_t fA;
		for ( ind_t i = 0; i < ROWS; ++i )
		{
			const ui32 mask = masks[ myMaskInds[ i ] ];		
			for ( k = A_rowPtr[ i ]; k < A_rowPtr[ i + 1 ]; ++k )
			{
				PREFETCH_T0( A_nzs + k, ( FETCH_DIST ) );
				fA = A_nzs[ k ];

				const nnz_t K = k - A_NZ_START;
				PREFETCH_T0( B_lengths + K, ( FETCH_DIST / 2 ) );

				for ( l = 0; l < B_lengths[ K ]; ++l, ++m )
				{
					PREFETCH_T0( B_colInds + m, FETCH_DIST );
					PREFETCH_T0( B_nzs + m, ( FETCH_DIST * 2 ) );

					accumulator[ B_colInds[ m ] & mask ] += fA * B_nzs[ m ];
				}
			}

			const ind_t C_rowInd = i;
			for ( k = C_rowPtr[ C_rowInd ]; k < C_rowPtr[ C_rowInd + 1 ]; ++k )
			{
				const ind_t hashIndex = C_colInds[ k ] & mask;
				C_nzs[ k ] = accumulator[ hashIndex ];
				accumulator[ hashIndex ] = 0;
			}
		}
	}
}

#endif

/*
StorVec1 = (float*) _mm_malloc((64*64)*4, 16);	
StorVec2 = (float*) _mm_malloc((64*64)*4, 16);
ResultVec = (float*) _mm_malloc((64*64)*4, 16);
.
.
.
for(int x=0; x<4096; x+=4)
{
*(__m128*) (ResultVec+x) = _mm_mul_ps(*((__m128 *)(StorVec1+x)), *((__m128 *)(StorVec2+x)));
}

---------------------------------------------

// put this macro in some common header :
#define PREFETCH_T0(addr,nrOfBytesAhead) _mm_prefetch(((char *)(addr))+nrOfBytesAhead,_MM_HINT_T0)

#define LIONELK_FETCH_DIST 128 // tune this value according to real world experiments

void LionelK()
{
__m128 *StorVec1 = (__m128*) _mm_malloc((64*64)*4, 16),
*StorVec2 = (__m128*) _mm_malloc((64*64)*4, 16),
*ResultVec = (__m128*) _mm_malloc((64*64)*4, 16);

for(int x=0; x<1024; x+=2) // Unrolled twice to fetch 32 Byte at a time, OK for PIII cache lines
{
PREFETCH_T0(ResultVec+x,LIONELK_FETCH_DIST);
PREFETCH_T0(StorVec1+x,LIONELK_FETCH_DIST);
PREFETCH_T0(StorVec2+x,LIONELK_FETCH_DIST);

ResultVec[x] = _mm_mul_ps(StorVec1[x],StorVec2[x]);
ResultVec[x+1] = _mm_mul_ps(StorVec1[x+1],StorVec2[x+1]);
}
}
*/
