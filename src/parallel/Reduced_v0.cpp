
#include <cassert>

#ifdef __INTEL_COMPILER
#include <immintrin.h>
#endif

#include "include/util/Generic.h"

template < typename zind_t, typename fp_t, typename znnz_t, typename mem >
spmv::Z< zind_t, fp_t, znnz_t, mem >::Z()
{
	init();
}

template < typename zind_t, typename fp_t, typename znnz_t, typename mem >
spmv::Z< zind_t, fp_t, znnz_t, mem >::~Z()
{
	free();
}

template < typename zind_t, typename fp_t, typename znnz_t, typename mem >
void
spmv::Z< zind_t, fp_t, znnz_t, mem >::init()
{
	length = 0;
	capacity = 0;
	lengths = nullptr;
	nzs = nullptr;
	inds = nullptr;
}

template < typename zind_t, typename fp_t, typename znnz_t, typename mem >
void
spmv::Z< zind_t, fp_t, znnz_t, mem >::free()
{
	if ( length > 0 )
	{
		if ( lengths != nullptr )
			mem::free( lengths, length, sizeof( znnz_t ) );

		length = 0;
		lengths = nullptr;
	}

	if ( capacity > 0 )
	{
		if ( nzs != nullptr )
			mem::free( nzs, capacity, sizeof( fp_t ) );
		if ( inds != nullptr )
			mem::free( inds, capacity, sizeof( zind_t ) );

		capacity = 0;
		nzs = nullptr;
		inds = nullptr;
	}
}



template < typename fp_t, typename mem >
void
spmv::allocate_ss_buffs( fp_t*** ssBuffs_out, const ui32& capacity,
						 const ui32& numThreads )
{
	fp_t** ssBuffs = nullptr;
	#pragma omp parallel num_threads( numThreads )
	{
		#pragma omp single
		{
			ssBuffs = (fp_t**) mem::alloc( numThreads, sizeof( fp_t* ) );
		}

		const ui32 ID = omp_get_thread_num();
		ssBuffs[ ID ] = (fp_t*) mem::alloc( capacity, sizeof( fp_t ) );
	}

	*ssBuffs_out = ssBuffs;
}



template < bool doAlloc,
           typename ind_t, typename fp_t, typename nnz_t,
           typename zind_t, typename znnz_t,
           typename A_mem, typename B_mem, typename C_mem, typename Z_mem >
void
spmv::symbolic(
	const ind_t& B_columns,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	ind_t const* const thrdPtrPfxSum,
	ui64 const* const thrdMultPfxSum,
	const ui32& NUM_THREADS, nnz_t* thrdCNnzPfxSum,
	fp_t** accumulators,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	spmv::Z< zind_t, fp_t, znnz_t, Z_mem >& data,
	ind_t* const extraMem )
{
	#pragma omp parallel num_threads( NUM_THREADS )
	{
		#pragma omp single
		{
			data.capacity = thrdMultPfxSum[ NUM_THREADS ];
			C.length = thrdPtrPfxSum[ NUM_THREADS ];

			if ( doAlloc )
			{
				data.nzs = (fp_t*)
					Z_mem::alloc( data.capacity, sizeof( fp_t ) );
				data.inds = (zind_t*)
					Z_mem::alloc( data.capacity, sizeof( zind_t ) );

				C.ptr = (nnz_t*)
					C_mem::alloc( C.length + 1, sizeof( nnz_t ) );
			}

		}

		const ui32 ID = omp_get_thread_num();
		const ind_t A_PTR_START = thrdPtrPfxSum[ ID ];
		const ind_t A_PTR_END = thrdPtrPfxSum[ ID + 1 ];
		const ui64 MULT_START = thrdMultPfxSum[ ID ];
		const ui64 MULT_END = thrdMultPfxSum[ ID + 1 ];

		zind_t* myZInds = data.inds + MULT_START;
		ind_t* myExtraMem = extraMem + MULT_START;
		fp_t* myZNzs = data.nzs + MULT_START;
		nnz_t myCNnz = 0;

		char* miniLookup = (char*) accumulators[ ID ];
		ind_t* C_list = ( (ind_t*) accumulators[ ID ] ) + B_columns;
		ui64 z = 0;
		for ( ind_t a = A_PTR_START; a < A_PTR_END; ++a )
		{
			ind_t currNnz = 0;
			ind_t zStart = z;
			for ( nnz_t na = A.ptr[ a ]; na < A.ptr[ a + 1 ]; ++na )
			{
				const ind_t IA = A.inds[ na ];
				const ind_t FA = A.nzs[ na ];
				for ( nnz_t nb = B.ptr[ IA ]; nb < B.ptr[ IA + 1 ]; ++nb, ++z )
				{
					const ind_t IB = B.inds[ nb ];
					const ind_t HASH_ADDRESS = IB >> 3;
					const ind_t BIT_MASK = ( 1 << ( IB & 7 ) );

					if ( ( miniLookup[ HASH_ADDRESS ] & BIT_MASK ) == 0 )
					{
						miniLookup[ HASH_ADDRESS ] |= BIT_MASK;
						C_list[ currNnz ] = IB;
						++currNnz;
					}

					myZNzs[ z ] = FA * B.nzs[ nb ];
					myZInds[ z ] = IA;
					myExtraMem[ z ] = IB;
				}
			}

			for ( nnz_t n = 0; n < currNnz; ++n )
			{
				const ind_t IC = C_list[ n ];
				const ind_t HASH_ADDRESS = IC >> 3;
				miniLookup[ HASH_ADDRESS ] = 0;
				C_list[ n ] = 0;
			}

			util::quicksort( myExtraMem + zStart, 0, z - zStart,
			                 myZNzs + zStart, myZInds + zStart );

			myCNnz += currNnz;
			C.ptr[ a + 1 ] = currNnz;
		}

		thrdCNnzPfxSum[ ID + 1 ] = myCNnz;
		#pragma omp barrier
		#pragma omp single
		{
			C.ptr[ 0 ] = 0;
			thrdCNnzPfxSum[ 0 ] = 0;
			for ( ui32 t = 0; t < NUM_THREADS; ++t )
				thrdCNnzPfxSum[ t + 1 ] += thrdCNnzPfxSum[ t ];

			data.length = thrdCNnzPfxSum[ NUM_THREADS ];

			if ( doAlloc )
			{
				data.lengths = (znnz_t*)
					Z_mem::alloc( data.length, sizeof( znnz_t ) );

				C.inds = (ind_t*)
					C_mem::alloc( data.length, sizeof( ind_t ) );
				C.nzs = (fp_t*)
					C_mem::alloc( data.length, sizeof( fp_t ) );
			}
		}

		// generate Z.ptr, C.inds
		ind_t* myZLengths = data.lengths + thrdCNnzPfxSum[ ID ];
		ind_t* myCInds = C.inds + thrdCNnzPfxSum[ ID ];
		ind_t* acc = (ind_t*) accumulators[ ID ];
		const ind_t BIT_MASK = ( 1 << 31 );
		const ind_t REVERSE_BIT_MASK = ~BIT_MASK;
		for ( ind_t a = A_PTR_START; a < A_PTR_END; ++a )
		{
			for ( nnz_t n = 0; n < C.ptr[ a + 1 ]; ++n )
				myZLengths[ n ] = 0;

			ind_t currNnz = 0;
			for ( nnz_t na = A.ptr[ a ]; na < A.ptr[ a + 1 ]; ++na )
			{
				const ind_t IA = A.inds[ na ];
				for ( nnz_t nb = B.ptr[ IA ]; nb < B.ptr[ IA + 1 ]; ++nb )
				{
					const ind_t IB = B.inds[ nb ];
					if ( acc[ IB ] == 0  )
					{
						acc[ IB ] = currNnz | BIT_MASK;
						++currNnz;
					}

					const ind_t C_INDEX = acc[ IB ] & REVERSE_BIT_MASK;
					myCInds[ C_INDEX ] = IB;
					++myZLengths[ C_INDEX ];
				}
			}

			for ( nnz_t n = 0; n < currNnz; ++n )
			{
				const ind_t IC = myCInds[ n ];
				acc[ IC ] = 0;
			}

			util::quicksort( myCInds, 0, currNnz, myZLengths );
			myCInds = myCInds + currNnz;
			myZLengths = myZLengths + currNnz;
		}

		#pragma omp barrier

		C.ptr[ A_PTR_START ] = thrdCNnzPfxSum[ ID ];
		for ( ind_t a = A_PTR_START + 1; a < A_PTR_END; ++a )
			C.ptr[ a ] += C.ptr[ a - 1 ];
	}

	C.ptr[ C.length ] = thrdCNnzPfxSum[ NUM_THREADS ];
}



template < typename fp_t, typename nnz_t,
           typename zind_t, typename znnz_t,
           typename Z_mem >
void
spmv::numeric( const spmv::Z< zind_t, fp_t, znnz_t, Z_mem >& data,
               fp_t const* const D,
               nnz_t const* const thrdCNnzPfxSum,
               ui64 const* const thrdMultPfxSum,
               const ui32& NUM_THREADS,
               fp_t* const C )
{
	#pragma omp parallel num_threads( NUM_THREADS )
	// for ( ui32 ID = 0; ID < NUM_THREADS; ++ID )
	{
		const ui32 ID = omp_get_thread_num();
		const nnz_t C_START = thrdCNnzPfxSum[ ID ];
		const nnz_t C_END = thrdCNnzPfxSum[ ID + 1 ];
		const nnz_t C_COUNT = C_END - C_START;

		ind_t* ZInds = data.inds + thrdMultPfxSum[ ID ];
		fp_t* ZNzs = data.nzs + thrdMultPfxSum[ ID ];
		fp_t* CNzs = C + C_START;
		znnz_t* ZLengths = data.lengths + C_START;
		for ( zind_t c = 0; c < C_COUNT; ++c )
		{
			fp_t res = 0;
			for ( znnz_t nz = 0; nz < ZLengths[ c ]; ++nz )
				res += D[ ZInds[ nz ] ] * ZNzs[ nz ];
			CNzs[ c ] = res;

			ZInds = ZInds + ZLengths[ c ];
			ZNzs = ZNzs + ZLengths[ c ];
		}
	}
}



template < typename fp_t, typename nnz_t,
           typename zind_t, typename znnz_t,
           typename Z_mem >
void
spmv::numeric( const spmv::Z< zind_t, fp_t, znnz_t, Z_mem >& data,
			   fp_t const* const D,
			   nnz_t const* const thrdCNnzPfxSum,
			   ui64 const* const thrdMultPfxSum,
			   const ui32& NUM_THREADS,
			   fp_t* const C,
			   fp_t** ssBuffPerThrd, const ui32& SS_BUFF_CAPACITY )
{
	#pragma omp parallel num_threads( NUM_THREADS )
	// for ( ui32 ID = 0; ID < NUM_THREADS; ++ID )
	{
		const ui32 ID = omp_get_thread_num();
		const nnz_t C_START = thrdCNnzPfxSum[ ID ];
		const nnz_t C_END = thrdCNnzPfxSum[ ID + 1 ];
		const nnz_t C_COUNT = C_END - C_START;
		fp_t* const ssBuff = ssBuffPerThrd[ ID ];

		ind_t* ZInds = data.inds + thrdMultPfxSum[ ID ];
		fp_t* ZNzs = data.nzs + thrdMultPfxSum[ ID ];
		fp_t* CNzs = C + C_START;
		znnz_t* ZLengths = data.lengths + C_START;
		ind_t b = 0;

		ui32 PEEL = ( C_START % 8 );
		if ( PEEL )
			PEEL = 8 - PEEL;

		zind_t c = 0;
		for ( c = 0; c < PEEL; ++c )
		{
			fp_t res = 0;
			for ( znnz_t nz = 0; nz < ZLengths[ c ]; ++nz )
				res += D[ ZInds[ nz ] ] * ZNzs[ nz ];
			CNzs[ c ] = res;
		}

		CNzs = CNzs + c;
		for ( ; c < C_COUNT; ++c )
		{
			fp_t res = 0;
			for ( znnz_t nz = 0; nz < ZLengths[ c ]; ++nz )
				res += D[ ZInds[ nz ] ] * ZNzs[ nz ];
			ssBuff[ b++ ] = res;

			if ( b >= SS_BUFF_CAPACITY )
			{
#ifdef __INTEL_COMPILER
				for ( ui32 s = 0; s < SS_BUFF_CAPACITY; s += 4 )
				{
					__m256d from = _mm256_load_pd( ssBuff + s );
					_mm256_stream_pd( CNzs, from );
					CNzs = CNzs + 4;
				}
#else
				for ( ui32 s = 0; s < SS_BUFF_CAPACITY; ++s )
					CNzs[ s ] = ssBuff[ s ];
				CNzs += SS_BUFF_CAPACITY;
#endif

				b = 0;
			}

			ZInds = ZInds + ZLengths[ c ];
			ZNzs = ZNzs + ZLengths[ c ];
		}

#ifdef __INTEL_COMPILER
		for ( ui32 s = 0; s < b; s += 4 )
		{
			__m256d from = _mm256_load_pd( ssBuff + s );
			_mm256_stream_pd( CNzs, from );
			CNzs = CNzs + 4;
		}

		b = b % 4;
		for ( ui32 s = 0; s < b; ++s )
			CNzs[ s ] = ssBuff[ s ];
#else
		for ( ui32 s = 0; s < b; ++s )
			CNzs[ s ] = ssBuff[ s ];
#endif

	}
}



template < typename zind_t, typename fp_t, typename mem >
spmv::Z_indirect< zind_t, fp_t, mem >::Z_indirect()
{
	init();
}

template < typename zind_t, typename fp_t, typename mem >
spmv::Z_indirect< zind_t, fp_t, mem >::~Z_indirect()
{
	free();
}

template < typename zind_t, typename fp_t, typename mem >
void
spmv::Z_indirect< zind_t, fp_t, mem >::init()
{
	capacity = 0;
	writeAddresses = nullptr;
	nzs = nullptr;
	inds = nullptr;
}

template < typename zind_t, typename fp_t, typename mem >
void
spmv::Z_indirect< zind_t, fp_t, mem >::free()
{
	if ( capacity >= 0 )
	{
		if ( writeAddresses != nullptr )
			mem::free( writeAddresses, capacity, sizeof( fp_t* ) );

		if ( nzs != nullptr )
			mem::free( nzs, capacity, sizeof( fp_t ) );

		if ( inds != nullptr )
			mem::free( inds, capacity, sizeof( zind_t ) );
	}

	init();
}



template < bool doAlloc,
           typename ind_t, typename fp_t, typename nnz_t, typename zind_t,
           typename A_mem, typename B_mem, typename C_mem, typename Z_mem >
void
spmv::symbolic(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	ind_t const* const thrdPtrPfxSum,
	ui64 const* const thrdMultPfxSum,
	const ui32& NUM_THREADS, nnz_t* thrdCNnzPfxSum,
	fp_t** accumulators,
	Cs< ind_t, fp_t, nnz_t, C_mem > &C,
	spmv::Z_indirect< zind_t, fp_t, Z_mem > &data,
	ind_t* const extraMem )
{
	#pragma omp parallel num_threads( NUM_THREADS )
	{
		#pragma omp single
		{
			data.capacity = thrdMultPfxSum[ NUM_THREADS ];
			C.length = thrdPtrPfxSum[ NUM_THREADS ];

			if ( doAlloc )
			{
				data.nzs = (fp_t*)
					Z_mem::alloc( data.capacity, sizeof( fp_t ) );
				data.inds = (zind_t*)
					Z_mem::alloc( data.capacity, sizeof( zind_t ) );
				data.writeAddresses = (fp_t**)
					Z_mem::alloc( data.capacity, sizeof( fp_t* ) );

				C.ptr = (nnz_t*)
					C_mem::alloc( C.length + 1, sizeof( nnz_t ) );
			}
		}

		const ui32 ID = omp_get_thread_num();
		const ind_t A_PTR_START = thrdPtrPfxSum[ ID ];
		const ind_t A_PTR_END = thrdPtrPfxSum[ ID + 1 ];
		const ui64 MULT_START = thrdMultPfxSum[ ID ];
		const ui64 MULT_END = thrdMultPfxSum[ ID + 1 ];

		// Generate C matrix (ptr and inds arrays)
		// --------------------------------------------------------------------
		nnz_t myCNnz = 0;
		char* miniLookup = (char*) accumulators[ ID ];
		{
			std::vector< ind_t > C_list;
			ui64 z = 0;
			for ( ind_t a = A_PTR_START; a < A_PTR_END; ++a )
			{
				ind_t currNnz = 0;
				ind_t zStart = z;
				for ( nnz_t na = A.ptr[ a ]; na < A.ptr[ a + 1 ]; ++na )
				{
					const ind_t IA = A.inds[ na ];
					for ( nnz_t nb = B.ptr[ IA ];
					      nb < B.ptr[ IA + 1 ];
					      ++nb, ++z )
					{
						const ind_t IB = B.inds[ nb ];
						const ind_t HASH_ADDRESS = IB >> 3;
						const ind_t BIT_MASK = ( 1 << ( IB & 7 ) );

						if ( ( miniLookup[ HASH_ADDRESS ] & BIT_MASK ) == 0 )
						{
							miniLookup[ HASH_ADDRESS ] |= BIT_MASK;
							C_list.push_back( IB );
							++currNnz;
						}
					}
				}

				for ( nnz_t n = 0; n < currNnz; ++n )
				{
					const ind_t IC = C_list[ myCNnz + n ];
					const ind_t HASH_ADDRESS = IC >> 3;
					miniLookup[ HASH_ADDRESS ] = 0;
				}

				std::sort( &C_list[ myCNnz ], &C_list[ myCNnz + currNnz ] );

				myCNnz += currNnz;
				C.ptr[ a + 1 ] = currNnz;
			}

			thrdCNnzPfxSum[ ID + 1 ] = myCNnz;

			#pragma omp barrier
			#pragma omp single
			{
				C.ptr[ 0 ] = 0;
				thrdCNnzPfxSum[ 0 ] = 0;
				for ( ui32 t = 0; t < NUM_THREADS; ++t )
					thrdCNnzPfxSum[ t + 1 ] += thrdCNnzPfxSum[ t ];

				if ( doAlloc )
				{
					const nnz_t CNNZ = thrdCNnzPfxSum[ NUM_THREADS ];
					C.inds = (ind_t*) C_mem::alloc( CNNZ, sizeof( ind_t ) );
					C.nzs = (fp_t*) C_mem::alloc( CNNZ, sizeof( fp_t ) );
				}
			}

			ind_t* myCInds = C.inds + thrdCNnzPfxSum[ ID ];
			for ( std::size_t i = 0; i < C_list.size(); ++i )
				myCInds[ i ] = C_list[ i ];
		}

		// Generate Z_indirect
		// --------------------------------------------------------------------
		zind_t* myZInds = data.inds + MULT_START;
		fp_t** myWriteAddresses = data.writeAddresses + MULT_START;
		fp_t* myZNzs = data.nzs + MULT_START;
		ind_t* myExtraMem = ( (ind_t*) extraMem ) + MULT_START;
		fp_t* myCNzs = C.nzs + thrdCNnzPfxSum[ ID ];
		ui64 z = 0;
		for ( ind_t a = A_PTR_START; a < A_PTR_END; ++a )
		{
			const ui64 zStart = z;
			ind_t currNnz = 0;
			for ( nnz_t na = A.ptr[ a ]; na < A.ptr[ a + 1 ]; ++na )
			{
				const ind_t IA = A.inds[ na ];
				const fp_t FA = A.nzs[ na ];
				for ( nnz_t nb = B.ptr[ IA ];
				      nb < B.ptr[ IA + 1 ];
				      ++nb, ++z )
				{
					const ind_t IB = B.inds[ nb ];
					myZNzs[ z ] = FA * B.nzs[ nb ];
					myZInds[ z ] = IA;
					myExtraMem[ z ] = IB;
				}
			}

			util::quicksort( myExtraMem + zStart, 0, z - zStart,
			                 myZNzs + zStart, myZInds + zStart );

			if ( z > zStart )
			{
				ind_t prev = myExtraMem[ zStart ];
				for ( ui64 k = zStart; k < z; ++k )
				{
					if ( prev != myExtraMem[ k ] )
					{
						++myCNzs;
						prev = myExtraMem[ k ];
					}

					myWriteAddresses[ k ] = myCNzs;
				}
				++myCNzs;
			}

		}

		#pragma omp barrier

		C.ptr[ A_PTR_START ] = thrdCNnzPfxSum[ ID ];
		for ( ind_t a = A_PTR_START + 1; a < A_PTR_END; ++a )
			C.ptr[ a ] += C.ptr[ a - 1 ];
	}

	C.ptr[ C.length ] = thrdCNnzPfxSum[ NUM_THREADS ];
}



template < typename fp_t, typename nnz_t, typename zind_t,
           typename Z_mem >
void
spmv::numeric( const spmv::Z_indirect< zind_t, fp_t, Z_mem >& data,
               fp_t const* const D,
               nnz_t const* const thrdCNnzPfxSum,
               ui64 const* const thrdMultPfxSum,
               const ui32& NUM_THREADS,
               fp_t* const C )
{
	#pragma omp parallel num_threads( NUM_THREADS )
	// for ( ui32 ID = 0; ID < NUM_THREADS; ++ID )
	{
		const ui32 ID = omp_get_thread_num();
		ui64 MULT_START = thrdMultPfxSum[ ID ];
		ui64 MULT_END = thrdMultPfxSum[ ID + 1 ];

		for ( nnz_t i = thrdCNnzPfxSum[ ID ];
		      i < thrdCNnzPfxSum[ ID + 1 ];
		      ++i )
			C[ i ] = 0;

		for ( ui64 z = MULT_START; z < MULT_END; ++z )
		{
			*data.writeAddresses[ z ] += data.nzs[ z ] * D[ data.inds[ z ] ];
		}
	}
}
