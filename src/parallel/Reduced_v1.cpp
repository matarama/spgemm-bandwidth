
#include <cassert>

template < typename coffset_t, typename fp_t, typename alength_t,
		   typename mem >
regularized::AB< coffset_t, fp_t, alength_t, mem >::AB()
{
	init();
}



template < typename coffset_t, typename fp_t, typename alength_t,
		   typename mem >
regularized::AB< coffset_t, fp_t, alength_t, mem >::~AB()
{
	free();
}



template < typename coffset_t, typename fp_t, typename alength_t,
		   typename mem >
void
regularized::AB< coffset_t, fp_t, alength_t, mem >::init()
{
	ALengths = nullptr;
	COffsets = nullptr;
	nzs = nullptr;

	ANnz = 0;
	length = 0;
}



template < typename coffset_t, typename fp_t, typename alength_t,
		   typename mem >
void
regularized::AB< coffset_t, fp_t, alength_t, mem >::free()
{
	if ( ANnz > 0 )
	{
		if ( ALengths != nullptr )
			mem::free( (void*) ALengths, ANnz, sizeof( alength_t ) );
		if ( COffsets != nullptr )
			mem::free( (void*) COffsets, length, sizeof( coffset_t ) );
		if ( nzs != nullptr )
			mem::free( (void*) nzs, length, sizeof( fp_t ) );
	}

	init();
}



template < bool doAlloc,
           typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename B_mem, typename C_mem, typename AB_mem >
void
regularized::symbolic(
	const ind_t& B_columns,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	ind_t const* const thrdPtrPfxSum, ui64 const* const thrdMultsPfxSum,
	fp_t** accumulators, const ui32& NUM_THREADS )
{
	assert( sizeof( fp_t ) > sizeof( ind_t ) );

	nnz_t perThrdCNnzPfxSum[ NUM_THREADS + 1 ];
	const ind_t BIT_MASK = ( 1 << 31 );
	const ind_t REVERSE_BIT_MASK = ~BIT_MASK;
	#pragma omp parallel num_threads( NUM_THREADS )
	{
		#pragma omp single
		{
			C.length = A.length;

			ab.ANnz = A.ptr[ A.length ];
			ab.length = thrdMultsPfxSum[ NUM_THREADS ];
			if ( doAlloc )
			{
				C.ptr = (nnz_t*)
					C_mem::alloc( C.length + 1, sizeof( nnz_t ) );

				ab.ALengths = (alength_t*)
					AB_mem::alloc( ab.ANnz, sizeof( alength_t ) );
				ab.COffsets = (coffset_t*)
					AB_mem::alloc( ab.length, sizeof( coffset_t ) );
				ab.nzs = (fp_t*)
					AB_mem::alloc( ab.length, sizeof( fp_t ) );
			}
		}

		const ui32 ID = omp_get_thread_num();
		const ind_t A_START = thrdPtrPfxSum[ ID ];
		const ind_t A_END = thrdPtrPfxSum[ ID + 1 ];
		const ind_t MULT_START = thrdMultsPfxSum[ ID ];
		const ind_t MULT_END = thrdMultsPfxSum[ ID + 1 ];
		ind_t* lookupArr = (ind_t*) accumulators[ ID ];
		ind_t* C_indices = ( (ind_t*) accumulators[ ID ] ) + B_columns;

		// std::cout << "thread-" << ID << std::endl;
		// std::cout << "MULT_START=" << MULT_START << std::endl;
		// std::cout << "MULT_END=" << MULT_END << std::endl;
		// std::cout << std::endl;

		// alength_t* myALengths = ab.ALenghts + A.ptr[ A_START ];
		// dind_t* myDIndices = ab.DIndices + A.ptr[ A_START ];
		nnz_t* myCPtr = C.ptr + A_START;
		coffset_t* myCOffsets = ab.COffsets + MULT_START;
		fp_t* myABNzs = ab.nzs + MULT_START;
		nnz_t myCNnz = 0;

		// calculate nnz in each row/column of C
		ui64 z = 0;
		for ( ind_t a = A_START; a < A_END; ++a )
		{
			ind_t currNnz = 0;
			for ( nnz_t na = A.ptr[ a ]; na < A.ptr[ a + 1 ]; ++na )
			{
				const ind_t IA = A.inds[ na ];
				const fp_t FA = A.nzs[ na ];

				ab.ALengths[ na ] = B.ptr[ IA + 1 ] - B.ptr[ IA ];
				// std::stringstream ss;
				// ss << "t-" << ID << "->" << na << "=" << AB.DIndices[ na ] << std::endl;
				// std::cout << ss.str();

				for ( nnz_t nb = B.ptr[ IA ]; nb < B.ptr[ IA + 1 ]; ++nb, ++z )
				{
					const ind_t IB = B.inds[ nb ];

					if ( lookupArr[ IB ] == 0 )
					{
						lookupArr[ IB ] = BIT_MASK | currNnz;
						C_indices[ currNnz ] = IB;
						++currNnz;
					}

					myCOffsets[ z ] = lookupArr[ IB ] & REVERSE_BIT_MASK;
					myABNzs[ z ] = FA * B.nzs[ nb ];
				}
			}

			for ( ind_t n = 0; n < currNnz; ++n )
			{
				lookupArr[ C_indices[ n ] ] = 0;
				C_indices[ n ] = 0;
			}

			C.ptr[ a + 1 ] = currNnz;
			myCNnz += currNnz;
		}

		perThrdCNnzPfxSum[ ID + 1 ] = myCNnz;

		#pragma omp barrier
		#pragma omp single
		{
			perThrdCNnzPfxSum[ 0 ] = 0;
			for ( ui32 t = 0; t < NUM_THREADS; ++t )
				perThrdCNnzPfxSum[ t + 1 ] += perThrdCNnzPfxSum[ t ];

			const nnz_t C_NNZ = perThrdCNnzPfxSum[ NUM_THREADS ];

			C.ptr[ 0 ] = 0;
			C.ptr[ C.length ] = C_NNZ;

			if ( doAlloc )
			{
				C.inds = (ind_t*) C_mem::alloc( C_NNZ, sizeof( ind_t ) );
				C.nzs = (fp_t*) C_mem::alloc( C_NNZ, sizeof( fp_t ) );
			}

			// std::cout << "perThrdCNnzPfxSum:";
			// for ( ui32 t = 0; t < NUM_THREADS + 1; ++t )
			// 	std::cout << " " << perThrdCNnzPfxSum[ t ];
			// std::cout << std::endl;
		}

		// Generate C.ptr
		C.ptr[ A_START ] = perThrdCNnzPfxSum[ ID ];
		for ( ind_t i = A_START + 1; i < A_END; ++i )
			C.ptr[ i ] += C.ptr[ i - 1 ];

		// --------------------------------------------------------------------
		#pragma omp barrier
		// --------------------------------------------------------------------

		z = 0;
		for ( ind_t i = A_START; i < A_END; ++i )
		{
			nnz_t totalMultsRow = 0;
			for ( nnz_t na = A.ptr[ i ]; na < A.ptr[ i + 1 ]; ++na )
				totalMultsRow += ab.ALengths[ na ];

			const nnz_t offset = C.ptr[ i ];
			for ( nnz_t l = 0; l < totalMultsRow; ++l, ++z )
				myCOffsets[ z ] += offset;
		}

		// Second symbolic multiplication for C.inds
		ind_t* myCInds = C.inds + perThrdCNnzPfxSum[ ID ];
		char* miniLookup = (char*) accumulators[ ID ];
		for ( ind_t a = A_START; a < A_END; ++a )
		{
			ind_t currNnz = 0;
			for ( nnz_t na = A.ptr[ a ]; na < A.ptr[ a + 1 ]; ++na )
			{
				const ind_t IA = A.inds[ na ];
				for ( nnz_t nb = B.ptr[ IA ]; nb < B.ptr[ IA + 1 ]; ++nb )
				{
					const ind_t IB = B.inds[ nb ];
					const ind_t HASH_ADDRESS = IB >> 3;
					const ind_t BIT_MASK = ( 1 << ( IB & 7 ) );

					if ( ( miniLookup[ HASH_ADDRESS ] & BIT_MASK )== 0 )
					{
						miniLookup[ HASH_ADDRESS ] |= BIT_MASK;
						myCInds[ currNnz ] = IB;
						++currNnz;
					}
				}
			}

			for ( ind_t n = 0; n < currNnz; ++n )
			{
				const ind_t HASH_ADDRESS = myCInds[ n ] >> 3;
				miniLookup[ HASH_ADDRESS ] = 0;
			}

			myCInds = myCInds + currNnz;
		}
	}
}



template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
regularized::numeric(
	const regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	ind_t const* const thrdPtrPfxSum, ui64 const* const thrdMultsPfxSum,
	const ui32& NUM_THREADS )
{
	#pragma omp parallel num_threads( NUM_THREADS )
	{
		const ui32 ID = omp_get_thread_num();
		const ind_t A_START = thrdPtrPfxSum[ ID ];
		const ind_t A_END = thrdPtrPfxSum[ ID + 1 ];

		const nnz_t C_NZ_START = C.ptr[ A_START ];
		const nnz_t C_NZ_END = C.ptr[ A_END ];
		for ( nnz_t nc = C_NZ_END -1; nc >= C_NZ_START; --nc )
			C.nzs[ nc ] = 0;

		const fp_t* myABNzs = ab.nzs + thrdMultsPfxSum[ ID ];
		const coffset_t* myCOffsets = ab.COffsets + thrdMultsPfxSum[ ID ];
		const nnz_t A_NZ_START = A.ptr[ A_START ];
		const nnz_t A_NZ_END = A.ptr[ A_END ];
		ui64 z = 0;
		for ( nnz_t na = A_NZ_START; na < A_NZ_END; ++na )
		{
            const fp_t FD = D[ A.inds[ na ] ];
			for ( alength_t l = 0; l < ab.ALengths[ na ]; ++l, ++z )
				C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
regularized::numeric_unrolled8(
	const regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	ind_t const* const thrdPtrPfxSum, ui64 const* const thrdMultsPfxSum,
	const ui32& NUM_THREADS )
{
	#pragma omp parallel num_threads( NUM_THREADS )
	{
		const ui32 ID = omp_get_thread_num();
		const ind_t A_START = thrdPtrPfxSum[ ID ];
		const ind_t A_END = thrdPtrPfxSum[ ID + 1 ];

		const nnz_t C_NZ_START = C.ptr[ A_START ];
		const nnz_t C_NZ_END = C.ptr[ A_END ];
		for ( nnz_t nc = C_NZ_END -1; nc >= C_NZ_START; --nc )
			C.nzs[ nc ] = 0;

		const fp_t* myABNzs = ab.nzs + thrdMultsPfxSum[ ID ];
		const coffset_t* myCOffsets = ab.COffsets + thrdMultsPfxSum[ ID ];
		const nnz_t A_NZ_START = A.ptr[ A_START ];
		const nnz_t A_NZ_END = A.ptr[ A_END ];
		ui64 z = 0;
		for ( nnz_t na = A_NZ_START; na < A_NZ_END; ++na )
		{
            const fp_t FD = D[ A.inds[ na ] ];
			switch( ab.ALengths[ na ] )
			{
				case 1:
					C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
					break;
				case 2:
					C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
					C.nzs[ myCOffsets[ z + 1 ] ] += FD * myABNzs[ z + 1 ];
					break;
				case 3:
					C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
					C.nzs[ myCOffsets[ z + 1 ] ] += FD * myABNzs[ z + 1 ];
					C.nzs[ myCOffsets[ z + 2 ] ] += FD * myABNzs[ z + 2 ];
					break;
				case 4:
					C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
					C.nzs[ myCOffsets[ z + 1 ] ] += FD * myABNzs[ z + 1 ];
					C.nzs[ myCOffsets[ z + 2 ] ] += FD * myABNzs[ z + 2 ];
					C.nzs[ myCOffsets[ z + 3 ] ] += FD * myABNzs[ z + 3 ];
					break;
				case 5:
					C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
					C.nzs[ myCOffsets[ z + 1 ] ] += FD * myABNzs[ z + 1 ];
					C.nzs[ myCOffsets[ z + 2 ] ] += FD * myABNzs[ z + 2 ];
					C.nzs[ myCOffsets[ z + 3 ] ] += FD * myABNzs[ z + 3 ];
					C.nzs[ myCOffsets[ z + 4 ] ] += FD * myABNzs[ z + 4 ];
					break;
				case 6:
					C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
					C.nzs[ myCOffsets[ z + 1 ] ] += FD * myABNzs[ z + 1 ];
					C.nzs[ myCOffsets[ z + 2 ] ] += FD * myABNzs[ z + 2 ];
					C.nzs[ myCOffsets[ z + 3 ] ] += FD * myABNzs[ z + 3 ];
					C.nzs[ myCOffsets[ z + 4 ] ] += FD * myABNzs[ z + 4 ];
					C.nzs[ myCOffsets[ z + 5 ] ] += FD * myABNzs[ z + 5 ];
					break;
				case 7:
					C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
					C.nzs[ myCOffsets[ z + 1 ] ] += FD * myABNzs[ z + 1 ];
					C.nzs[ myCOffsets[ z + 2 ] ] += FD * myABNzs[ z + 2 ];
					C.nzs[ myCOffsets[ z + 3 ] ] += FD * myABNzs[ z + 3 ];
					C.nzs[ myCOffsets[ z + 4 ] ] += FD * myABNzs[ z + 4 ];
					C.nzs[ myCOffsets[ z + 5 ] ] += FD * myABNzs[ z + 5 ];
					C.nzs[ myCOffsets[ z + 6 ] ] += FD * myABNzs[ z + 6 ];
					break;
				case 8:
					C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
					C.nzs[ myCOffsets[ z + 1 ] ] += FD * myABNzs[ z + 1 ];
					C.nzs[ myCOffsets[ z + 2 ] ] += FD * myABNzs[ z + 2 ];
					C.nzs[ myCOffsets[ z + 3 ] ] += FD * myABNzs[ z + 3 ];
					C.nzs[ myCOffsets[ z + 4 ] ] += FD * myABNzs[ z + 4 ];
					C.nzs[ myCOffsets[ z + 5 ] ] += FD * myABNzs[ z + 5 ];
					C.nzs[ myCOffsets[ z + 6 ] ] += FD * myABNzs[ z + 6 ];
					C.nzs[ myCOffsets[ z + 7 ] ] += FD * myABNzs[ z + 7 ];
					break;
				default:
					for ( alength_t l = 0; l < ab.ALengths[ na ]; ++l )
						C.nzs[ myCOffsets[ z + l ] ] += FD * myABNzs[ z + l ];
			}

			z += ab.ALengths[ na ];
		}
	}
}




template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
regularized::numeric(
	const regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	ui64 const* const thrdMultsPfxSum,
	nnz_t const* const aSlicePfxSum,
	nnz_t const* const cSlicePfxSum,
	ui32 const* const slicesPerThrdPfxSum,
	const ui32& NUM_THREADS )
{
	#pragma omp parallel num_threads( NUM_THREADS )
	{
		const ui32 ID = omp_get_thread_num();
		ui64 z = 0;
		for ( ui32 s = slicesPerThrdPfxSum[ ID ];
			  s < slicesPerThrdPfxSum[ ID + 1 ];
			  ++s )
		{
			for ( nnz_t nc = cSlicePfxSum[ s + 1 ] - 1;
				  nc >= cSlicePfxSum[ s ];
				  --nc )
				C.nzs[ nc ] = 0;

			const fp_t* myABNzs = ab.nzs + thrdMultsPfxSum[ ID ];
			const coffset_t* myCOffsets = ab.COffsets + thrdMultsPfxSum[ ID ];
			for ( nnz_t na = aSlicePfxSum[ s ];
				  na < aSlicePfxSum[ s + 1 ];
				  ++na )
			{
				const fp_t FD = D[ A.inds[ na ] ];
				for ( alength_t l = 0; l < ab.ALengths[ na ]; ++l, ++z )
					C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
			}
		}
	}
}



template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
regularized::numeric_unrolled8(
	const regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	ui64 const* const thrdMultsPfxSum,
	nnz_t const* const aSlicePfxSum,
	nnz_t const* const cSlicePfxSum,
	ui32 const* const slicesPerThrdPfxSum,
	const ui32& NUM_THREADS )
{
	#pragma omp parallel num_threads( NUM_THREADS )
	{
		const ui32 ID = omp_get_thread_num();
		ui64 z = 0;
		for ( ui32 s = slicesPerThrdPfxSum[ ID ];
			  s < slicesPerThrdPfxSum[ ID + 1 ];
			  ++s )
		{
			for ( nnz_t nc = cSlicePfxSum[ s + 1 ] - 1;
				  nc >= cSlicePfxSum[ s ];
				  --nc )
				C.nzs[ nc ] = 0;

			const fp_t* myABNzs = ab.nzs + thrdMultsPfxSum[ ID ];
			const coffset_t* myCOffsets = ab.COffsets + thrdMultsPfxSum[ ID ];
			for ( nnz_t na = aSlicePfxSum[ s ];
				  na < aSlicePfxSum[ s + 1 ];
				  ++na )
			{
				const fp_t FD = D[ A.inds[ na ] ];
				switch( ab.ALengths[ na ] )
				{
					case 1:
						C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
						break;
					case 2:
						C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
						C.nzs[ myCOffsets[ z + 1 ] ] += FD * myABNzs[ z + 1 ];
						break;
					case 3:
						C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
						C.nzs[ myCOffsets[ z + 1 ] ] += FD * myABNzs[ z + 1 ];
						C.nzs[ myCOffsets[ z + 2 ] ] += FD * myABNzs[ z + 2 ];
						break;
					case 4:
						C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
						C.nzs[ myCOffsets[ z + 1 ] ] += FD * myABNzs[ z + 1 ];
						C.nzs[ myCOffsets[ z + 2 ] ] += FD * myABNzs[ z + 2 ];
						C.nzs[ myCOffsets[ z + 3 ] ] += FD * myABNzs[ z + 3 ];
						break;
					case 5:
						C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
						C.nzs[ myCOffsets[ z + 1 ] ] += FD * myABNzs[ z + 1 ];
						C.nzs[ myCOffsets[ z + 2 ] ] += FD * myABNzs[ z + 2 ];
						C.nzs[ myCOffsets[ z + 3 ] ] += FD * myABNzs[ z + 3 ];
						C.nzs[ myCOffsets[ z + 4 ] ] += FD * myABNzs[ z + 4 ];
						break;
					case 6:
						C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
						C.nzs[ myCOffsets[ z + 1 ] ] += FD * myABNzs[ z + 1 ];
						C.nzs[ myCOffsets[ z + 2 ] ] += FD * myABNzs[ z + 2 ];
						C.nzs[ myCOffsets[ z + 3 ] ] += FD * myABNzs[ z + 3 ];
						C.nzs[ myCOffsets[ z + 4 ] ] += FD * myABNzs[ z + 4 ];
						C.nzs[ myCOffsets[ z + 5 ] ] += FD * myABNzs[ z + 5 ];
						break;
					case 7:
						C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
						C.nzs[ myCOffsets[ z + 1 ] ] += FD * myABNzs[ z + 1 ];
						C.nzs[ myCOffsets[ z + 2 ] ] += FD * myABNzs[ z + 2 ];
						C.nzs[ myCOffsets[ z + 3 ] ] += FD * myABNzs[ z + 3 ];
						C.nzs[ myCOffsets[ z + 4 ] ] += FD * myABNzs[ z + 4 ];
						C.nzs[ myCOffsets[ z + 5 ] ] += FD * myABNzs[ z + 5 ];
						C.nzs[ myCOffsets[ z + 6 ] ] += FD * myABNzs[ z + 6 ];
						break;
					case 8:
						C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
						C.nzs[ myCOffsets[ z + 1 ] ] += FD * myABNzs[ z + 1 ];
						C.nzs[ myCOffsets[ z + 2 ] ] += FD * myABNzs[ z + 2 ];
						C.nzs[ myCOffsets[ z + 3 ] ] += FD * myABNzs[ z + 3 ];
						C.nzs[ myCOffsets[ z + 4 ] ] += FD * myABNzs[ z + 4 ];
						C.nzs[ myCOffsets[ z + 5 ] ] += FD * myABNzs[ z + 5 ];
						C.nzs[ myCOffsets[ z + 6 ] ] += FD * myABNzs[ z + 6 ];
						C.nzs[ myCOffsets[ z + 7 ] ] += FD * myABNzs[ z + 7 ];
						break;
					default:
						for ( alength_t l = 0; l < ab.ALengths[ na ]; ++l )
							C.nzs[ myCOffsets[ z + l ] ] += FD * myABNzs[ z + l ];
				}

				z += ab.ALengths[ na ];
			}
		}
	}
}



template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
regularized::numeric_doubleThread(
	const regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	ui64 const* const thrdMultsPfxSum,
	nnz_t const* const aSlicePfxSum,
	nnz_t const* const cSlicePfxSum,
	ui32 const* const slicesPerThrdPfxSum,
	omp_lock_t* const locks,
	const ui32& NUM_THREADS )
{
	#pragma omp parallel num_threads( NUM_THREADS )
	{
		const ui32 ID = omp_get_thread_num();
		const bool IS_SLAVE = ID % 2;
		const ui32 INDEX = ID / 2;
		const ui32 SLICE_START = slicesPerThrdPfxSum[ INDEX ];
		const ui32 SLICE_END = slicesPerThrdPfxSum[ INDEX + 1 ];

		omp_lock_t& myLock = locks[ INDEX ];
		const fp_t* myABNzs = ab.nzs + thrdMultsPfxSum[ INDEX ];
		const coffset_t* myCOffsets = ab.COffsets + thrdMultsPfxSum[ INDEX ];
		ui64 z = 0;

		for ( ui32 s = SLICE_START; s < SLICE_END; ++s )
		{
			if ( IS_SLAVE )
			{
				// slave thread (only uses local cache, doesn't request anything)
				omp_set_lock( &myLock );

				const ui32 SLICES = SLICE_END - SLICE_START;
				const ui32 NEXT_SLICE = ( s + 1 ) % SLICES + SLICE_START;
				// const ui32 NEXT_SLICE = ( s + 1 ); //  % SLICES + SLICE_START;
				if ( NEXT_SLICE < SLICE_END )
				{
					for ( nnz_t nc = cSlicePfxSum[ NEXT_SLICE + 1 ] - 1;
					  nc >= cSlicePfxSum[ NEXT_SLICE ];
					  --nc )
					C.nzs[ nc ] = 0;
				}

				omp_unset_lock( &myLock );
			}
			else
			{
				for ( nnz_t na = aSlicePfxSum[ s ];
					  na < aSlicePfxSum[ s + 1 ];
					  ++na )
				{
					const fp_t FD = D[ A.inds[ na ] ];
					for ( alength_t l = 0; l < ab.ALengths[ na ]; ++l, ++z )
						C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
				}

				omp_set_lock( &myLock );
				omp_unset_lock( &myLock );
			}
		}
	}
}



template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
regularized::numeric_doubleThread(
	const regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	ui64 const* const thrdMultsPfxSum,
	nnz_t const* const aSlicePfxSum,
	nnz_t const* const cSlicePfxSum,
	ui32 const* const slicesPerThrdPfxSum,
	char* conds,
	const ui32& NUM_THREADS )
{
	#pragma omp parallel num_threads( NUM_THREADS )
	{
		const ui32 ID = omp_get_thread_num();
		const bool IS_SLAVE = ID % 2;
		const ui32 INDEX = ID / 2;
		const ui32 SLICE_START = slicesPerThrdPfxSum[ INDEX ];
		const ui32 SLICE_END = slicesPerThrdPfxSum[ INDEX + 1 ];
		const ui32 VI = INDEX * 64;

		const fp_t* myABNzs = ab.nzs + thrdMultsPfxSum[ INDEX ];
		const coffset_t* myCOffsets = ab.COffsets + thrdMultsPfxSum[ INDEX ];
		ui64 z = 0;

		for ( ui32 s = SLICE_START; s < SLICE_END; ++s )
		{
			if ( IS_SLAVE )
			{
				std::stringstream ss;
				ss << "slave thread-" << ID
				   << " waiting on condition variable(" << VI << ")=" << conds[ VI ] << std::endl;
				std::cout << ss.str();
				// slave thread (only deals with local cache)
				while ( conds[ VI ] == 'm' );

				const ui32 SLICES = SLICE_END - SLICE_START;
				const ui32 NEXT_SLICE = ( s + 1 ) % SLICES + SLICE_START;
				for ( nnz_t nc = cSlicePfxSum[ NEXT_SLICE + 1 ] - 1;
					  nc >= cSlicePfxSum[ NEXT_SLICE ];
					  --nc )
					C.nzs[ nc ] = 0;

				ss << "slave thread job done waiting on (" << VI << ")=" << conds[ VI ] << std::endl;
				std::cout << ss.str();

				#pragma omp atomic write
				conds[ VI ] = 'm';

				ss << "slave thread made condition variable " << conds[ VI ] << std::endl;
				std::cout << ss.str();
			}
			else
			{
				std::stringstream ss;
				ss << "Computing thread-" << ID << "..." << std::endl;
				std::cout << ss.str();
				for ( nnz_t na = aSlicePfxSum[ s ];
					  na < aSlicePfxSum[ s + 1 ];
					  ++na )
				{
					const fp_t FD = D[ A.inds[ na ] ];
					for ( alength_t l = 0; l < ab.ALengths[ na ]; ++l, ++z )
						C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
				}

				ss << "Now waiting on condition variable (" << VI << ")=" << conds[ VI ] << std::endl;
				std::cout << ss.str();
				while ( conds[ VI ] == 's' );

				#pragma omp atomic write
				conds[ VI ] = 's';

				ss << "Made condition variable (" << VI << ") zero" << std::endl;
				std::cout << ss.str();
			}
		}
	}
}



template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
regularized::numeric2(
	const regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	ind_t const* const thrdPtrPfxSum, ui64 const* const thrdMultsPfxSum,
	const ui32& NUM_THREADS )
{
	#pragma omp parallel num_threads( NUM_THREADS )
	{
		const ui32 ID = omp_get_thread_num();
		const ind_t A_START = thrdPtrPfxSum[ ID ];
		const ind_t A_END = thrdPtrPfxSum[ ID + 1 ];

		// const nnz_t C_NZ_START = C.ptr[ A_START ];
		// const nnz_t C_NZ_END = C.ptr[ A_END ];
		// for ( nnz_t nc = C_NZ_START; nc < C_NZ_END; ++nc )
		// 	C.nzs[ nc ] = 0;

		const fp_t* myABNzs = ab.nzs + thrdMultsPfxSum[ ID ];
		const coffset_t* myCOffsets = ab.COffsets + thrdMultsPfxSum[ ID ];
		const nnz_t A_NZ_START = A.ptr[ A_START ];
		const nnz_t A_NZ_END = A.ptr[ A_END ];
		ui64 z = 0;
		for ( nnz_t na = A_NZ_START; na < A_NZ_END; ++na )
		{
            const fp_t FD = D[ A.inds[ na ] ];
			for ( alength_t l = 0; l < ab.ALengths[ na ]; ++l, ++z )
				C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
		}
	}
}



template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
regularized::numeric(
	const regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	ind_t const* const thrdPtrPfxSum, ui64 const* const thrdMultsPfxSum,
	const ui32& NUM_THREADS, const ui32& ITERATIONS )
{
	#pragma omp parallel num_threads( NUM_THREADS )
	{
		const ui32 ID = omp_get_thread_num();
		const ind_t A_START = thrdPtrPfxSum[ ID ];
		const ind_t A_END = thrdPtrPfxSum[ ID + 1 ];
		const nnz_t C_NZ_START = C.ptr[ A_START ];
		const nnz_t C_NZ_END = C.ptr[ A_END ];
		const nnz_t A_NZ_START = A.ptr[ A_START ];
		const nnz_t A_NZ_END = A.ptr[ A_END ];
		const fp_t* myABNzs = ab.nzs + thrdMultsPfxSum[ ID ];
		const coffset_t* myCOffsets = ab.COffsets + thrdMultsPfxSum[ ID ];

		for ( ui32 w = 0; w < ITERATIONS; ++w )
		{
			for ( nnz_t nc = C_NZ_END - 1; nc >= C_NZ_START; --nc )
				C.nzs[ nc ] = 0;

			ui64 z = 0;
			for ( nnz_t na = A_NZ_START; na < A_NZ_END; ++na )
			{
				const fp_t FD = D[ A.inds[ na ] ];
				for ( alength_t l = 0; l < ab.ALengths[ na ]; ++l, ++z )
					C.nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
			}

			#pragma omp barrier
		}
	}
}



// ----------------------------------------------------------------------------


template < typename dind_t, typename fp_t, typename mem >
regularized::AB_indirect< dind_t, fp_t, mem >::AB_indirect()
{
	init();
}


template < typename dind_t, typename fp_t, typename mem >
regularized::AB_indirect< dind_t, fp_t, mem >::~AB_indirect()
{
	free();
}


template < typename dind_t, typename fp_t, typename mem >
void
regularized::AB_indirect< dind_t, fp_t, mem >::init()
{
	DIndices = nullptr;
	writeAddresses = nullptr;
	nzs = nullptr;
	length = 0;
}


template < typename dind_t, typename fp_t, typename mem >
void
regularized::AB_indirect< dind_t, fp_t, mem >::free()
{
	if ( length > 0 )
	{
		if ( DIndices != nullptr )
			mem::free( DIndices, length, sizeof( dind_t ) );
		if ( nzs != nullptr )
			mem::free( nzs, length, sizeof( fp_t ) );
		if ( writeAddresses != nullptr )
			mem::free( writeAddresses, length, sizeof( fp_t* ) );
	}

	init();
}


template < bool doAlloc,
           typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem, typename AB_mem >
void
regularized::symbolic(
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A,
	const Cs< ind_t, fp_t, nnz_t, B_mem >& B,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	regularized::AB_indirect< ind_t, fp_t, AB_mem >& ab,
	ind_t const* const thrdPtrPfxSum, ui64 const* const thrdMultsPfxSum,
	nnz_t* const thrdCNnzPfxSum,
	fp_t** accumulators, const ui32& NUM_THREADS )
{
	assert( sizeof( fp_t ) > sizeof( ind_t ) );

	#pragma omp parallel num_threads( NUM_THREADS )
	{
		#pragma omp single
		{
			C.length = A.length;

			if ( doAlloc )
			{
				C.ptr = (nnz_t*)
					C_mem::alloc( C.length + 1, sizeof( nnz_t ) );
			}
		}

		const ui32 ID = omp_get_thread_num();
		const ind_t A_START = thrdPtrPfxSum[ ID ];
		const ind_t A_END = thrdPtrPfxSum[ ID + 1 ];
		const ind_t MULT_START = thrdMultsPfxSum[ ID ];
		const ind_t MULT_END = thrdMultsPfxSum[ ID + 1 ];

		nnz_t* myCPtr = C.ptr + A_START;
		nnz_t myCNnz = 0;
		{
			char* miniLookup = (char*) accumulators[ ID ];
			std::vector< ind_t > C_list;
			for ( ind_t a = A_START; a < A_END; ++a )
			{
				ind_t currNnz = 0;
				for ( nnz_t na = A.ptr[ a ]; na < A.ptr[ a + 1 ]; ++na )
				{
					const ind_t IA = A.inds[ na ];
					for ( nnz_t nb = B.ptr[ IA ];
					      nb < B.ptr[ IA + 1 ];
					      ++nb )
					{
						const ind_t IB = B.inds[ nb ];
						const ind_t HASH_ADDRESS = IB >> 3;
						const ind_t BIT_MASK = ( 1 << ( IB & 7 ) );

						if ( ( miniLookup[ HASH_ADDRESS ] & BIT_MASK ) == 0 )
						{
							miniLookup[ HASH_ADDRESS ] |= BIT_MASK;
							C_list.push_back( IB );
							++currNnz;
						}
					}
				}

				for ( nnz_t n = 0; n < currNnz; ++n )
				{
					const ind_t IC = C_list[ myCNnz + n ];
					const ind_t HASH_ADDRESS = IC >> 3;
					miniLookup[ HASH_ADDRESS ] = 0;
				}

				myCNnz += currNnz;
				C.ptr[ a + 1 ] = currNnz;
			}

			thrdCNnzPfxSum[ ID + 1 ] = myCNnz;

			#pragma omp barrier
			#pragma omp single
			{
				C.ptr[ 0 ] = 0;
				thrdCNnzPfxSum[ 0 ] = 0;
				for ( ui32 t = 0; t < NUM_THREADS; ++t )
					thrdCNnzPfxSum[ t + 1 ] += thrdCNnzPfxSum[ t ];

				if ( doAlloc )
				{
					const nnz_t CNNZ = thrdCNnzPfxSum[ NUM_THREADS ];
					C.inds = (ind_t*) C_mem::alloc( CNNZ, sizeof( ind_t ) );
					C.nzs = (fp_t*) C_mem::alloc( CNNZ, sizeof( fp_t ) );

					ab.length = thrdMultsPfxSum[ NUM_THREADS ];
					ab.DIndices = (ind_t*)
						AB_mem::alloc( ab.length, sizeof( ind_t ) );
					ab.writeAddresses = (fp_t**)
						AB_mem::alloc( ab.length, sizeof( fp_t* ) );
					ab.nzs = (fp_t*)
						AB_mem::alloc( ab.length, sizeof( fp_t ) );
				}
			}

			ind_t* myCInds = C.inds + thrdCNnzPfxSum[ ID ];
			for ( std::size_t i = 0; i < C_list.size(); ++i )
				myCInds[ i ] = C_list[ i ];
		}

		const ind_t BIT_MASK = ( 1 << 31 );
		const ind_t REVERSE_BIT_MASK = ~BIT_MASK;
		ind_t* myDIndices = ab.DIndices + thrdMultsPfxSum[ ID ];
		fp_t** myWriteAddresses = ab.writeAddresses + thrdMultsPfxSum[ ID ];
		fp_t* myNzs = ab.nzs + thrdMultsPfxSum[ ID ];
		fp_t* CNzs = C.nzs + thrdCNnzPfxSum[ ID ];
		ind_t* CInds = C.inds + thrdCNnzPfxSum[ ID ];
		ind_t* acc = (ind_t*) accumulators[ ID ];
		ui64 z = 0;
		for ( ind_t a = A_START; a < A_END; ++a )
		{
			nnz_t currNnz = 0;
			for ( nnz_t na = A.ptr[ a ]; na < A.ptr[ a + 1 ]; ++na )
			{
				const ind_t IA = A.inds[ na ];
				const fp_t FA = A.nzs[ na ];

				for ( nnz_t nb = B.ptr[ IA ]; nb < B.ptr[ IA + 1 ]; ++nb, ++z )
				{
					const ind_t IB = B.inds[ nb ];

					if ( acc[ IB ] == 0 )
					{
						acc[ IB ] = BIT_MASK | currNnz;
						++currNnz;
					}

					myDIndices[ z ] = IA;
					myNzs[ z ] = FA * B.nzs[ nb ];

					const ind_t OFFSET = acc[ IB ] & REVERSE_BIT_MASK;
					myWriteAddresses[ z ] = CNzs + OFFSET;
				}
			}

			for ( nnz_t nc = 0; nc < currNnz; ++nc )
				acc[ CInds[ nc ] ] = 0;

			CInds = CInds + currNnz;
			CNzs = CNzs + currNnz;
		}
	}
}



template < typename ind_t, typename fp_t, typename nnz_t, typename AB_mem >
void
regularized::numeric(
	const regularized::AB_indirect< ind_t, fp_t, AB_mem >& ab,
	fp_t const* const D, fp_t* const C,
	nnz_t const* const thrdCNnzPfxSum,
	ui64 const* const thrdMultsPfxSum,
	const ui32& NUM_THREADS )
{
	#pragma omp parallel num_threads( NUM_THREADS )
	{
		const ui32 ID = omp_get_thread_num();

		for ( nnz_t n = thrdCNnzPfxSum[ ID ]; n < thrdCNnzPfxSum[ ID + 1 ]; ++n )
			C[ n ] = 0;

		const ind_t MULT_START = thrdMultsPfxSum[ ID ];
		const ind_t MULT_END = thrdMultsPfxSum[ ID + 1 ];
		const ind_t MULT_COUNT = MULT_END - MULT_START;

		const fp_t* myABNzs = ab.nzs + MULT_START;
		const ind_t* myDIndices = ab.DIndices + MULT_START;
		fp_t** myWriteAddresses = ab.writeAddresses + MULT_START;
		ui64 z = 0;
		for ( ui64 z = 0; z < MULT_COUNT; ++z )
		{
			*myWriteAddresses[ z ] += myABNzs[ z ] * D[ myDIndices[ z ] ];
			// *myWriteAddresses[ z ] += D[ myDIndices[ z ] ];
		}
	}
}



template < typename ind_t, typename fp_t, typename nnz_t, typename AB_mem >
void
regularized::numeric(
	const regularized::AB_indirect< ind_t, fp_t, AB_mem >& ab,
	fp_t const* const D, fp_t* const C,
	nnz_t const* const thrdCNnzPfxSum,
	ui64 const* const thrdMultsPfxSum,
	const ui32& NUM_THREADS, const ui32& ITERATIONS )
{
	#pragma omp parallel num_threads( NUM_THREADS )
	{
		const ui32 ID = omp_get_thread_num();
		const ind_t MULT_START = thrdMultsPfxSum[ ID ];
		const ind_t MULT_END = thrdMultsPfxSum[ ID + 1 ];
		const ind_t MULT_COUNT = MULT_END - MULT_START;
		const fp_t* myABNzs = ab.nzs + MULT_START;
		const ind_t* myDIndices = ab.DIndices + MULT_START;
		fp_t** myWriteAddresses = ab.writeAddresses + MULT_START;

		for ( ui32 w = 0; w < ITERATIONS; ++w )
		{
			for ( nnz_t n = thrdCNnzPfxSum[ ID ]; n < thrdCNnzPfxSum[ ID + 1 ]; ++n )
				C[ n ] = 0;

			ui64 z = 0;
			for ( ui64 z = 0; z < MULT_COUNT; ++z )
				*myWriteAddresses[ z ] += myABNzs[ z ] * D[ myDIndices[ z ] ];

			#pragma omp barrier
		}
	}
}
