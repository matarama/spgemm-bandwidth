
template < typename ind_t, typename fp_t, typename nnz_t, typename mem >
sparse_storage::Cs< ind_t, fp_t, nnz_t, mem >::Cs()
: length( 0 ), ptr( nullptr ), inds( nullptr ), nzs( nullptr )
{
}

template < typename ind_t, typename fp_t, typename nnz_t, typename mem >
sparse_storage::Cs< ind_t, fp_t, nnz_t, mem >::~Cs()
{
	free();
}

template < typename ind_t, typename fp_t, typename nnz_t, typename mem >
void
sparse_storage::Cs< ind_t, fp_t, nnz_t, mem >::init()
{
	length = 0;
	ptr = nullptr;
	inds = nullptr;
	nzs = nullptr;
}

template < typename ind_t, typename fp_t, typename nnz_t, typename mem >
void
sparse_storage::Cs< ind_t, fp_t, nnz_t, mem >::free()
{
	if ( length > 0 )
	{
		mem::free( nzs, ptr[ length ], sizeof( fp_t ) );
		mem::free( inds, ptr[ length ], sizeof( ind_t ) );
		mem::free( ptr, length + 1, sizeof( nnz_t ) );
	}

	init();
}

template < typename ind_t, typename fp_t, typename nnz_t, typename mem >
void
sparse_storage::Cs< ind_t, fp_t, nnz_t, mem >::reset()
{
	if ( nnz() > 0 )
	{
		for ( nnz_t i = 0; i < nnz(); ++i )
			nzs[ i ] = 0;
	}
}


template < typename ind_t, typename fp_t, typename nnz_t, typename mem >
nnz_t
sparse_storage::Cs< ind_t, fp_t, nnz_t, mem >::nnz() const
{
	if ( length > 0 )
		return ptr[ length ];
	return 0;
}

template < typename find_t, typename ffp_t, typename fnnz_t, typename fmem,
           typename tind_t, typename tfp_t, typename tnnz_t, typename tmem >
void
sparse_storage::copy( const  Cs< find_t, ffp_t, fnnz_t, fmem >& from,
                      Cs< tind_t, tfp_t, tnnz_t, tmem >& to )
{
	if ( from.nnz() <= 0 )
		return;

	to.free();
	to.length = from.length;
	#pragma omp parallel
	#pragma omp single
	{
		to.ptr = (tnnz_t*) tmem::alloc( to.length + 1, sizeof( tnnz_t ) );
		to.nzs = (tfp_t*) tmem::alloc( from.nnz(), sizeof( tfp_t ) );
		to.inds = (tind_t*) tmem::alloc( from.nnz(), sizeof( tind_t ) );

		for ( ind_t i = 0; i < to.length + 1; ++i )
			to.ptr[ i ] = from.ptr[ i ];

		for ( nnz_t n = 0; n < from.nnz(); ++n )
		{
			to.nzs[ n ] = from.nzs[ n ];
			to.inds[ n ] = from.inds[ n ];
		}
	}
}

template < typename ind_t, typename fp_t, typename nnz_t, typename mem >
void
sparse_storage::convertToCsr(
	Triplets& from, Cs< ind_t, fp_t, nnz_t, mem >& csr )
{
	csr.free();
	csr.length = from.rows();
	#pragma omp parallel
	#pragma omp single
	{
		csr.ptr = (nnz_t*) mem::alloc( csr.length + 1, sizeof( nnz_t ) );
		csr.nzs = (fp_t*) mem::alloc( from.length(), sizeof( fp_t ) );
		csr.inds = (ind_t*) mem::alloc( from.length(), sizeof( ind_t ) );

		for ( ind_t i = 0; i < from.rows() + 1; ++i )
			csr.ptr[ i ] = 0;

		ind_t prevRow = 0;
		from.sortRowMajorStd();
		csr.ptr[ 0 ] = 0;
		for ( std::size_t t = 0; t < from.length(); ++t )
		{
			csr.nzs[ t ] = from[ t ].nz;
			csr.inds[ t ] = from[ t ].j;

			++csr.ptr[ from[ t ].i + 1 ];
		}

		for ( ind_t i = 0; i < from.rows(); ++i )
			csr.ptr[ i + 1 ] += csr.ptr[ i ];

		// csr.ptr[ csr.length ] = from.length();
	}
}

template < typename ind_t, typename fp_t, typename nnz_t, typename mem >
void
sparse_storage::convertToCsc(
	Triplets& from, Cs< ind_t, fp_t, nnz_t, mem >& csc )
{
	csc.free();
	csc.length = from.columns();
	#pragma omp parallel
	#pragma omp single
	{
		csc.ptr = (nnz_t*) mem::alloc( csc.length + 1, sizeof( nnz_t ) );
		csc.nzs = (fp_t*) mem::alloc( from.length(), sizeof( fp_t ) );
		csc.inds = (ind_t*) mem::alloc( from.length(), sizeof( ind_t ) );

		for ( ind_t j = 0; j < from.columns() + 1; ++j )
			csc.ptr[ j ] = 0;
		ind_t prevColumn = 0;
		from.sortColMajorStd();
		csc.ptr[ 0 ] = 0;
		for ( std::size_t t = 0; t < from.length(); ++t )
		{
			csc.nzs[ t ] = from[ t ].nz;
			csc.inds[ t ] = from[ t ].i;

			++csc.ptr[ from[ t ].j + 1 ];
		}

		for ( ind_t j = 0; j < from.columns(); ++j )
			csc.ptr[ j + 1 ] = csc.ptr[ j ];

		// csc.ptr[ csc.length ] = from.length();
	}
}
