/*
 * Csr.cpp
 *
 *  Created on: Dec 20, 2016
 *      Author: memoks
 */

#include <omp.h>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <sstream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iterator>

#include "include/util/Memory.h"

// Resource Management
// --------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory >
Csr< ind_t, fp_t, nnz_t, Memory >::Csr()
: rowCount( 0 ), columnCount( 0 ), nnz( 0 ),
  rowPtr( nullptr ), colInd( nullptr ), nzs( nullptr )
{
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory >
Csr< ind_t, fp_t, nnz_t, Memory >::Csr( Triplets& triplets )
: rowCount( 0 ), columnCount( 0 ), nnz( 0 ),
  rowPtr( nullptr ), colInd( nullptr ), nzs( nullptr )
{
	extract( triplets );
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory >
Csr< ind_t, fp_t, nnz_t, Memory >::Csr(
	const Csr& other )
: Csr()
{
	rowPtr = static_cast< nnz_t* >(
		Memory::alloc( other.rowCount + 1, sizeof( nnz_t ) ) );
	colInd = static_cast< ind_t* >(
		Memory::alloc( other.nnz, sizeof( ind_t ) ) );
	nzs = static_cast< fp_t* >(
		Memory::alloc( other.nnz, sizeof( fp_t ) ) );
	_copy( other );
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory >
Csr< ind_t, fp_t, nnz_t, Memory >::~Csr()
{
	free();
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory >
Csr< ind_t, fp_t, nnz_t, Memory >&
Csr< ind_t, fp_t, nnz_t, Memory >::operator =(
	const Csr& other )
{
	if ( this != &other )
	{
		free();
		rowPtr = static_cast< nnz_t* >(
			Memory::alloc( other.rowCount + 1, sizeof( nnz_t ) ) );
		colInd = static_cast< ind_t* >(
			Memory::alloc( other.nnz, sizeof( ind_t ) ) );
		nzs = static_cast< fp_t* >(
			Memory::alloc( other.nnz, sizeof( fp_t ) ) );
		_copy( other );
	}

	return *this;
}

// --------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory >
void
Csr< ind_t, fp_t, nnz_t, Memory >::zero()
{
	for ( glbNnz_t i = 0; i < nnz; ++i )
		nzs[ i ] = 0;
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory >
void
Csr< ind_t, fp_t, nnz_t, Memory >::extract( Triplets& triplets )
{
	if ( triplets.isEmpty() )
		return;

	triplets.sortRowMajor();

	rowCount = triplets.rows();
	columnCount = triplets.columns();
	nnz = triplets.length();

	colInd = static_cast< ind_t* >(
		Memory::alloc( triplets.length(), sizeof( ind_t ) ) );
	nzs = static_cast< fp_t* >(
		Memory::alloc( triplets.length(), sizeof( fp_t ) ) );
	rowPtr = static_cast< nnz_t* >(
		Memory::alloc( triplets.rows() + 1, sizeof( nnz_t ) ) );

	for ( ind_t i = 0; i < rowCount + 1; ++i )
		rowPtr[ i ] = 0;

	// populate CSR arrays
	for ( std::size_t t = 0; t < triplets.length(); ++t )
	{
		nzs[ t ] = static_cast< fp_t >( triplets[ t ].nz );
		colInd[ t ] = static_cast< ind_t >( triplets[ t ].j );
		++rowPtr[ triplets[ t ].i + 1 ];
	}

	for ( std::size_t i = 1; i <= rowCount; ++i )
		rowPtr[ i ] += rowPtr[ i - 1 ];
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory >
void
Csr< ind_t, fp_t, nnz_t, Memory >::extract( Triplets& triplets,
                                            ind_t const* const thrdRowPfxSum,
                                            const ui32& numThreads )
{
	if ( triplets.isEmpty() )
		return;

	triplets.sortRowMajor();

	std::shared_ptr< std::vector< glbNnz_t > > ptRowPtr( triplets.rowPtr() );
	std::vector< glbInd_t >& tRowPtr = *ptRowPtr.get();

	rowCount = triplets.rows();
	columnCount = triplets.columns();
	nnz = triplets.length();

	#pragma omp parallel num_threads( numThreads )
	{
		const int ID = omp_get_thread_num();

		#pragma omp single
		{
			colInd = static_cast< ind_t* >(
				Memory::alloc( nnz, sizeof( ind_t ) ) );
			nzs = static_cast< fp_t* >(
				Memory::alloc( nnz, sizeof( fp_t ) ) );
			rowPtr = static_cast< nnz_t* >(
				Memory::alloc( rowCount + 1, sizeof( nnz_t ) ) );
		}

		const ind_t ROW_START = thrdRowPfxSum[ ID ];
		const ind_t ROW_END = thrdRowPfxSum[ ID ];

		for ( std::size_t r = ROW_START; r < ROW_END; ++r )
			rowPtr[ r ] = static_cast< nnz_t >( tRowPtr[ r ] );

		for ( std::size_t t = 0; t < triplets.length(); ++t )
		{
			nzs[ t ] = static_cast< fp_t >( triplets[ t ].nz );
			colInd[ t ] = static_cast< ind_t >( triplets[ t ].j );
		}
	}

	rowPtr[ rowCount ] = tRowPtr[ rowCount ];
}


template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory >
bool
Csr< ind_t, fp_t, nnz_t, Memory >::isNull() const
{
	return rowCount <= 0;
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory >
void
Csr< ind_t, fp_t, nnz_t, Memory >::init()
{
	rowCount = 0;
	columnCount = 0;
	nnz = 0;
	rowPtr = nullptr;
	colInd = nullptr;
	nzs = nullptr;
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory >
void
Csr< ind_t, fp_t, nnz_t, Memory >::free()
{
	if ( !isNull() )
	{
		if ( rowPtr != nullptr )
			Memory::free( rowPtr, rowCount + 1, sizeof( nnz_t ) );
		if ( colInd != nullptr )
			Memory::free( colInd, nnz, sizeof( ind_t ) );
		if ( nzs != nullptr )
			Memory::free( nzs, nnz, sizeof( fp_t ) );
	}
	
	init();
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory >
template < typename MemoryOther >
void
Csr< ind_t, fp_t, nnz_t, Memory >::copy(
	const Csr< ind_t, fp_t, nnz_t, MemoryOther >& other,
	ind_t const* const thrdRowPfxSum, const ui32& numThreads )
{
	#pragma omp parallel num_threads( numThreads )
	{
		#pragma omp single
		{
			rowCount = other.rowCount;
			columnCount = other.columnCount;
			nnz = other.nnz;

			rowPtr = static_cast< nnz_t* >(
				Memory::alloc( other.rowCount + 1, sizeof( nnz_t ) ) );
			colInd = static_cast< ind_t* >(
				Memory::alloc( other.nnz, sizeof( ind_t ) ) );
			nzs = static_cast< fp_t* >(
				Memory::alloc( other.nnz, sizeof( fp_t ) ) );
		}

		const int ID = omp_get_thread_num();

		const ind_t ROW_START = thrdRowPfxSum[ ID ];
		const ind_t ROW_END = thrdRowPfxSum[ ID + 1 ];
		for ( ind_t r = ROW_START; r < ROW_END; ++r )
			rowPtr[ r ] = other.rowPtr[ r ];

		const nnz_t NZ_START = other.rowPtr[ ROW_START ];
		const nnz_t NZ_END = other.rowPtr[ ROW_END ];
		for ( nnz_t n = NZ_START; n < NZ_END; ++n )
			colInd[ n ] = other.colInd[ n ];
	}
	rowPtr[ rowCount ] = nnz;
}

// --------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory >
std::ostream&
Csr< ind_t, fp_t, nnz_t, Memory >::stats( std::ostream& out ) const
{
	if ( isNull() )
		return out;

	glbInd_t nonEmptyRowCount = 0;
	for ( glbInd_t i = 0; i < rowCount; ++i )
		if( rowPtr[ i + 1 ] - rowPtr[ i ] <= 0 )
			++nonEmptyRowCount;

	out << "Non-empty row count: " << rowCount - nonEmptyRowCount << "/"
		<< rowCount << std::endl;
	out << "Nnz: " << nnz << std::endl;

	return out;
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory >
std::ostream&
Csr< ind_t, fp_t, nnz_t, Memory >::plot( std::ostream& out ) const
{
	stats( out ) << std::endl;

	if ( isNull() )
		return out;

	out << "rowPtr: ";
	for ( ind_t i = 0; i < rowCount + 1; ++i )
		out << rowPtr[ i ] << " ";
	out << std::endl;

	out << "colInd: ";
	for ( nnz_t i = 0; i < nnz; ++i )
		out << colInd[ i ] << " ";
	out << std::endl;

	// out << "nzs: ";
	// for ( nnz_t i = 0; i < nnz; ++i )
	// 	out << nzs[ i ] << " ";
	// out << std::endl;
	
	/*
	out << "rows: ";
	for ( ind_t i = 0; i < rowCount; ++i )
	{
		out << "i=" << i << " -> " << rowPtr[ i ] << " =>";

		out << " colInds:";
		for ( nnz_t j = rowPtr[ i ]; j < rowPtr[ i + 1 ]; ++j )
			out << " " << colInd[ j ];
		out << "  ";

		out << " nzs:";
		for ( nnz_t j = rowPtr[ i ]; j < rowPtr[ i + 1 ]; ++j )
			out << " " << nzs[ j ];

		out << std::endl;
	}
	out << std::endl;
	*/

	return out;
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory >
std::string
Csr< ind_t, fp_t, nnz_t, Memory >::stats() const
{
	std::stringstream ss;
	stats( ss );
	return ss.str();
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory >
std::string
Csr< ind_t, fp_t, nnz_t, Memory >::plot() const
{
	std::stringstream ss;
	plot( ss );
	return ss.str();
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory >
bool
Csr< ind_t, fp_t, nnz_t, Memory >::isSame( fp_t const* const res )
{
	bool isSame = true;
	fp_t total1 = 0;
	fp_t total2 = 0;
	for ( ind_t r = 0; r < rowCount; ++r )
	{
		for ( nnz_t n = rowPtr[ r ]; n < rowPtr[ r + 1 ]; ++n )
		{
			total1 += nzs[ n ];
			total2 += res[ n ];
			if ( nzs[ n ] - res[ n ] > 0.001 ||
			     nzs[ n ] - res[ n ] < -0.001 )
			{
#ifdef DEBUG
				std::cout << "n=" << n << " r=" << r
				          << " " << nzs[ n ] << "-" << res[ n ]
				          << "=" << nzs[ n ] - res[ n ] << std::endl;
#endif
				isSame = false;
			}
		}
	}

#ifdef DEBUG
	if ( !isSame )
	{
		std::cout << std::endl;
		std::cout << "total1=" << total1
		          << " total2=" << total2
		          << std::endl;
	}
#endif

	return isSame;
}

// -----------------------------------------------------------------------------

template < typename ind_t, typename fp_t, typename nnz_t,
           typename Memory >
void
Csr< ind_t, fp_t, nnz_t, Memory >::_copy(
	const Csr& other )
{
	rowCount = other.rowCount;
	columnCount = other.columnCount;
	nnz = other.nnz;

	for ( ind_t r = 0; r <= other.rowCount; ++r )
		rowPtr[ r ] = other.rowPtr[ r ];

	for ( nnz_t n = 0; n < other.nnz; ++n )
	{
		colInd[ n ] = other.colInd[ n ];
		nzs[ n ] = other.nzs[ n ];
	}
}
