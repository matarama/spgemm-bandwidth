/*
 * DynamicGustavson.cpp
 *
 *  Created on: Aug 26, 2017
 *      Author: memoks
 */

#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <map>
#include <omp.h>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#endif

#include "include/sparse_types.h"
#include "include/util/Generic.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Csr.h"
#include "include/io/InputReader.h"
#include "include/io/MMInfo.h"
#include "include/io/Cli.h"
#include "include/logging/Timer.h"
#include "include/parallel/Gustavson.h"
#include "include/parallel/RowByRow3p_v0.h"

// #############################################################################

int main( int argc, char* argv[] )
{
	Cli cli( argc, argv );

	// struct phf myphf;
	// ui32 keyCount = 10;
	// ui32 lambda = 2;
	// ui32 alpha = 100;
	// phf_seed_t seed = 0;
	// ui32 keys[] = { 4, 67, 2, 7, 9, 12, 1, 40, 21, 31 };
	// PHF::init< ui32, false >( &myphf, &keys[0], keyCount, lambda, alpha, seed );

	// std::cout << "phf-test: " << std::endl;
	// for ( ui32 i = 0; i < keyCount; ++i )
	// {
	// 	std::cout << keys[ i ] << " -> " << PHF::hash( &myphf, keys[ i ] ) << std::endl;
	// }
	// std::cout << std::endl;
	
	
	// Parameters
	// ------------------------------------------------------------------------
	if ( !cli.isSet( { Cli::MMF_PATH_A } ) )
	{
		return EXIT_FAILURE;
	}

	ui32 iterations = cli.getIterations();
	ui32 numThreads = cli.getNumBlocks();
	std::string mmfPathA = cli.getMmfPathA();
	std::string mmfPathB = cli.getMmfPathB();
	ui64 cacheLineBytes = cli.getCacheLineBytes();
	bool transposeB = cli.getTransposeB();

	omp_set_num_threads( numThreads );

	std::cout << mmfPathA.substr( mmfPathA.find_last_of( '/' ) + 1,
	                              mmfPathA.find_last_of( '.' ) - 1 ) << ",";

	// Read matrix market format file.
	// ------------------------------------------------------------------------
	MMInfo A_mmInfo;
	int& A_isSymmetric = A_mmInfo.isSymmetric;
	int A_rowCount = 0;
	int A_colCount = 0;
	int A_nnz = 0;
	int* A_is = NULL;
	int* A_js = NULL;
	double* A_nzs = NULL;
	InputReader::readMMF( mmfPathA, A_mmInfo, &A_rowCount, &A_colCount,
	                      &A_nnz, &A_is, &A_js, &A_nzs);
	Triplet* tripletArrA = Triplet::array( A_nnz, A_is, A_js, A_nzs );
	Triplets _A( 0, 0, A_rowCount, A_colCount,
	             tripletArrA, tripletArrA + A_nnz );

	MMInfo B_mmInfo;
	int B_isSymmetric = 0;
	int B_rowCount = 0;
	int B_colCount = 0;
	int B_nnz = 0;
	int* B_is = NULL;
	int* B_js = NULL;
	double* B_nzs = NULL;

	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		InputReader::readMMF( mmfPathB, B_mmInfo, &B_rowCount, &B_colCount,
		                      &B_nnz, &B_is, &B_js, &B_nzs);
		B_isSymmetric = B_mmInfo.isSymmetric;
	}
	else
	{
		mmfPathB = mmfPathA;
		B_isSymmetric = A_isSymmetric;
		B_rowCount = A_rowCount;
		B_colCount = A_colCount;
		B_nnz = A_nnz;
		B_is = A_is;
		B_js = A_js;
		B_nzs = A_nzs;

		if( B_rowCount != B_colCount )
			transposeB = true;
	}

	Triplet* tripletArrB = Triplet::array( B_nnz, B_is, B_js, B_nzs );
	Triplets _B = Triplets( 0, 0, B_rowCount, B_colCount,
	                        tripletArrB, tripletArrB + B_nnz );

	if ( transposeB )
		_B.transpose();

	// clean up
	free( A_is );
	free( A_js );
	free( A_nzs );	
	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		free( B_is );
		free( B_js );
		free( B_nzs );
	}

	// Permute A and B matrices
	if ( cli.getPermuteAB() )
		Triplets::permuteRandom( _A, _B );

	// std::cout << numThreads << "t," << std::endl;
	// -------------------------------------------------------------------------
	glbInd_t* thrdRowPfxSum = nullptr;
	std::vector< ui64 > thrdMultPfxSum;
	util::partition( _A, _B, &thrdRowPfxSum, thrdMultPfxSum, numThreads );

	Cs< glbInd_t, fp_t, glbNnz_t, allocator::Aligned > A;
	Cs< glbInd_t, fp_t, glbNnz_t, allocator::Aligned > B;
	convertToCsr( _A, A );
	convertToCsr( _B, B );

	Cs< glbInd_t, fp_t, glbNnz_t, allocator::Aligned > C;
	{
		fp_t** accumulators = nullptr;
		#pragma omp parallel num_threads( numThreads )
		{
			#pragma omp single
			accumulators = util::alloc< fp_t* >( numThreads );

			const ui32 ID = omp_get_thread_num();
			accumulators[ ID ] = util::newVector< fp_t >( _B.columns() );
		}

		rbr::gustavson::symbolic_stl< true, false >(
			A, B, C, accumulators, thrdRowPfxSum, numThreads );

		for ( ui32 t = 0; t < numThreads; ++t )
			util::free( accumulators[ t ] );
		util::free( accumulators );
	}

	std::stringstream sheader;
	sheader << "A,";

	{
		ui64 noOfCacheLinesRead_B = 0;
		ui64 noOfLoads_accumulator = 0;
		ui64 noOfLoads_A = 0;
		ui64 noOfLoads_B = 0;
		ui64 noOfLoads_C = 0;
		ui64 noOfStores_accumulator = 0;
		ui64 noOfStores_C = 0;
		rbr::gustavson::count(
			A, B, C, cacheLineBytes, thrdRowPfxSum, numThreads,
			noOfCacheLinesRead_B,
			noOfLoads_accumulator, noOfLoads_A, noOfLoads_B, noOfLoads_C,
			noOfStores_accumulator, noOfStores_C );

		ui64 totalLoads =
			noOfLoads_accumulator + noOfLoads_A + noOfLoads_B + noOfLoads_C;
		ui64 totalStores = noOfStores_accumulator + noOfStores_C;
		RECORD( "Gustavson" );
		RECORD( noOfCacheLinesRead_B );
		RECORD( totalLoads );
		RECORD( totalStores );
		RECORD( " " );
		RECORD( noOfLoads_accumulator );
		RECORD( noOfLoads_A );
		RECORD( noOfLoads_B );
		RECORD( noOfLoads_C );
		RECORD( noOfStores_accumulator );
		RECORD( noOfStores_C );

		sheader << "Gustavson, cacheLinesRead_B, loads, stores, , "
		        << "loads_acc, loads_A, loads_B, loads_C, "
		        << "stores_acc, stores_C, ";
	}

	{
		ui64 noOfCacheLinesRead_B_prime = 0;
		ui64 noOfLoads_accumulator = 0;
		ui64 noOfLoads_A = 0;
		ui64 noOfLoads_B_prime = 0;
		ui64 noOfLoads_C = 0;
		ui64 noOfLoads_masks = 0;
		ui64 noOfStores_accumulator = 0;
		ui64 noOfStores_B_prime = 0;
		ui64 noOfStores_C = 0;

		rbr3p::v0::count( A, B, C, cacheLineBytes,
		                  thrdRowPfxSum, numThreads,
		                  noOfCacheLinesRead_B_prime,
		                  noOfLoads_accumulator,
		                  noOfLoads_A,
		                  noOfLoads_B_prime,
		                  noOfLoads_C,
		                  noOfLoads_masks,
		                  noOfStores_accumulator,
		                  noOfStores_B_prime,
		                  noOfStores_C );

		ui64 totalLoads = noOfLoads_accumulator + noOfLoads_A +
			noOfLoads_B_prime + noOfLoads_C +
			noOfLoads_masks;
		ui64 totalStores = noOfStores_accumulator +
			noOfStores_C;

		ui64 p3Stores = noOfStores_accumulator + noOfStores_C;
		RECORD( "Rbr3p_v0" );
		RECORD( noOfCacheLinesRead_B_prime );
		RECORD( totalLoads );
		RECORD( totalStores );
		RECORD( " " );
		RECORD( noOfLoads_accumulator );
		RECORD( noOfLoads_A );
		RECORD( noOfLoads_B_prime );
		RECORD( noOfLoads_C );
		RECORD( noOfLoads_masks );
		RECORD( noOfStores_accumulator );
		RECORD( noOfStores_B_prime );
		RECORD( noOfStores_C );

		sheader << "Rbr3p_v0, cacheLinesRead_B', totalLoads, totalStores, "
		        << "noOfLoads_accumulator, noOfLoads_A, "
		        << "noOfLoads_B', noOfLoads_C, "
		        << "noOfLoads_masks, noOfStores_accumulator, "
		        << "noOfStores_B', noOfStores_C" << std::endl;
	}
	

	// std::cout << std::endl;
	// std::cout << sheader.str() << std::endl;

	util::free( thrdRowPfxSum );
	delete [] tripletArrA;
	delete [] tripletArrB;
	return EXIT_SUCCESS;
}

