
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <map>
#include <omp.h>
#include <set>
#include <unordered_set>
#include <stdint.h>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#endif

#include "include/sparse_types.h"
#include "include/util/Generic.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Csr.h"
#include "include/io/InputReader.h"
#include "include/io/MMInfo.h"
#include "include/io/Cli.h"
#include "include/logging/Timer.h"

using namespace allocator;

#define UNROLL 1

#if UNROLL == 1
#define DISTANCE_METHOD( acc, arr, lfsr, distance ) \
    do                                              \
    {                                               \
        switch( distance )                          \
        {                                           \
            case 2:                                 \
                acc += arr[ lfsr ];                 \
                acc += arr[ lfsr + 1 ];             \
                break;                              \
            case 3:                                 \
                acc += arr[ lfsr ];                 \
                acc += arr[ lfsr + 1 ];             \
                acc += arr[ lfsr + 2 ];             \
                break;                              \
            case 4:                                 \
                acc += arr[ lfsr ];                 \
                acc += arr[ lfsr + 1 ];             \
                acc += arr[ lfsr + 2 ];             \
                acc += arr[ lfsr + 3 ];             \
                break;                              \
            case 5:                                 \
                acc += arr[ lfsr ];                 \
                acc += arr[ lfsr + 1 ];             \
                acc += arr[ lfsr + 2 ];             \
                acc += arr[ lfsr + 3 ];             \
                acc += arr[ lfsr + 4 ];             \
                break;                              \
            case 6:                                 \
                acc += arr[ lfsr ];                 \
                acc += arr[ lfsr + 1 ];             \
                acc += arr[ lfsr + 2 ];             \
                acc += arr[ lfsr + 3 ];             \
                acc += arr[ lfsr + 4 ];             \
                acc += arr[ lfsr + 5 ];             \
                break;                              \
            case 7:                                 \
                acc += arr[ lfsr ];                 \
                acc += arr[ lfsr + 1 ];             \
                acc += arr[ lfsr + 2 ];             \
                acc += arr[ lfsr + 3 ];             \
                acc += arr[ lfsr + 4 ];             \
                acc += arr[ lfsr + 5 ];             \
                acc += arr[ lfsr + 6 ];             \
                break;                              \
            case 8:                                 \
                acc += arr[ lfsr ];                 \
                acc += arr[ lfsr + 1 ];             \
                acc += arr[ lfsr + 2 ];             \
                acc += arr[ lfsr + 3 ];             \
                acc += arr[ lfsr + 4 ];             \
                acc += arr[ lfsr + 5 ];             \
                acc += arr[ lfsr + 6 ];             \
                acc += arr[ lfsr + 7 ];             \
                break;                              \
            default:                                \
                for( ui32 j = 0; j < distance; ++j )\
                {                                   \
                    acc += arr[ lfsr + j ];         \
                }                                   \
                break;                              \
        }                                           \
    } while ( 0 )

#else
#define DISTANCE_METHOD( acc, arr, lfsr, distance )  \
    do                                               \
    {                                                \
        for( ui32 j = 0; j < distance; ++j )         \
        {                                            \
            acc += arr[ lfsr + j ];                  \
        }                                            \
    } while( 0 )
#endif

// lfsr algorithm used (for 2^21 length data -> 16MB)
void
lfsr( const ui32& START_STATE = 0 )
{
    const unsigned MSB_MASK = 0x00100000u;
    const unsigned STATE_MASK = 0x001FFFFFu;

    unsigned lfsr = START_STATE & STATE_MASK;
    unsigned bit;
    unsigned period = 0;

    do
    {
        bit = ( ( lfsr >> 0 ) ^ ( lfsr >> 2 ) );
        lfsr = ( lfsr >> 1 ) | ( ( bit << 20 ) & MSB_MASK );
        ++period;
        // std::cout << lfsr << std::endl;
    }
    while ( lfsr != START_STATE );

    std::cout << "period=" << period << std::endl;
}

// lfsr algorithm used (for 2^15 length data -> 512KB)
void
lfsr_local( const ui32& START_STATE = 0 )
{
    const unsigned MSB_MASK = 0x00004000u;
    const unsigned STATE_MASK = 0x0007FFFu;

    unsigned lfsr = START_STATE & STATE_MASK;
    unsigned bit;
    unsigned period = 0;

    do
    {
        bit = ( ( lfsr >> 0 ) ^ ( lfsr >> 1 ) );
        lfsr = ( lfsr >> 1 ) | ( ( bit << 14 ) & MSB_MASK );
        // std::cout << lfsr << std::endl;
        ++period;
    }
    while ( lfsr != START_STATE );

    std::cout << "period=" << period << std::endl;
}

// ----------------------------------------------------------------------------

double
localRead1( double const* const DATA, ui32 const* const START_STATES,
            const ui32& NO_OF_READS, const ui32& PER_THRD_LENGTH,
            const ui32& NUM_THREADS )
{
    const ui32 MSB_MASK = 0x00004000u;
    const ui32 SHAM = 15;

    // const ui32 PER_THRD_LENGTH = pow( 2, 15 );
    double total = 0.0;
    double partials[ NUM_THREADS ];
    #pragma omp parallel num_threads( NUM_THREADS )
    {
        const ui32 ID = omp_get_thread_num();
        double const* const local = DATA + ID * PER_THRD_LENGTH;
        ui32 lfsr = START_STATES[ ID ];
        ui32 bit;

        double partial = 0.0;
        for ( ui32 i = 0; i < NO_OF_READS; ++i )
        {
            bit = ( ( lfsr >> 0 ) ^ ( lfsr >> 2 ) );
            lfsr = ( lfsr >> 1 ) | ( (bit << SHAM ) & MSB_MASK );
            partial += DATA[ lfsr ];
        }

        partials[ ID ] = partial;
    }

    for ( ui32 t = 0; t < NUM_THREADS; ++t )
        total += partials[ t ];

    return total;
}

double
localRead( double const* const DATA, ui32 const* const START_STATES,
           const ui32& NO_OF_READS, const ui32& READ_LENGTH,
           const ui32& PER_THRD_LENGTH, const ui32& NUM_THREADS )
{
    const ui32 MSB_MASK = 0x00004000u;
    const ui32 SHAM = 15;

    // const ui32 PER_THRD_LENGTH = pow( 2, 15 );
    double total = 0.0;
    double partials[ NUM_THREADS ];
    #pragma omp parallel num_threads( NUM_THREADS )
    {
        const ui32 ID = omp_get_thread_num();
        double const* const local = DATA + ID * PER_THRD_LENGTH;
        ui32 lfsr = START_STATES[ ID ];
        ui32 bit;

        double partial = 0.0;
        for ( ui32 i = 0; i < NO_OF_READS; ++i )
        {
            bit = ( ( lfsr >> 0 ) ^ ( lfsr >> 2 ) );
            lfsr = ( lfsr >> 1 ) | ( (bit << SHAM ) & MSB_MASK );

            DISTANCE_METHOD( partial, DATA, lfsr, READ_LENGTH );
            // for ( ui32 j = 0; j < READ_LENGTH; ++j )
            //     partial += DATA[ lfsr + j ];
        }

        partials[ ID ] = partial;
    }

    for ( ui32 t = 0; t < NUM_THREADS; ++t )
        total += partials[ t ];

    return total;
}

double
networkTraffic1( double const* const DATA, ui32 const* const START_STATES,
                 const ui32& NO_OF_READS,
                 const ui32& NUM_THREADS )
{
    const ui32 MSB_MASK = 0x00100000u;
    const ui32 SHAM = 20;

    double total = 0.0;
    double partials[ NUM_THREADS ];
    #pragma omp parallel num_threads( NUM_THREADS )
    {
        const ui32 ID = omp_get_thread_num();
        ui32 lfsr = START_STATES[ ID ];
        ui32 bit;

        double partial = 0.0;
        for ( ui32 i = 0; i < NO_OF_READS; ++i )
        {
            bit = ( ( lfsr >> 0 ) ^ ( lfsr >> 2 ) );
            lfsr = ( lfsr >> 1 ) | ( (bit << SHAM ) & MSB_MASK );
            partial += DATA[ lfsr ];
        }

        partials[ ID ] = partial;
    }

    for ( ui32 t = 0; t < NUM_THREADS; ++t )
        total += partials[ t ];

    return total;
}


double
networkTraffic( double const* const DATA, ui32 const* const START_STATES,
                const ui32& NO_OF_READS, const ui32& READ_LENGTH,
                const ui32& NUM_THREADS )
{
    const ui32 MSB_MASK = 0x00100000u;
    const ui32 SHAM = 20;

    double total = 0.0;
    double partials[ NUM_THREADS ];
    #pragma omp parallel num_threads( NUM_THREADS )
    {
        const ui32 ID = omp_get_thread_num();
        ui32 lfsr = START_STATES[ ID ];
        ui32 bit;

        double partial = 0.0;
        for ( ui32 i = 0; i < NO_OF_READS; ++i )
        {
            bit = ( ( lfsr >> 0 ) ^ ( lfsr >> 2 ) );
            lfsr = ( lfsr >> 1 ) | ( ( bit << SHAM ) & MSB_MASK );

            DISTANCE_METHOD( partial, DATA, lfsr, READ_LENGTH );
            // for ( ui32 j = 0; j < READ_LENGTH; ++j )
            //     partial += DATA[ lfsr + j ];
        }

        partials[ ID ] = partial;
    }

    for ( ui32 t = 0; t < NUM_THREADS; ++t )
        total += partials[ t ];

    return total;
}


double
stream1( double** perThrdData, const ui32& NO_OF_READS,
         const ui32& NUM_THREADS )
{
    double total = 0.0;
    double partials[ NUM_THREADS ];
    #pragma omp parallel num_threads( NUM_THREADS )
    {
        const ui32 ID = omp_get_thread_num();
        double const* const DATA = perThrdData[ ID ];
        double partial = 0.0;
        for ( ui32 i = 0; i < NO_OF_READS; ++i )
            partial += DATA[ i ];

        partials[ ID ] = partial;
    }

    for ( ui32 t = 0; t < NUM_THREADS; ++t )
        total += partials[ t ];

    return total;
}

double
stream( double** perThrdData,
        const ui32& NO_OF_READS, const ui32& READ_LENGTH,
        const ui32& NUM_THREADS )
{
    double total = 0.0;
    double partials[ NUM_THREADS ];
    #pragma omp parallel num_threads( NUM_THREADS )
    {
        const ui32 ID = omp_get_thread_num();
        double const* const DATA = perThrdData[ ID ];
        double partial = 0.0;
        ui32 ind = 0;
        for ( ui32 i = 0; i < NO_OF_READS; ++i )
        {
            DISTANCE_METHOD( partial, DATA, ind, READ_LENGTH );
            ind += READ_LENGTH;
            // for ( ui32 j = 0; j < READ_LENGTH; ++j, ++ind )
            // partial += DATA[ ind ];
        }


        partials[ ID ] = partial;
    }

    for ( ui32 t = 0; t < NUM_THREADS; ++t )
        total += partials[ t ];

    return total;
}

template < typename mem >
double
doExperiment(
    const ui32& ITERATIONS,
    const ui32& DATA_LENGTH, const ui32& INITIAL_NO_OF_READS,
    std::initializer_list< ui32 > readLengths,
    std::initializer_list< ui32 > readMultipliers,
    const ui32& NUM_THREADS )
{
    double dummy = 0.0;

    const unsigned MASK = 0x001FFFFFu;
    ui32 startStates[ NUM_THREADS ];
    for ( ui32 i = 0; i < NUM_THREADS; ++i )
        startStates[ i ] = rand() & MASK;

    ui32 maxReadLength = 0;
    for ( auto rl = readLengths.begin(); rl != readLengths.end(); ++rl )
        maxReadLength = ( *rl > maxReadLength ) ? *rl : maxReadLength;

    ui32 maxReadMultiplier = 0;
    for ( auto rm = readMultipliers.begin(); rm != readMultipliers.end(); ++rm )
        maxReadMultiplier = ( *rm > maxReadMultiplier ) ? *rm : maxReadMultiplier;

    // blocked random access tests
    // ------------------------------------------------------------------------
    std::cout << "Network-Traffic" << std::endl;
    const ui32 REAL_LENGTH = DATA_LENGTH + maxReadLength;
    double* data = (double*) mem::alloc( REAL_LENGTH, sizeof( double ) );
    #pragma omp parallel for
    for ( ui64 i = 0; i < REAL_LENGTH; ++i )
        data[ i ] = i;

    RRUN( ITERATIONS,
          dummy += networkTraffic1( data, startStates, INITIAL_NO_OF_READS,
                                    NUM_THREADS ); );
    for ( auto rl = readLengths.begin();
          rl != readLengths.end();
          ++rl )
    {
        RRUN( ITERATIONS, dummy += networkTraffic(
                  data, startStates, INITIAL_NO_OF_READS,
                  *rl, NUM_THREADS ); );
    }
    std::cout << std::endl;

    for ( auto rm = readMultipliers.begin();
          rm != readMultipliers.end();
          ++rm )
    {
        RRUN( ITERATIONS,
              dummy += networkTraffic1(
                  data, startStates, *rm * INITIAL_NO_OF_READS,
                  NUM_THREADS ); );

        for ( auto rl = readLengths.begin();
              rl != readLengths.end();
              ++rl )
        {
            RRUN( ITERATIONS, dummy += networkTraffic(
                      data, startStates, *rm * INITIAL_NO_OF_READS,
                      *rl, NUM_THREADS ); );
        }

        std::cout << std::endl;
    }
    std::cout << std::endl;

    // local reads
    // ------------------------------------------------------------------------
    const ui32 PER_THRD_LENGTH = ( (ui32) pow( 2, 15 ) * 68 ) / NUM_THREADS;
    const ui32 MASK_SMALL = 0x0007FFFu;
    for ( ui32 i = 0; i < NUM_THREADS; ++i )
        startStates[ i ] = rand() & MASK_SMALL;

    std::cout << "Local-Read" << std::endl;
    RRUN( ITERATIONS,
          dummy += localRead1( data, startStates, INITIAL_NO_OF_READS,
                               PER_THRD_LENGTH, NUM_THREADS ); );
    for ( auto rl = readLengths.begin();
          rl != readLengths.end();
          ++rl )
    {
        RRUN( ITERATIONS, dummy += localRead(
                  data, startStates, INITIAL_NO_OF_READS,
                  *rl, PER_THRD_LENGTH, NUM_THREADS ); );
    }
    std::cout << std::endl;

    for ( auto rm = readMultipliers.begin();
          rm != readMultipliers.end();
          ++rm )
    {
        RRUN( ITERATIONS,
              dummy += localRead1(
                  data, startStates, *rm * INITIAL_NO_OF_READS,
                  PER_THRD_LENGTH, NUM_THREADS ); );

        for ( auto rl = readLengths.begin();
              rl != readLengths.end();
              ++rl )
        {
            RRUN( ITERATIONS, dummy += localRead(
                      data, startStates, *rm * INITIAL_NO_OF_READS,
                      *rl, PER_THRD_LENGTH, NUM_THREADS ); );
        }

        std::cout << std::endl;
    }
    std::cout << std::endl;


    mem::free( data, REAL_LENGTH, sizeof( double ) );

    // stream tests
    // ------------------------------------------------------------------------
    std::cout << "Stream" << std::endl;
    double* perThrdData[ NUM_THREADS ];
    const ui32 LEN = maxReadLength * maxReadMultiplier * INITIAL_NO_OF_READS;
    #pragma omp parallel num_threads( NUM_THREADS )
    {
        const ui32 ID = omp_get_thread_num();
        perThrdData[ ID ] = (double*) mem::alloc( LEN, sizeof( double ) );

        double* data = perThrdData[ ID ];
        for ( ui32 i = 0; i < LEN; ++i )
            data[ i ] = i;
    }

    RRUN( ITERATIONS,
              dummy += stream1( perThrdData, INITIAL_NO_OF_READS,
                                NUM_THREADS ); );
    for ( auto rl = readLengths.begin();
          rl != readLengths.end();
          ++rl )
    {
        RRUN( ITERATIONS,
              dummy += stream( perThrdData, INITIAL_NO_OF_READS, *rl,
                               NUM_THREADS ); );
    }
    std::cout << std::endl;

    for ( auto rm = readMultipliers.begin();
          rm != readMultipliers.end();
          ++rm )
    {
        RRUN( ITERATIONS,
              dummy += stream1( perThrdData, *rm * INITIAL_NO_OF_READS,
                                NUM_THREADS ); );

        for ( auto rl = readLengths.begin();
              rl != readLengths.end();
              ++rl )
        {
            RRUN( ITERATIONS,
                  dummy += stream( perThrdData, *rm * INITIAL_NO_OF_READS, *rl,
                                   NUM_THREADS ); );
        }

        std::cout << std::endl;
    }
    std::cout << std::endl;

    for ( ui32 t = 0; t < NUM_THREADS; ++t )
        mem::free( perThrdData[ t ], LEN, sizeof( double ) );

    return dummy;
}

int main( int argc, char* argv[] )
{
	Cli cli( argc, argv );

	// Parafmeters
	// ------------------------------------------------------------------------
	if ( !cli.isSet( { Cli::NUM_BLOCKS } ) )
	{
		return EXIT_FAILURE;
	}

	ui32 iterations = cli.getIterations();
	ui32 numThreads = cli.getNumBlocks();
	double networkL2MB = 17;
    double localL2KB = 256;

    const ui64 DATA_LENGTH = ( 17 * pow( 2, 20 ) ) / 8;
    const ui32 INITIAL_NO_OF_READS = DATA_LENGTH / numThreads;

    std::cout << "ddr4" << std::endl;
    double dummy = doExperiment< Aligned >(
        iterations, DATA_LENGTH, INITIAL_NO_OF_READS,
        { 2, 3, 4, 6, 8, 12, 16, 24, 32, 64 },
        { 2, 3, 4, 6, 8, 12, 16, 24, 32 },
        numThreads );
    std::cout << std::endl;
    std::cout << "dummy=" << dummy << std::endl;

#ifdef WITH_HBW_MEMORY
    std::cout << "Mcdram" << std::endl;
	double dummy2 = doExperiment< Mcdram >(
        iterations, DATA_LENGTH, INITIAL_NO_OF_READS,
        { 2, 3, 4, 6, 8, 12, 16, 24, 32, 64 },
        { 2, 3, 4, 6, 8, 12, 16, 24, 32 },
        numThreads );
    std::cout << std::endl;
    std::cout << "dummy2=" << dummy2 << std::endl;
#endif

    // double data[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
    // ui32 startStates[] = { 9, 1 };
    // ui32 noOfReads = 8;
    // double dummy = networkTraffic( data, startStates, noOfReads, 4, 2 );
    // std::cout << dummy << std::endl;

    // const ui32 startState = 1;
    // lfsr( startState );
    // lfsr_local( startState );
	return EXIT_SUCCESS;

}
