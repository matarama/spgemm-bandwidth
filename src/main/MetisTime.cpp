
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <set>
#include <omp.h>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#endif

#include "include/sparse_types.h"
#include "include/util/Generic.h"
#include "include/util/Kernel.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
#include "include/data_structure/Triplet.h"
#include "include/data_structure/DenseArray.h"
#include "include/storage_format/Csr.h"
#include "include/storage_format/Csr2.h"
#include "include/io/InputReader.h"
#include "include/io/MMInfo.h"
#include "include/io/Cli.h"
#include "include/logging/Timer.h"
#include "include/parallel/Gustavson.h"
#include "include/parallel/GustavsonMultiHashMap.h"
#include "include/parallel/GustavsonIncorrect.h"

using namespace rbr;

// #############################################################################

extern void readFile( const std::string& path, std::vector< glbInd_t >& v );

extern void generateThrdRowPfxSum(
	const std::vector< glbInd_t >& A_rowPfxSumPerPart,
	const ui32& numThreads,
	glbInd_t** thrdRowPfxSum_out );

extern void generateThrdRowPfxSumAux(
	glbInd_t const* const A_rowPfxSumPerPart,
	const ui32& length,
	const ui32& numThreads,
	glbInd_t* thrdRowPfxSum );

int main( int argc, char* argv[] )
{
	Cli cli( argc, argv );

	// Parameters
	// ------------------------------------------------------------------------
	if ( !cli.isSet( { Cli::MMF_PATH_A, Cli::NUM_BLOCKS,
	                   Cli::PATH_TO_ROW_ORDERING_A,
	                   Cli::PATH_TO_ROW_PREFIX_SUM_PER_PART_A,
	                   Cli::PATH_TO_ROW_ORDERING_B} ) )
	{
		return EXIT_FAILURE;
	}

	ui32 iterations = cli.getIterations();
	std::string mmfPathA = cli.getMmfPathA();
	std::string mmfPathB = cli.getMmfPathB();
	ui32 numThreads = cli.getNumBlocks();
	bool transposeB = cli.getTransposeB();
	std::vector< ui32 > vAccSizes = cli.getAccSizes();
	std::string A_pathToRowOrdering = cli.getPathToRowOrderingA();
	std::string A_pathToRowPfxSumPerPart = cli.getPathToRowPfxSumPerPartA();
	std::string B_pathToRowOrdering = cli.getPathToRowOrderingB();

	omp_set_num_threads( numThreads );
#ifdef __INTEL_COMPILER
	mkl_set_num_threads( numThreads );
#endif

	// Read matrix market format file.
	// ------------------------------------------------------------------------
	MMInfo A_mmInfo;
	int& A_isSymmetric = A_mmInfo.isSymmetric;
	int A_rowCount = 0;
	int A_colCount = 0;
	int A_nnz = 0;
	int* A_is = NULL;
	int* A_js = NULL;
	double* A_nzs = NULL;
	InputReader::readMMF( mmfPathA, A_mmInfo, &A_rowCount, &A_colCount,
	                      &A_nnz, &A_is, &A_js, &A_nzs);
	Triplet* tripletArrA = Triplet::array( A_nnz, A_is, A_js, A_nzs );
	Triplets A( 0, 0, A_rowCount, A_colCount,
	            tripletArrA, tripletArrA + A_nnz );
	std::cout << mmfPathA.substr( mmfPathA.find_last_of( '/' ) + 1,
	                              mmfPathA.find_last_of( '.' ) - 1 ) << ",";

	MMInfo B_mmInfo;
	int B_isSymmetric = 0;
	int B_rowCount = 0;
	int B_colCount = 0;
	int B_nnz = 0;
	int* B_is = NULL;
	int* B_js = NULL;
	double* B_nzs = NULL;

	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		InputReader::readMMF( mmfPathB, B_mmInfo, &B_rowCount, &B_colCount,
		                      &B_nnz, &B_is, &B_js, &B_nzs);
		B_isSymmetric = B_mmInfo.isSymmetric;
	}
	else
	{
		mmfPathB = mmfPathA;
		B_isSymmetric = A_isSymmetric;
		B_rowCount = A_rowCount;
		B_colCount = A_colCount;
		B_nnz = A_nnz;
		B_is = A_is;
		B_js = A_js;
		B_nzs = A_nzs;
	}

	Triplet* tripletArrB = Triplet::array( B_nnz, B_is, B_js, B_nzs );
	Triplets B = Triplets( 0, 0, B_rowCount, B_colCount,
	                       tripletArrB, tripletArrB + B_nnz );

	if ( transposeB )
		B.transpose();

	// clean up
	free( A_is );
	free( A_js );
	free( A_nzs );	
	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		free( B_is );
		free( B_js );
		free( B_nzs );
	}

	// Reorder A's rows & columns, B's rows
	// -------------------------------------------------------------------------
	// {
	// 	std::cout << "Before ordering: " << std::endl;
		
	// 	std::cout << "A:" << std::endl;
	// 	std::shared_ptr< std::vector< glbNnz_t > > pRowPtrA = A.rowPtr();
	// 	std::vector< glbNnz_t >& rowPtrA = *pRowPtrA.get();
	// 	A.sortRowMajor();
	// 	for ( glbInd_t r = 0; r < A_rowCount; ++r )
	// 	{
	// 		std::cout << r << "(" << rowPtrA[ r + 1 ] - rowPtrA[ r ] <<  ") ->";
	// 		for ( glbNnz_t n = rowPtrA[ r ]; n < rowPtrA[ r + 1 ]; ++n )
	// 			std::cout << " " << A[ n ].j;
	// 		std::cout << std::endl;
	// 	}
	// 	std::cout << std::endl;

	// 	std::cout << "B:" << std::endl;
	// 	std::shared_ptr< std::vector< glbNnz_t > > pRowPtrB = B.rowPtr();
	// 	std::vector< glbNnz_t >& rowPtrB = *pRowPtrB.get();
	// 	B.sortRowMajor();
	// 	for ( glbInd_t r = 0; r < B_rowCount; ++r )
	// 	{
	// 		std::cout << r << "(" << rowPtrB[ r + 1 ] - rowPtrB[ r ] <<  ") ->";
	// 		for ( glbNnz_t n = rowPtrB[ r ]; n < rowPtrB[ r + 1 ]; ++n )
	// 			std::cout << " " << B[ n ].j;
	// 		std::cout << std::endl;
	// 	}
	// 	std::cout << std::endl;
	// }

	{
		std::vector< glbInd_t > A_rowOrdering;
		std::vector< glbInd_t > B_rowOrdering;
		readFile( A_pathToRowOrdering, A_rowOrdering );
		readFile( B_pathToRowOrdering, B_rowOrdering );

		Triplets::updateRowColumnIndices( A, B, A_rowOrdering, B_rowOrdering );

		A.sortRowMajor();
		B.sortRowMajor();
	}

	// {
	// 	std::cout << "After ordering: " << std::endl;
		
	// 	std::cout << "A:" << std::endl;
	// 	std::shared_ptr< std::vector< glbNnz_t > > pRowPtrA = A.rowPtr();
	// 	std::vector< glbNnz_t >& rowPtrA = *pRowPtrA.get();
	// 	A.sortRowMajor();
	// 	for ( glbInd_t r = 0; r < A_rowCount; ++r )
	// 	{
	// 		std::cout << r << "(" << rowPtrA[ r + 1 ] - rowPtrA[ r ] <<  ") ->";
	// 		for ( glbNnz_t n = rowPtrA[ r ]; n < rowPtrA[ r + 1 ]; ++n )
	// 			std::cout << " " << A[ n ].j;
	// 		std::cout << std::endl;
	// 	}
	// 	std::cout << std::endl;

	// 	std::cout << "B:" << std::endl;
	// 	std::shared_ptr< std::vector< glbNnz_t > > pRowPtrB = B.rowPtr();
	// 	std::vector< glbNnz_t >& rowPtrB = *pRowPtrB.get();
	// 	B.sortRowMajor();
	// 	for ( glbInd_t r = 0; r < B_rowCount; ++r )
	// 	{
	// 		std::cout << r << "(" << rowPtrB[ r + 1 ] - rowPtrB[ r ] <<  ") ->";
	// 		for ( glbNnz_t n = rowPtrB[ r ]; n < rowPtrB[ r + 1 ]; ++n )
	// 			std::cout << " " << B[ n ].j;
	// 		std::cout << std::endl;
	// 	}
	// 	std::cout << std::endl;
	// }
	
	// Preprocess matrix C = A x B
	// -------------------------------------------------------------------------
	ui64 rowStartC = 0;
    ui64 colStartC = 0;
	ui64 nnzC = 0;
    ui64 flops = 0;
    Triplet* tripletArrC = util::preprocessC( A, B, rowStartC, colStartC,
	                                          nnzC, flops );
	Triplets C( rowStartC, colStartC, A.rows(), B.columns(),
	            tripletArrC, tripletArrC + nnzC );


	std::cout << numThreads << "t,";
	// Generate thrdRowPfxSum array from subMtxPfxSum (METIS output)
	// ------------------------------------------------------------------------
	glbInd_t* thrdRowPfxSum = nullptr;
	{
		std::vector< glbInd_t > A_rowPfxSumPerPart;
		readFile( A_pathToRowPfxSumPerPart, A_rowPfxSumPerPart );
		generateThrdRowPfxSum( A_rowPfxSumPerPart, numThreads, &thrdRowPfxSum );
		const ui32 NUM_PARTS = A_rowPfxSumPerPart.size() - 1;
		RECORD( NUM_PARTS );

		// std::cout << "A_rowPfxSumPerPart ("
		//           << A_rowPfxSumPerPart.size() << "): ";
		// for ( std::size_t i = 0; i < A_rowPfxSumPerPart.size(); ++i )
		// 	std::cout << A_rowPfxSumPerPart[ i ] << " ";
		// std::cout << std::endl;
		// std::cout << std::endl;

		// std::cout << "thrdRowPfxSum (" << numThreads + 1 << ")";
		// for ( std::size_t i = 0; i <= numThreads; ++i )
		// 	std::cout << thrdRowPfxSum[ i ] << " ";
		// std::cout << std::endl;
		// std::cout << std::endl;
	}

	{
		Csr< glbInd_t, fp_t, glbNnz_t > csrC;
		Csr< glbInd_t, fp_t, glbNnz_t > csrA;
		Csr< glbInd_t, fp_t, glbNnz_t > csrB;
		gustavson::prepare( A, B, csrA, csrB, csrC );

		fp_t** accumulators = nullptr;
		#pragma omp parallel num_threads( numThreads )
		{                  
			#pragma omp single
			{
				accumulators = util::alloc< fp_t* >( numThreads );
			}
			int tid = omp_get_thread_num();
			accumulators[ tid ] = util::newVector< fp_t >( A.rows() );
		}        

		RECORD( "Static" );
		RRUN( iterations,
		      gustavson::CAB_dense( csrA, csrB, csrC, accumulators,
		                            numThreads, thrdRowPfxSum ); );

		// Using Csr2 data structure for B
		// ---------------------------------------------------------------------
		RECORD( "Static-csr2" );
		Csr2< glbInd_t, fp_t, glbNnz_t > csrB2 = csrB;
		Csr< glbInd_t, fp_t, glbNnz_t > csrC2 = csrC;
		RRUN( iterations,
		      gustavson::CAB_dense( csrA, csrB2, csrC2, accumulators,
		                            numThreads, thrdRowPfxSum ); );
		
		// Correctness check
		csrC2.zero();
		gustavson::CAB_dense( csrA, csrB2, csrC2, accumulators,
		                      numThreads, thrdRowPfxSum );

		RECORD( ( util::isSame( csrC.getNzs(), csrC2.getNzs(),
		                        csrC.nnz() ) ? " " : "!" ) );

		// Using dynamic scheduling of OMP
		// ---------------------------------------------------------------------
		RECORD( "Dynamic" );
		RRUN( iterations,
		      gustavson::CAB_dense_dynamic( csrA, csrB, csrC2,
		                                    accumulators ); );

		csrC2.zero();
		gustavson::CAB_dense_dynamic( csrA, csrB, csrC2, accumulators );

		RECORD( ( util::isSame( csrC.getNzs(), csrC2.getNzs(),
		                        csrC.nnz() ) ? " " : "!" ) );
	}
	std::cout << std::endl;

	delete [] tripletArrC;
	delete [] tripletArrA;
	delete [] tripletArrB;
	return EXIT_SUCCESS;
}

void
readFile( const std::string& path, std::vector< glbInd_t >& v )
{
	std::ifstream inputFile( path );
	std::size_t len = 0;
	inputFile >> len;
	v.resize( len, 0 );

	for ( std::size_t i = 0; i < len; ++i )
		inputFile >> v[ i ];
	
	inputFile.close();
}

void
generateThrdRowPfxSum( const std::vector< glbInd_t >& A_rowPfxSumPerPart,
                       const ui32& numThreads,
                       glbInd_t** thrdRowPfxSum_out )
{
	glbInd_t* thrdRowPfxSum = nullptr;
	#pragma omp parallel
	#pragma omp single
	thrdRowPfxSum = util::newVector< glbInd_t >( numThreads );
	
	generateThrdRowPfxSumAux( &A_rowPfxSumPerPart[ 0 ],
	                          A_rowPfxSumPerPart.size(),
	                          numThreads, thrdRowPfxSum );

	*thrdRowPfxSum_out = thrdRowPfxSum;
}

void
generateThrdRowPfxSumAux( glbInd_t const* const A_rowPfxSumPerPart,
                          const ui32& length,
                          const ui32& numThreads,
                          glbInd_t* thrdRowPfxSum )
{
	if ( length < 2 || numThreads < 2 )
	{
		thrdRowPfxSum[ 1 ] = A_rowPfxSumPerPart[ length - 1 ];
	}
	else
	{
		ui32 leftLength = length / 2;
		ui32 rightLength = length - leftLength;

		ui32 leftThreads = numThreads / 2;
		ui32 rightThreads = numThreads - leftThreads;

		generateThrdRowPfxSumAux( A_rowPfxSumPerPart,
		                          leftLength, leftThreads, thrdRowPfxSum );

		generateThrdRowPfxSumAux( A_rowPfxSumPerPart + leftLength,
		                          rightLength, rightThreads,
		                          thrdRowPfxSum + leftThreads );
	}
}
