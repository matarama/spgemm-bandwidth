
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <set>
#include <omp.h>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#endif

#include "include/sparse_types.h"
#include "include/util/Generic.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Csr.h"
#include "include/io/InputReader.h"
#include "include/io/MMInfo.h"
#include "include/io/Cli.h"
#include "include/logging/Timer.h"
#include "include/parallel/Gustavson.h"
#include "include/parallel/GustavsonIncorrect.h"

using namespace rbr;

// #############################################################################

extern void readFile( const std::string& path, std::vector< glbInd_t >& v,
                      const glbInd_t& limit = 0 );

extern void generateThrdRowPfxSum(
	const std::vector< glbInd_t >& A_rowPfxSumPerPart,
	const ui32& numThreads,
	glbInd_t** thrdRowPfxSum_out );

extern void generateThrdRowPfxSumAux(
	glbInd_t const* const A_rowPfxSumPerPart,
	const ui32& length,
	const ui32& numThreads,
	glbInd_t* thrdRowPfxSum );

extern void reorder( const std::vector< glbInd_t >& partVectorA,
                     const std::vector< glbInd_t >& partVectorB,
                     Triplets& A, Triplets& B,
                     glbInd_t** thrdRowPfxSum_out );

extern void generateOrdering( const std::vector< glbInd_t >& partVector,
                              std::vector< glbInd_t >& ordering,
                              std::vector< glbInd_t >& pfxSum );

extern void generateOrdering( const std::string& partVectorPath,
                              std::vector< glbInd_t >& ordering,
                              std::vector< glbInd_t >& pfxSum,
                              const glbInd_t& limit = 0);


int main( int argc, char* argv[] )
{
	Cli cli( argc, argv );

	// Parameters
	// ------------------------------------------------------------------------
	if ( !cli.isSet( { Cli::MMF_PATH_A, Cli::NUM_BLOCKS } ) )
	{
		return EXIT_FAILURE;
	}

	ui32 iterations = cli.getIterations();
	std::string mmfPathA = cli.getMmfPathA();
	std::string mmfPathB = cli.getMmfPathB();
	ui32 numThreads = cli.getNumBlocks();
	bool transposeB = cli.getTransposeB();
	std::vector< ui32 > vAccSizes = cli.getAccSizes();
	
	omp_set_num_threads( numThreads );
#ifdef __INTEL_COMPILER
	mkl_set_num_threads( numThreads );
#endif

	// Read matrix market format file.
	// ------------------------------------------------------------------------
	MMInfo A_mmInfo;
	int& A_isSymmetric = A_mmInfo.isSymmetric;
	int A_rowCount = 0;
	int A_colCount = 0;
	int A_nnz = 0;
	int* A_is = NULL;
	int* A_js = NULL;
	double* A_nzs = NULL;
	InputReader::readMMF( mmfPathA, A_mmInfo, &A_rowCount, &A_colCount,
	                      &A_nnz, &A_is, &A_js, &A_nzs);
	Triplet* tripletArrA = Triplet::array( A_nnz, A_is, A_js, A_nzs );
	Triplets _A( 0, 0, A_rowCount, A_colCount,
	             tripletArrA, tripletArrA + A_nnz );
	std::cout << mmfPathA.substr( mmfPathA.find_last_of( '/' ) + 1,
	                              mmfPathA.find_last_of( '.' ) - 1 ) << ",";

	MMInfo B_mmInfo;
	int B_isSymmetric = 0;
	int B_rowCount = 0;
	int B_colCount = 0;
	int B_nnz = 0;
	int* B_is = NULL;
	int* B_js = NULL;
	double* B_nzs = NULL;

	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		InputReader::readMMF( mmfPathB, B_mmInfo, &B_rowCount, &B_colCount,
		                      &B_nnz, &B_is, &B_js, &B_nzs);
		B_isSymmetric = B_mmInfo.isSymmetric;
	}
	else
	{
		mmfPathB = mmfPathA;
		B_isSymmetric = A_isSymmetric;
		B_rowCount = A_rowCount;
		B_colCount = A_colCount;
		B_nnz = A_nnz;
		B_is = A_is;
		B_js = A_js;
		B_nzs = A_nzs;
	}

	Triplet* tripletArrB = Triplet::array( B_nnz, B_is, B_js, B_nzs );
	Triplets _B = Triplets( 0, 0, B_rowCount, B_colCount,
	                        tripletArrB, tripletArrB + B_nnz );

	if ( transposeB )
		_B.transpose();

	// clean up
	free( A_is );
	free( A_js );
	free( A_nzs );	
	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		free( B_is );
		free( B_js );
		free( B_nzs );
	}

	// Order A matrix
	// -------------------------------------------------------------------------
	
	if ( cli.isSet( Cli::PATH_TO_PART_VECTOR_A ) )
	{
		std::cout << "metis,";
		std::string pathToPartVectorA = cli.getPathToPartVectorA();
		glbInd_t numParts = 0;
	
		{
			{
				std::vector< glbInd_t > A_partPfxSum;
				std::vector< glbInd_t > A_rowOrdering;
				generateOrdering( pathToPartVectorA, A_rowOrdering, A_partPfxSum,
				                  _A.rows() );

				// remap A's row-indices
				_A.remapI( A_rowOrdering );

				std::cout << numThreads << "t,";
				std::cout << numParts << ",";
			}

			// Reorder A and B triplets
			_A.sortRowMajor();
		}
	}
	else
	{
		std::cout << "no-metis,";
	}

	// Create row to thread mapping
	std::vector< ui64 > multPfxSumPerThrd;
	glbInd_t* thrdRowPfxSum = nullptr;
	util::partition( _A, _B, &thrdRowPfxSum, multPfxSumPerThrd, numThreads );

	fp_t** accumulators = nullptr;
	ind_t* C_offsets_ddr4 = nullptr;
	ind_t* C_offsetsPerThrd_ddr4[ numThreads ];

	// Runs using ddr4 memory
	// -------------------------------------------------------------------------
	{
		Csr< glbInd_t, fp_t, glbNnz_t, allocator::Aligned > C_ddr4;
		Csr< glbInd_t, fp_t, glbNnz_t, allocator::Aligned > A_ddr4;
		Csr< glbInd_t, fp_t, glbNnz_t, allocator::Aligned > B_ddr4;

		#pragma omp parallel num_threads( numThreads )
		{                  
			#pragma omp single
			{
				C_offsets_ddr4 =
					util::alloc< ind_t >( multPfxSumPerThrd.back() );
				accumulators = util::alloc< fp_t* >( numThreads );
			}
			int tid = omp_get_thread_num();
			accumulators[ tid ] = util::newVector< fp_t >( _B.columns() );
			C_offsetsPerThrd_ddr4[ tid ] =
				C_offsets_ddr4 + multPfxSumPerThrd[ tid ];

			#pragma omp sections
			{
				#pragma omp section
				A_ddr4.extract( _A );

				#pragma omp section
				B_ddr4.extract( _B );
			}
		}

		gustavson::symbolic_cheating_stl_alloc(
			A_ddr4, B_ddr4, C_ddr4, accumulators, C_offsetsPerThrd_ddr4,
			thrdRowPfxSum, numThreads );

		// for ( ui32 t = 0; t < numThreads; ++t )
		// {
		// 	std::cout << "t-" << t << " offsets: ";
		// 	for ( ui64 i = multPfxSumPerThrd[ t ];
		// 	      i < multPfxSumPerThrd[ t + 1 ];
		// 	      ++i )
		// 	{
		// 		std::cout << C_offsets_ddr4[ i ] << " ";
		// 	}
		// 	std::cout << std::endl;
		// }
			
		Csr< glbInd_t, fp_t, glbNnz_t, allocator::Aligned > C2_ddr4 = C_ddr4;

		RECORD( "ddr4" );
		RECORD( "Symbolic" );
		RRUN( iterations,
		      gustavson::symbolic_cheating_stl(
			      A_ddr4, B_ddr4, C_ddr4, accumulators, C_offsetsPerThrd_ddr4,
			      thrdRowPfxSum, numThreads ); );

		// ---------------------------------------------------------------------
		RECORD( "Metis-acc" );
		RRUN( iterations,
		      gustavson::numeric( A_ddr4, B_ddr4, C_ddr4, accumulators,
		                          thrdRowPfxSum, numThreads ); );
		
		RECORD( "Metis-cheating" );
		RRUN( iterations,
		      gustavson::numeric( A_ddr4, B_ddr4, C2_ddr4,
		                          C_offsetsPerThrd_ddr4,
		                          thrdRowPfxSum, numThreads ); );
		RECORD( ( util::isSame( C_ddr4.nzs, C2_ddr4.nzs,
		                        C_ddr4.nnz ) ? " " : "!" ) );

#ifdef WITH_HBW_MEMORY
		// Using MCDRAM memory allocator
		// ---------------------------------------------------------------------
		Csr< glbInd_t, fp_t, glbNnz_t, allocator::Mcdram > A_mcdram( _A );
		Csr< glbInd_t, fp_t, glbNnz_t, allocator::Mcdram > B_mcdram( _B );
		Csr< glbInd_t, fp_t, glbNnz_t, allocator::Mcdram > C_mcdram;

		gustavson::symbolic_cheating_stl_alloc(
			A_mcdram, B_mcdram, C_mcdram, accumulators, C_offsetsPerThrd_ddr4,
			thrdRowPfxSum, numThreads );

		ind_t* C_offsets_mcdram = (ind_t*) allocator::Mcdram::alloc(
			multPfxSumPerThrd.back(), sizeof( ind_t ) );

		ind_t* C_offsetsPerThrd_mcdram[ numThreads ];
		for ( ui32 t = 0; t < numThreads; ++t )
			C_offsetsPerThrd_mcdram[ t ] =
				C_offsets_mcdram + multPfxSumPerThrd[ t ];

		RECORD( "mcdram" );
		RECORD( "Symbolic" );
		RRUN( iterations,
		      gustavson::symbolic_cheating_stl(
			      A_mcdram, B_mcdram, C_mcdram, accumulators,
			      C_offsetsPerThrd_mcdram, thrdRowPfxSum, numThreads ); );

		RECORD( "mcdram" );
		RECORD( "Symbolic" );
		RRUN( iterations,
		      gustavson::symbolic_stl(
			      A_mcdram, B_mcdram, C_mcdram, accumulators,
			      thrdRowPfxSum, numThreads ); );

		// ---------------------------------------------------------------------
		RECORD( "Metis-acc" );
		RRUN( iterations,
		      gustavson::numeric( A_mcdram, B_mcdram, C_mcdram, accumulators,
		                          thrdRowPfxSum, numThreads ); );
		RECORD( ( util::isSame( C_ddr4.nzs, C_mcdram.nzs,
		                        C_ddr4.nnz ) ? " " : "!" ) );

		RECORD( "Metis-cheating" );
		RRUN( iterations,
		      gustavson::numeric( A_mcdram, B_mcdram, C_mcdram,
		                          C_offsetsPerThrd_mcdram,
		                          thrdRowPfxSum, numThreads ); );
		RECORD( ( util::isSame( C_ddr4.nzs, C_mcdram.nzs,
		                        C_ddr4.nnz ) ? " " : "!" ) );

		allocator::Mcdram::free( (void*) C_offsets_mcdram,
		                         multPfxSumPerThrd.back(),
		                         sizeof( ind_t ) );
#endif

	}
	std::cout << std::endl;

	for ( ui32 t = 0; t < numThreads; ++t )
		util::free( accumulators[ t ] );
	util::free( accumulators );
	util::free( thrdRowPfxSum );
	util::free( C_offsets_ddr4 );

	delete [] tripletArrA;
	delete [] tripletArrB;
	return EXIT_SUCCESS;
}

void
readFile( const std::string& path, std::vector< glbInd_t >& v,
          const glbInd_t& limit )
{
	std::ifstream inputFile( path );

	long partId = 0;
	if ( limit > 0 )
	{
		while ( inputFile >> partId && v.size() < limit )
			v.push_back( partId );
	}
	else
	{
		while ( inputFile >> partId )
			v.push_back( partId );
	}

	inputFile.close();
}

void
generateThrdRowPfxSum( const std::vector< glbInd_t >& A_rowPfxSumPerPart,
                       const ui32& numThreads,
                       glbInd_t** thrdRowPfxSum_out )
{
	glbInd_t* thrdRowPfxSum = nullptr;
	#pragma omp parallel
	#pragma omp single
	thrdRowPfxSum = util::newVector< glbInd_t >( numThreads );
	
	generateThrdRowPfxSumAux( &A_rowPfxSumPerPart[ 0 ],
	                          A_rowPfxSumPerPart.size(),
	                          numThreads, thrdRowPfxSum );

	*thrdRowPfxSum_out = thrdRowPfxSum;
}

void
generateThrdRowPfxSumAux( glbInd_t const* const A_rowPfxSumPerPart,
                          const ui32& length,
                          const ui32& numThreads,
                          glbInd_t* thrdRowPfxSum )
{
	if ( length < 2 || numThreads < 2 )
	{
		thrdRowPfxSum[ 1 ] = A_rowPfxSumPerPart[ length - 1 ];
	}
	else
	{
		ui32 leftLength = length / 2;
		ui32 rightLength = length - leftLength;

		ui32 leftThreads = numThreads / 2;
		ui32 rightThreads = numThreads - leftThreads;

		generateThrdRowPfxSumAux( A_rowPfxSumPerPart,
		                          leftLength, leftThreads, thrdRowPfxSum );

		generateThrdRowPfxSumAux( A_rowPfxSumPerPart + leftLength,
		                          rightLength, rightThreads,
		                          thrdRowPfxSum + leftThreads );
	}
}

void
reorder( const std::vector< glbInd_t >& partVectorA,
         const std::vector< glbInd_t >& partVectorB,
         Triplets& A, Triplets& B, glbInd_t** thrdRowPfxSum_out )
{
	std::vector< glbInd_t > rowOrderingA( A.rows(), 0 );
	
	
	std::vector< glbInd_t > rowOrderingB( B.rows(), 0 );
}

void
generateOrdering( const std::vector< glbInd_t >& partVector,
                  std::vector< glbInd_t >& ordering,
                  std::vector< glbInd_t >& pfxSum )
{
	// Count how many partitions there are
	glbInd_t k = 0;
	for ( std::size_t i = 0; i < partVector.size(); ++i )
		if ( partVector[ i ] > k )
			k = partVector[ i ];
	++k;

	// generate pfx-sum (beginning & end of each part)
	pfxSum.resize( k + 1, 0 );
	for ( std::size_t i = 0; i < partVector.size(); ++i )
		++pfxSum[ partVector[ i ] + 1 ];

	std::vector< glbInd_t > counter( k, 0 );
	for ( glbInd_t i = 0; i < k; ++i )
	{
		pfxSum[ i + 1 ] += pfxSum[ i ];
		counter[ i ] = pfxSum[ i ];
	}

	ordering.resize( partVector.size(), 0 );
	for ( std::size_t i = 0; i < partVector.size(); ++i )
	{
		ordering[ i ] = counter[ partVector[ i ] ];
		++counter[ partVector[ i ] ];
	}
}

void
generateOrdering( const std::string& partVectorPath,
                  std::vector< glbInd_t >& ordering,
                  std::vector< glbInd_t >& pfxSum,
                  const glbInd_t& limit )
{
	std::vector< glbInd_t > partVector;
	readFile( partVectorPath, partVector, limit );
	generateOrdering( partVector, ordering, pfxSum );
}
