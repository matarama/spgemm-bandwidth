
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <map>
#include <omp.h>
#include <set>
#include <unordered_set>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#include "ittnotify.h"
#endif

#include "include/sparse_types.h"
#include "include/util/Generic.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Cs.h"
#include "include/io/InputReader.h"
#include "include/io/MMInfo.h"
#include "include/io/Cli.h"
#include "include/logging/Timer.h"
#include "include/parallel/Gustavson.h"
#include "include/parallel/Reduced_v0.h"

using namespace allocator;
using namespace sparse_storage;
using namespace spmv;

template < typename ind_t, typename fp_t, typename nnz_t,
           typename zind_t, typename znnz_t,
           typename mem >
void
runTests(
	Triplets& _A, Triplets& _B, fp_t** accumulators, const ui32 numThreads,
	const ui32& iterations, const bool& sorted );

int main( int argc, char** argv )
{
#ifdef __INTEL_COMPILER
	__itt_pause();
#endif

	Cli cli( argc, argv );

	// Parameters
	// ------------------------------------------------------------------------
	if ( !cli.isSet( { Cli::MMF_PATH_A, Cli::NUM_BLOCKS } ) )
	{
		return EXIT_FAILURE;
	}

	ui32 iterations = cli.getIterations();
	std::string mmfPathA = cli.getMmfPathA();
	std::string mmfPathB = cli.getMmfPathB();
	ui32 numThreads = cli.getNumBlocks();
	bool transposeB = cli.getTransposeB();
	std::vector< ui32 > vAccSizes = cli.getAccSizes();
	bool isSorted = cli.isSorted();

	omp_set_num_threads( numThreads );

	// Read matrix market format file.
	// ------------------------------------------------------------------------
	MMInfo A_mmInfo;
	int& A_isSymmetric = A_mmInfo.isSymmetric;
	int A_rowCount = 0;
	int A_colCount = 0;
	int A_nnz = 0;
	int* A_is = NULL;
	int* A_js = NULL;
	double* A_nzs = NULL;
	InputReader::readMMF( mmfPathA, A_mmInfo, &A_rowCount, &A_colCount,
	                      &A_nnz, &A_is, &A_js, &A_nzs);
	Triplet* tripletArrA = Triplet::array( A_nnz, A_is, A_js, A_nzs );
	Triplets _A( 0, 0, A_rowCount, A_colCount,
	             tripletArrA, tripletArrA + A_nnz );
	std::cout << mmfPathA.substr( mmfPathA.find_last_of( '/' ) + 1,
	                              mmfPathA.find_last_of( '.' ) - 1 )
	          << "," << A_nnz << "," << A_rowCount << "," << A_colCount << ",";

	MMInfo B_mmInfo;
	int B_isSymmetric = 0;
	int B_rowCount = 0;
	int B_colCount = 0;
	int B_nnz = 0;
	int* B_is = NULL;
	int* B_js = NULL;
	double* B_nzs = NULL;

	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		InputReader::readMMF( mmfPathB, B_mmInfo, &B_rowCount, &B_colCount,
		                      &B_nnz, &B_is, &B_js, &B_nzs);
		B_isSymmetric = B_mmInfo.isSymmetric;
	}
	else
	{
		mmfPathB = mmfPathA;
		B_isSymmetric = A_isSymmetric;
		B_rowCount = A_rowCount;
		B_colCount = A_colCount;
		B_nnz = A_nnz;
		B_is = A_is;
		B_js = A_js;
		B_nzs = A_nzs;

		if( B_rowCount != B_colCount )
			transposeB = true;
	}

	Triplet* tripletArrB = Triplet::array( B_nnz, B_is, B_js, B_nzs );
	Triplets _B = Triplets( 0, 0, B_rowCount, B_colCount,
	                        tripletArrB, tripletArrB + B_nnz );

	if ( transposeB )
		_B.transpose();

	// clean up
	free( A_is );
	free( A_js );
	free( A_nzs );
	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		free( B_is );
		free( B_js );
		free( B_nzs );
	}

	// Permute A and B matrices
	if ( cli.getPermuteAB() )
		Triplets::permuteRandom( _A, _B );

	fp_t** accumulators = nullptr;
	#pragma omp parallel num_threads( numThreads )
	{
		#pragma omp single
		{
			accumulators = util::alloc< fp_t* >( numThreads );
		}
		int tid = omp_get_thread_num();
		accumulators[ tid ] = util::newVector< fp_t >( _B.columns() );
	}

	runTests< glbInd_t, fp_t, glbNnz_t, glbInd_t, glbNnz_t, Aligned >(
		_A, _B, accumulators, numThreads, iterations, isSorted );

#ifdef WITH_HBW_MEMORY
	runTests< glbInd_t, fp_t, glbNnz_t, glbInd_t, glbNnz_t, Mcdram >(
		_A, _B, accumulators, numThreads, iterations, isSorted );
#endif

	// clean up
	for ( ui32 t = 0; t < numThreads; ++t )
		util::free( accumulators[ t ] );
	util::free( accumulators );

	delete [] tripletArrA;
	delete [] tripletArrB;
	return EXIT_SUCCESS;
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename zind_t, typename znnz_t,
           typename mem >
void
runTests(
	Triplets& _A, Triplets& _B, fp_t** accumulators, const ui32 numThreads,
	const ui32& iterations, const bool& sorted )
{
	ui64* rowMultsPfxSum = nullptr;
	ui64* thrdMultsPfxSum = nullptr;
	nnz_t* thrdCNnzPfxSum = nullptr;
	ind_t* thrdRowPfxSum = nullptr;
	{
		#pragma omp parallel
		#pragma omp single
		{
			rowMultsPfxSum = (ui64*) Aligned::alloc( _A.rows() + 1, sizeof( ui64 ) );
			thrdMultsPfxSum = (ui64*) mem::alloc( numThreads + 1, sizeof( ui64 ) );
			thrdCNnzPfxSum = (nnz_t*) mem::alloc( numThreads + 1, sizeof( nnz_t ) );
			thrdRowPfxSum = (ind_t*) mem::alloc( numThreads + 1, sizeof( ind_t ) );
		}


		util::partitionRowWise(
			_A, _B, numThreads, thrdRowPfxSum, thrdMultsPfxSum, rowMultsPfxSum );

		// std::cout << std::endl;
		// std::cout << "thrdRowPfxSum:";
		// for ( ui32 t = 0; t < numThreads + 1; ++t )
		// 	std::cout << " " << thrdRowPfxSum[ t ];
		// std::cout << std::endl;

		// std::cout << "thrdMultsPfxSum:";
		// for ( ui32 t = 0; t < numThreads + 1; ++t )
		// 	std::cout << " " << thrdMultsPfxSum[ t ];
		// std::cout << std::endl;

		// std::cout << "rowMultsPfxSum: ";
		// for ( ind_t i = 0; i < _B.columns() + 1; ++i )
		// 	std::cout << rowMultsPfxSum[ i ] << " ";
		// std::cout << std::endl;
	}

	// sort A's rows in descending order by # of multiplications
	if ( sorted )
	{
		for ( ind_t i = _A.rows(); i > 0; --i )
			rowMultsPfxSum[ i ] -= rowMultsPfxSum[ i - 1 ];

		_A.sortRowBy( &rowMultsPfxSum[ 1 ], thrdRowPfxSum, numThreads );
	}
	Aligned::free( rowMultsPfxSum, _A.rows() + 1, sizeof( ui64 ) );

	RECORD( sorted );

	Cs< ind_t, fp_t, nnz_t, mem > A;
	Cs< ind_t, fp_t, nnz_t, mem > B;
	convertToCsr( _A, A );
	convertToCsr( _B, B );

	fp_t* D = (fp_t*) mem::alloc( _A.columns(), sizeof( fp_t ) );
	for ( glbInd_t i = 0; i < _A.columns(); ++i )
		D[ i ] = 1;

	Cs< glbInd_t, fp_t, glbNnz_t, Aligned > CGust;
	rbr::gustavson::symbolic_stl< true, true >(
		A, B, CGust, accumulators, thrdRowPfxSum, numThreads );

	RECORD( "Gustavson" );
	RECORD( "Symbolic" );
	RRUN( iterations,
	      rbr::gustavson::symbolic_stl(
		      A, B, CGust, accumulators, thrdRowPfxSum, numThreads ); );

	rbr::gustavson::symbolic_stl< false, true >(
		A, B, CGust, accumulators, thrdRowPfxSum, numThreads );

	RECORD( "Numeric" );
	RRUN( iterations,
	      rbr::gustavson::numeric(
		      A, B, D, CGust, accumulators, thrdRowPfxSum, numThreads ); );

	// ------------------------------------------------------------------------

	ind_t* extraMem = nullptr;
	#pragma omp parallel
	#pragma omp single
	extraMem =
		(ind_t*) mem::alloc( thrdMultsPfxSum[ numThreads ], sizeof( ind_t ) );

	// base spmv algorithm
	// ------------------------------------------------------------------------
	Cs< ind_t, fp_t, nnz_t, mem > C;
	spmv::Z< ind_t, fp_t, nnz_t, mem > z;
	spmv::symbolic< true >(
		_B.columns(),
		A, B, thrdRowPfxSum, thrdMultsPfxSum, numThreads, thrdCNnzPfxSum,
		accumulators, C, z, extraMem );

	// Inccorect test to see effects of branch prediction penaly
	#pragma omp parallel num_threads( numThreads )
	{
		const ui32 ID = omp_get_thread_num();
		const nnz_t START = thrdCNnzPfxSum[ ID ];
		const nnz_t END = thrdCNnzPfxSum[ ID + 1 ];
		std::sort( z.lengths + START, z.lengths + END );
	}

	spmv::numeric( z, D, thrdCNnzPfxSum, thrdMultsPfxSum, numThreads, C.nzs );
#ifdef __INTEL_COMPILER
	__itt_resume();
#endif
	spmv::numeric( z, D, thrdCNnzPfxSum, thrdMultsPfxSum, numThreads, C.nzs );
	spmv::numeric( z, D, thrdCNnzPfxSum, thrdMultsPfxSum, numThreads, C.nzs );
	spmv::numeric( z, D, thrdCNnzPfxSum, thrdMultsPfxSum, numThreads, C.nzs );
	spmv::numeric( z, D, thrdCNnzPfxSum, thrdMultsPfxSum, numThreads, C.nzs );
	spmv::numeric( z, D, thrdCNnzPfxSum, thrdMultsPfxSum, numThreads, C.nzs );
#ifdef __INTEL_COMPILER
	__itt_pause();
#endif

	C.free();
	z.free();


	mem::free( D, _A.rows(), sizeof( fp_t ) );
	mem::free( extraMem, thrdMultsPfxSum[ numThreads ], sizeof( ind_t ) );
	mem::free( thrdCNnzPfxSum, numThreads + 1, sizeof( nnz_t ) );
	mem::free( thrdRowPfxSum, numThreads + 1, sizeof( nnz_t ) );
	mem::free( thrdMultsPfxSum, numThreads + 1, sizeof( nnz_t ) );
	std::cout.flush();
}
