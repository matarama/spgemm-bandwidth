
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <map>
#include <omp.h>
#include <set>
#include <unordered_set>
#include <bitset>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#endif

#ifdef WITH_MT_METIS
#include "mtmetis.h"
#else
#include "metis.h"
#endif

#include "include/sparse_types.h"
#include "include/util/Generic.h"
#include "include/util/Kernel.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
#include "include/util/MultiwayMerge.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Csr.h"
#include "include/io/InputReader.h"
#include "include/io/MMInfo.h"
#include "include/io/Cli.h"
#include "include/logging/Timer.h"

#ifdef WITH_MT_METIS
using vtx_type = mtmetis_vtx_type;
using adj_type = mtmetis_adj_type;
using wgt_type = mtmetis_wgt_type;
using pid_type = mtmetis_pid_type;
using real_type = mtmetis_real_type;
using option_type = double;
#else
using vtx_type = int;
using adj_type = int;
using wgt_type = int;
using pid_type = int;
using real_type = float;
using option_type = int;
#endif

// ############################################################################

extern void handleMetisError( const int& code, const std::string& message );
extern void generateOrdering( const vtx_type& noOfVertices,
                              pid_type const* const partVector, const int& k,
                              vtx_type** ordering_out, adj_type** pfxSum_out );
extern void printArr( const std::string& dir, const std::string& name,
                      const std::string& suffix,
                      vtx_type const* const arr, const vtx_type& length );

int main( int argc, char* argv[] )
{
	Cli cli( argc, argv );

	std::cout << "---------------------------------"
	          << "---------------------------------------"
	          << std::endl;

	// Parameters
	// ------------------------------------------------------------------------
	if ( !cli.isSet( { Cli::MMF_PATH_A, Cli::OUTPUT_DIR,
	                   Cli::TARGETTED_CACHE_SIZE_KB,
	                   Cli::METIS_B_VERTEX_COUNT } ) )
	{
		return EXIT_FAILURE;
	}

	ui32 iterations = cli.getIterations();
	std::string mmfPathA = cli.getMmfPathA();
	std::string mmfPathB = cli.getMmfPathB();
	ui32 numThreads = cli.getNumBlocks();
	bool transposeB = cli.getTransposeB();
	double targettedCacheSizeKB = cli.getTargettedCacheSizeKB();
	int metisBVertexCount = cli.getMetisBVertexCount();
	std::string outputDir = cli.getOutputDir();

	omp_set_num_threads( numThreads );
#ifdef __INTEL_COMPILER
	mkl_set_num_threads( numThreads );
#endif

	// Read matrix market format file.
	// ------------------------------------------------------------------------
	MMInfo A_mmInfo;
	int& A_isSymmetric = A_mmInfo.isSymmetric;
	int A_rowCount = 0;
	int A_colCount = 0;
	int A_nnz = 0;
	int* A_is = NULL;
	int* A_js = NULL;
	double* A_nzs = NULL;
	InputReader::readMMF( mmfPathA, A_mmInfo, &A_rowCount, &A_colCount,
	                      &A_nnz, &A_is, &A_js, &A_nzs);
	Triplet* tripletArrA = Triplet::array( A_nnz, A_is, A_js, A_nzs );
	Triplets A( 0, 0, A_rowCount, A_colCount,
	            tripletArrA, tripletArrA + A_nnz );
	std::string mmfNameA =
		mmfPathA.substr( mmfPathA.find_last_of( '/' ) + 1,
		                 mmfPathA.find_last_of( '.' ) - 1 );
	std::cout << "A: " << mmfNameA << std::endl;

	MMInfo B_mmInfo;
	int B_isSymmetric = 0;
	int B_rowCount = 0;
	int B_colCount = 0;
	int B_nnz = 0;
	int* B_is = NULL;
	int* B_js = NULL;
	double* B_nzs = NULL;

	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		InputReader::readMMF( mmfPathB, B_mmInfo, &B_rowCount, &B_colCount,
		                      &B_nnz, &B_is, &B_js, &B_nzs);
		B_isSymmetric = B_mmInfo.isSymmetric;
	}
	else
	{
		mmfPathB = mmfPathA;
		B_isSymmetric = A_isSymmetric;
		B_rowCount = A_rowCount;
		B_colCount = A_colCount;
		B_nnz = A_nnz;
		B_is = A_is;
		B_js = A_js;
		B_nzs = A_nzs;
	}

	Triplet* tripletArrB = Triplet::array( B_nnz, B_is, B_js, B_nzs );
	Triplets B = Triplets( 0, 0, B_rowCount, B_colCount,
	                       tripletArrB, tripletArrB + B_nnz );
	std::string mmfNameB =
		mmfPathB.substr( mmfPathA.find_last_of( '/' ) + 1,
		                 mmfPathA.find_last_of( '.' ) - 1 );
	std::cout << "B: " << mmfNameB << std::endl << std::endl;

	if ( transposeB )
		B.transpose();

	// clean up
	free( A_is );
	free( A_js );
	free( A_nzs );	
	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		free( B_is );
		free( B_js );
		free( B_nzs );
	}

	// Permute A and B matrices
	if ( cli.getPermuteAB() )
		Triplets::permuteRandom( A, B );

	A.sortRowMajor();
	B.sortRowMajor();

	std::shared_ptr< std::vector< glbNnz_t > > sA_rowPtr = A.rowPtr();
	std::shared_ptr< std::vector< glbNnz_t > > sB_rowPtr = B.rowPtr();
	std::vector< glbNnz_t > A_rowPtr = *sA_rowPtr.get();
	std::vector< glbNnz_t > B_rowPtr = *sB_rowPtr.get();

	double A_sizeKB = A.KBs_CSR( sizeof( glbInd_t ), sizeof( fp_t ),
	                             sizeof( glbNnz_t ) );
	// std::cout << "A_sizeKB: " << A_sizeKB << std::endl;

	pid_type A_k = A_sizeKB / ( targettedCacheSizeKB / 2 ) + 1;
	// int A_k = 3;

	// Allocate B row used bit vector per thread
	int bytesSingleEntry = ( A_k + 7 ) / 8; // how many bytes per entry
	// how many chars per entry
	int totalLength = bytesSingleEntry * B_rowCount;

	// These are used to temporal bipartite graph
	// Keeps track of how many a entries access each row of B?
	int* B_perRowPartAccessPtr = util::newVector< int >( B_rowCount + 1 );
	int* B_perRowPartAccessArr = util::newVector< int >( A_nnz );
	int* B_perRowPartAccessLength = util::newVector< int >( B_rowCount );

	double A_preparationTime = 0.0d;
	double B_preparationTime = 0.0d;

	// Reorder A
	// -------------------------------------------------------------------------
	{
		TIMER_START();
		vtx_type noOfVertices = A_rowCount + B_rowCount;
		vtx_type noOfBalanceConstraints = 1;
		adj_type* edgeCountPfxSumPerVertex = util::alloc< adj_type >( noOfVertices + 1 );
		wgt_type* vertexWeights = util::newVector< wgt_type >( noOfVertices );
		vtx_type* vertexSizes = util::newVector< vtx_type >( noOfVertices );
		vtx_type* edgesPerVertex = util::alloc< vtx_type >( A_nnz * 2 );
		wgt_type* edgeWeights = util::alloc< wgt_type >( A_nnz * 2 );
		edgeCountPfxSumPerVertex[ 0 ] = 0;
		adj_type* edgeCountPfxSumPerBVertex =
			util::newVector< adj_type >( B_rowCount + 1 );
		edgeCountPfxSumPerBVertex[ 0 ] = 0;
		for ( glbInd_t r = 0; r < A_rowCount; ++r )
		{
			edgeCountPfxSumPerVertex[ r + 1 ] = A_rowPtr[ r + 1 ];
			for ( glbNnz_t i = A_rowPtr[ r ]; i < A_rowPtr[ r + 1 ]; ++i )
			{
				glbInd_t jA = A[ i ].j;
				vertexWeights[ r ] += B_rowPtr[ jA + 1 ] - B_rowPtr[ jA ];
				++edgeCountPfxSumPerBVertex[ jA + 1 ];

				// edge from {A} to {B}
				edgesPerVertex[ i ] = jA;
				edgeWeights[ i ] = B_rowPtr[ jA + 1 ] - B_rowPtr[ jA ];

				// calculate partition access array's length
				// for corresponding B row
				++B_perRowPartAccessPtr[ jA + 1 ];
			}
		}

		// std::cout << "edgeCountPfxSumPerBVertex:";
		edgeCountPfxSumPerBVertex[ 0 ] = edgeCountPfxSumPerVertex[ A_rowCount ];
		for ( glbInd_t r = 0; r < B_rowCount; ++r )
		{
			edgeCountPfxSumPerBVertex[ r + 1 ] +=
				edgeCountPfxSumPerBVertex[ r ];
			edgeCountPfxSumPerVertex[ A_rowCount + r ] =
				edgeCountPfxSumPerBVertex[ r ];
			// std::cout << edgeCountPfxSumPerBVertex[ r + 1 ] << std::endl;
		}
		edgeCountPfxSumPerVertex[ A_rowCount + B_rowCount ] =
			edgeCountPfxSumPerBVertex[ B_rowCount ];

		for ( glbInd_t i = 0; i < A_nnz; ++i )
		{
			glbInd_t iA = A[ i ].i;
			glbInd_t jA = A[ i ].j;
			glbInd_t idB = A_rowCount + jA - 1;

			// edge from {B} to {A}
			// std::cout << "adding for " << idB << " : "
			//           << edgeCountPfxSumPerBVertex[ jA ] << std::endl;
			edgesPerVertex[ edgeCountPfxSumPerBVertex[ jA ] ] = A[ i ].i;
			edgeWeights[ edgeCountPfxSumPerBVertex[ jA ] ] =
				B_rowPtr[ jA + 1 ] - B_rowPtr[ jA ];
			++edgeCountPfxSumPerBVertex[ jA ];
		}

		real_type* tpWeights = util::alloc< real_type >( A_k );
		real_type unbalanceVector = 1.3; // 0.30 imbalance allowed
		option_type* options = NULL;
		wgt_type objVal = 0;
		pid_type* A_partVector = util::alloc< pid_type >( noOfVertices );

		float totalWeight = 1.0;
		float perPartWeight = totalWeight / A_k;
		for ( int i = 0; i < A_k - 1; ++i )
		{
			tpWeights[ i ] = perPartWeight;
			totalWeight -= perPartWeight;
		}
		tpWeights[ A_k - 1 ] = totalWeight;

		// std::cout << "edgesPerVertex" << std::endl;;
		// for ( ui32 i = 0; i < noOfVertices; ++i )
		// {
		// 	std::cout << i << " (" << vertexWeights[ i ] << ") -> ";
		// 	for ( ui32 j = edgeCountPfxSumPerVertex[ i ];
		// 	      j < edgeCountPfxSumPerVertex[ i + 1 ];
		// 	      ++j )
		// 	{
		// 		std::cout << edgesPerVertex[ j ] << "("
		// 		          << edgeWeights[ j ] << ") ";
		// 	}
		// 	std::cout << std::endl;
		// }		

		TIMER_STOP();
		A_preparationTime = TIMER_DURATION();
		std::cout << "Preparation_Time: " << A_preparationTime << std::endl;
		std::cout << "A_nnz: " << A.length() << std::endl;
		std::cout << "A_vertexCount: " << noOfVertices << std::endl;
		std::cout << "A_edgeCount: "
		          << edgeCountPfxSumPerVertex[ noOfVertices ] / 2
		          << std::endl;
		std::cout << "A_k: " << A_k << std::endl;
		std::cout.flush();


		TIMER_START();
#ifdef WITH_MT_METIS
		int retCodeA = MTMETIS_PartGraphRecursive(
			&noOfVertices, &noOfBalanceConstraints,
			edgeCountPfxSumPerVertex, edgesPerVertex,
			vertexWeights, vertexSizes, edgeWeights, &A_k, tpWeights,
			&unbalanceVector, options, &objVal, A_partVector );
#else
		int retCodeA = METIS_PartGraphRecursive(
			&noOfVertices, &noOfBalanceConstraints,
			edgeCountPfxSumPerVertex, edgesPerVertex,
			vertexWeights, vertexSizes, edgeWeights, &A_k, tpWeights,
			&unbalanceVector, options, &objVal, A_partVector );
#endif
		TIMER_STOP();
		double A_metisTime = TIMER_DURATION();

		// std::cout << "A_partVector:" << std::endl;
		// for ( int i = 0; i < noOfVertices; ++i )
		// 	std::cout << i << " -> " << A_partVector[ i ] << std::endl;
		// std::cout << std::endl;

		handleMetisError( retCodeA, "A" );
		std::cout << "Metis_Time: " << A_metisTime << std::endl;
		std::cout << "A_ojbVal: " << objVal << std::endl;
		std::cout << std::endl << std::endl;
		std::cout.flush();

		vtx_type* A_ordering = nullptr;
		adj_type* A_pfxSum = nullptr;
		generateOrdering( A_rowCount, A_partVector, A_k,
		                  &A_ordering, &A_pfxSum );

		printArr( outputDir, mmfNameA, "A_rowOrder", A_ordering, A_rowCount );
		printArr( outputDir, mmfNameA, "A_rowPfxSumPerPart", A_pfxSum, A_k + 1 );

		util::free( A_ordering );
		util::free( A_pfxSum );

		// std::cout << "A_pfxSum: ";
		// for ( int i = 0; i <= A_k; ++i )
		// 	std::cout << A_pfxSum[ i ] << " ";
		// std::cout << std::endl;

		// std::cout << "A_ordering: " << std::endl;
		// for ( int i = 0; i < A_rowCount; ++i )
		// 	std::cout << i << " -> " << A_ordering[ i ] << std::endl;
		// std::cout << std::endl;

		TIMER_START();
		// Generate B matrix access array
		for ( glbInd_t r = 0; r < B_rowCount; ++r )
			B_perRowPartAccessPtr[ r + 1 ] += B_perRowPartAccessPtr[ r ];

		for ( glbInd_t r = 0; r < A_rowCount; ++r )
		{
			int partId = A_partVector[ r ];

			for ( glbNnz_t i = A_rowPtr[ r ]; i < A_rowPtr[ r + 1 ]; ++i )
			{
				glbInd_t jA = A[ i ].j;
				int* B_rowAccessArr = B_perRowPartAccessArr + B_perRowPartAccessPtr[ jA ];
				int length = B_perRowPartAccessLength[ jA ];
				int p = 0;
				while ( p < length && partId > B_rowAccessArr[ p ] )
					++p;

				if ( B_rowAccessArr[ p ] != partId )
				{
					int l =  p;
					while ( l < length - 1 )
						B_rowAccessArr[ l + 1 ] = B_rowAccessArr[ l ];
					B_rowAccessArr[ p ] = partId;

					++B_perRowPartAccessLength[ jA ];
				}
			}
		}

		// std::cout << "B_rowBits: " << std::endl;
		// for ( glbInd_t r = 0; r < B_rowCount; ++r )
		// {
		// 	int endIndex = (r + 1) * bytesSingleEntry - 1;
		// 	std::cout << r << " -> ";
		// 	for ( int i = 0; i < bytesSingleEntry; ++i )
		// 		std::cout << std::bitset< 8 >(B_perRowThreadBitVector[ endIndex - i ]);
		// 	std::cout << std::endl;
		// }

		util::free( edgeCountPfxSumPerVertex );
		util::free( vertexWeights );
		util::free( vertexSizes );
		util::free( edgesPerVertex );
		util::free( edgeWeights );
		util::free( edgeCountPfxSumPerBVertex );
		util::free( tpWeights );
	}

	// std::cout << "B triplets: " << std::endl;
	// std::shared_ptr< std::vector< glbNnz_t > > pRowPtrB = B.rowPtr();
	// std::vector< glbNnz_t >& rowPtrB = *pRowPtrB.get();
	// B.sortRowMajor();
	// for ( std::size_t r = 0; r < B.rows(); ++r )
	// {
	// 	std::cout << r << " -> ";
	// 	for ( glbNnz_t n = rowPtrB[ r ]; n < rowPtrB[ r + 1 ]; ++n )
	// 	{
	// 		std::cout << B[ n ].j << " ";
	// 	}
	// 	std::cout << std::endl;
	// }
	// std::cout << std::endl;
	
	// Reorder B
	{
		std::shared_ptr< std::vector< glbNnz_t > > pB_rowPtr = B.rowPtr();
		std::shared_ptr< std::vector< glbNnz_t > > pB_colPtr = B.colPtr();
		std::vector< glbNnz_t >& B_rowPtr = *pB_rowPtr.get();
		std::vector< glbNnz_t >& B_colPtr = *pB_colPtr.get();
		std::vector< glbInd_t > B_colInd( B_nnz );
		std::vector< glbInd_t > B_rowInd( B_nnz );

		B.sortRowMajor();
		for ( glbNnz_t i = 0; i < B.length(); ++i )
			B_colInd[ i ] = B[ i ].j;

		B.sortColMajor();
		for ( glbNnz_t i = 0; i < B.length(); ++i )
			B_rowInd[ i ] = B[ i ].i;

		glbInd_t heapCapacity = 0;
		for ( glbInd_t r = 0; r < B_rowCount; ++r )
			if ( B_rowPtr[ r + 1 ] - B_rowPtr[ r ] > heapCapacity )
				heapCapacity = B_rowPtr[ r + 1 ] - B_rowPtr[ r];

		
		adj_type* edgeCountPfxSumPerVertex = util::alloc< adj_type >( B_rowCount + 1 );
		edgeCountPfxSumPerVertex[ 0 ] = 0;
		wgt_type* vertexWeights = util::alloc< wgt_type >( B_rowCount );
		for ( glbInd_t r = 0; r < B_rowCount; ++r )
			vertexWeights[ r ] = 1;
		vtx_type* vertexSizes = util::newVector< vtx_type >( B_rowCount );

		std::vector< vtx_type > edgesPerVertex;
		std::vector< int > edgeWeights;
		edgesPerVertex.push_back( -1 );
		edgeWeights.push_back( - 1 );

		MultiwayMerge::MinHeap< glbInd_t, glbInd_t, glbNnz_t > heap;
		heap.init( heapCapacity );

		// std::cout << "Beginning multiway merging..." << std::endl;
		// Use multiway merging to generate B graph METIS input
		for ( glbInd_t r = 0; r < B_rowCount; ++r )
		{
			glbInd_t currRowEdgeCount = 0;

			std::cout << "r=" << r << " -> ";
			std::cout << B_rowPtr[ r + 1 ] - B_rowPtr[ r ] << " B elems" << std::endl;
			// populate heap
			for ( glbNnz_t i = B_rowPtr[ r ]; i < B_rowPtr[ r + 1 ]; ++i )
			{
				glbInd_t jB = B_colInd[ i ];
				std::cout << "jB=" << jB << std::endl;

				if ( B_colPtr[ jB + 1 ] - B_colPtr[ jB ] > 0 )
				{
					glbNnz_t index = B_colPtr[ jB ];
					glbInd_t iB = B_rowInd[ index ];

					glbInd_t key = iB;
					glbInd_t value = jB;

					heap.insert( key, value, index );
				}

				std::cout << "Heap: " << std::endl;
				heap.str( std::cout );
			}

			int* fromPartIdsBegin = B_perRowPartAccessArr + B_perRowPartAccessPtr[ r ];
			int fromPartIdsLength = B_perRowPartAccessPtr[ r + 1 ] - B_perRowPartAccessPtr[ r ];
			glbInd_t key;
			glbInd_t value;
			glbNnz_t index;
			while ( heap.size > 0 )
			{
				heap.min( key, value, index );
				if ( key == r )
				{
					++index;
					if ( index < B_colPtr[ value + 1 ] )
						key = B_rowInd[ index ];
					else
						index = -1;
				}
				else
				{
					std::cout << "key=" << key
					          << " value=" << value
					          << " index=" << index << std::endl;

					// See if there is an edge between from and to
					int* toPartIdsBegin = B_perRowPartAccessArr + B_perRowPartAccessPtr[ key ];
					int toPartIdsLength =
						B_perRowPartAccessPtr[ key + 1 ] - B_perRowPartAccessPtr[ key ];

					int minLength = fromPartIdsLength < toPartIdsLength ? fromPartIdsLength : toPartIdsLength;
					bool isEdge = false;
					int f = 0, t = 0;
					while ( f < fromPartIdsLength && t < toPartIdsLength && !isEdge )
					{
						if ( fromPartIdsBegin[ f ] < toPartIdsBegin[ t ] )
							++f;
						else if ( fromPartIdsBegin[ f ] > toPartIdsBegin[ t ] )
							++t;
						else
							isEdge = true;
					}

					if ( isEdge )
					{
						if ( edgesPerVertex.back() == key )
						{
							edgeWeights.back() += 1;
						}
						else
						{
							++currRowEdgeCount;
							edgesPerVertex.push_back( key );
							edgeWeights.push_back( 1 );
						}
					}

					std::cout << r << " -> " << key << " (" << isEdge << ") weight:"
					          << edgeWeights.back() << std::endl;

					// insert next element from the list (if any) into heap
					++index;
					if ( index < B_colPtr[ value + 1 ] )
						key = B_rowInd[ index ];
					else
						index = -1;
				}

				heap.heapify( key, value, index );
			}

			edgeCountPfxSumPerVertex[ r + 1 ] =
				edgeCountPfxSumPerVertex[ r ] + currRowEdgeCount;
		}

		// for ( int r = 0; r < B_rowCount; ++r )
		// {
		// 	std::cout << r << " -> ";
		// 	for ( int i = edgeCountPfxSumPerVertex[ r ];
		// 	      i < edgeCountPfxSumPerVertex[ r + 1 ];
		// 	      ++i )
		// 	{
		// 		std::cout << edgesPerVertex[ i + 1 ]
		// 		          << " (" << edgeWeights[ i + 1 ] << ") ";
		// 	}

		// 	std::cout << std::endl;
		// }

		vtx_type B_rows = B_rowCount;
		real_type unbalanceVector = 1.3; // 0.30 imbalance allowed
		option_type* options = NULL;
		int objVal= 0;
		pid_type B_k = ( ( B_rowCount + metisBVertexCount - 1 ) / metisBVertexCount );
		vtx_type noOfBalanceConstraints = 1;
		pid_type* B_partVector = util::alloc< pid_type >( B_rowCount );
		real_type* tpWeights = util::alloc< float >( B_k );
		real_type totalWeight = 1.0;
		real_type perPartWeight = totalWeight / B_k;
		for ( pid_type i = 0; i < B_k - 1; ++i )
		{
			tpWeights[ i ] = perPartWeight;
			totalWeight -= perPartWeight;
		}
		tpWeights[ B_k - 1 ] = totalWeight;

		// std::cout << std::endl;
		// std::cout << "Calling METIS on B..." << std::endl;

		TIMER_STOP();
		B_preparationTime = TIMER_DURATION();
		std::cout << "Preparation_Time: " << B_preparationTime << std::endl;
		std::cout << "B_nnz: " << B.length() << std::endl;
		std::cout << "B_vertexCount: " << B_rowCount << std::endl;
		std::cout << "B_edgeCount: "
		          << edgeCountPfxSumPerVertex[ B_rowCount ] / 2
		          << std::endl;
		std::cout << "B_k: " << B_k << std::endl;
		std::cout.flush();


		TIMER_START();
#ifdef WITH_MT_METIS
		int retCodeB = MTMETIS_PartGraphRecursive(
			&B_rows, &noOfBalanceConstraints,
			edgeCountPfxSumPerVertex, &edgesPerVertex[ 1 ],
			vertexWeights, vertexSizes, &edgeWeights[ 1 ], &B_k, tpWeights,
			&unbalanceVector, options, &objVal, B_partVector );
#else
		int retCodeB = METIS_PartGraphRecursive(
			&B_rows, &noOfBalanceConstraints,
			edgeCountPfxSumPerVertex, &edgesPerVertex[ 1 ],
			vertexWeights, vertexSizes, &edgeWeights[ 1 ], &B_k, tpWeights,
			&unbalanceVector, options, &objVal, B_partVector );
#endif
		TIMER_STOP();
		double B_metisTime = TIMER_DURATION();

		handleMetisError( retCodeB, "B" );
		std::cout << "Metis_Time: " << B_metisTime << std::endl;
		std::cout << "B_objVal: " << objVal << std::endl;
		std::cout.flush();
		// std::cout << "B_partVector: " << std::endl;
		// for ( glbInd_t r = 0; r < B_rowCount; ++r )
		// 	std::cout << r << " -> " << B_partVector[ r ] << std::endl;

		pid_type* B_ordering = nullptr;
		adj_type* B_pfxSum = nullptr;
		generateOrdering( B_rowCount, B_partVector, B_k,
		                  &B_ordering, &B_pfxSum );

		printArr( outputDir, mmfNameB, "B_rowOrder", B_ordering, B_rowCount );

		util::free( B_ordering );
		util::free( B_pfxSum );
		util::free( edgeCountPfxSumPerVertex );
		util::free( vertexWeights );
		util::free( vertexSizes );
		util::free( tpWeights );
	}

	util::free( B_perRowPartAccessPtr );
	util::free( B_perRowPartAccessArr );
	util::free( B_perRowPartAccessLength );
	std::cout << std::endl;

	delete [] tripletArrA;
	delete [] tripletArrB;
	return EXIT_SUCCESS;
}

void handleMetisError( const int& code, const std::string& message )
{
#ifndef WITH_MT_METIS
	switch ( code )
	{
		case METIS_OK:
			std::cout << "METIS_OK (" << message << ")" << std::endl;
			break;
		case METIS_ERROR_INPUT:
			std::cout << "METIS_ERROR_INPUT ("
			          << message << ")" << std::endl;
			break;
		case METIS_ERROR_MEMORY:
			std::cout << "METIS_ERROR_MEMORY ("
			          << message << ")" << std::endl;
			break;
		case METIS_ERROR:
			std::cout << "METIS_ERROR ("
			          << message << ")" << std::endl;
			break;
		default:
			std::cout << "Unknown error ("
			          << message << ")" << std::endl;
			break;
	}

	if ( code != METIS_OK )
		exit( 0 );
#endif
}

void
generateOrdering( const vtx_type& noOfVertices,
                  pid_type const* const partVector, const int& k,
                  vtx_type** ordering_out, adj_type** pfxSum_out )
{
	adj_type* counterArr = util::newVector< adj_type >( k + 1 );
	for ( vtx_type v = 0; v < noOfVertices; ++v )
		++counterArr[ partVector[ v ] + 1 ];

	for ( pid_type i = 1; i <= k; ++i )
		counterArr[ i ] += counterArr[ i - 1 ];

	vtx_type* ordering = util::alloc< vtx_type >( noOfVertices );
	for ( vtx_type v = 0; v < noOfVertices; ++v )
	{
		adj_type& c = counterArr[ partVector[ v ] ];
		
		ordering[ v ] = c;
		++c;
	}

	for ( pid_type i = 0; i <= k; ++i )
		counterArr[ i ] = 0;

	for ( vtx_type v = 0; v < noOfVertices; ++v )
		++counterArr[ partVector[ v ] + 1 ];

	for ( pid_type i = 1; i <= k; ++i )
		counterArr[ i ] += counterArr[ i - 1 ];

	*pfxSum_out = counterArr;
	*ordering_out = ordering;
}

void
printArr( const std::string& dir, const std::string& name,
          const std::string& suffix,
          vtx_type const* const arr, const vtx_type& length )
{
	bool goon;
	long counter = -1;
	std::string path;
	do
	{
		++counter;
		path = dir + "/" + name + "_" + suffix + std::to_string( counter );
		std::ifstream f( path );

		goon = f.is_open();
		
	} while( goon );

	std::ofstream of( path );
	// of.open( path.c_str() );

	of << length << std::endl;
	for ( vtx_type i = 0; i < length; ++i )
		of << arr[ i ] << std::endl;
	of.close();
}
