
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <map>
#include <omp.h>
#include <set>
#include <unordered_set>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#endif

#include "include/sparse_types.h"
#include "include/util/Generic.h"
#include "include/util/Kernel.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
#include "include/data_structure/Triplet.h"
#include "include/data_structure/DenseArray.h"
#include "include/storage_format/Csr.h"
#include "include/io/InputReader.h"
#include "include/io/MMInfo.h"
#include "include/io/Cli.h"
#include "include/logging/Timer.h"

int main( int argc, char* argv[] )
{
	Cli cli( argc, argv );

	// Parameters
	// ------------------------------------------------------------------------
	if ( !cli.isSet( { Cli::MMF_PATH_A, Cli::NUM_BLOCKS, Cli::ACC_SIZES } ) )
	{
		return EXIT_FAILURE;
	}

	ui32 iterations = cli.getIterations();
	std::string mmfPathA = cli.getMmfPathA();
	std::string mmfPathB = cli.getMmfPathB();
	ui32 numThreads = cli.getNumBlocks();
	bool transposeB = cli.getTransposeB();
	std::vector< ui32 > vAccSizes = cli.getAccSizes();		

	omp_set_num_threads( numThreads );
#ifdef __INTEL_COMPILER
	mkl_set_num_threads( numThreads );
#endif

	// Read matrix market format file.
	// ------------------------------------------------------------------------
	MMInfo A_mmInfo;
	int& A_isSymmetric = A_mmInfo.isSymmetric;
	int A_rowCount = 0;
	int A_colCount = 0;
	int A_nnz = 0;
	int* A_is = NULL;
	int* A_js = NULL;
	double* A_nzs = NULL;
	InputReader::readMMF( mmfPathA, A_mmInfo, &A_rowCount, &A_colCount,
	                      &A_nnz, &A_is, &A_js, &A_nzs);
	Triplet* tripletArrA = Triplet::array( A_nnz, A_is, A_js, A_nzs );
	Triplets A( 0, 0, A_rowCount, A_colCount,
	            tripletArrA, tripletArrA + A_nnz );
	std::cout << mmfPathA.substr( mmfPathA.find_last_of( '/' ) + 1,
	                              mmfPathA.find_last_of( '.' ) - 1 ) << ",";

	MMInfo B_mmInfo;
	int B_isSymmetric = 0;
	int B_rowCount = 0;
	int B_colCount = 0;
	int B_nnz = 0;
	int* B_is = NULL;
	int* B_js = NULL;
	double* B_nzs = NULL;

	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		InputReader::readMMF( mmfPathB, B_mmInfo, &B_rowCount, &B_colCount,
		                      &B_nnz, &B_is, &B_js, &B_nzs);
		B_isSymmetric = B_mmInfo.isSymmetric;
	}
	else
	{
		mmfPathB = mmfPathA;
		B_isSymmetric = A_isSymmetric;
		B_rowCount = A_rowCount;
		B_colCount = A_colCount;
		B_nnz = A_nnz;
		B_is = A_is;
		B_js = A_js;
		B_nzs = A_nzs;
	}

	Triplet* tripletArrB = Triplet::array( B_nnz, B_is, B_js, B_nzs );
	Triplets B = Triplets( 0, 0, B_rowCount, B_colCount,
	                       tripletArrB, tripletArrB + B_nnz );

	if ( transposeB )
		B.transpose();

	// clean up
	free( A_is );
	free( A_js );
	free( A_nzs );	
	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		free( B_is );
		free( B_js );
		free( B_nzs );
	}

	// Permute A and B matrices
	if ( cli.getPermuteAB() )
		Triplets::permuteRandom( A, B );

	// Preprocess matrix C = A x B
	ui64 rowStartC = 0;
    ui64 colStartC = 0;
	ui64 nnzC = 0;
    ui64 flops = 0;
    Triplet* tripletArrC = util::preprocessC( A, B, rowStartC, colStartC,
	                                          nnzC, flops );
	Triplets C( rowStartC, colStartC, A.rows(), B.columns(),
	            tripletArrC, tripletArrC + nnzC );

	// -------------------------------------------------------------------------

	std::vector< ui32 > vAccLengths( vAccSizes.size() + 1, 0 );
	for ( ui32 i = 0; i < vAccSizes.size(); ++i )
		vAccLengths[ i ] = ( vAccSizes[ i ] * 1024 ) / sizeof( fp_t );

	glbInd_t powerOf2 = 2;
	while ( powerOf2 < A.rows() )
		powerOf2 *= 2;
	vAccLengths[ vAccSizes.size() ] = powerOf2;

	Csr< glbInd_t, fp_t, glbNnz_t > csrC;
	csrC.extract( C );

	char* maskIndexPerRow = util::alloc< char >( C.rows() );

	// Allocate accumulators
	fp_t** accumulatorPerThrd = nullptr;
	#pragma omp parallel num_threads( numThreads )
	{
		#pragma omp single
		{
			accumulatorPerThrd = util::alloc< fp_t* >( numThreads );
		}

		int _id = omp_get_thread_num();
		accumulatorPerThrd[ _id ] = util::newVector< fp_t >( B_colCount );
	}

	// Measure timings of different multimap generation algorithms
	// -------------------------------------------------------------------------
	RECORD( "STL" );
	RRUN( iterations,
	      util::generateMultiMapSTL(
		      vAccLengths, csrC.getRowCount(), B.columns(),
		      csrC.rowPtr, csrC.colInd, numThreads, maskIndexPerRow ); );

	RECORD( "Symbolic" );
	RRUN( iterations,
	      util::generateMultiMapSymbolic(
		      vAccLengths, csrC.getRowCount(), B.columns(),
		      csrC.rowPtr, csrC.colInd, numThreads,
		      accumulatorPerThrd, maskIndexPerRow ); );

	RECORD( "LinearTime" );
	RRUN( iterations,
	      util::generateMultiMapLinearTime(
		      vAccLengths, csrC.getRowCount(), B.columns(),
		      csrC.rowPtr, csrC.colInd,
		      numThreads, accumulatorPerThrd, maskIndexPerRow ); );

	std::cout << std::endl;

	delete [] tripletArrC;
	delete [] tripletArrA;
	delete [] tripletArrB;
	return EXIT_SUCCESS;
}
