/*
 * StaticRowByRow3p_v0.cpp
 *
 *  Created on: Sep 26, 2018
 *      Author: memoks
 */

#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <map>
#include <omp.h>
#include <set>
#include <unordered_set>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#endif

#include "include/sparse_types.h"
#include "include/util/Generic.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Csr.h"
#include "include/io/InputReader.h"
#include "include/io/MMInfo.h"
#include "include/io/Cli.h"
#include "include/logging/Timer.h"
#include "include/parallel/Gustavson.h"
#include "include/parallel/RowByRow3p_v0.h"


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem,
           typename correct_mem >
void
run_3p( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
        const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
        Csr< ind_t, fp_t, nnz_t, C_mem >& C, const ui32& numThreads,
        rbr3p::v0::Thrd< glbInd_t, fp_t, glbNnz_t, B_prime_mem >** thrds,
        ind_t const* const thrdRowPfxSum,
        ind_t const* const thrdRowPfxSum4, ind_t const* const thrdRowPfxSum8,
        fp_t** accumulators, 
        ui32* masks, char* maskIndexPerRow, const ui32& noOfMasks,
        const Csr< ind_t, fp_t, nnz_t, correct_mem >& correct,
        const ui32& iterations );


template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename correct_mem >
void
run_gust( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
          const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
          Csr< ind_t, fp_t, nnz_t, C_mem >& C, const ui32& numThreads,
          ind_t const* const thrdRowPfxSum, fp_t** accumulators,
          ui32* masks, char* maskIndexPerRow, const ui32& noOfMasks,
          const Csr< ind_t, fp_t, nnz_t, correct_mem >& correct,
          const ui32& iterations );

// ############################################################################

using namespace rbr;

int main( int argc, char* argv[] )
{
	Cli cli( argc, argv );

	// Parameters
	// ------------------------------------------------------------------------
	if ( !cli.isSet( { Cli::MMF_PATH_A, Cli::NUM_BLOCKS, Cli::ACC_SIZES } ) )
	{
		return EXIT_FAILURE;
	}

	ui32 iterations = cli.getIterations();
	std::string mmfPathA = cli.getMmfPathA();
	std::string mmfPathB = cli.getMmfPathB();
	ui32 numThreads = cli.getNumBlocks();
	bool transposeB = cli.getTransposeB();
	std::vector< ui32 > vAccSizes = cli.getAccSizes();

	omp_set_num_threads( numThreads );

	// Read matrix market format file.
	// ------------------------------------------------------------------------
	MMInfo A_mmInfo;
	int& A_isSymmetric = A_mmInfo.isSymmetric;
	int A_rowCount = 0;
	int A_colCount = 0;
	int A_nnz = 0;
	int* A_is = NULL;
	int* A_js = NULL;
	double* A_nzs = NULL;
	InputReader::readMMF( mmfPathA, A_mmInfo, &A_rowCount, &A_colCount,
	                      &A_nnz, &A_is, &A_js, &A_nzs);
	Triplet* tripletArrA = Triplet::array( A_nnz, A_is, A_js, A_nzs );
	Triplets _A( 0, 0, A_rowCount, A_colCount,
	             tripletArrA, tripletArrA + A_nnz );
	std::cout << mmfPathA.substr( mmfPathA.find_last_of( '/' ) + 1,
	                              mmfPathA.find_last_of( '.' ) - 1 ) << ",";

	MMInfo B_mmInfo;
	int B_isSymmetric = 0;
	int B_rowCount = 0;
	int B_colCount = 0;
	int B_nnz = 0;
	int* B_is = NULL;
	int* B_js = NULL;
	double* B_nzs = NULL;

	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		InputReader::readMMF( mmfPathB, B_mmInfo, &B_rowCount, &B_colCount,
		                      &B_nnz, &B_is, &B_js, &B_nzs);
		B_isSymmetric = B_mmInfo.isSymmetric;
	}
	else
	{
		mmfPathB = mmfPathA;
		B_isSymmetric = A_isSymmetric;
		B_rowCount = A_rowCount;
		B_colCount = A_colCount;
		B_nnz = A_nnz;
		B_is = A_is;
		B_js = A_js;
		B_nzs = A_nzs;
	}

	Triplet* tripletArrB = Triplet::array( B_nnz, B_is, B_js, B_nzs );
	Triplets _B = Triplets( 0, 0, B_rowCount, B_colCount,
	                        tripletArrB, tripletArrB + B_nnz );

	if ( transposeB )
		_B.transpose();

	// clean up
	free( A_is );
	free( A_js );
	free( A_nzs );	
	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		free( B_is );
		free( B_js );
		free( B_nzs );
	}

	// Permute A and B matrices
	if ( cli.getPermuteAB() )
		Triplets::permuteRandom( _A, _B );

	glbInd_t* thrdRowPfxSum = nullptr;
	std::vector< ui64 > multPfxSumPerThrd;
	util::partition( _A, _B, &thrdRowPfxSum, multPfxSumPerThrd, numThreads );

	// Preprocess matrix C = A x B
	fp_t** accumulators = nullptr;
	#pragma omp parallel num_threads( numThreads )
	{
		#pragma omp single
		{
			accumulators = util::alloc< fp_t* >( numThreads );
		}
		int tid = omp_get_thread_num();
		accumulators[ tid ] = util::newVector< fp_t >( _B.columns() );
	}

	// -------------------------------------------------------------------------

	std::vector< ui32 > vAccLengths( vAccSizes.size() + 1, 0 );
	for ( ui32 i = 0; i < vAccSizes.size(); ++i )
		vAccLengths[ i ] = ( vAccSizes[ i ] * 1024 ) / sizeof( fp_t );
	// std::vector< ui32 > vAccLengths;
	// vAccLengths.push_back( 16 );
	// vAccLengths.push_back( 32 );
	// vAccLengths.push_back( 64 );
	// vAccLengths.push_back( 128 );

	glbInd_t powerOf2 = 2;
	while ( powerOf2 < _B.columns() )
		powerOf2 *= 2;
	vAccLengths[ vAccSizes.size() ] = powerOf2;
	// vAccLengths.push_back( powerOf2 );
    
	// RowByRow 3 phase algorithm
	// ------------------------------------------------------------------------

	{
		Csr< glbInd_t, fp_t, glbNnz_t > A( _A );
		Csr< glbInd_t, fp_t, glbNnz_t > B( _B );
		Csr< glbInd_t, fp_t, glbNnz_t > C;
		ui32 noOfMasks = vAccLengths.size() - 1;
		ui32* masks = nullptr;
		char* maskIndexPerRow = nullptr;
		#pragma omp parallel num_threads( numThreads )
		{                  
			#pragma omp single
			{
				maskIndexPerRow = util::alloc< char >( _A.rows() );
				masks = util::alloc< ui32 >( vAccLengths.size() );
				for ( ui32 i = 0; i < vAccLengths.size(); ++i )
					masks[ i ] = vAccLengths[ i ] - 1;
			}
		}

		// For correctness check
		// ---------------------------------------------------------------------
		Csr< glbInd_t, fp_t, glbNnz_t > correct;
		rbr::gustavson::symbolic_stl_alloc( A, B, correct, accumulators,
		                                    thrdRowPfxSum, numThreads );
		rbr::gustavson::numeric(
			A, B, correct, accumulators, thrdRowPfxSum, numThreads );

		glbInd_t* thrdRowPfxSum4 = nullptr;
		glbInd_t* thrdRowPfxSum8 = nullptr;
		util::align( thrdRowPfxSum, numThreads, correct.rowPtr, 4,
		             &thrdRowPfxSum4 );
		util::align( thrdRowPfxSum, numThreads, correct.rowPtr, 8,
		             &thrdRowPfxSum8 );

		// align row slices for streamin store optimization to work
		// ---------------------------------------------------------------------
		// util::align4( thrdRowPfxSum, numThreads, correct.rowPtr );
		// util::align8( thrdRowPfxSum, numThreads, correct.rowPtr );

		// Using aligned ddr4 memory allocator
		// ---------------------------------------------------------------------
		Csr< glbInd_t, fp_t, glbNnz_t, allocator::Aligned > A_ddr4( _A );
		Csr< glbInd_t, fp_t, glbNnz_t, allocator::Aligned > B_ddr4( _B );
		Csr< glbInd_t, fp_t, glbNnz_t, allocator::Aligned > C_ddr4;
		rbr3p::v0::Thrd< glbInd_t, fp_t,
		                 glbNnz_t, allocator::Aligned >** thrds_ddr4 = nullptr;
		ui32 capacity = 2048;
		rbr3p::v0::alloc( numThreads, capacity, capacity, capacity,
		                  &thrds_ddr4 );
		rbr3p::v0::phase1( A_ddr4, B_ddr4, thrdRowPfxSum, numThreads,
		                   thrds_ddr4 );
		rbr3p::v0::symbolic_alloc(
			A_ddr4, B_ddr4, C_ddr4, accumulators, thrdRowPfxSum, numThreads,
			thrds_ddr4, maskIndexPerRow, masks, noOfMasks );

		RECORD( "ddr4" );
		RECORD( "3p" );
		run_3p( A_ddr4, B_ddr4, C_ddr4, numThreads, thrds_ddr4, 
		        thrdRowPfxSum, thrdRowPfxSum4, thrdRowPfxSum8,
		        accumulators, masks, maskIndexPerRow, noOfMasks,
		        correct, iterations );

		RECORD( "gust" );
		run_gust( A_ddr4, B_ddr4, C_ddr4, numThreads, thrdRowPfxSum,
		          accumulators, masks, maskIndexPerRow, noOfMasks, correct,
		          iterations );

		{
			Csr< glbInd_t, fp_t, glbNnz_t, allocator::Aligned > Bs[ numThreads ];
			for ( ui32 t = 0; t < numThreads; ++t )
				Bs[ t ] = Csr< glbInd_t, fp_t, glbNnz_t, allocator::Aligned >( _B );

			RECORD( "phase2-privateB" );
			RRUN( iterations,
			      rbr3p::v0::phase2( A_ddr4, Bs, thrdRowPfxSum,
			                         numThreads, thrds_ddr4 ); );

			RECORD( "gustavson-privateB" );
			RRUN( iterations,
			      gustavson::numeric( A_ddr4, Bs, C_ddr4, accumulators,
			                          thrdRowPfxSum, numThreads ); );

			RECORD( "gustavson-privateB-incorrect" );
			RRUN( iterations,
			      gustavson::numeric_incorrect( A_ddr4, Bs, C_ddr4, accumulators,
			                                    thrdRowPfxSum, numThreads ); );
		}

		std::cout.flush();

#ifdef WITH_LIB_NUMA
		Csr< glbInd_t, fp_t, glbNnz_t, allocator::NumaInterleaved > A_numa( _A );
		Csr< glbInd_t, fp_t, glbNnz_t, allocator::NumaInterleaved > B_numa( _B );
		Csr< glbInd_t, fp_t, glbNnz_t, allocator::NumaInterleaved > C_numa;
		rbr3p::v0::Thrd< glbInd_t, fp_t, glbNnz_t,
		                 allocator::NumaLocal >** thrds_numa = nullptr;
		rbr3p::v0::alloc( numThreads, capacity, capacity, capacity,
		                  &thrds_numa );
		rbr3p::v0::phase1( A_numa, B_numa, thrdRowPfxSum,
		                   numThreads, thrds_numa );
		rbr3p::v0::symbolic_alloc(
			A_numa, B_numa, C_numa, accumulators, thrdRowPfxSum, numThreads,
			thrds_numa, maskIndexPerRow, masks, noOfMasks );

		RECORD( "A-B-C-numa" );
		RECORD( "3p" );
		run_3p( A_numa, B_numa, C_numa, numThreads, thrds_numa,
		        thrdRowPfxSum, thrdRowPfxSum4, thrdRowPfxSum8,
		        accumulators, masks, maskIndexPerRow, noOfMasks, correct,
		        iterations );
		RECORD( "gust" );
		run_gust( A_numa, B_numa, C_numa, numThreads, thrdRowPfxSum,
		          accumulators, masks, maskIndexPerRow, noOfMasks, correct,
		          iterations );

		RECORD( "B-numa" );
		RECORD( "3p" );
		run_3p( A_ddr4, B_numa, C_ddr4, numThreads, thrds_numa,
		        thrdRowPfxSum, thrdRowPfxSum4, thrdRowPfxSum8,
		        accumulators, masks, maskIndexPerRow, noOfMasks, correct,
		        iterations );
		RECORD( "gust" );
		run_gust( A_ddr4, B_numa, C_ddr4, numThreads, thrdRowPfxSum,
		          accumulators, masks, maskIndexPerRow, noOfMasks, correct,
		          iterations );

		std::cout.flush();
#endif

#ifdef WITH_HBW_MEMORY
		// Using MCDRAM memory allocator
		// ---------------------------------------------------------------------
		Csr< glbInd_t, fp_t, glbNnz_t, allocator::Mcdram > A_mcdram( _A );
		Csr< glbInd_t, fp_t, glbNnz_t, allocator::Mcdram > B_mcdram( _B );
		Csr< glbInd_t, fp_t, glbNnz_t, allocator::Mcdram > C_mcdram;
		rbr3p::v0::Thrd< glbInd_t, fp_t,
		                 glbNnz_t, allocator::Mcdram >** thrds_mcdram = nullptr;
		rbr3p::v0::alloc( numThreads, capacity, capacity, capacity,
		                  &thrds_mcdram );
		rbr3p::v0::phase1( A_mcdram, B_mcdram, thrdRowPfxSum,
		                   numThreads, thrds_mcdram );
		rbr3p::v0::symbolic_alloc(
			A_mcdram, B_mcdram, C_mcdram, accumulators, thrdRowPfxSum,
			numThreads, thrds_mcdram, maskIndexPerRow, masks, noOfMasks );

		RECORD( "mcdram" );
		RECORD( "3p" );
		run_3p( A_mcdram, B_mcdram, C_mcdram, numThreads, thrds_mcdram,
		        thrdRowPfxSum, thrdRowPfxSum4, thrdRowPfxSum8,
		        accumulators, masks, maskIndexPerRow, noOfMasks, correct,
		        iterations );
		RECORD( "gust" );
		run_gust( A_mcdram, B_mcdram, C_mcdram, numThreads, thrdRowPfxSum,
		          accumulators, masks, maskIndexPerRow, noOfMasks, correct,
		          iterations );

		std::cout.flush();

		// Using both ddr4 and mcdram
		// ---------------------------------------------------------------------
		RECORD( "B'-mcdram" );
		RECORD( "3p" );
		run_3p( A_ddr4, B_ddr4, C_ddr4, numThreads, thrds_mcdram,
		        thrdRowPfxSum, thrdRowPfxSum4, thrdRowPfxSum8,
		        accumulators, masks, maskIndexPerRow, noOfMasks, correct,
		        iterations );
		
		RECORD( "A-B'-mcdram" );
		RECORD( "3p" );
		run_3p( A_mcdram, B_ddr4, C_ddr4, numThreads, thrds_mcdram,
		        thrdRowPfxSum, thrdRowPfxSum4, thrdRowPfxSum8,
		        accumulators, masks, maskIndexPerRow, noOfMasks, correct,
		        iterations );

		RECORD( "B'-C-mcdram" );
		RECORD( "3p" );
		run_3p( A_ddr4, B_ddr4, C_mcdram, numThreads, thrds_mcdram,
		        thrdRowPfxSum, thrdRowPfxSum4, thrdRowPfxSum8,
		        accumulators, masks, maskIndexPerRow, noOfMasks, correct,
		        iterations );

		RECORD( "A-B'-C-mcdram" );
		RECORD( "3p" );
		run_3p( A_mcdram, B_ddr4, C_mcdram, numThreads, thrds_mcdram,
		        thrdRowPfxSum, thrdRowPfxSum4, thrdRowPfxSum8,
		        accumulators, masks, maskIndexPerRow, noOfMasks, correct,
		        iterations );
		std::cout.flush();
#endif

		std::cout.flush();

		util::free( thrdRowPfxSum );
		util::free( thrdRowPfxSum4 );
		util::free( thrdRowPfxSum8 );
		util::free( masks );
		util::free( maskIndexPerRow );
		for ( ui32 i = 0; i < numThreads; ++i )
		{
			thrds_ddr4[ i ]->free();
			util::free( thrds_ddr4[ i ] );
			util::free( accumulators[ i ] );

#ifdef WITH_HBW_MEM
			thrds_mcdram[ i ]->free();
			util::free( thrds_mcdram[ i ] );
#endif

#ifdef WITH_LIB_NUMA
			thrds_numa[ i ]->free();
			util::free( thrds_numa[ i ] );
#endif
		}
		util::free( thrds_ddr4 );
		util::free( accumulators );
#ifdef WITH_HBW_MEM
			util::free( thrds_mcdram );
#endif
#ifdef WITH_LIB_NUMA
			util::free( thrds_numa );
#endif
	}

	std::cout << std::endl;

	delete [] tripletArrA;
	delete [] tripletArrB;
	return EXIT_SUCCESS;
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename B_prime_mem,
           typename correct_mem >
void
run_3p( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
        const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
        Csr< ind_t, fp_t, nnz_t, C_mem >& C, const ui32& numThreads,
        rbr3p::v0::Thrd< glbInd_t, fp_t, glbNnz_t, B_prime_mem >** thrds,
        ind_t const* const thrdRowPfxSum,
        ind_t const* const thrdRowPfxSum4, ind_t const* const thrdRowPfxSum8,
        fp_t** accumulators, 
        ui32* masks, char* maskIndexPerRow, const ui32& noOfMasks,
        const Csr< ind_t, fp_t, nnz_t, correct_mem >& correct,
        const ui32& iterations )
{
	rbr3p::v0::numeric( A, C, masks, maskIndexPerRow, accumulators,
	                    thrdRowPfxSum, numThreads, thrds );

	RECORD( "symbolic" );
	RRUN( iterations, symbolic( A, B, C, accumulators,
	                            thrdRowPfxSum, numThreads, thrds,
	                            maskIndexPerRow, masks, noOfMasks ); );

	RECORD( "phase2" );
	RRUN( iterations,
	      rbr3p::v0::phase2( A, B, thrdRowPfxSum, numThreads, thrds ); );

	RECORD( "numeric" );
	C.zero();
	RRUN( iterations,
	      rbr3p::v0::numeric( A, C, masks, maskIndexPerRow,
	                          accumulators, thrdRowPfxSum,
	                          numThreads, thrds ); );

	RECORD( ( util::isSame( correct.nzs, C.nzs,
	                        correct.nnz ) ? " " : "!" ) );

	RECORD( "numeric_incorrect" );
	RRUN( iterations,
	      rbr3p::v0::numeric_incorrect(
		      A, C, accumulators, thrdRowPfxSum, numThreads, thrds ); );

// #ifdef __INTEL_COMPILER
// 	RECORD( "numeric_simd" );
// 	C.zero();
// 	RRUN( iterations,
// 	      rbr3p::v0::numeric_simd(
// 		      A, C, masks, maskIndexPerRow, accumulators,
// 		      thrdRowPfxSum, numThreads, thrds ); );

// 	RECORD( ( util::isSame( correct.nzs, C.nzs,
// 	                        correct.nnz ) ? " " : "!" ) );
// #endif

// #ifdef __INTEL_COMPILER
// 	RECORD( "numeric_prefetch" );
// 	RRUN( iterations,
// 	      rbr3p::v0::numeric_prefetch(
// 		      A, C, masks, maskIndexPerRow, accumulators,
// 		      thrdRowPfxSum, numThreads, thrds ); );

// 	RECORD( ( util::isSame( correct.nzs, C.nzs,
// 	                        correct.nnz ) ? " " : "!" ) );
// #endif

// #ifdef WITH_AVX512
// 	RECORD( "numeric_avx512_sr" );
// 	for ( ui32 capacity = 64; capacity <= 2048; capacity *= 2 )
// 	{
// 		rbr3p::v0::setCacheCapacity(
// 			thrds, numThreads, capacity, capacity, capacity );

// 		C.zero();
// 		RRUN( iterations,
// 		      rbr3p::v0::numeric_avx512_sr(
// 			      A, C, masks, maskIndexPerRow, accumulators,
// 			      thrdRowPfxSum, numThreads, thrds ); );

// 		RECORD( ( util::isSame( correct.nzs, C.nzs,
// 		                        correct.nnz ) ? " " : "!" ) );
// 	}

// 	RECORD( "numeric_avx512_ss" );
// 	for ( ui32 capacity = 64; capacity <= 2048; capacity *= 2 )
// 	{
// 		rbr3p::v0::setCacheCapacity(
// 			thrds, numThreads, capacity, capacity, capacity );

// 		C.zero();
// 		RRUN( iterations,
// 		      rbr3p::v0::numeric_avx512_ss(
// 			      A, C, masks, maskIndexPerRow, accumulators,
// 			      thrdRowPfxSum, thrdRowPfxSum8, numThreads, thrds ); );

// 		RECORD( ( util::isSame( correct.nzs, C.nzs,
// 		                        correct.nnz ) ? " " : "!" ) );
// 	}
// #endif

// #ifdef WITH_AVX
// 	RECORD( "numeric_avx_ss" );
// 	for ( ui32 capacity = 64; capacity <= 2048; capacity *= 2 )
// 	{
// 		rbr3p::v0::setCacheCapacity(
// 			thrds, numThreads, capacity, capacity, capacity );

// 		C.zero();
// 		RRUN( iterations,
// 		      rbr3p::v0::numeric_avx_ss(
// 			      A, C, masks, maskIndexPerRow, accumulators,
// 			      thrdRowPfxSum, thrdRowPfxSum4, numThreads, thrds ); );

// 		RECORD( ( util::isSame( correct.nzs, C.nzs,
// 		                        correct.nnz ) ? " " : "!" ) );
// 	}
// #endif
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename A_mem, typename B_mem, typename C_mem,
           typename correct_mem >
void
run_gust( const Csr< ind_t, fp_t, nnz_t, A_mem >& A,
          const Csr< ind_t, fp_t, nnz_t, B_mem >& B,
          Csr< ind_t, fp_t, nnz_t, C_mem >& C, const ui32& numThreads,
          ind_t const* const thrdRowPfxSum, fp_t** accumulators,
          ui32* masks, char* maskIndexPerRow, const ui32& noOfMasks,
          const Csr< ind_t, fp_t, nnz_t, correct_mem >& correct,
          const ui32& iterations )
{
	RECORD( "Symbolic" );
	RRUN( iterations,
	      gustavson::symbolic_stl(
		      A, B, C, accumulators, thrdRowPfxSum, numThreads ); );

	RECORD( "Numeric" );
	RRUN( iterations,
	      gustavson::numeric( A, B, C, accumulators,
	                          thrdRowPfxSum, numThreads ); );

	RECORD( "Numeric-Multimap" );
	C.zero();
	RRUN( iterations,
	      gustavson::numeric( A, B, C, accumulators,
	                          thrdRowPfxSum, numThreads,
	                          maskIndexPerRow, masks ); );
	RECORD( ( util::isSame( correct.nzs, C.nzs, correct.nnz ) ? " " : "!" ) );

	RECORD( "numeric_incorrect" );
	RRUN( iterations,
	      gustavson::numeric_incorrect( A, B, C, accumulators,
	                                    thrdRowPfxSum, numThreads ); );
}
