
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <map>
#include <omp.h>
#include <set>
#include <unordered_set>
#include <bitset>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#endif

#include "include/sparse_types.h"
#include "include/util/Generic.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
#include "include/util/MultiwayMerge.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Csr.h"
#include "include/io/InputReader.h"
#include "include/io/MMInfo.h"
#include "include/io/Cli.h"
#include "include/logging/Timer.h"

// ############################################################################

extern void readFile( const std::string& path, std::vector< glbInd_t >& v );
extern void printGraph(
	const std::string& path,
	const int& noOfVertices, const int& noOfEdges,
	int const* const edgeCountPtr, int const* const adjacencyList,
	int const* const vertexWeights, int const* const edgeWeights );

int main( int argc, char* argv[] )
{
	Cli cli( argc, argv );

	std::cout << "---------------------------------"
	          << "---------------------------------------"
	          << std::endl;

	// Parameters
	// ------------------------------------------------------------------------
	if ( !cli.isSet( { Cli::MMF_PATH_A, 
	                   Cli::PATH_TO_PART_VECTOR_A,
	                   Cli::OUTPUT_PATH } ) )
	{
		return EXIT_FAILURE;
	}

	ui32 iterations = cli.getIterations();
	std::string mmfPathA = cli.getMmfPathA();
	std::string mmfPathB = cli.getMmfPathB();
	ui32 numThreads = cli.getNumBlocks();
	bool transposeB = cli.getTransposeB();
	std::string pathToPartVectorA = cli.getPathToPartVectorA();
	std::string outputPath = cli.getOutputPath();

	omp_set_num_threads( numThreads );
#ifdef __INTEL_COMPILER
	mkl_set_num_threads( numThreads );
#endif

	// Read matrix market format file.
	// ------------------------------------------------------------------------
	MMInfo A_mmInfo;
	int& A_isSymmetric = A_mmInfo.isSymmetric;
	int A_rowCount = 0;
	int A_colCount = 0;
	int A_nnz = 0;
	int* A_is = NULL;
	int* A_js = NULL;
	double* A_nzs = NULL;
	InputReader::readMMF( mmfPathA, A_mmInfo, &A_rowCount, &A_colCount,
	                      &A_nnz, &A_is, &A_js, &A_nzs );
	Triplet* tripletArrA = Triplet::array( A_nnz, A_is, A_js, A_nzs );
	Triplets A( 0, 0, A_rowCount, A_colCount,
	            tripletArrA, tripletArrA + A_nnz );
	std::string mmfNameA =
		mmfPathA.substr( mmfPathA.find_last_of( '/' ) + 1,
		                 mmfPathA.find_last_of( '.' ) - 1 );
	std::cout << "A: " << mmfNameA << std::endl;

	MMInfo B_mmInfo;
	int B_isSymmetric = 0;
	int B_rowCount = 0;
	int B_colCount = 0;
	int B_nnz = 0;
	int* B_is = NULL;
	int* B_js = NULL;
	double* B_nzs = NULL;

	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		InputReader::readMMF( mmfPathB, B_mmInfo, &B_rowCount, &B_colCount,
		                      &B_nnz, &B_is, &B_js, &B_nzs);
		B_isSymmetric = B_mmInfo.isSymmetric;
	}
	else
	{
		mmfPathB = mmfPathA;
		B_isSymmetric = A_isSymmetric;
		B_rowCount = A_rowCount;
		B_colCount = A_colCount;
		B_nnz = A_nnz;
		B_is = A_is;
		B_js = A_js;
		B_nzs = A_nzs;
	}

	Triplet* tripletArrB = Triplet::array( B_nnz, B_is, B_js, B_nzs );
	Triplets B = Triplets( 0, 0, B_rowCount, B_colCount,
	                       tripletArrB, tripletArrB + B_nnz );
	std::string mmfNameB =
		mmfPathB.substr( mmfPathA.find_last_of( '/' ) + 1,
		                 mmfPathA.find_last_of( '.' ) - 1 );
	std::cout << "B: " << mmfNameB << std::endl << std::endl;

	if ( transposeB )
		B.transpose();

	// clean up
	free( A_is );
	free( A_js );
	free( A_nzs );	
	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		free( B_is );
		free( B_js );
		free( B_nzs );
	}

	// Permute A and B matrices
	if ( cli.getPermuteAB() )
		Triplets::permuteRandom( A, B );

	A.sortRowMajor();
	B.sortRowMajor();

	std::shared_ptr< std::vector< glbNnz_t > > sA_rowPtr = A.rowPtr();
	std::shared_ptr< std::vector< glbNnz_t > > sB_rowPtr = B.rowPtr();
	std::vector< glbNnz_t > A_rowPtr = *sA_rowPtr.get();
	std::vector< glbNnz_t > B_rowPtr = *sB_rowPtr.get();

	// Allocate B row used bit vector per thread
	int A_k = 0;

	double B_preparationTime = 0.0d;
	std::vector< std::set< int > > rowAccessArrs( B_rowCount );
	// Reorder A
	// -------------------------------------------------------------------------
	{
		std::vector< glbInd_t > A_partVector;
		readFile( pathToPartVectorA, A_partVector );

		for ( std::size_t i = 0; i < A_partVector.size(); ++i )
			if ( A_partVector[ i ] > A_k )
				A_k = A_partVector[ i ];
		++A_k;

		// std::cout << "A_pfxSum: ";
		// for ( int i = 0; i <= A_k; ++i )
		// 	std::cout << A_pfxSum[ i ] << " ";
		// std::cout << std::endl;

		// std::cout << "A_ordering: " << std::endl;
		// for ( int i = 0; i < A_rowCount; ++i )
		// 	std::cout << i << " -> " << A_ordering[ i ] << std::endl;
		// std::cout << std::endl;

		TIMER_START();
		// Generate B matrix part access bit vectosry
		for ( glbInd_t r = 0; r < A_rowCount; ++r )
		{
			int partId = A_partVector[ r ];
			ui32 shamt = partId % 8;
			int offset = partId / 8;

			// std::cout << "partId= " << partId << std::endl;
			for ( glbNnz_t i = A_rowPtr[ r ]; i < A_rowPtr[ r + 1 ]; ++i )
			{
				glbInd_t jA = A[ i ].j;

				rowAccessArrs[ jA ].insert( partId );

				// std::cout << "partId = " << partId << " ";
				// std::cout << "jA = " << jA << " ";
			}
		}
	}

	std::shared_ptr< std::vector< glbNnz_t > > pRowPtrB = B.rowPtr();
	std::vector< glbNnz_t >& rowPtrB = *pRowPtrB.get();
	B.sortRowMajor();
	// for ( std::size_t r = 0; r < B.rows(); ++r )
	// {
	// 	std::cout << r << " -> ";
	// 	for ( glbNnz_t n = rowPtrB[ r ]; n < rowPtrB[ r + 1 ]; ++n )
	// 	{
	// 		std::cout << B[ n ].j << " ";
	// 	}
	// 	std::cout << std::endl;
	// }
	// std::cout << std::endl;
	
	// Reorder B
	{
		std::shared_ptr< std::vector< glbNnz_t > > pB_rowPtr = B.rowPtr();
		std::shared_ptr< std::vector< glbNnz_t > > pB_colPtr = B.colPtr();
		std::vector< glbNnz_t >& B_rowPtr = *pB_rowPtr.get();
		std::vector< glbNnz_t >& B_colPtr = *pB_colPtr.get();
		std::vector< glbInd_t > B_colInd( B_nnz );
		std::vector< glbInd_t > B_rowInd( B_nnz );

		B.sortRowMajor();
		for ( glbNnz_t i = 0; i < B.length(); ++i )
			B_colInd[ i ] = B[ i ].j;

		B.sortColMajor();
		for ( glbNnz_t i = 0; i < B.length(); ++i )
			B_rowInd[ i ] = B[ i ].i;

		glbInd_t heapCapacity = 0;
		for ( glbInd_t r = 0; r < B_rowCount; ++r )
			if ( B_rowPtr[ r + 1 ] - B_rowPtr[ r ] > heapCapacity )
				heapCapacity = B_rowPtr[ r + 1 ] - B_rowPtr[ r];

		
		int* edgeCountPfxSumPerVertex = util::alloc< int >( B_rowCount + 1 );
		edgeCountPfxSumPerVertex[ 0 ] = 0;
		int* vertexWeights = util::alloc< int >( B_rowCount );
		for ( glbInd_t r = 0; r < B_rowCount; ++r )
			vertexWeights[ r ] = 1;
		int* vertexSizes = util::newVector< int >( B_rowCount );

		std::vector< int > edgesPerVertex;
		std::vector< int > edgeWeights;
		edgesPerVertex.push_back( -1 );
		edgeWeights.push_back( -1 );

		MultiwayMerge::MinHeap< glbInd_t, glbInd_t, glbNnz_t > heap;
		heap.init( heapCapacity );

		// std::cout << "Beginning multiway merging..." << std::endl;
		// Use multiway merging to generate B graph METIS input
		for ( glbInd_t r = 0; r < B_rowCount; ++r )
		{
			glbInd_t currRowEdgeCount = 0;

			// std::cout << "r=" << r << " -> ";
			// std::cout << B_rowPtr[ r + 1 ] - B_rowPtr[ r ] << " B elems"
			//           << std::endl;
			// populate heap
			for ( glbNnz_t i = B_rowPtr[ r ]; i < B_rowPtr[ r + 1 ]; ++i )
			{
				glbInd_t jB = B_colInd[ i ];
				// std::cout << "jB=" << jB << std::endl;

				if ( B_colPtr[ jB + 1 ] - B_colPtr[ jB ] > 0 )
				{
					glbNnz_t index = B_colPtr[ jB ];
					glbInd_t iB = B_rowInd[ index ];

					glbInd_t key = iB;
					glbInd_t value = jB;

					heap.insert( key, value, index );
				}

				// std::cout << "Heap: " << std::endl;
				// heap.str( std::cout );
			}

			std::set< int >& fromSet = rowAccessArrs[ r ];
			// char* fromBitSetBegin = B_perRowThreadBitVector + r * bytesSingleEntry;
			glbInd_t key;
			glbInd_t value;
			glbNnz_t index;
			while ( heap.size > 0 )
			{
				heap.min( key, value, index );
				if ( key == r )
				{
					++index;
					if ( index < B_colPtr[ value + 1 ] )
						key = B_rowInd[ index ];
					else
						index = -1;
				}
				else
				{
					// std::cout << "key=" << key
					//           << " value=" << value
					//           << " index=" << index << std::endl;

					// TODO check if rows are accessed by the same A partitions
					// bool isEdge = false;
					// char* toBitSetBegin = B_perRowThreadBitVector + key * bytesSingleEntry;
					// for ( int b = 0; b < bytesSingleEntry && !isEdge; ++b )
					// 	isEdge = (bool) (fromBitSetBegin[ b ] & toBitSetBegin[ b ]);

					std::set< int >& toSet = rowAccessArrs[ r ];
					bool isEdge = false;
					auto fromIt = fromSet.cbegin();
					auto toIt = toSet.cbegin();
					while ( fromIt != fromSet.cend() && toIt != toSet.cend() && !isEdge )
					{
						if ( *fromIt > *toIt )
							++toIt;
						else if ( *fromIt < *toIt )
							++fromIt;
						else
							isEdge = true;
					}

					if ( isEdge )
					{
						if ( edgesPerVertex.back() == key )
						{
							edgeWeights.back() += 1;
						}
						else
						{
							++currRowEdgeCount;
							edgesPerVertex.push_back( key );
							edgeWeights.push_back( 1 );
						}
					}

					// std::cout << r << " -> " << key << " (" << isEdge << ") "
					//           << "weight=" << edgeWeights.back() << std::endl;

					// insert next element from the list (if any) into heap
					++index;
					if ( index < B_colPtr[ value + 1 ] )
						key = B_rowInd[ index ];
					else
						index = -1;
				}

				heap.heapify( key, value, index );
			}

			edgeCountPfxSumPerVertex[ r + 1 ] =
				edgeCountPfxSumPerVertex[ r ] + currRowEdgeCount;
		}

		// for ( int r = 0; r < B_rowCount; ++r )
		// {
		// 	std::cout << r << " -> ";
		// 	for ( int i = edgeCountPfxSumPerVertex[ r ];
		// 	      i < edgeCountPfxSumPerVertex[ r + 1 ];
		// 	      ++i )
		// 	{
		// 		std::cout << edgesPerVertex[ i + 1 ]
		// 		          << " (" << edgeWeights[ i + 1 ] << ") ";
		// 	}

		// 	std::cout << std::endl;
		// }

		TIMER_STOP();
		B_preparationTime = TIMER_DURATION();
		std::cout << "Preparation_Time: " << B_preparationTime << std::endl;
		std::cout << "B_nnz: " << B.length() << std::endl;
		std::cout << "B_vertexCount: " << B_rowCount << std::endl;
		std::cout << "B_edgeCount: "
		          << edgeCountPfxSumPerVertex[ B_rowCount ] / 2
		          << std::endl;
		std::cout.flush();

		// convert adjacency list into 1 based indexing
		for ( int e = 0; e < edgeCountPfxSumPerVertex[ B_rowCount ]; ++e )
			++edgesPerVertex[ e ];

		printGraph( outputPath,
		            B_rowCount, edgeCountPfxSumPerVertex[ B_rowCount ] / 2,
		            edgeCountPfxSumPerVertex, &edgesPerVertex[ 1 ],
		            vertexWeights, &edgeWeights[ 1 ] );
		
		util::free( edgeCountPfxSumPerVertex );
		util::free( vertexWeights );
		util::free( vertexSizes );
	}

	std::cout << std::endl;

	delete [] tripletArrA;
	delete [] tripletArrB;
	return EXIT_SUCCESS;
}

void
readFile( const std::string& path, std::vector< glbInd_t >& v )
{
	std::ifstream inputFile( path );

	long partId = 0;
	while ( inputFile >> partId )
		v.push_back( partId );

	inputFile.close();
}

void
printGraph( const std::string& path,
            const int& noOfVertices, const int& noOfEdges,
            int const* const edgeCountPtr,
            int const* const adjacencyList,
            int const* const vertexWeights,
            int const* const edgeWeights )
{
	std::ofstream of( path );

	of << noOfVertices << " " << noOfEdges << " 11 1" << std::endl;
	for ( int v = 0; v < noOfVertices; ++v )
	{
		of << vertexWeights[ v ];
		for ( int e = edgeCountPtr[ v ]; e < edgeCountPtr[ v + 1 ]; ++e )
			of << " " << adjacencyList[ e ] << " " << edgeWeights[ e ];
		of << std::endl;
	}

	of.close();
}
