
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <map>
#include <omp.h>
#include <set>
#include <unordered_set>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#endif

#include "include/sparse_types.h"
#include "include/util/Generic.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Cs.h"
#include "include/io/InputReader.h"
#include "include/io/MMInfo.h"
#include "include/io/Cli.h"
#include "include/logging/Timer.h"
#include "include/parallel/RealSpmv.h"


using namespace allocator;
using namespace sparse_storage;
using namespace spmv;


template < typename ind_t, typename fp_t, typename nnz_t, typename mem >
void
runTests(
	Triplets& _A, ui32 numThreads,
	const ui32& iterations, const bool& sorted,
	ind_t const* const thrdRowPfxSum );


int main( int argc, char** argv )
{
	Cli cli( argc, argv );

	// Parameters
	// ------------------------------------------------------------------------
	if ( !cli.isSet( { Cli::MMF_PATH_A, Cli::NUM_BLOCKS } ) )
	{
		return EXIT_FAILURE;
	}

	ui32 iterations = cli.getIterations();
	std::string mmfPathA = cli.getMmfPathA();
	std::string mmfPathB = cli.getMmfPathB();
	ui32 numThreads = cli.getNumBlocks();
	bool transposeB = cli.getTransposeB();
	std::vector< ui32 > vAccSizes = cli.getAccSizes();
	bool isSorted = cli.isSorted();

	omp_set_num_threads( numThreads );

	// Read matrix market format file.
	// ------------------------------------------------------------------------
	MMInfo A_mmInfo;
	int& A_isSymmetric = A_mmInfo.isSymmetric;
	int A_rowCount = 0;
	int A_colCount = 0;
	int A_nnz = 0;
	int* A_is = NULL;
	int* A_js = NULL;
	double* A_nzs = NULL;
	InputReader::readMMF( mmfPathA, A_mmInfo, &A_rowCount, &A_colCount,
	                      &A_nnz, &A_is, &A_js, &A_nzs);
	Triplet* tripletArrA = Triplet::array( A_nnz, A_is, A_js, A_nzs );
	Triplets _A( 0, 0, A_rowCount, A_colCount,
	             tripletArrA, tripletArrA + A_nnz );
	std::cout << mmfPathA.substr( mmfPathA.find_last_of( '/' ) + 1,
	                              mmfPathA.find_last_of( '.' ) - 1 ) << ",";

	// clean up
	free( A_is );
	free( A_js );
	free( A_nzs );

	ind_t* thrdRowPfxSum =
		(ind_t*) Aligned::alloc( numThreads + 1, sizeof(ind_t) );
	util::partitionRowWise( _A, numThreads, thrdRowPfxSum );

	// std::cout << std::endl;
	// std::cout << "ThrdRowPfxSum: ";
	// for ( ui32 t = 0; t < numThreads + 1; ++t )
	// 	std::cout << thrdRowPfxSum[ t ] << " ";
	// std::cout << std::endl;

	runTests< glbInd_t, fp_t, glbNnz_t, Aligned >(
		_A, numThreads, iterations, false, thrdRowPfxSum );
#ifdef WITH_HBW_MEMORY
	runTests< glbInd_t, fp_t, glbNnz_t, Mcdram >(
		_A, numThreads, iterations, false, thrdRowPfxSum );
#endif

	runTests< glbInd_t, fp_t, glbNnz_t, Aligned >(
		_A, numThreads, iterations, true, thrdRowPfxSum );
#ifdef WITH_HBW_MEMORY
	runTests< glbInd_t, fp_t, glbNnz_t, Mcdram >(
		_A, numThreads, iterations, true, thrdRowPfxSum );
#endif


	Aligned::free( thrdRowPfxSum, numThreads + 1, sizeof(ind_t) );
	delete [] tripletArrA;
	return EXIT_SUCCESS;
}

template < typename ind_t, typename fp_t, typename nnz_t, typename mem >
void
runTests(
	Triplets& _A, ui32 numThreads,
	const ui32& iterations, const bool& sorted,
	ind_t const* const thrdRowPfxSum )
{
	// sort A's rows in descending order by # of multiplications
	if ( sorted )
	{
		std::shared_ptr< std::vector< glbInd_t > > pNnzPerRow( _A.nnzPerRow() );
		std::vector< glbInd_t >& nnzPerRow = *pNnzPerRow.get();
		_A.sortRowBy( &nnzPerRow[ 0 ], thrdRowPfxSum, numThreads );
	}

	if ( sorted )
		RECORD( "sorted" );

	Cs< ind_t, fp_t, nnz_t, mem > A;
	convertToCsr( _A, A );

	fp_t* x = (fp_t*) mem::alloc( _A.columns(), sizeof( fp_t ) );
	for ( glbInd_t i = 0; i < _A.columns(); ++i )
		x[ i ] = 1;

	fp_t* y = (fp_t*) mem::alloc( _A.rows(), sizeof( fp_t ) );
	fp_t* yi = (fp_t*) mem::alloc( _A.rows(), sizeof( fp_t ) );

	fp_t** writeAddresses =
		(fp_t**) mem::alloc( _A.length(), sizeof( fp_t* ) );
	for ( ind_t r = 0; r < A.length; ++r )
	{
		for ( nnz_t n = A.ptr[ r ]; n < A.ptr[ r + 1 ]; ++n )
		{
			writeAddresses[ n ] = yi + r;
		}
	}

	RECORD( "Spmv" );
	RECORD( "for" );
	RRUN( iterations,
		  spmv::yAx( A, x, y, thrdRowPfxSum, numThreads ); );

	RECORD( "indirect" );
	RRUN( iterations,
	      spmv::yAx( A, x, yi, writeAddresses, thrdRowPfxSum, numThreads ); );

	RECORD( ( util::isSame( y, yi, _A.rows() ) ? " " : "!" ) );

	mem::free( y, _A.rows(), sizeof( fp_t ) );
	mem::free( yi, _A.rows(), sizeof( fp_t ) );
	mem::free( x, _A.columns(), sizeof( fp_t ) );
	mem::free( writeAddresses, _A.length(), sizeof( fp_t** ) );
	std::cout.flush();
}
