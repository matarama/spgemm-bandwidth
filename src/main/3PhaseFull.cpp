
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <map>
#include <omp.h>
#include <set>
#include <unordered_set>
#include <thread>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#endif

#include "include/sparse_types.h"
#include "include/util/Generic.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Cs.h"
#include "include/io/InputReader.h"
#include "include/io/MMInfo.h"
#include "include/io/Cli.h"
#include "include/logging/Timer.h"
#include "include/parallel/Gustavson.h"
#include "include/parallel/Reduced_v1.h"

using namespace allocator;
using namespace sparse_storage;


template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
job(
	regularized::AB< coffset_t, fp_t, alength_t, AB_mem > const* const ab,
	Cs< ind_t, fp_t, nnz_t, A_mem > const* const A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >* const C,
	const ind_t& A_START, const ind_t& A_END, const ui64& FLOP_START );



template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
numeric(
	const regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	ind_t const* const thrdPtrPfxSum, ui64 const* const thrdMultsPfxSum,
	const ui32& NUM_THREADS,
	const ui32& warmUps, const ui32& iterations );



template < typename ind_t, typename fp_t, typename nnz_t, typename znnz_t,
           typename mem >
void
runTests(
	Triplets& _A, Triplets& _B, fp_t** accumulators, const ui32 numThreads,
	const ui32& iterations, const bool& isSorted, const double& TAGET_SIZE );



int main( int argc, char** argv )
{
	Cli cli( argc, argv );

	// Parameters
	// ------------------------------------------------------------------------
	if ( !cli.isSet( { Cli::MMF_PATH_A, Cli::NUM_BLOCKS,
					   Cli::TARGETTED_CACHE_SIZE_KB } ) )
	{
		return EXIT_FAILURE;
	}

	ui32 iterations = cli.getIterations();
	std::string mmfPathA = cli.getMmfPathA();
	std::string mmfPathB = cli.getMmfPathB();
	ui32 numThreads = cli.getNumBlocks();
	bool transposeB = cli.getTransposeB();
	std::vector< ui32 > vAccSizes = cli.getAccSizes();
	bool isSorted = cli.isSorted();
	omp_set_num_threads( numThreads );
	const double TARGET_SIZE = cli.getTargettedCacheSizeKB();

	// Read matrix market format file.
	// ------------------------------------------------------------------------
	MMInfo A_mmInfo;
	int& A_isSymmetric = A_mmInfo.isSymmetric;
	int A_rowCount = 0;
	int A_colCount = 0;
	int A_nnz = 0;
	int* A_is = NULL;
	int* A_js = NULL;
	double* A_nzs = NULL;
	InputReader::readMMF( mmfPathA, A_mmInfo, &A_rowCount, &A_colCount,
	                      &A_nnz, &A_is, &A_js, &A_nzs);
	Triplet* tripletArrA = Triplet::array( A_nnz, A_is, A_js, A_nzs );
	Triplets _A( 0, 0, A_rowCount, A_colCount,
	             tripletArrA, tripletArrA + A_nnz );
	std::cout << mmfPathA.substr( mmfPathA.find_last_of( '/' ) + 1,
	                              mmfPathA.find_last_of( '.' ) - 1 )
	          << "," << A_nnz << "," << A_rowCount << "," << A_colCount << ",";

	MMInfo B_mmInfo;
	int B_isSymmetric = 0;
	int B_rowCount = 0;
	int B_colCount = 0;
	int B_nnz = 0;
	int* B_is = NULL;
	int* B_js = NULL;
	double* B_nzs = NULL;

	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		InputReader::readMMF( mmfPathB, B_mmInfo, &B_rowCount, &B_colCount,
		                      &B_nnz, &B_is, &B_js, &B_nzs);
		B_isSymmetric = B_mmInfo.isSymmetric;
	}
	else
	{
		mmfPathB = mmfPathA;
		B_isSymmetric = A_isSymmetric;
		B_rowCount = A_rowCount;
		B_colCount = A_colCount;
		B_nnz = A_nnz;
		B_is = A_is;
		B_js = A_js;
		B_nzs = A_nzs;

		if( B_rowCount != B_colCount )
			transposeB = true;
	}

	Triplet* tripletArrB = Triplet::array( B_nnz, B_is, B_js, B_nzs );
	Triplets _B = Triplets( 0, 0, B_rowCount, B_colCount,
	                        tripletArrB, tripletArrB + B_nnz );

	if ( transposeB )
		_B.transpose();

	// clean up
	free( A_is );
	free( A_js );
	free( A_nzs );
	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		free( B_is );
		free( B_js );
		free( B_nzs );
	}

	// Permute A and B matrices
	if ( cli.getPermuteAB() )
		Triplets::permuteRandom( _A, _B );

	fp_t** accumulators = nullptr;
	#pragma omp parallel num_threads( numThreads )
	{
		#pragma omp single
		{
			accumulators = util::alloc< fp_t* >( numThreads );
		}
		int tid = omp_get_thread_num();
		accumulators[ tid ] = util::newVector< fp_t >( _B.columns() );
	}

	runTests< glbInd_t, fp_t, glbNnz_t, glbNnz_t, Aligned >(
		_A, _B, accumulators, numThreads, iterations, isSorted, TARGET_SIZE );

#ifdef WITH_HBW_MEMORY
	runTests< glbInd_t, fp_t, glbNnz_t, glbNnz_t, Mcdram >(
		_A, _B, accumulators, numThreads, iterations, isSorted, TARGET_SIZE );
#endif

	// clean up
	for ( ui32 t = 0; t < numThreads; ++t )
		util::free( accumulators[ t ] );
	util::free( accumulators );
	delete [] tripletArrA;
	delete [] tripletArrB;
	return EXIT_SUCCESS;
}

template < typename ind_t, typename fp_t, typename nnz_t, typename znnz_t,
           typename mem >
void
runTests(
	Triplets& _A, Triplets& _B, fp_t** accumulators, const ui32 numThreads,
	const ui32& iterations, const bool& isSorted, const double& TARGET_SIZE )
{
	ui64* thrdMultsPfxSum = nullptr;
	ind_t* thrdRowPfxSum = nullptr;
	nnz_t* thrdCNnzPfxSum = nullptr;
	ui64* rowMultsPfxSum = nullptr;
	{

		#pragma omp parallel
		#pragma omp single
		{
			rowMultsPfxSum = (ui64*) Aligned::alloc( _A.rows() + 1, sizeof( ui64 ) );
			thrdMultsPfxSum = (ui64*) mem::alloc( numThreads + 1, sizeof( ui64 ) );
			thrdRowPfxSum = (ind_t*) mem::alloc( numThreads + 1, sizeof( ind_t ) );
			thrdCNnzPfxSum = (nnz_t*) mem::alloc( numThreads + 1, sizeof( nnz_t ) );
		}

		util::partitionRowWise(
			_A, _B, numThreads, thrdRowPfxSum, thrdMultsPfxSum, rowMultsPfxSum );
	}

    double flops = thrdMultsPfxSum[ numThreads ] * 2 + _A.length();

	// sort A's rows in descending order by # of multiplications
	if ( isSorted )
	{
		for ( ind_t i = _A.rows(); i > 0; --i )
			rowMultsPfxSum[ i ] -= rowMultsPfxSum[ i - 1 ];

		_A.sortRowBy( &rowMultsPfxSum[ 1 ], thrdRowPfxSum, numThreads );
	}

	Aligned::free( rowMultsPfxSum, _A.rows() + 1, sizeof( ui64 ) );

	RECORD( isSorted );

	Cs< ind_t, fp_t, nnz_t, mem > A;
	Cs< ind_t, fp_t, nnz_t, mem > B;
	convertToCsr( _A, A );
	convertToCsr( _B, B );

	fp_t* D = (fp_t*) mem::alloc( _A.columns(), sizeof( fp_t ) );
	for ( ind_t i = 0; i < _A.columns(); ++i )
		D[ i ] = 1;

	// Gustavson's algorithm
	// ------------------------------------------------------------------------
	Cs< ind_t, fp_t, nnz_t, mem > gust;
	rbr::gustavson::symbolic_stl< true, false >(
		A, B, gust, accumulators, thrdRowPfxSum, numThreads );

	RECORD( "Gustavson" );
	RECORD( "Symbolic" );
	RRUN( iterations,
	      rbr::gustavson::symbolic_stl(
		      A, B, gust, accumulators, thrdRowPfxSum, numThreads ); );

	RECORD( "Numeric" );
	GTRUN( iterations, flops,
		   rbr::gustavson::numeric(
			   A, B, D, gust, accumulators, thrdRowPfxSum, numThreads ); );
	std::cout.flush();

	// RECORD( "Numeric-no-wait" );
	// MEASURE_SINGLE(
	// 	rbr::gustavson::numeric( A, B, D, gust, accumulators,
	// 							 thrdRowPfxSum, numThreads, iterations ); );


	// Generate C-row-slice partitioning for 3rd phase
	// Criteria: every slice of length |slice.nzs| should fit in cache
	// ------------------------------------------------------------------------
	const double SIZE_BYTES = TARGET_SIZE * pow( 2, 10 );
	double limit = SIZE_BYTES;
	std::vector< std::vector< nnz_t > > vc;
	std::vector< std::vector< nnz_t > > va;
	for ( ui32 t = 0; t < numThreads; ++t )
	{
		double sizeSoFar = 0;
		vc.push_back( std::vector< nnz_t >() );
		va.push_back( std::vector< nnz_t >() );
		for ( ind_t r = thrdRowPfxSum[ t ]; r < thrdRowPfxSum[ t + 1 ]; ++r )
		{
			sizeSoFar = gust.ptr[ r + 1 ] * 12;
			if ( sizeSoFar >= limit )
			{
				vc[ t ].push_back( gust.ptr[ r + 1 ] );
				va[ t ].push_back( A.ptr[ r + 1 ] );
				limit += SIZE_BYTES;
			}
		}

		if ( vc[ t ].size() == 0 ||
			 vc[ t ].back() < gust.ptr[ thrdRowPfxSum[ t + 1 ] ] )
		{
			vc[ t ].push_back( gust.ptr[ thrdRowPfxSum[ t + 1 ] ] );
			va[ t ].push_back( A.ptr[ thrdRowPfxSum[ t + 1 ] ] );
		}
	}

	nnz_t* rowSliceCPfxSum = nullptr;
	nnz_t* rowSliceAPfxSum = nullptr;
	ui32* rowSlicePerThrdPfxSum = nullptr;
	#pragma omp parallel
	#pragma omp single
	{
		rowSlicePerThrdPfxSum = (ui32*) mem::alloc( numThreads + 1, sizeof( ui32 ) );
		rowSlicePerThrdPfxSum[ 0 ] = 0;
		for ( ui32 t = 0; t < numThreads; ++t )
			rowSlicePerThrdPfxSum[ t + 1 ] = vc[ t ].size() + rowSlicePerThrdPfxSum[ t ];

		rowSliceCPfxSum = (nnz_t*) mem::alloc(
			rowSlicePerThrdPfxSum[ numThreads ] + 1, sizeof( nnz_t ) );
		rowSliceAPfxSum = (nnz_t*) mem::alloc(
			rowSlicePerThrdPfxSum[ numThreads ] + 1, sizeof( nnz_t ) );

		ui32 z = 1;
		rowSliceCPfxSum[ 0 ] = 0;
		rowSliceAPfxSum[ 0 ] = 0;
		for ( ui32 t = 0; t < numThreads; ++t )
		{
			std::vector< nnz_t >& vecc = vc[ t ];
			std::vector< nnz_t >& veca = va[ t ];
			for ( ui32 i = 0; i < vecc.size(); ++i, ++z )
			{
				rowSliceCPfxSum[ z ] = vecc[ i ];
				rowSliceAPfxSum[ z ] = veca[ i ];
			}
		}
	}

	// Regularized algorithm
	// ------------------------------------------------------------------------
	regularized::AB< ind_t, fp_t, ind_t, mem > ab;
	Cs< ind_t, fp_t, nnz_t, mem > C;

	regularized::symbolic< true >(
		_B.columns(), A, B, C, ab, thrdRowPfxSum, thrdMultsPfxSum,
		accumulators, numThreads );

	RECORD( "3-phase" );
	RECORD( "Symbolic" );
	RRUN( iterations,
	      regularized::symbolic(
		      _B.columns(),
		      A, B, C, ab, thrdRowPfxSum, thrdMultsPfxSum,
		      accumulators, numThreads ); );

	omp_lock_t locks[ numThreads ];
	for ( ui32 t = 0; t < numThreads; ++t )
		omp_init_lock( &locks[ t ] );

	RECORD( "Numeric-lock" );
	C.reset();
	GTRUN( iterations, flops,
		   regularized::numeric_doubleThread(
			   ab, A, D, C, thrdMultsPfxSum, rowSliceAPfxSum, rowSliceCPfxSum,
			   rowSlicePerThrdPfxSum, locks, numThreads * 2 ); );
	RECORD( ( util::isSame( gust.nzs, C.nzs, gust.nnz() ) ? " " : "!" ) );

	for ( ui32 t = 0; t < numThreads; ++t )
		omp_destroy_lock( &locks[ t ] );


	char* conds = (char*) mem::alloc( numThreads * 64, sizeof( char ) );
	for ( ui32 i = 0; i < numThreads; ++i )
		conds[ i * 64 ] = 's';

	// RECORD( "Numeric-atomic" );
	// C.reset();
	// GTRUN( iterations, flops,
	// 	   regularized::numeric_doubleThread(
	// 		   ab, A, D, C, thrdMultsPfxSum, rowSliceAPfxSum, rowSliceCPfxSum,
	// 		   rowSlicePerThrdPfxSum, conds, numThreads * 2 ); );
	// RECORD( ( util::isSame( gust.nzs, C.nzs, gust.nnz() ) ? " " : "!" ) );
	// RECORD( "" );

	mem::free( conds, numThreads * 64, sizeof( char ) );
	C.free();
	ab.free();


	// ------------------------------------------------------------------------

	mem::free( rowSliceCPfxSum, rowSlicePerThrdPfxSum[ numThreads ] + 1, sizeof( nnz_t ) );
	mem::free( rowSliceAPfxSum, rowSlicePerThrdPfxSum[ numThreads ] + 1, sizeof( nnz_t ) );
	mem::free( rowSlicePerThrdPfxSum, numThreads + 1, sizeof( ui32 ) );
	mem::free( thrdRowPfxSum, numThreads + 1, sizeof( ind_t ) );
	mem::free( thrdCNnzPfxSum, numThreads + 1, sizeof( nnz_t ) );
	mem::free( thrdMultsPfxSum, numThreads + 1, sizeof( nnz_t ) );
}

/*
template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
job(
	regularized::AB< coffset_t, fp_t, alength_t, AB_mem > const* const ab,
	Cs< ind_t, fp_t, nnz_t, A_mem > const* const A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >* const C,
	const ind_t& A_START, const ind_t& A_END, const ui64& FLOP_START )
{
	const nnz_t C_NZ_START = C->ptr[ A_START ];
	const nnz_t C_NZ_END = C->ptr[ A_END ];
	for ( nnz_t nc = C_NZ_START; nc < C_NZ_END; ++nc )
		C->nzs[ nc ] = 0;

	const fp_t* myABNzs = ab->nzs + FLOP_START;
	const coffset_t* myCOffsets = ab->COffsets + FLOP_START;
	const nnz_t A_NZ_START = A->ptr[ A_START ];
	const nnz_t A_NZ_END = A->ptr[ A_END ];
	ui64 z = 0;
	for ( nnz_t na = A_NZ_START; na < A_NZ_END; ++na )
	{
		const fp_t FD = D[ A->inds[ na ] ];
		for ( alength_t l = 0; l < ab->ALengths[ na ]; ++l, ++z )
			C->nzs[ myCOffsets[ z ] ] += FD * myABNzs[ z ];
	}
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename coffset_t, typename alength_t,
           typename A_mem, typename C_mem, typename AB_mem >
void
numeric(
	const regularized::AB< coffset_t, fp_t, alength_t, AB_mem >& ab,
	const Cs< ind_t, fp_t, nnz_t, A_mem >& A, fp_t const* const D,
	Cs< ind_t, fp_t, nnz_t, C_mem >& C,
	ind_t const* const thrdPtrPfxSum, ui64 const* const thrdMultsPfxSum,
	const ui32& NUM_THREADS,
	const ui32& warmUps, const ui32& iterations )
{
	std::thread thrds[ NUM_THREADS ];

	// warm up
	for ( ui32 iter = 0; iter < warmUps; ++iter )
	{
		double results[ NUM_THREADS ];
		for ( int i = 0; i < NUM_THREADS - 1; ++i )
			thrds[ i ] = std::thread(
				job< ind_t, fp_t, nnz_t, coffset_t, alength_t, A_mem, C_mem, AB_mem >,
				&ab, &A, D, &C, thrdPtrPfxSum[ i ],
				thrdPtrPfxSum[ i + 1 ], thrdMultsPfxSum[ i ] );

		job( &ab, &A, D, &C,
			 thrdPtrPfxSum[ NUM_THREADS - 1 ], thrdPtrPfxSum[ NUM_THREADS ],
			 thrdMultsPfxSum[ NUM_THREADS - 1 ] );

		for ( ui32 i = 0; i < NUM_THREADS - 1; ++i )
			thrds[ i ].join();
	}

	// measure
	globalTimer.start();
	for ( ui32 iter = 0; iter < iterations; ++iter )
	{
		for ( int i = 0; i < NUM_THREADS - 1; ++i )
			thrds[ i ] = std::thread(
				job< ind_t, fp_t, nnz_t, coffset_t, alength_t, A_mem, C_mem, AB_mem >,
				&ab, &A, D, &C, thrdPtrPfxSum[ i ],
				thrdPtrPfxSum[ i + 1 ], thrdMultsPfxSum[ i ] );

		job( &ab, &A, D, &C,
			 thrdPtrPfxSum[ NUM_THREADS - 1 ], thrdPtrPfxSum[ NUM_THREADS ],
			 thrdMultsPfxSum[ NUM_THREADS - 1 ] );

		for ( ui32 i = 0; i < NUM_THREADS - 1; ++i )
			thrds[ i ].join();

	}
	globalTimer.finish();

	double dur = globalTimer.getDuration() / iterations;
	RECORD( dur );
}
*/
