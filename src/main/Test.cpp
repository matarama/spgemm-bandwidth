
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <map>
#include <omp.h>
#include <set>
#include <unordered_set>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#endif

#include "include/sparse_types.h"
#include "include/util/Generic.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Cs.h"
#include "include/io/InputReader.h"
#include "include/io/MMInfo.h"
#include "include/io/Cli.h"
#include "include/logging/Timer.h"
#include "include/parallel/Gustavson.h"
#include "include/parallel/Reduced_v0.h"

using namespace allocator;
using namespace sparse_storage;

template < typename ind_t, typename fp_t, typename nnz_t,
           typename zind_t, typename znnz_t,
           typename mem >
void
runTests(
	Triplets& _A, Triplets& _B, fp_t** accumulators, const ui32 numThreads,
	const ui32& iterations );

int main( int argc, char** argv )
{
	Cli cli( argc, argv );

	// Parameters
	// ------------------------------------------------------------------------
	if ( !cli.isSet( { Cli::MMF_PATH_A, Cli::NUM_BLOCKS } ) )
	{
		return EXIT_FAILURE;
	}

	ui32 iterations = cli.getIterations();
	std::string mmfPathA = cli.getMmfPathA();
	std::string mmfPathB = cli.getMmfPathB();
	ui32 numThreads = cli.getNumBlocks();
	bool transposeB = cli.getTransposeB();
	std::vector< ui32 > vAccSizes = cli.getAccSizes();

	omp_set_num_threads( numThreads );

	// Read matrix market format file.
	// ------------------------------------------------------------------------
	MMInfo A_mmInfo;
	int& A_isSymmetric = A_mmInfo.isSymmetric;
	int A_rowCount = 0;
	int A_colCount = 0;
	int A_nnz = 0;
	int* A_is = NULL;
	int* A_js = NULL;
	double* A_nzs = NULL;
	InputReader::readMMF( mmfPathA, A_mmInfo, &A_rowCount, &A_colCount,
	                      &A_nnz, &A_is, &A_js, &A_nzs);
	Triplet* tripletArrA = Triplet::array( A_nnz, A_is, A_js, A_nzs );
	Triplets _A( 0, 0, A_rowCount, A_colCount,
	             tripletArrA, tripletArrA + A_nnz );
	std::cout << mmfPathA.substr( mmfPathA.find_last_of( '/' ) + 1,
	                              mmfPathA.find_last_of( '.' ) - 1 )
	          << "," << A_nnz << "," << A_rowCount << "," << A_colCount << ",";

	MMInfo B_mmInfo;
	int B_isSymmetric = 0;
	int B_rowCount = 0;
	int B_colCount = 0;
	int B_nnz = 0;
	int* B_is = NULL;
	int* B_js = NULL;
	double* B_nzs = NULL;

	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		InputReader::readMMF( mmfPathB, B_mmInfo, &B_rowCount, &B_colCount,
		                      &B_nnz, &B_is, &B_js, &B_nzs);
		B_isSymmetric = B_mmInfo.isSymmetric;
	}
	else
	{
		mmfPathB = mmfPathA;
		B_isSymmetric = A_isSymmetric;
		B_rowCount = A_rowCount;
		B_colCount = A_colCount;
		B_nnz = A_nnz;
		B_is = A_is;
		B_js = A_js;
		B_nzs = A_nzs;
	}

	Triplet* tripletArrB = Triplet::array( B_nnz, B_is, B_js, B_nzs );
	Triplets _B = Triplets( 0, 0, B_rowCount, B_colCount,
	                        tripletArrB, tripletArrB + B_nnz );

	if ( transposeB )
		_B.transpose();

	// clean up
	free( A_is );
	free( A_js );
	free( A_nzs );	
	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		free( B_is );
		free( B_js );
		free( B_nzs );
	}

	// Permute A and B matrices
	if ( cli.getPermuteAB() )
		Triplets::permuteRandom( _A, _B );

	fp_t** accumulators = nullptr;
	#pragma omp parallel num_threads( numThreads )
	{
		#pragma omp single
		{
			accumulators = util::alloc< fp_t* >( numThreads );
		}
		int tid = omp_get_thread_num();
		accumulators[ tid ] = util::newVector< fp_t >( _B.columns() );
	}

	runTests< glbInd_t, fp_t, glbNnz_t, glbInd_t, glbNnz_t, Aligned >(
		_A, _B, accumulators, numThreads, iterations );

#ifdef WITH_HBW_MEMORY
	runTests< glbInd_t, fp_t, glbNnz_t, glbInd_t, glbNnz_t, Mcdram >(
		_A, _B, accumulators, numThreads, iterations );
#endif

	// clean up
	for ( ui32 t = 0; t < numThreads; ++t )
		util::free( accumulators[ t ] );
	util::free( accumulators );
	
	delete [] tripletArrA;
	delete [] tripletArrB;
	return EXIT_SUCCESS;
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename zind_t, typename znnz_t,
           typename mem >
void
runTests(
	Triplets& _A, Triplets& _B, fp_t** accumulators, const ui32 numThreads,
	const ui32& iterations )
{
	ui64* colMultsPfxSum = nullptr;
	ui64 thrdFlop_2PfxSum[ numThreads + 1 ];
	glbNnz_t thrdColPfxSum[ numThreads + 1 ];
	{
		#pragma omp parallel
		#pragma omp single
		colMultsPfxSum = (ui64*) mem::alloc( _B.columns() + 1, sizeof( ui64 ) );

		util::partitionColumnWise(
			_A, _B, numThreads, thrdColPfxSum, thrdFlop_2PfxSum, colMultsPfxSum );
		// std::shared_ptr< std::vector< glbNnz_t > > pB_colPtr = _B.colPtr();
		// std::vector< glbNnz_t >& B_colPtr = *pB_colPtr.get();
		// std::shared_ptr< std::vector< glbNnz_t > > pA_colPtr = _A.colPtr();
		// std::vector< glbNnz_t >& A_colPtr = *pA_colPtr.get();
		// util::partition( &B_colPtr[ 0 ], _B, false, &A_colPtr[ 0 ], numThreads,
		//                  thrdColPfxSum, thrdFlop_2PfxSum, colMultsPfxSum );

		std::cout << std::endl;
		std::cout << "thrdColPfxSum:";
		for ( ui32 t = 0; t < numThreads + 1; ++t )
			std::cout << " " << thrdColPfxSum[ t ];
		std::cout << std::endl;

		std::cout << "thrdFlops_2PfxSum:";
		for ( ui32 t = 0; t < numThreads + 1; ++t )
			std::cout << " " << thrdFlop_2PfxSum[ t ];
		std::cout << std::endl;

		std::cout << "colMultsPfxSum: ";
		for ( ind_t i = 0; i < _B.columns() + 1; ++i )
			std::cout << colMultsPfxSum[ i ] << " ";
		std::cout << std::endl;
	}

	Cs< ind_t, fp_t, nnz_t, mem > C;
	Cs< zind_t, fp_t, znnz_t, mem > Z;
	Cs< ind_t, fp_t, nnz_t, mem > A;
	Cs< ind_t, fp_t, nnz_t, mem > B;
	// convertToCsr( _A, A );
	convertToCsc( _A, A );
	convertToCsc( _B, B );

	fp_t* D = (fp_t*) mem::alloc( _A.rows(), sizeof( fp_t ) );
	for ( glbInd_t i = 0; i < _A.rows(); ++i )
		D[ i ] = 1;

	glbNnz_t thrdCNnzPfxSum[ numThreads + 1 ];
	fp_t** writeAddresses = nullptr;
	// reduced::v0::symbolic_alloc(
	// 	A, B, thrdColPfxSum, thrdFlop_2PfxSum, numThreads,
	// 	thrdCNnzPfxSum, Z, C );

	reduced::v0::symbolic_alloc_sort(
		A, B, thrdColPfxSum, colMultsPfxSum, numThreads,
		thrdCNnzPfxSum, Z, C, accumulators, &writeAddresses );

	RECORD( "Z" );
	RECORD( "Symbolic" );
	// RRUN( iterations,
	//       reduced::v0::symbolic(
	// 	      A, B, thrdColPfxSum, thrdFlop_2PfxSum, numThreads,
	// 	      thrdCNnzPfxSum, Z, C ); );

	RRUN( iterations,
	      reduced::v0::symbolic_sort(
		      A, B, thrdColPfxSum, colMultsPfxSum, numThreads,
		      thrdCNnzPfxSum, Z, C, accumulators, writeAddresses ); );

	RECORD( "Numeric" );
	RRUN( iterations,
	      reduced::v0::numeric( Z, D, thrdCNnzPfxSum, numThreads, C.nzs ); );


	// std::cout << std::endl;
	// std::cout << "columns:" << _B.columns() << std::endl;
	// std::cout << "thrdColPfxSum:";
	// for ( ui32 t = 0; t < numThreads + 1; ++t )
	// 	std::cout << thrdColPfxSum[ t ] << " ";
	// std::cout << std::endl;

	Cs< glbInd_t, fp_t, glbNnz_t, Aligned > A2;
	Cs< glbInd_t, fp_t, glbNnz_t, Aligned > C2;
	convertToCsc( _A, A2 );

	rbr::gustavson::symbolic_stl_alloc< true >(
		A2, B, C2, accumulators, thrdColPfxSum, numThreads );

	// std::cout << "C2.rowPtr:";
	// for ( ind_t i = 0; i < C2.length + 1; ++i )
	// 	std::cout << " " << C2.ptr[ i ];
	// std::cout << std::endl;

	RECORD( "Gustavson" );
	RECORD( "Symbolic" );
	RRUN( iterations,
	      rbr::gustavson::symbolic_stl< false >(
		      A2, B, C2, accumulators, thrdColPfxSum, numThreads ); );

	rbr::gustavson::symbolic_stl< true >(
		A2, B, C2, accumulators, thrdColPfxSum, numThreads );

	RECORD( "Numeric" );
	RRUN( iterations,
	      rbr::gustavson::numeric(
		      A2, B, D, C2, accumulators, thrdColPfxSum, numThreads ); );
	std::cout.flush();


	RECORD( ( util::isSame( C2.inds, C.inds, C2.nnz() ) ? " " : "!" ) );
	RECORD( ( util::isSame( C2.nzs, C.nzs, C2.nnz() ) ? " " : "!" ) );

	mem::free( D, _A.rows(), sizeof( fp_t ) );
	std::cout.flush();
}
