/*
 * StaticRowByRow3p_v1.cpp
 *
 *  Created on: Sep 26, 2018
 *      Author: memoks
 */

#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <map>
#include <omp.h>
#include <set>
#include <unordered_set>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#endif

#include "include/sparse_types.h"
#include "include/util/Generic.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Csr.h"
#include "include/io/InputReader.h"
#include "include/io/MMInfo.h"
#include "include/io/Cli.h"
#include "include/logging/Timer.h"
#include "include/parallel/RowByRow3p_v1.h"

// #############################################################################

int main( int argc, char* argv[] )
{
	Cli cli( argc, argv );

	// Parafmeters
	// ------------------------------------------------------------------------
	if ( !cli.isSet( { Cli::MMF_PATH_A, Cli::NUM_BLOCKS, Cli::ACC_SIZES,
					Cli::B_SIZE_KB } ) )
	{
		return EXIT_FAILURE;
	}

	ui32 iterations = cli.getIterations();
	std::string mmfPathA = cli.getMmfPathA();
	std::string mmfPathB = cli.getMmfPathB();
	ui32 numThreads = cli.getNumBlocks();
	bool transposeB = cli.getTransposeB();
	std::vector< ui32 > vAccSizes = cli.getAccSizes();
	double BSizeKB = cli.getBSizeKB();

	omp_set_num_threads( numThreads );

	std::stringstream sheader;

	// Read matrix market format file.
	// ------------------------------------------------------------------------
	MMInfo A_mmInfo;
	int& A_isSymmetric = A_mmInfo.isSymmetric;
	int A_rowCount = 0;
	int A_colCount = 0;
	int A_nnz = 0;
	int* A_is = NULL;
	int* A_js = NULL;
	double* A_nzs = NULL;
	InputReader::readMMF( mmfPathA, A_mmInfo, &A_rowCount, &A_colCount,
	                      &A_nnz, &A_is, &A_js, &A_nzs);
	Triplet* tripletArrA = Triplet::array( A_nnz, A_is, A_js, A_nzs );
	Triplets A( 0, 0, A_rowCount, A_colCount,
	            tripletArrA, tripletArrA + A_nnz );
	std::cout << mmfPathA.substr( mmfPathA.find_last_of( '/' ) + 1,
	                              mmfPathA.find_last_of( '.' ) - 1 ) << ",";

	MMInfo B_mmInfo;
	int B_isSymmetric = 0;
	int B_rowCount = 0;
	int B_colCount = 0;
	int B_nnz = 0;
	int* B_is = NULL;
	int* B_js = NULL;
	double* B_nzs = NULL;

	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		InputReader::readMMF( mmfPathB, B_mmInfo, &B_rowCount, &B_colCount,
		                      &B_nnz, &B_is, &B_js, &B_nzs);
		B_isSymmetric = B_mmInfo.isSymmetric;
	}
	else
	{
		mmfPathB = mmfPathA;
		B_isSymmetric = A_isSymmetric;
		B_rowCount = A_rowCount;
		B_colCount = A_colCount;
		B_nnz = A_nnz;
		B_is = A_is;
		B_js = A_js;
		B_nzs = A_nzs;
	}

	Triplet* tripletArrB = Triplet::array( B_nnz, B_is, B_js, B_nzs );
	Triplets B = Triplets( 0, 0, B_rowCount, B_colCount,
	                       tripletArrB, tripletArrB + B_nnz );

	if ( transposeB )
		B.transpose();

	// clean up
	free( A_is );
	free( A_js );
	free( A_nzs );	
	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		free( B_is );
		free( B_js );
		free( B_nzs );
	}

	// Permute A and B matrices
	if ( cli.getPermuteAB() )
		Triplets::permuteRandom( A, B );

	// Preprocess matrix C = A x B
	fp_t** accumulators = nullptr;
	#pragma omp parallel num_threads( numThreads )
	{
		#pragma omp single
		{
			accumulators = util::alloc< fp_t* >( numThreads );
		}
		int tid = omp_get_thread_num();
		accumulators[ tid ] = util::newVector< fp_t >( A.rows() );
	}

	ui64 rowStartC = 0;
    ui64 colStartC = 0;
	ui64 nnzC = 0;
    ui64 flops = 0;
    Triplet* tripletArrC = util::preprocessC( A, B, accumulators, nnzC, flops );
    Triplets C( 0, 0, A.rows(), B.columns(), tripletArrC, tripletArrC + nnzC );

	// -------------------------------------------------------------------------

	std::vector< ui32 > vAccLengths( vAccSizes.size() + 1, 0 );
	for ( ui32 i = 0; i < vAccSizes.size(); ++i )
		vAccLengths[ i ] = ( vAccSizes[ i ] * 1024 ) / sizeof( fp_t );

	glbInd_t powerOf2 = 2;
	while ( powerOf2 < A.rows() )
		powerOf2 *= 2;
	vAccLengths[ vAccSizes.size() ] = powerOf2;

	// RowByRow 3 phase algorithm
	// ------------------------------------------------------------------------

	{
		Csr< glbInd_t, fp_t, glbNnz_t > rowByRow3p;
		Csr< glbInd_t, fp_t, glbNnz_t > csrA;
		Csr< glbInd_t, fp_t, glbNnz_t > csrB;
		ind_t* thrdRowPfxSum = nullptr;
		ui32* thrdACountPfxSum = nullptr;
		rbr3p::v1::Thrd< ind_t, fp_t, nnz_t >** thrds = nullptr;
		rbr3p::v1::prep( A, B, C, numThreads, csrA, csrB, rowByRow3p, &thrds,
		                 &thrdRowPfxSum );
		glbInd_t* B_bucketRowPfxSum = nullptr;
		ui32 totalBucketCount = 0;
		
		ui32 noOfMasks = vAccLengths.size();
		ui32* masks = nullptr;
		char* maskIndexPerRow = nullptr;
		fp_t** accumulators = nullptr;
		#pragma omp parallel num_threads( numThreads )
		{                  
			#pragma omp single
			{
				maskIndexPerRow = util::alloc< char >( A.rows() );
				masks = util::alloc< ui32 >( noOfMasks );
				for ( ui32 i = 0; i < noOfMasks; ++i )
					masks[ i ] = vAccLengths[ i ] - 1;
			}
			int _id = omp_get_thread_num();
		}

		{
			std::shared_ptr< std::vector< ui64 > > pMultsPerRow =
				Triplets::calculateMultsPerRow( A, B );
			util::generateMultiMapSTL(
				vAccLengths, C.rows(), C.columns(),
				rowByRow3p.rowPtr, rowByRow3p.colInd,
				numThreads, maskIndexPerRow );

			// RRUN( iterations,
			//       markNoCollisionRows(
			// 	      vAccLengths, rowByRow3p, numThreads,
			// 	      maskIndexPerRow, *pMultsPerRow.get() ); );
		}

		// For correctness check
		// ---------------------------------------------------------------------
		Csr< glbInd_t, fp_t, glbNnz_t > gust = rowByRow3p;
		gust.zero();
		rbr::gustavson::CAB( csrA, csrB, gust, numThreads, thrdRowPfxSum,
		                     accumulators, maskIndexPerRow, noOfMasks, masks );

		// Timings
		// #####################################################################

		rbr3p::v1::phase0_alloc( B, BSizeKB, &B_bucketRowPfxSum,
		                         totalBucketCount );

		RECORD( numThreads );
		RECORD( totalBucketCount );

		RECORD( "p0" );
		RRUN( iterations,
		      rbr3p::v1::phase0( B, BSizeKB, B_bucketRowPfxSum,
		                         totalBucketCount );
			);

		// std::cout << "B_l2_rowPtr:";
		// for ( ui32 i = 0; i < totalBucketCount + 1; ++i )
		// 	std::cout << " " << B_bucketRowPfxSum[ i ];
		// std::cout << std::endl;

		RECORD( "p1" );
		ui64* B_bucketLengths = nullptr;
		glbInd_t** allB_bucketsColInd = nullptr;
		fp_t** allB_bucketsNzs = nullptr;
		rbr3p::v1::phase1_alloc(
			csrA, csrB, B_bucketRowPfxSum, totalBucketCount,
			thrds, numThreads,
			&B_bucketLengths, &allB_bucketsColInd, &allB_bucketsNzs );

		RRUN( iterations,
		      rbr3p::v1::phase1(
			      csrA, csrB, B_bucketRowPfxSum, totalBucketCount,
			      thrds, numThreads, B_bucketLengths, allB_bucketsColInd,
			      allB_bucketsNzs );
			);


		RECORD( "p2" );
		RRUN( iterations, rbr3p::v1::phase2( csrB, thrds, numThreads ); );


		RECORD( "p3" );
		RRUN( iterations,
		      rbr3p::v1::phase3( csrA, rowByRow3p, masks, maskIndexPerRow,
		                         accumulators, thrds, numThreads ); );

		// Correctness check
		RECORD( ( util::isSame( gust.getNzs(), rowByRow3p.getNzs(),
		                        gust.nnz ) ? " " : "!" ) );

		RECORD( "p3_avx" );
#ifdef WITH_AVX512
		RECORD( "p3_avx512" );
#endif
		for ( ui32 cacheCapacity = 64;
		      cacheCapacity <= 2048;
		      cacheCapacity *= 2 )
		{
			rbr3p::v1::allocCache( thrds, numThreads, cacheCapacity );

			rowByRow3p.zero();
			RRUN( iterations,
			      rbr3p::v1::phase3_avx(
				      csrA, rowByRow3p, masks, maskIndexPerRow,
				      accumulators, thrds, numThreads ); );

			// Correctness check
			RECORD( ( util::isSame( gust.getNzs(), rowByRow3p.getNzs(),
			                        gust.nnz ) ? " " : "!" ) );

#ifdef WITH_AVX512
			rowByRow3p.zero();
			RRUN( iterations,
			      rbr3p::v1::phase3_avx512(
				      csrA, rowByRow3p, masks, maskIndexPerRow,
				      accumulators, thrds, numThreads ); );

			// Correctness check
			RECORD( ( util::isSame( gust.getNzs(), rowByRow3p.getNzs(),
			                        gust.nnz() ) ? " " : "!" ) );
			RECORD( "|" );
#endif
		}

		// clean up
		for ( ui32 i = 0; i < numThreads; ++i )
		{
			thrds[ i ]->free();
			util::free( thrds[ i ] );
			util::free( accumulators[ i ] );
		}
		util::free( thrds );
		util::free( accumulators );
		util::free( B_bucketLengths );
		util::free( allB_bucketsColInd );
		util::free( allB_bucketsNzs );
		util::free( thrdRowPfxSum );
		util::free( B_bucketRowPfxSum );
		util::free( masks );
		util::free( maskIndexPerRow );
	}

	std::cout << std::endl;

	delete [] tripletArrC;
	delete [] tripletArrA;
	delete [] tripletArrB;
	return EXIT_SUCCESS;
}
