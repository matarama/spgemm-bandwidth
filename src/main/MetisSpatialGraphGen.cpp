
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <map>
#include <omp.h>
#include <set>
#include <unordered_set>
#include <bitset>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#endif

#include "include/sparse_types.h"
#include "include/util/Generic.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Csr.h"
#include "include/io/InputReader.h"
#include "include/io/MMInfo.h"
#include "include/io/Cli.h"
#include "include/logging/Timer.h"

using vtx_type = int;
using adj_type = int;
using wgt_type = int;
using pid_type = int;
using real_type = float;
using option_type = int;

// ############################################################################

extern void
printGraph( const std::string& outputPath,
            const vtx_type& noOfVertices, const adj_type& noOfEdges,
            adj_type const* const edgeCountPtr,
            vtx_type const* const adjacencyList,
            wgt_type const* const vertexWeights,
            wgt_type const* const edgeWeights );

int main( int argc, char* argv[] )
{
	Cli cli( argc, argv );

	std::cout << "---------------------------------"
	          << "---------------------------------------"
	          << std::endl;

	// Parameters
	// ------------------------------------------------------------------------
	if ( !cli.isSet( { Cli::MMF_PATH_A, Cli::OUTPUT_PATH } ) )
	{
		return EXIT_FAILURE;
	}
	ui32 iterations = cli.getIterations();

	std::string mmfPathA = cli.getMmfPathA();
	std::string mmfPathB = cli.getMmfPathB();
	ui32 numThreads = cli.getNumBlocks();
	bool transposeB = cli.getTransposeB();
	std::string outputPath = cli.getOutputPath();

	omp_set_num_threads( numThreads );
#ifdef __INTEL_COMPILER
	mkl_set_num_threads( numThreads );
#endif

	// Read matrix market format file.
	// ------------------------------------------------------------------------
	MMInfo A_mmInfo;
	int& A_isSymmetric = A_mmInfo.isSymmetric;
	int A_rowCount = 0;
	int A_colCount = 0;
	int A_nnz = 0;
	int* A_is = NULL;
	int* A_js = NULL;
	double* A_nzs = NULL;
	InputReader::readMMF( mmfPathA, A_mmInfo, &A_rowCount, &A_colCount,
	                      &A_nnz, &A_is, &A_js, &A_nzs);
	Triplet* tripletArrA = Triplet::array( A_nnz, A_is, A_js, A_nzs );
	Triplets A( 0, 0, A_rowCount, A_colCount,
	            tripletArrA, tripletArrA + A_nnz );
	std::string mmfNameA =
		mmfPathA.substr( mmfPathA.find_last_of( '/' ) + 1,
		                 mmfPathA.find_last_of( '.' ) - 1 );
	std::cout << "A: " << mmfNameA << std::endl;

	MMInfo B_mmInfo;
	int B_isSymmetric = 0;
	int B_rowCount = 0;
	int B_colCount = 0;
	int B_nnz = 0;
	int* B_is = NULL;
	int* B_js = NULL;
	double* B_nzs = NULL;

	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		InputReader::readMMF( mmfPathB, B_mmInfo, &B_rowCount, &B_colCount,
		                      &B_nnz, &B_is, &B_js, &B_nzs);
		B_isSymmetric = B_mmInfo.isSymmetric;
	}
	else
	{
		mmfPathB = mmfPathA;
		B_isSymmetric = A_isSymmetric;
		B_rowCount = A_rowCount;
		B_colCount = A_colCount;
		B_nnz = A_nnz;
		B_is = A_is;
		B_js = A_js;
		B_nzs = A_nzs;
	}

	Triplet* tripletArrB = Triplet::array( B_nnz, B_is, B_js, B_nzs );
	Triplets B = Triplets( 0, 0, B_rowCount, B_colCount,
	                       tripletArrB, tripletArrB + B_nnz );
	std::string mmfNameB =
		mmfPathB.substr( mmfPathA.find_last_of( '/' ) + 1,
		                 mmfPathA.find_last_of( '.' ) - 1 );
	std::cout << "B: " << mmfNameB << std::endl << std::endl;

	if ( transposeB )
		B.transpose();

	// clean up
	free( A_is );
	free( A_js );
	free( A_nzs );	
	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		free( B_is );
		free( B_js );
		free( B_nzs );
	}

	// Permute A and B matrices
	if ( cli.getPermuteAB() )
		Triplets::permuteRandom( A, B );

	A.sortRowMajor();
	B.sortRowMajor();

	std::shared_ptr< std::vector< glbNnz_t > > sA_rowPtr = A.rowPtr();
	std::shared_ptr< std::vector< glbNnz_t > > sB_rowPtr = B.rowPtr();
	std::vector< glbNnz_t > A_rowPtr = *sA_rowPtr.get();
	std::vector< glbNnz_t > B_rowPtr = *sB_rowPtr.get();

	double A_preparationTime = 0.0d;
	// Reorder A
	// -------------------------------------------------------------------------
	{
		TIMER_START();
		vtx_type A_rows = A_rowCount;
		vtx_type noOfVertices = A_rowCount + B_rowCount;
		vtx_type noOfBalanceConstraints = 1;
		adj_type* edgeCountPfxSumPerVertex = util::alloc< adj_type >( noOfVertices + 1 );
		wgt_type* vertexWeights = util::newVector< wgt_type >( noOfVertices );
		vtx_type* vertexSizes = util::newVector< vtx_type >( noOfVertices );
		vtx_type* edgesPerVertex = util::alloc< vtx_type >( A_nnz * 2 );
		wgt_type* edgeWeights = util::alloc< wgt_type >( A_nnz * 2 );;
		edgeCountPfxSumPerVertex[ 0 ] = 0;
		adj_type* edgeCountPfxSumPerBVertex =
			util::newVector< adj_type >( B_rowCount + 1 );
		edgeCountPfxSumPerBVertex[ 0 ] = 0;
		for ( glbInd_t r = 0; r < A_rowCount; ++r )
		{
			edgeCountPfxSumPerVertex[ r + 1 ] = A_rowPtr[ r + 1 ];
			for ( glbNnz_t i = A_rowPtr[ r ]; i < A_rowPtr[ r + 1 ]; ++i )
			{
				glbInd_t jA = A[ i ].j;
				vertexWeights[ r ] += B_rowPtr[ jA + 1 ] - B_rowPtr[ jA ];
				++edgeCountPfxSumPerBVertex[ jA + 1 ];

				// edge from {A} to {B}
				edgesPerVertex[ i ] = jA;
				edgeWeights[ i ] = B_rowPtr[ jA + 1 ] - B_rowPtr[ jA ];
			}
		}

		// std::cout << "edgeCountPfxSumPerBVertex:";
		edgeCountPfxSumPerBVertex[ 0 ] = edgeCountPfxSumPerVertex[ A_rowCount ];
		for ( glbInd_t r = 0; r < B_rowCount; ++r )
		{
			edgeCountPfxSumPerBVertex[ r + 1 ] +=
				edgeCountPfxSumPerBVertex[ r ];
			edgeCountPfxSumPerVertex[ A_rowCount + r ] =
				edgeCountPfxSumPerBVertex[ r ];
			// std::cout << edgeCountPfxSumPerBVertex[ r + 1 ] << std::endl;
		}
		edgeCountPfxSumPerVertex[ A_rowCount + B_rowCount ] =
			edgeCountPfxSumPerBVertex[ B_rowCount ];

		for ( glbInd_t i = 0; i < A_nnz; ++i )
		{
			glbInd_t iA = A[ i ].i;
			glbInd_t jA = A[ i ].j;
			glbInd_t idB = A_rowCount + jA - 1;

			// edge from {B} to {A}
			// std::cout << "adding for " << idB << " : "
			//           << edgeCountPfxSumPerBVertex[ jA ] << std::endl;
			edgesPerVertex[ edgeCountPfxSumPerBVertex[ jA ] ] = A[ i ].i;
			edgeWeights[ edgeCountPfxSumPerBVertex[ jA ] ] =
				B_rowPtr[ jA + 1 ] - B_rowPtr[ jA ];
			++edgeCountPfxSumPerBVertex[ jA ];
		}

		// std::cout << "edgesPerVertex" << std::endl;;
		// for ( ui32 i = 0; i < noOfVertices; ++i )
		// {
		// 	std::cout << i << " (" << vertexWeights[ i ] << ") -> ";
		// 	for ( ui32 j = edgeCountPfxSumPerVertex[ i ];
		// 	      j < edgeCountPfxSumPerVertex[ i + 1 ];
		// 	      ++j )
		// 	{
		// 		std::cout << edgesPerVertex[ j ] << "("
		// 		          << edgeWeights[ j ] << ") ";
		// 	}
		// 	std::cout << std::endl;
		// }

		TIMER_STOP();
		A_preparationTime = TIMER_DURATION();
		std::cout << "Preparation_Time: " << A_preparationTime << std::endl;
		std::cout << "A_nnz: " << A.length() << std::endl;
		std::cout << "A_vertexCount: " << noOfVertices << std::endl;
		std::cout << "A_edgeCount: "
		          << edgeCountPfxSumPerVertex[ noOfVertices ] / 2
		          << std::endl;
		std::cout.flush();

		// convert adjacency list into 1 based indexing
		for ( int e = 0; e < edgeCountPfxSumPerVertex[ noOfVertices ]; ++e )
			++edgesPerVertex[ e ];

		printGraph( outputPath,
		            noOfVertices, edgeCountPfxSumPerVertex[ noOfVertices ] / 2,
		            edgeCountPfxSumPerVertex, edgesPerVertex,
		            vertexWeights, edgeWeights );

		util::free( edgeCountPfxSumPerVertex );
		util::free( vertexWeights );
		util::free( vertexSizes );
		util::free( edgesPerVertex );
		util::free( edgeWeights );
		util::free( edgeCountPfxSumPerBVertex );
	}

	std::cout << std::endl;

	delete [] tripletArrA;
	delete [] tripletArrB;
	return EXIT_SUCCESS;
}

void
printGraph( const std::string& outputPath,
            const vtx_type& noOfVertices, const adj_type& noOfEdges,
            adj_type const* const edgeCountPtr,
            vtx_type const* const adjacencyList,
            wgt_type const* const vertexWeights,
            wgt_type const* const edgeWeights )
{
	std::ofstream of( outputPath );

	of << noOfVertices << " " << noOfEdges << " 11 1" << std::endl;
	for ( vtx_type v = 0; v < noOfVertices; ++v )
	{
		of << vertexWeights[ v ];
		for ( adj_type e = edgeCountPtr[ v ]; e < edgeCountPtr[ v + 1 ]; ++e )
			of << " " << adjacencyList[ e ] << " " << edgeWeights[ e ];
		of << std::endl;
	}

	of.close();
}

