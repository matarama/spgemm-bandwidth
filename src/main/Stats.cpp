/*
 * Stats.cpp
 *
 *  Created on: Aug 26, 2017
 *      Author: memoks
 */


#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <map>
#include <omp.h>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#endif

#include "include/sparse_types.h"
#include "include/util/Generic.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
// #include "include/util/Data.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Csr.h"
#include "include/io/InputReader.h"
#include "include/io/MMInfo.h"
#include "include/io/Cli.h"
#include "include/logging/Timer.h"

// #############################################################################

int main( int argc, char* argv[] )
{
	Cli cli( argc, argv );

	// Parameters
	// ------------------------------------------------------------------------
	if ( !cli.isSet( { Cli::MMF_PATH_A } ) )
	{
		return EXIT_FAILURE;
	}

	std::string mmfPathA = cli.getMmfPathA();
	std::string mmfPathB = cli.getMmfPathB();
	bool transposeB = cli.getTransposeB();

	// Read matrix market format file.
	// ------------------------------------------------------------------------

	MMInfo A_mmInfo;
	int& A_isSymmetric = A_mmInfo.isSymmetric;
	int A_rowCount = 0;
	int A_colCount = 0;
	int A_nnz = 0;
	int* A_is = NULL;
	int* A_js = NULL;
	double* A_nzs = NULL;
	InputReader::readMMF( mmfPathA, A_mmInfo, &A_rowCount, &A_colCount,
	                      &A_nnz, &A_is, &A_js, &A_nzs);
	Triplet* tripletArrA = Triplet::array( A_nnz, A_is, A_js, A_nzs );
	Triplets A( 0, 0, A_rowCount, A_colCount,
	            tripletArrA, tripletArrA + A_nnz );
	std::shared_ptr< std::vector< glbInd_t > > pRowPtrA = A.rowPtr();

	MMInfo B_mmInfo;
	int B_isSymmetric = 0;
	int B_rowCount = 0;
	int B_colCount = 0;
	int B_nnz = 0;
	int* B_is = NULL;
	int* B_js = NULL;
	double* B_nzs = NULL;

	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		InputReader::readMMF( mmfPathB, B_mmInfo, &B_rowCount, &B_colCount,
		                      &B_nnz, &B_is, &B_js, &B_nzs);
		B_isSymmetric = B_mmInfo.isSymmetric;
	}
	else
	{
		mmfPathB = mmfPathA;
		B_isSymmetric = A_isSymmetric;
		B_rowCount = A_rowCount;
		B_colCount = A_colCount;
		B_nnz = A_nnz;
		B_is = A_is;
		B_js = A_js;
		B_nzs = A_nzs;

		if( B_rowCount != B_colCount )
			transposeB = true;
	}

	Triplet* tripletArrB = Triplet::array( B_nnz, B_is, B_js, B_nzs );
	Triplets B = Triplets( 0, 0, B_rowCount, B_colCount,
	                       tripletArrB, tripletArrB + B_nnz );
	if ( transposeB )
		B.transpose();
	std::shared_ptr< std::vector< glbInd_t > > pRowPtrB = B.rowPtr();

	// clean up
	free( A_is );
	free( A_js );
	free( A_nzs );
	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		free( B_is );
		free( B_js );
		free( B_nzs );
	}

	std::shared_ptr< std::vector< glbInd_t > > A_pNnzPerRow = A.nnzPerRow();
	std::shared_ptr< std::vector< glbInd_t > > B_pNnzPerRow = B.nnzPerRow();
	std::vector< glbInd_t >& A_nnzPerRow = *A_pNnzPerRow.get();
	std::vector< glbInd_t >& B_nnzPerRow = *B_pNnzPerRow.get();
	// Record stats of A, B
	// ------------------------------------------------------------------------

	auto startPos = 0;
	if ( mmfPathA.rfind( '/' ) != std::string::npos )
		startPos = mmfPathA.rfind( '/' ) + 1;
	auto endPos = mmfPathA.length();
	if ( mmfPathA.rfind( '.' ) != std::string::npos )
		endPos = mmfPathA.rfind( '.' );
	std::string nameA = mmfPathA.substr( startPos, endPos - startPos );
	std::string dirA = mmfPathA.substr( 0, startPos );

	if ( mmfPathB.rfind( '/' ) != std::string::npos )
		startPos = mmfPathB.rfind( '/' ) + 1;
	if ( mmfPathB.rfind( '.' ) != std::string::npos )
		endPos = mmfPathB.rfind( '.' );
	std::string nameB = mmfPathB.substr( startPos, endPos - startPos );
	std::string dirB = mmfPathB.substr( 0, startPos );

	std::stringstream sstats;
	std::stringstream sheader;

	double MB_A =
		A.MBs_CSR( sizeof( ind_t ), sizeof( fp_t ), sizeof( nnz_t ) );

	sstats << nameA << "," << A.rows() << "," << A.columns() << ","
	       << A.length() << "," << MB_A << "," << A_isSymmetric << ","
	       << ( (double) A.length() ) / ( A.rows() * A.columns() ) << ",";
    sheader << "A,rows,columns,nnz,MB (csr),sym,fill,";
	A.stats( sstats, sheader );


	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		double MB_B =
			B.MBs_CSR( sizeof( ind_t ), sizeof( fp_t ), sizeof( nnz_t ) );

		sstats << nameB << "," << B.rows() << "," << B.columns() << ","
		       << B.length() << "," << MB_B << "," << B_isSymmetric << ","
		       << ( (double) B.length() ) / ( B.rows() * B.columns() ) << ",";
		sheader << "A,rows,columns,nnz,MB (csr),sym,fill,";
		B.stats( sstats, sheader );
	}

	// Preprocess matrix C = A x B
	ui32 numThreads = 0;
	#pragma omp parallel
	#pragma omp master
	numThreads = omp_get_num_threads();

	fp_t** accumulators = nullptr;
	#pragma omp parallel num_threads( numThreads )
	{
		#pragma omp single
		{
			accumulators = util::alloc< fp_t* >( numThreads );
		}
		int tid = omp_get_thread_num();
		accumulators[ tid ] = util::newVector< fp_t >( A.rows() );
	}


	ui64 nnzC = 0;
    ui64 mults = 0;
    ui64 multiplicationCountPerThrd[ numThreads ];
    Triplet* tripletArrC =
	    util::preprocessC( A, B, numThreads, accumulators, nnzC, mults,
	                       multiplicationCountPerThrd );

    Triplets C( 0, 0, A.rows(), B.columns(), tripletArrC, tripletArrC + nnzC );
	std::shared_ptr< std::vector< glbInd_t > > C_pNnzPerRow = C.nnzPerRow();
	std::vector< glbInd_t >& C_nnzPerRow = *C_pNnzPerRow.get();
	double MB_C =
		C.MBs_CSR( sizeof( ind_t ), sizeof( fp_t ), sizeof( nnz_t ) );

	sstats << "C," << C.rows() << "," << C.columns() << ","
	       << C.length() << "," << MB_C << ","
	       << (double) mults / C.length() << ","
	       << (double) C.length() / A.length() << ","
	       << ( (double) C.length() ) / ( C.rows() * C.columns() ) << ",";
	sheader << "C,rows,columns,nnz,MB (csr),cfe,nnz(C)/nnz(A),fill,";
	C.stats( sstats, sheader );
	sstats << mults << ",";
	sheader << "# of multiplications,";

	std::shared_ptr< std::vector< ui64 > > pMultsPerRow(
		Triplets::calculateMultsPerRow( A, B ) );
	std::vector< ui64 >& multsPerRow = *pMultsPerRow.get();
	std::sort( multsPerRow.begin(), multsPerRow.end(),
			   [](int a, int b) {
				   return a < b;
			   } );
	std::stringstream scalefreePlotHeader;
	std::stringstream scalefreePlot;
	// Per row multiplication count statistics
    // (min, mean, geo-mean, std-dev, max)
	{
		ui64 total = multsPerRow[ 0 ];
		double avg = 0;
		double geoMean = multsPerRow[ 0 ] > 0 ? log10( multsPerRow[ 0 ] ) : 0;
		double stdDev = 0;
		double min = multsPerRow[ 0 ];
		double max = multsPerRow[ 0 ];
		double cv = 0;

		for ( std::size_t i = 1; i < multsPerRow.size(); ++i )
		{
			if ( min > multsPerRow[ i ] )
				min = multsPerRow[ i ];
			else if ( max < multsPerRow[ i ] )
				max = multsPerRow[ i ];

			total += multsPerRow[ i ];
			if ( multsPerRow[ i ] > 0 )
				geoMean += log10( (double) multsPerRow[ i ] );
		}
		avg = total / (double) multsPerRow.size();
		geoMean = pow( 10, geoMean / multsPerRow.size() );

		for ( glbInd_t i = 0; i < A.rows(); ++i )
			stdDev += pow( multsPerRow[ i ] - avg, 2 );
		stdDev = sqrt( stdDev / multsPerRow.size() );

		cv = stdDev / avg;

		sstats << min << "," << avg << ","
		       << geoMean << "," << stdDev << ","
			   << max << "," << cv << ",";
		sheader << "min,avg,geo,std-dev,max,cv,";

		// print scale percentage of rows for each %5 computation
		const double STEP = ( total + 19 ) / 20;
		double target = STEP;
		double curr = 0;
		scalefreePlot << "Scale-mults-per-row,";
		for ( std::size_t i = 0; i < multsPerRow.size(); ++i )
		{
			curr += multsPerRow[ i ];
			if ( curr >= target )
			{
				target += STEP;
				scalefreePlot <<  ( (double) i / multsPerRow.size() ) * 100 << ",";
			}
		}

		scalefreePlotHeader << "Scale-mults-per-row,5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,";
	}

	// Per C nnz multiplication count statistics
	// (min, mean, geo-mean, std-dev, max)
	std::shared_ptr< std::vector< glbInd_t > > pMultsPerCNonZero(
		Triplets::calculateMultsPerCNonZero( A, B, C ) );
	std::vector< glbInd_t >& multsPerCNonZero = *pMultsPerCNonZero.get();
	std::sort( multsPerCNonZero.begin(), multsPerCNonZero.end(),
			   [](int a, int b) {
				   return b > a;
			   } );
	{
		double total = multsPerCNonZero[ 0 ];
		double avg = 0;
		double geoMean =
			multsPerCNonZero[ 0 ] > 0 ? log10( multsPerCNonZero[ 0 ] ) : 0;
		double stdDev = 0;
		double min = multsPerCNonZero[ 0 ];
		double max = multsPerCNonZero[ 0 ];
		double cv = 0;


		for ( std::size_t i = 1; i < multsPerCNonZero.size(); ++i )
		{
			if ( min > multsPerCNonZero[ i ] )
				min = multsPerCNonZero[ i ];
			else if ( max < multsPerCNonZero[ i ] )
				max = multsPerCNonZero[ i ];

			total += multsPerCNonZero[ i ];
			if ( multsPerCNonZero[ i ] > 0 )
				geoMean += log10( (double) multsPerCNonZero[ i ] );
		}
		avg = total / multsPerCNonZero.size();
		geoMean = pow( 10, geoMean / multsPerCNonZero.size() );

		for ( glbInd_t i = 0; i < C.length(); ++i )
			stdDev += pow( multsPerCNonZero[ i ] - avg, 2 );
		stdDev = sqrt( stdDev / multsPerCNonZero.size() );

		cv = stdDev / avg;

		sstats << min << "," << avg << ","
		       << geoMean << "," << stdDev << ","
			   << max << "," << cv << ",";
		sheader << "min,avg,geo,std-dev,max,cv,";

		// print scale percentage of rows for each %5 computation
		const double STEP = ( total + 19 ) / 20;
		double target = STEP;
		double curr = 0;
		scalefreePlot << "Scale-mults-per-C-nz,";
		for ( std::size_t i = 0; i < multsPerCNonZero.size(); ++i )
		{
			curr += multsPerCNonZero[ i ];
			if ( curr >= target )
			{
				target += STEP;
				scalefreePlot <<  ( (double) i / multsPerCNonZero.size() ) * 100 << ",";
			}
		}

		scalefreePlotHeader << "Scale-mults-per-C-nz,5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,";
	}


	std::shared_ptr< std::vector< double > > pCfePerRow =
		Triplets::calculateCompressionRatioPerRow( multsPerRow, C );
	std::vector< double >& cfePerRow = *pCfePerRow.get();
	// Per row compression ratio statistics
	// (min, mean, geo-mean, std-dev, max)
	{
		double avg = cfePerRow[ 0 ];
		double geoMean = cfePerRow[ 0 ] > 0 ? log10( cfePerRow[ 0 ] ) : 0;
		double stdDev = 0;
		double min = cfePerRow[ 0 ];
		double max = cfePerRow[ 0 ];
		double cv = 0;

		for ( std::size_t i = 0; i < cfePerRow.size(); ++i )
		{
			if ( min > cfePerRow[ i ] )
				min = cfePerRow[ i ];
			else if ( max < cfePerRow[ i ] )
				max = cfePerRow[ i ];

			avg += cfePerRow[ i ];
			if ( cfePerRow[ i ] > 0 )
				geoMean += log10( cfePerRow[ i ] );
		}
		avg = avg / cfePerRow.size();
		geoMean = pow( 10, geoMean / cfePerRow.size() );

		for ( glbInd_t i = 0; i < A.rows(); ++i )
			stdDev += pow( cfePerRow[ i ] - avg, 2 );
		stdDev = sqrt( stdDev / cfePerRow.size() );

		cv = stdDev / avg;

		sstats << min << "," << avg << ","
		       << geoMean << "," << stdDev << ","
			   << max << "," << cv << ",";
		sheader << "min,avg,geo,std-dev,max,cv,";
	}

	// Print row and column nnz histogram for each matrix
	// -------------------------------------------------------------------------
	// if ( A.isSkewedRow() )
	sstats << "A,";
	sheader << "A,";
	util::printHistogramExponential( sstats, sheader, A_nnzPerRow );
	util::printHistogramExponential( sstats, sheader, *A.nnzPerCol().get() );
	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		sstats << "B,";
		sheader << "B,";
		util::printHistogramExponential( sstats, sheader, B_nnzPerRow );
		util::printHistogramExponential( sstats, sheader,
		                                 *B.nnzPerCol().get() );
	}

	// if ( C.isSkewedRow() )
	sstats << "C,";
	sheader << "C,";
	util::printHistogramExponential( sstats, sheader, C_nnzPerRow );
	util::printHistogramExponential( sstats, sheader, *C.nnzPerCol().get() );

	// Print multiplication count & compression ratio per row histogram
	// -------------------------------------------------------------------------
	sstats << "M,";
	sheader << "M,";
	util::printHistogramExponential( sstats, sheader, multsPerRow );
	sstats << "CR,";
	sheader << "CR,";
	util::printHistogramLinear( sstats, sheader, cfePerRow );

	// Scale free plot (on row basis)
	// ------------------------------------------------------------------------

	sheader << scalefreePlotHeader.str();
	sstats << scalefreePlot.str();

	// -------------------------------------------------------------------------
	std::cout << sheader.str() << std::endl;
	std::cout << sstats.str() << std::endl;

	// clean up
	// -------------------------------------------------------------------------

	for ( ui32 t = 0; t < numThreads; ++t )
		util::free( accumulators[ t ] );
	util::free( accumulators );

	delete [] tripletArrC;
	delete [] tripletArrA;
	delete [] tripletArrB;
	return EXIT_SUCCESS;
}
