/*
 * StaticRowByRow3p_v0.cpp
 *
 *  Created on: Sep 26, 2018
 *      Author: memoks
 */

#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <map>
#include <omp.h>
#include <set>
#include <cmath>
#include <unordered_set>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#endif

#include "include/sparse_types.h"
#include "include/util/Generic.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Csr.h"
#include "include/io/InputReader.h"
#include "include/io/MMInfo.h"
#include "include/io/Cli.h"
#include "include/logging/Timer.h"


int main( int argc, char* argv[] )
{
	Cli cli( argc, argv );

	// Parameters
	// ------------------------------------------------------------------------
	if ( !cli.isSet( { Cli::MMF_PATH_A, Cli::OUTPUT_DIR } ) )
	{
		return EXIT_FAILURE;
	}

	std::string mmfPathA = cli.getMmfPathA();
	std::string outDir = cli.getOutputDir();

	// Read matrix market format file.
	// ------------------------------------------------------------------------
	MMInfo A_mmInfo;
	int& A_isSymmetric = A_mmInfo.isSymmetric;
	int A_rowCount = 0;
	int A_colCount = 0;
	int A_nnz = 0;
	int* A_is = NULL;
	int* A_js = NULL;
	double* A_nzs = NULL;
	InputReader::readMMF( mmfPathA, A_mmInfo, &A_rowCount, &A_colCount,
	                      &A_nnz, &A_is, &A_js, &A_nzs);
	Triplet* tripletArrA = Triplet::array( A_nnz, A_is, A_js, A_nzs );
	free( A_is );
	free( A_js );
	free( A_nzs );

	Triplets _A( 0, 0, A_rowCount, A_colCount,
	             tripletArrA, tripletArrA + A_nnz );
	std::string nameA = mmfPathA.substr( mmfPathA.find_last_of( '/' ) + 1,
	                                     mmfPathA.find_last_of( '.' ) - 1 );

	_A.sortColMajor();
	std::shared_ptr< std::vector< glbNnz_t > > pA_colPtr( _A.colPtr() );
	std::vector< glbNnz_t >& A_colPtr = *pA_colPtr.get();

	std::vector< glbInd_t > scales = { 10, 12, 14, 16, 18 };	
	for ( auto it = scales.cbegin(); it != scales.cend(); ++it )
	{
		glbInd_t scale = *it;
		glbInd_t columnCount = pow( 2, scale );

		std::vector< glbInd_t > colPtr;
		colPtr.push_back( 0 );
		std::vector< glbInd_t > rowInds;

		for ( glbInd_t c = 0; c < columnCount; ++c )
		{
			glbInd_t r = rand() % _A.columns();
			glbInd_t nnz = A_colPtr[ r + 1 ] - A_colPtr[ r ];
			colPtr.push_back( nnz + colPtr.back() );

			for ( glbNnz_t n = A_colPtr[ r ]; n < A_colPtr[ r + 1 ]; ++n )
				rowInds.push_back( _A[ n ].i );
		}


		std::string path = nameA + "_" + std::to_string( scale ) + ".mtx";
		std::ofstream of( outDir + "/" + path );

		of << "%%MatrixMarket matrix coordinate pattern general" << std::endl;
		of << "% -------------------------------------------------------"
		     << std::endl;
		of << "% name                   : " << path << std::endl;
		of << "% rows                   : " << _A.columns() << std::endl;
		of << "% columns                : " << columnCount << std::endl;
		of << "% nnz                    : " << colPtr[ columnCount ]
		     << std::endl;
		of << "% -------------------------------------------------------"
		     << std::endl;

		of << _A.columns() << " "
		     << columnCount << " "
		     << colPtr[ columnCount ] << std::endl;

		for ( glbInd_t c = 0; c < columnCount; ++c )
		{
			for ( glbNnz_t n = colPtr[ c ]; n < colPtr[ c + 1 ]; ++n )
			{
				of << rowInds[ n ] << " " << c << std::endl;
			}
		}

		of.close();
	}

	std::cout << nameA << std::endl;

	delete [] tripletArrA;
	return EXIT_SUCCESS;
}

