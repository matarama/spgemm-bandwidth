
/**
 * Generates relatively simple matrices to measure constant factor 
 * overhead of spgemm routines. Generated matrices will have the 
 * same # of non-zeros per row but distributed to random columns.
 */

#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <sstream>
#include <memory>

#include "include/sparse_types.h"
#include "include/data_structure/Triplet.h"
#include "include/io/Cli.h"

// using glbInd_t = ui64;
// using glbNnz_t = ui64;
// using trpFp_t = float;

// using ind_t = ui64;
// using nnz_t = ui64;
// using fp_t = float;

int
main( int argc, char* argv[] )
{
	Cli cli( argc, argv );
	if ( !cli.isSet( { Cli::OUTPUT_DIR, Cli::MMF_PATH_A, Cli::ROWS,
					   Cli::COLUMNS, Cli::NNZ_PER_ROW } ) )
	{
		return EXIT_FAILURE;
	}

	std::string outputDir = cli.getOutputDir();
	std::string mmfPathA = cli.getMmfPathA();
	ui64 rows = cli.getRows();
	ui64 columns = cli.getColumns();
	ui64 nnzPerRow = cli.getNnzPerRow();
	ui64 denseRows = cli.getDenseRows();
	ui64 nnzPerRowDense = cli.getNnzPerRowDense();
	ui64 nnz = ( rows - denseRows ) * nnzPerRow +
		denseRows * nnzPerRowDense;
	cli.str( std::cout ) << std::endl;
	bool isMatrixDense = cli.isSet( Cli::DENSE_ROWS )
		&& cli.isSet( Cli::NNZ_PER_ROW_DENSE );
	std::stringstream stempPath;
	stempPath << outputDir << "/" << rows << "_" << nnzPerRow;
	if ( isMatrixDense )
		stempPath << "_" << denseRows << "_" << nnzPerRowDense << ".mtx";
	else
		stempPath << ".mtx";
	std::string tempPath = stempPath.str();

	std::unique_ptr< Triplet[] > tripletArr;
	if ( isMatrixDense )
		tripletArr = Triplet::generate( rows, columns, nnzPerRow,
		                                denseRows, nnzPerRowDense );
	else
		tripletArr = Triplet::generate( rows, columns, nnzPerRow );

	std::stringstream sheader;
	sheader << "%%MatrixMarket matrix coordinate pattern general" << std::endl
	        << "%-------------------------------------------------"
	        << "------------------------------" << std::endl
	        << "% Generated using SyntheticMatrixGenerator.cpp" << std::endl
	        << "% Parameters" << std::endl
	        << "% rows               : " << rows << std::endl
	        << "% columns            : " << columns << std::endl
	        << "% nnz                : " << nnz << std::endl
	        << "% nnzPerRow          : " << nnzPerRow << std::endl
            << "% denseRows          : " << denseRows << std::endl
	        << "% denseNnzPerRow     : " << nnzPerRowDense << std::endl
	        << "%-------------------------------------------------"
	        << "------------------------------" << std::endl;
	std::string header = sheader.str();

	Triplet::matrixMarketForm( tripletArr.get(), rows, columns, nnz,
	                           header, tempPath );

	// Copy to destination path
	// (since destination path is on arc, it would have been too slow to
	// write there directly)
	std::stringstream scommand;
	scommand << "mv " << tempPath << " "  << mmfPathA;
	system( scommand.str().c_str() );

	return EXIT_SUCCESS;
}
