
#include <cstdlib>
#include <cstdio>
#include <thread>
#include <chrono>
#include <iostream>

template < typename t > void job( const t sleepDur );
template < typename t > void wrapper( t const* const sleepDurs, const int numThreads );


int
main( int argc, char** argv )
{
	int numThreads = 12;
	int sleepDur[ numThreads ];
	for ( int i = 0; i < numThreads; ++i )
		sleepDur[ i ] = i;

	wrapper( sleepDur, numThreads );

	return EXIT_SUCCESS;
}


template < typename t >
void
job( const t sleepDur )
{
	std::cout << "thread " << std::this_thread::get_id()
			  << "will sleep for " << sleepDur << std::endl;

	std::this_thread::sleep_for(
		std::chrono::seconds( sleepDur ) );

	std::cout << "thread " << std::this_thread::get_id()
			  << " done." << std::endl;
}

template < typename t >
void
wrapper( t const* const sleepDurs, const int numThreads )
{
	std::thread thrds[ numThreads ];
	for ( int i = 0; i < numThreads; ++i )
        thrds[ i ] = std::thread( job< int >, sleepDurs[ i ] );

	for ( int i = 0; i < numThreads; ++i )
		thrds[ i ].join();
}
