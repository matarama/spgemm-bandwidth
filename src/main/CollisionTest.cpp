/*
 * CollisionTest.cpp
 *
 *  Created on: Aug 26, 2017
 *      Author: memoks
 */

#include <cassert>
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <set>
#include <omp.h>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#endif

#include "include/sparse_types.h"
#include "include/util/Generic.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Csr.h"
#include "include/io/InputReader.h"
#include "include/io/MMInfo.h"
#include "include/io/Cli.h"
#include "include/logging/Timer.h"
#include "include/parallel/Gustavson.h"

void
countCollisions( const ui64& accLength,
                 const Csr< glbInd_t, fp_t, glbNnz_t >& C,
                 const ui32& numThreads,
                 const std::vector< ui64 >& multsPerRow,
	             glbInd_t const* const thrdRowPfxSum,
                 ui64& rowCollisionCount,
                 ui64& elementCollisionCount,
                 ui64& weightedRowCollisionCount );

void
countCollisionsShamt( const ui64& accLength,
                      const Csr< glbInd_t, fp_t, glbNnz_t >& C,
                      const ui32& numThreads,
                      const std::vector< ui64 >& multsPerRow,
                      ui64& rowCollisionCount,
                      ui64& elementCollisionCount,
                      ui64& weightedRowCollisionCount );

// #############################################################################

int main( int argc, char* argv[] )
{
	Cli cli( argc, argv );

	// struct phf myphf;
	// ui32 keyCount = 10;
	// ui32 lambda = 2;
	// ui32 alpha = 100;
	// phf_seed_t seed = 0;
	// ui32 keys[] = { 4, 67, 2, 7, 9, 12, 1, 40, 21, 31 };
	// PHF::init< ui32, false >( &myphf, &keys[0], keyCount, lambda, alpha, seed );

	// std::cout << "phf-test: " << std::endl;
	// for ( ui32 i = 0; i < keyCount; ++i )
	// {
	// 	std::cout << keys[ i ] << " -> " << PHF::hash( &myphf, keys[ i ] ) << std::endl;
	// }
	// std::cout << std::endl;

	// Parameters
	// ------------------------------------------------------------------------
	if ( !cli.isSet( { Cli::MMF_PATH_A, Cli::NUM_BLOCKS, Cli::SHAMT } ) )
	{
		return EXIT_FAILURE;
	}

	ui32 iterations = cli.getIterations();
	std::string mmfPathA = cli.getMmfPathA();
	std::string mmfPathB = cli.getMmfPathB();
	ui32 numThreads = cli.getNumBlocks();
	bool transposeB = cli.getTransposeB();
	bool shamt = cli.getShamt();
	
	omp_set_num_threads( numThreads );
#ifdef __INTEL_COMPILER
	mkl_set_num_threads( numThreads );
#endif

	// Read matrix market format file.
	// ------------------------------------------------------------------------
	MMInfo A_mmInfo;
	int& A_isSymmetric = A_mmInfo.isSymmetric;
	int A_rowCount = 0;
	int A_colCount = 0;
	int A_nnz = 0;
	int* A_is = NULL;
	int* A_js = NULL;
	double* A_nzs = NULL;
	InputReader::readMMF( mmfPathA, A_mmInfo, &A_rowCount, &A_colCount,
	                      &A_nnz, &A_is, &A_js, &A_nzs);
	Triplet* tripletArrA = Triplet::array( A_nnz, A_is, A_js, A_nzs );
	Triplets _A( 0, 0, A_rowCount, A_colCount,
	             tripletArrA, tripletArrA + A_nnz );
	std::cout << mmfPathA.substr( mmfPathA.find_last_of( '/' ) + 1,
	                              mmfPathA.find_last_of( '.' ) - 1 ) << ",";

	MMInfo B_mmInfo;
	int B_isSymmetric = 0;
	int B_rowCount = 0;
	int B_colCount = 0;
	int B_nnz = 0;
	int* B_is = NULL;
	int* B_js = NULL;
	double* B_nzs = NULL;

	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		InputReader::readMMF( mmfPathB, B_mmInfo, &B_rowCount, &B_colCount,
		                      &B_nnz, &B_is, &B_js, &B_nzs);
		B_isSymmetric = B_mmInfo.isSymmetric;
	}
	else
	{
		mmfPathB = mmfPathA;
		B_isSymmetric = A_isSymmetric;
		B_rowCount = A_rowCount;
		B_colCount = A_colCount;
		B_nnz = A_nnz;
		B_is = A_is;
		B_js = A_js;
		B_nzs = A_nzs;
	}

	Triplet* tripletArrB = Triplet::array( B_nnz, B_is, B_js, B_nzs );
	Triplets _B = Triplets( 0, 0, B_rowCount, B_colCount,
	                        tripletArrB, tripletArrB + B_nnz );

	if ( transposeB )
		_B.transpose();

	// clean up
	free( A_is );
	free( A_js );
	free( A_nzs );	
	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		free( B_is );
		free( B_js );
		free( B_nzs );
	}

	// Permute A and B matrices
	if ( cli.getPermuteAB() )
		Triplets::permuteRandom( _A, _B );

	// -------------------------------------------------------------------------
	glbInd_t* thrdRowPfxSum = nullptr;
	Csr< glbInd_t, fp_t, glbNnz_t > C;
	{
		util::partition( _A, _B, &thrdRowPfxSum, numThreads );

		fp_t** accumulators = nullptr;
		#pragma omp parallel num_threads( numThreads )
		{
			#pragma omp single
			accumulators = util::alloc< fp_t* >( numThreads );

			const ui32 ID = omp_get_thread_num();
			accumulators[ ID ] = util::newVector< fp_t >( _B.columns() );
		}

		Csr< glbInd_t, fp_t, glbNnz_t > A( _A );
		Csr< glbInd_t, fp_t, glbNnz_t > B( _B );
		rbr::gustavson::symbolic_stl_alloc( A, B, C, accumulators,
		                                    thrdRowPfxSum, numThreads );

		for ( ui32 t = 0; t < numThreads; ++t )
			util::free( accumulators[ t ] );
		util::free( accumulators );
	}

	// ------------------------------------------------------------------------

	{
		std::shared_ptr< std::vector< ui64 > > pMultsPerRow(
			Triplets::calculateMultsPerRow( _A, _B ) );
		std::vector< ui64 >& multsPerRow = *pMultsPerRow.get();
	
		if ( !shamt )
		{
			for ( ui64 accLength = 1024; accLength <= 262144; accLength *= 2)
			{
				ui64 rowCollisionCount = 0;
				ui64 elementCollisionCount = 0;
				ui64 weightedRowCollisionCount = 0;
				countCollisions( accLength, C, numThreads, multsPerRow,
				                 thrdRowPfxSum,
				                 rowCollisionCount,
				                 elementCollisionCount,
				                 weightedRowCollisionCount );

				std::cout << rowCollisionCount << ",";
				std::cout << elementCollisionCount << ",";
				std::cout << weightedRowCollisionCount << ",";
			}

		}
		else
		{
			for ( ui64 accLength = 1024; accLength <= 262144; accLength *= 2)
			{
				ui64 rowCollisionCount = 0;
				ui64 elementCollisionCount = 0;
				ui64 weightedRowCollisionCount = 0;
				countCollisionsShamt( accLength, C, numThreads, multsPerRow,
				                      rowCollisionCount,
				                      elementCollisionCount,
				                      weightedRowCollisionCount );

				std::cout << rowCollisionCount << ",";
				std::cout << elementCollisionCount << ",";
				std::cout << weightedRowCollisionCount << ",";
			}
		}
	}

	std::cout << std::endl;

	util::free( thrdRowPfxSum );
	delete [] tripletArrA;
	delete [] tripletArrB;
	return EXIT_SUCCESS;
}

void
countCollisions( const ui64& accLength,
                 const Csr< glbInd_t, fp_t, glbNnz_t >& C,
                 const ui32& numThreads,
                 const std::vector< ui64 >& multsPerRow,
                 glbInd_t const* const thrdRowPfxSum,
                 ui64& rowCollisionCount,
                 ui64& elementCollisionCount,
                 ui64& weightedRowCollisionCount )
{
	assert( accLength % 2 == 0 );
	const glbInd_t mask = accLength - 1;
	const glbInd_t* C_rowPtr = C.rowPtr;
	const glbInd_t* C_colInd = C.colInd;
	rowCollisionCount = 0;
	elementCollisionCount = 0;
	weightedRowCollisionCount = 0;

	ui64 rccArr[ numThreads ];
	ui64 eccArr[ numThreads ];
	ui64 wrccArr[ numThreads ];
	#pragma omp parallel num_threads( numThreads )
	{
		ui32 tid = omp_get_thread_num();
		glbInd_t tRowStart = thrdRowPfxSum[ tid ];
		glbInd_t tRowEnd = thrdRowPfxSum[ tid + 1 ];
		ui64 tRowCollisionCount = 0;
		ui64 tElementCollisionCount = 0;
		ui64 tWeightedRowCollisionCount = 0;

		for ( glbInd_t i = tRowStart; i < tRowEnd; ++i )
		{
			// std::cout << std::endl << "row-" << i << ": ";

			std::set< glbInd_t > colIndSet;
			bool hasCollision = false;
			for ( glbNnz_t n = C_rowPtr[ i ]; n < C_rowPtr[ i + 1 ]; ++n )
			{
				glbInd_t j = C_colInd[ n ];
				if ( colIndSet.find( j & mask ) == colIndSet.end() )
				{
					colIndSet.insert( j & mask );
					// std::cout << j << " ";
				}
				else
				{
					++tElementCollisionCount;
					hasCollision = true;
					// std::cout << j << "! ";
				}
			}

			if ( hasCollision )
			{
				++tRowCollisionCount;
				tWeightedRowCollisionCount += multsPerRow[ i ];
			}
                
		}
		// std::cout << std::endl;

		rccArr[ tid ] = tRowCollisionCount;
		eccArr[ tid ] = tElementCollisionCount;
		wrccArr[ tid ] = tWeightedRowCollisionCount;

		#pragma omp barrier
		#pragma omp single
		{
			for ( ui32 i = 0; i < numThreads; ++i )
			{
				rowCollisionCount += rccArr[ i ];
				elementCollisionCount += eccArr[ i ];
				weightedRowCollisionCount += wrccArr[ i ];
			}
		}
	}
}

void
countCollisionsShamt( const ui64& accLength,
                      const Csr< glbInd_t, fp_t, glbNnz_t >& C,
                      const ui32& numThreads,
                      const std::vector< ui64 >& multsPerRow,
                      ui64& rowCollisionCount,
                      ui64& elementCollisionCount,
                      ui64& weightedRowCollisionCount )
{
	assert( accLength % 2 == 0 );
	const glbInd_t mask = accLength - 1;
	const glbInd_t* C_rowPtr = C.rowPtr;
	const glbInd_t* C_colInd = C.colInd;
	rowCollisionCount = 0;
	elementCollisionCount = 0;
	weightedRowCollisionCount = 0;

	std::vector< glbInd_t > threadRowPfxSum( numThreads + 1, 0 );
	glbInd_t rowsPerThread = C.rowCount / numThreads;
	for ( ui32 i = 0; i < numThreads; ++i )
		threadRowPfxSum[ i + 1 ] = threadRowPfxSum[ i ] + rowsPerThread;
	threadRowPfxSum[ numThreads ] = C.rowCount;

	ui64 rccArr[ numThreads ];
	ui64 eccArr[ numThreads ];
	ui64 wrccArr[ numThreads ];
	#pragma omp parallel num_threads( numThreads )
	{
		ui32 tid = omp_get_thread_num();
		glbInd_t tRowStart = threadRowPfxSum[ tid ];
		glbInd_t tRowEnd = threadRowPfxSum[ tid + 1 ];
		ui64 tRowCollisionCount = 0;
		ui64 tElementCollisionCount = 0;
		ui64 tWeightedRowCollisionCount = 0;

		for ( glbInd_t i = tRowStart; i < tRowEnd; ++i )
		{
			// std::cout << std::endl << "row-" << i << ": ";

			bool hasCollision = false;
			ui64 currRowElementCollisionCount = 0;
			std::set< glbInd_t > colIndSet;
			for ( glbNnz_t n = C_rowPtr[ i ]; n < C_rowPtr[ i + 1 ]; ++n )
			{
				glbInd_t j = C_colInd[ n ];
				if ( colIndSet.find( j & mask ) == colIndSet.end() )
				{
					colIndSet.insert( j & mask );
					// std::cout << j << " ";
				}
				else
				{
					++currRowElementCollisionCount;
					hasCollision = true;
					// std::cout << j << "! ";
				}
			}

			// std::cout << std::endl;

			// try to remove collisions with shamt
			if ( hasCollision )
			{
				ui32 pow = 0;
				ui32 temp = accLength;
				while ( temp >= 2 )
				{
					++pow;
					temp /= 2;
				}
			
				ui32 shamt = 1;
				ui32 limit = sizeof( glbInd_t ) * 8 - pow;
				// std::cout << "limit:" << limit << std::endl;
				glbInd_t newMask = mask * 2;
				bool stillHasCollision = true;
				if ( C_rowPtr[ i + 1 ] - C_rowPtr[ i ] <= accLength )
				{
					while ( shamt < limit && stillHasCollision )
					{
						// std::cout << "shamt=" << shamt
						//           << " mask=" << newMask << std::endl;
						std::set< glbInd_t > colIndSet;
						stillHasCollision = false;
						for ( glbNnz_t n = C_rowPtr[ i ];
						      n < C_rowPtr[ i + 1 ];
						      ++n )
						{
							glbInd_t j = C_colInd[ n ];
							// std::cout << "-> " << (j & newMask) << std::endl;
							if ( colIndSet.find( j & newMask ) == colIndSet.end() )
							{
								colIndSet.insert( j & newMask );
							}
							else
							{
								newMask = newMask * 2;
								++shamt;
								stillHasCollision = true;
								break;
							}
						}
					}
				}
				
                
				if ( stillHasCollision )
				{
					++tRowCollisionCount;
					tWeightedRowCollisionCount += multsPerRow[ i ];
					tElementCollisionCount += currRowElementCollisionCount;
				}
			}
		}
		// std::cout << std::endl;

		rccArr[ tid ] = tRowCollisionCount;
		eccArr[ tid ] = tElementCollisionCount;
		wrccArr[ tid ] = tWeightedRowCollisionCount;

		#pragma omp barrier
		#pragma omp single
		{
			for ( ui32 i = 0; i < numThreads; ++i )
			{
				rowCollisionCount += rccArr[ i ];
				elementCollisionCount += eccArr[ i ];
				weightedRowCollisionCount += wrccArr[ i ];
			}
		}
	}
}
