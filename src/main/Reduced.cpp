#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <map>
#include <omp.h>
#include <set>
#include <unordered_set>

#ifdef __INTEL_COMPILER
#include <mkl.h>
#endif

#include "include/sparse_types.h"
#include "include/util/Generic.h"
#include "include/util/Memory.h"
#include "include/util/Spgemm.h"
#include "include/data_structure/Triplet.h"
#include "include/storage_format/Csr.h"
#include "include/io/InputReader.h"
#include "include/io/MMInfo.h"
#include "include/io/Cli.h"
#include "include/logging/Timer.h"
#include "include/parallel/Gustavson.h"
#include "include/parallel/RowByRowReduced_v0.h"


template < typename ind_t, typename fp_t, typename nnz_t,
           typename target_mem, typename correct_mem >
void
runTests( Triplets& _A, Triplets& _B, fp_t** accumulators,
          ind_t const* const thrdRowPfxSum, const ui32& numThreads,
          Csr< ind_t, fp_t, nnz_t, correct_mem >& correct,
          const ui32& iterations );


int main( int argc, char* argv[] )
{
	Cli cli( argc, argv );

	// Parameters
	// ------------------------------------------------------------------------
	if ( !cli.isSet( { Cli::MMF_PATH_A, Cli::NUM_BLOCKS } ) )
	{
		return EXIT_FAILURE;
	}

	ui32 iterations = cli.getIterations();
	std::string mmfPathA = cli.getMmfPathA();
	std::string mmfPathB = cli.getMmfPathB();
	ui32 numThreads = cli.getNumBlocks();
	bool transposeB = cli.getTransposeB();
	std::vector< ui32 > vAccSizes = cli.getAccSizes();

	omp_set_num_threads( numThreads );

	// Read matrix market format file.
	// ------------------------------------------------------------------------
	MMInfo A_mmInfo;
	int& A_isSymmetric = A_mmInfo.isSymmetric;
	int A_rowCount = 0;
	int A_colCount = 0;
	int A_nnz = 0;
	int* A_is = NULL;
	int* A_js = NULL;
	double* A_nzs = NULL;
	InputReader::readMMF( mmfPathA, A_mmInfo, &A_rowCount, &A_colCount,
	                      &A_nnz, &A_is, &A_js, &A_nzs);
	Triplet* tripletArrA = Triplet::array( A_nnz, A_is, A_js, A_nzs );
	Triplets _A( 0, 0, A_rowCount, A_colCount,
	             tripletArrA, tripletArrA + A_nnz );
	std::cout << mmfPathA.substr( mmfPathA.find_last_of( '/' ) + 1,
	                              mmfPathA.find_last_of( '.' ) - 1 ) << ",";

	MMInfo B_mmInfo;
	int B_isSymmetric = 0;
	int B_rowCount = 0;
	int B_colCount = 0;
	int B_nnz = 0;
	int* B_is = NULL;
	int* B_js = NULL;
	double* B_nzs = NULL;

	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		InputReader::readMMF( mmfPathB, B_mmInfo, &B_rowCount, &B_colCount,
		                      &B_nnz, &B_is, &B_js, &B_nzs);
		B_isSymmetric = B_mmInfo.isSymmetric;
	}
	else
	{
		mmfPathB = mmfPathA;
		B_isSymmetric = A_isSymmetric;
		B_rowCount = A_rowCount;
		B_colCount = A_colCount;
		B_nnz = A_nnz;
		B_is = A_is;
		B_js = A_js;
		B_nzs = A_nzs;
	}

	Triplet* tripletArrB = Triplet::array( B_nnz, B_is, B_js, B_nzs );
	Triplets _B = Triplets( 0, 0, B_rowCount, B_colCount,
	                        tripletArrB, tripletArrB + B_nnz );

	if ( transposeB )
		_B.transpose();

	// clean up
	free( A_is );
	free( A_js );
	free( A_nzs );	
	if ( cli.isSet( Cli::MMF_PATH_B ) )
	{
		free( B_is );
		free( B_js );
		free( B_nzs );
	}

	// Permute A and B matrices
	if ( cli.getPermuteAB() )
		Triplets::permuteRandom( _A, _B );

	glbInd_t* thrdRowPfxSum = nullptr;
	std::vector< ui64 > multPfxSumPerThrd;
	util::partition( _A, _B, &thrdRowPfxSum, multPfxSumPerThrd, numThreads );

	// Allocate & initialize accumulators
	fp_t** accumulators = nullptr;
	#pragma omp parallel num_threads( numThreads )
	{
		#pragma omp single
		{
			accumulators = util::alloc< fp_t* >( numThreads );
		}
		int tid = omp_get_thread_num();
		accumulators[ tid ] = util::newVector< fp_t >( _B.columns() );
	}

	// Run Gustavson's algorithm for correctness check
	Csr< glbInd_t, fp_t, glbNnz_t, allocator::Aligned > A_ddr4( _A );
	Csr< glbInd_t, fp_t, glbNnz_t, allocator::Aligned > B_ddr4( _B );
	Csr< glbInd_t, fp_t, glbNnz_t, allocator::Aligned > gustavson;
	rbr::gustavson::symbolic_stl_alloc(
		A_ddr4, B_ddr4, gustavson, accumulators, thrdRowPfxSum, numThreads );
	rbr::gustavson::numeric(
		A_ddr4, B_ddr4, gustavson, accumulators, thrdRowPfxSum, numThreads );

	// ddr4
	RECORD( "ddr4" );
	runTests< glbInd_t, fp_t, glbNnz_t, allocator::Aligned, allocator::Aligned >(
		_A, _B, accumulators, thrdRowPfxSum, numThreads, gustavson, iterations );

#ifdef WITH_HBW_MEMORY
	// mcdram
	RECORD( "mcdram");
	runTests< glbInd_t, fp_t, glbNnz_t, allocator::Aligned, allocator::Mcdram >(
		_A, _B, accumulators, thrdRowPfxSum, numThreads, gustavson, iterations );
#endif /* WITH_HBW_MEMORY */

	// clean up
	for ( ui32 t = 0; t < numThreads; ++t )
	{
		util::free( accumulators[ t ] );
	}
	util::free( accumulators );

	delete [] tripletArrA;
	delete [] tripletArrB;
	return EXIT_SUCCESS;
}

template < typename ind_t, typename fp_t, typename nnz_t,
           typename target_mem, typename correct_mem >
void
runTests( Triplets& _A, Triplets& _B, fp_t** accumulators,
          ind_t const* const thrdRowPfxSum, const ui32& numThreads,
          Csr< ind_t, fp_t, nnz_t, correct_mem >& correct,
          const ui32& iterations )
{
	Csr< glbInd_t, fp_t, glbNnz_t, target_mem > A( _A );
	Csr< glbInd_t, fp_t, glbNnz_t, target_mem > B( _B );
	fp_t* D = (fp_t*)
		target_mem::alloc( B.columnCount, sizeof( fp_t ) );
	for ( ind_t i = 0; i < B.columnCount; ++i )
		D[ i ] = 1;

	Csr< glbInd_t, fp_t, glbNnz_t, target_mem > C_prime;
	rbr_reduced::v0::symbolic_alloc(
		A, B, C_prime, accumulators, thrdRowPfxSum, numThreads );

	RECORD( ( util::isSame( correct.colInd, C_prime.colInd,
	                        correct.nnz ) ? " " : "!" ) );
	RECORD( ( util::isSame( correct.nzs, C_prime.nzs,
	                        correct.nnz ) ? " " : "!" ) );

	Csr< glbInd_t, fp_t, glbNnz_t, target_mem > C;
	C.copy( C_prime, thrdRowPfxSum, numThreads );

	nnz_t workPfxSumPerThrd[ numThreads + 1 ];
	workPfxSumPerThrd[ 0 ] = 0;
	const nnz_t WORK = ( C_prime.nnz + numThreads - 1 ) / numThreads;
	const nnz_t LEFT_OVER = C_prime.nnz - WORK * numThreads;
	for ( ui32 t = 0; t < numThreads; ++t )
		if ( t < LEFT_OVER )
			workPfxSumPerThrd[ t + 1 ] = WORK + 1 + workPfxSumPerThrd[ t ];
		else
			workPfxSumPerThrd[ t + 1 ] = WORK + workPfxSumPerThrd[ t ];

	RRUN( iterations,
	      rbr_reduced::v0::symbolic(
		      A, B, C_prime, accumulators, thrdRowPfxSum, numThreads ); );

	RRUN( iterations,
	      rbr_reduced::v0::numeric(
		      C_prime, C, D, workPfxSumPerThrd, numThreads ); );

	RECORD( ( util::isSame( correct.nzs, C.nzs,
	                        correct.nnz ) ? " " : "!" ) );

	target_mem::free( D, B.columnCount, sizeof( fp_t ) );
}
