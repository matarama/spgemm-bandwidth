/*
 * Triplet.cpp
 *
 *  Created on: Dec 20, 2016
 *      Author: memoks
 */

#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <memory>
#include <omp.h>
#include <set>

#include "include/data_structure/Triplet.h"

// Triplet
// ##########################################################################

// --------------------------------------------------------------------------
Triplet::Triplet()
: Triplet( 0, 0, 0 )
{
}

Triplet::Triplet( glbInd_t i, glbInd_t j, fp_t nz )
: nz( nz ), i( i ), j( j )
{
}

Triplet::Triplet( const Triplet& other )
: nz( other.nz ), i( other.i ), j( other.j )
{
}

Triplet::~Triplet()
{
}

Triplet&
Triplet::operator =( const Triplet& other )
{
	if ( this != &other )
	{
		clear();
		copy(other);
	}

	return *this;
}

void
Triplet::clear()
{
	nz = 0.0;
	i = 0;
	j = 0;
}

void
Triplet::copy( const Triplet& other )
{
	nz = other.nz;
	i = other.i;
	j = other.j;
}

// --------------------------------------------------------------------------

bool
Triplet::operator ==( const Triplet& other ) const
{
	return nz == other.nz && i == other.i && j == other.j;
}

// Comparison functions
// --------------------------------------------------------------------------

bool
Triplet::cmpRowMajor( const Triplet& l, const Triplet& r )
{
	if ( l.i < r.i )
		return true;
	else if ( l.i > r.i )
		return false;
	else
	{
		if ( l.j < r.j )
			return true;
		else // if ( l.j2 > r.j2 )
			return false;
	}
}

bool
Triplet::cmpColMajor( const Triplet& l, const Triplet& r )
{
	if ( l.j < r.j )
		return true;
	else if ( l.j > r.j )
		return false;
	else
	{
		if ( l.i < r.i )
			return true;
		else // if ( l.i2 > r.i2 )
			return false;
	}
}

// --------------------------------------------------------------------------

Triplet*
Triplet::array( const int& nnz, const int* const is, const int* const js,
                const double* const nzs )
{
	Triplet* triplets = new Triplet[ nnz ];
	for ( int k = 0; k < nnz; ++k )
		triplets[ k ] = Triplet( is[ k ], js[ k ], nzs[ k ] );

	return triplets;
}

std::unique_ptr< Triplet[] >
Triplet::generate( const ui64& rowCount, const ui64& colCount,
                   const ui64& nnzPerRow )
{
	std::unique_ptr< Triplet[] > tripArr(
		new Triplet[ rowCount * nnzPerRow ] );

	for ( ui64 i = 0; i < rowCount; ++i )
	{
		std::set< glbInd_t > indices;
		for ( ui64 n = 0; n < nnzPerRow; ++n )
		{
			ui64 index = i * nnzPerRow + n;
			glbInd_t j = rand() % colCount;
			while ( indices.find( j ) != indices.end() )
				j = rand() % colCount;

			indices.insert( j );
			tripArr[ index ] = Triplet( i, j, 1 );
		}
	}

	return tripArr;
}

std::unique_ptr< Triplet[] >
Triplet::generate( const ui64& rowCount, const ui64& colCount,
                   const ui64& nnzPerRow,
                   const ui64& denseRowCount, const ui64& nnzPerRowDense )
{
	ui64 totalNnz = ( rowCount - denseRowCount ) * nnzPerRow +
		denseRowCount * nnzPerRowDense;
	std::unique_ptr< Triplet[] > tripArr( new Triplet[ totalNnz ] );

	ui64 index = 0;
	ui64 denseRowRatio = rowCount / denseRowCount;
	for ( ui64 i = 0; i < rowCount; ++i )
	{
		ui64 limit = nnzPerRow;
		if ( i % denseRowRatio == 0 && i != 0 )
			limit = nnzPerRowDense;

		std::set< glbInd_t > indices;
		for ( ui64 n = 0; n < limit; ++n, ++index )
		{
			glbInd_t j = rand() % colCount;
			while ( indices.find( j ) != indices.end() )
				j = rand() % colCount;

			indices.insert( j );
			tripArr[ index ] = Triplet( i, j, 1 );
		}
	}

	return tripArr;
}

void
Triplet::matrixMarketForm(
	Triplet* trpArr,
	const ui64& rows, const ui64& columns, const ui64& nnz,
	const std::string& header, const std::string& mmfPath )
{
	std::ofstream ofs( mmfPath );
	ofs << header << std::endl;
	ofs << rows << " " << columns << " " << nnz << std::endl;

	// convert to 1 based indexing
	for ( ui64 i = 0; i < nnz; ++i )
		ofs << trpArr[ i ].i + 1 << " "
		    << trpArr[ i ].j + 1 << std::endl;

	ofs.close();
}

// --------------------------------------------------------------------------

std::ostream&
Triplet::str( std::ostream& out ) const
{
	return out << "(" << i << ", " << j << ", " << nz << ")";
}


std::string
Triplet::str() const
{
	std::stringstream ss;
	str( ss );
	return ss.str();
}


// Triplets
// ##########################################################################

// --------------------------------------------------------------------------
Triplets::Triplets()
: Triplets( 0, 0, 0, 0 )
{
}

Triplets::Triplets( glbInd_t iStart, glbInd_t jStart,
					glbInd_t iLength, glbInd_t jLength )
: Triplets( iStart, jStart, iLength, jLength, nullptr, nullptr )
{
}

Triplets::Triplets( glbInd_t iStart, glbInd_t jStart,
					glbInd_t iLength, glbInd_t jLength,
					Triplet* begin, Triplet* end )
: _begin( begin ), _end( end ),
  _iStart( iStart ), _jStart( jStart ),
  _iLength( iLength ), _jLength( jLength )
{
}

Triplets::Triplets( const Triplets& other )
: Triplets()
{
	copy( other );
}

Triplets::~Triplets()
{
	clear();
}

Triplets&
Triplets::operator =( const Triplets& other )
{
	if ( this != &other )
	{
		clear();
		copy( other );
	}

	return *this;
}

void
Triplets::clear()
{
	_iStart = 0;
	_jStart = 0;
	_iLength = 0;
	_jLength = 0;
	_begin = nullptr;
	_end = nullptr;
}

void
Triplets::copy( const Triplets& other )
{
	_iStart = other._iStart;
	_jStart = other._jStart;
	_iLength = other._iLength;
	_jLength = other._jLength;
	_begin = other._begin;
	_end = other._end;
}

// --------------------------------------------------------------------------

Triplet&
Triplets::operator []( std::size_t i )
{
	return _begin[ i ];
}


const Triplet&
Triplets::operator []( std::size_t i ) const
{
	return _begin[ i ];
}

Triplet&
Triplets::back()
{
	return _begin[ length() - 1 ];
}

const Triplet&
Triplets::back() const
{
	return _begin[ length() - 1 ];
}

// --------------------------------------------------------------------------

void
Triplets::addToI( long i )
{
	for ( std::size_t k = 0; k < length(); ++k )
		_begin[ k ].i += i;
}

void
Triplets::addToJ( long j )
{
	for ( std::size_t k = 0; k < length(); ++k )
		_begin[ k ].j += j;
}

void
Triplets::addToIJ( long i, long j )
{
	addToI( i );
	addToJ( j );
}

void
Triplets::remapI( const std::vector< glbInd_t >& iVec )
{
	assert( iVec.size() == _iLength );

	for ( std::size_t k = 0; k < length(); ++k )
		_begin[ k ].i = iVec[ _begin[ k ].i ];
}

void
Triplets::remapJ( const std::vector< glbInd_t >& jVec )
{
	assert( jVec.size() == _jLength );

	for ( std::size_t k = 0; k < length(); ++k )
		_begin[ k ].j = jVec[ _begin[ k ].j ];
}

void
Triplets::remapIJ( const std::vector< glbInd_t >& iVec,
				   const std::vector< glbInd_t >& jVec )
{
	remapI( iVec );
	remapJ( jVec );
}

void
Triplets::transpose()
{
	std::swap( _iLength, _jLength );
	std::swap( _iStart, _jStart );

	for ( std::size_t t = 0; t < length(); ++t )
	{
		Triplet& triplet = operator []( t );
		std::swap( triplet.i, triplet.j );
	}
}

void
Triplets::permuteRandom()
{
	// generate row & column order
	std::vector< glbInd_t > rowMap;
	std::vector< glbInd_t > columnMap;
	for ( glbInd_t i = 0; i < rows(); ++i )
		rowMap.push_back( i );
	for ( glbInd_t j = 0; j < columns(); ++j )
		columnMap.push_back( j );

	for ( glbInd_t i = 0; i < rows(); ++i )
	{
		glbInd_t newIndex = ( rand() % ( rows() - i ) ) + i;
		std::swap( rowMap[ i ], rowMap[ newIndex ] );
	}

	for ( glbInd_t j = 0; j < columns(); ++j )
	{
		glbInd_t newIndex = ( rand() % ( columns() - j ) ) + j;
		std::swap( columnMap[ j ], columnMap[ newIndex ] );
	}

	// reorder
	remapIJ( rowMap, columnMap );
}

void
Triplets::updateRowColumnIndices( Triplets& A, Triplets& B,
                                  const std::vector< int >& A_rowOrder,
                                  const std::vector< int >& B_rowOrder )
{
	// update A's row and column indices
	for ( std::size_t n = 0; n < A.length(); ++n )
	{
		A[ n ].i = A_rowOrder[ A[ n ].i ];
		A[ n ].j = B_rowOrder[ A[ n ].j ];
	}

	// update B's row indices
	for ( std::size_t n = 0; n < B.length(); ++n )
		B[ n ].i = B_rowOrder[ B[ n ].i ];
}

bool
Triplets::isSkewedRow() const
{
	std::shared_ptr< std::vector < glbInd_t > > pnnzPer = nnzPerRow();
	std::vector< glbInd_t >& nnzPer = *pnnzPer.get();

	double avg = length() / (double) rows();
	double max = nnzPer[ 0 ];
	for ( std::size_t i = 1; i < nnzPer.size(); ++i )
	{
		if ( nnzPer[ i ] > max )
			max = nnzPer[ i ];
	}

	return max >= avg * 50;
}

bool
Triplets::isSkewedColumn() const
{
	std::shared_ptr< std::vector < glbInd_t > > pnnzPer = nnzPerCol();
	std::vector< glbInd_t >& nnzPer = *pnnzPer.get();

	double avg = length() / (double) rows();
	double max = nnzPer[ 0 ];
	for ( std::size_t i = 1; i < nnzPer.size(); ++i )
	{
		if ( nnzPer[ i ] > max )
			max = nnzPer[ i ];
	}

	return max >= avg * 50;
}

// --------------------------------------------------------------------------

void
Triplets::sortRowMajorStd()
{
	if ( !isSortedRowMajor() )
		std::sort( _begin, _end, Triplet::cmpRowMajor );
}

void
Triplets::sortRowMajor( ui32 numThreads )
{
	if ( isSortedRowMajor() )
		return;

	// Generate rowPtr to use in counting sort
	std::vector< ui64 > rowPtr( rows() + 1, 0 );
	Triplet* ptr = _begin;
	for ( ui64 n = 0; n < length(); ++n, ++ptr )
		++rowPtr[ ptr->i + 1 ];

	for ( std::size_t i = 0; i < rows(); ++i )
		rowPtr[ i + 1 ] += rowPtr[ i ];

	std::vector< ui64 > rowPtrCpy( rowPtr );
	// std::cout << std::endl;
	// for ( std::size_t i = 0; i < rows() + 1; ++i )
	// 	std::cout << rowPtrCpy[ i ] << " ";
	// std::cout << std::endl;

	// counting sort
	std::vector< Triplet > partiallySorted( length(), Triplet() );
	ptr = _begin;
	for ( ui64 n = 0; n < length(); ++n, ++ptr )
	{
		partiallySorted[ rowPtrCpy[ ptr->i ] ] = *ptr;
		++rowPtrCpy[ ptr->i ];
	}

	// sort each row
	for ( std::size_t i = 0; i < rows(); ++i )
	{
		std::sort( &partiallySorted[ rowPtr[ i ] ],
		           &partiallySorted[ rowPtr[ i + 1 ] ],
		           Triplet::cmpRowMajor ) ;
	}

	// copy results
	for ( ui64 n = 0; n < length(); ++n )
		_begin[ n ] = partiallySorted[ n ];
}

void
Triplets::sortColMajorStd()
{
	if ( !isSortedColMajor() )
		std::sort( _begin, _end, Triplet::cmpColMajor );
}

void
Triplets::sortColMajor( ui32 numThreads )
{
	if ( isSortedColMajor() )
		return;

	// Generate colPtr to use in counting sort
	std::vector< ui64 > colPtr( columns() + 1, 0 );
	Triplet* ptr = _begin;
	for ( ui64 n = 0; n < length(); ++n, ++ptr )
		++colPtr[ ptr->j + 1 ];

	for ( std::size_t i = 0; i < columns(); ++i )
		colPtr[ i + 1 ] += colPtr[ i ];

	std::vector< ui64 > colPtrCpy( colPtr );
	// counting sort
	std::vector< Triplet > partiallySorted( length(), Triplet() );
	ptr = _begin;
	for ( ui64 n = 0; n < length(); ++n, ++ptr )
	{
		partiallySorted[ colPtrCpy[ ptr->j ] ] = *ptr;
		++colPtrCpy[ ptr->j ];
	}

	// sort each col
	if ( numThreads < 2 )
	{
		for ( std::size_t i = 0; i < columns(); ++i )
		{
			std::sort( &partiallySorted[ colPtr[ i ] ],
			           &partiallySorted[ colPtr[ i + 1 ] ],
			           Triplet::cmpColMajor ) ;
		}
	}
	else
	{
		#pragma omp parallel for schedule( dynamic, 20 )
        for ( std::size_t i = 0; i < columns(); ++i )
		{
			std::sort( &partiallySorted[ colPtr[ i ] ],
			           &partiallySorted[ colPtr[ i + 1 ] ],
			           Triplet::cmpColMajor ) ;
		}
	}


	// copy results
	for ( ui64 n = 0; n < length(); ++n )
		_begin[ n ] = partiallySorted[ n ];
}

void
Triplets::sanityCheck() const
{
	for ( auto it = cbegin(); it != cend(); ++it )
	{
		Triplet& t = *it;

		if ( ( t.i < rowStart() && t.i > rowEnd() ) ||
		     ( t.j < colStart() && t.j > colEnd() ) )
			std::cout << t << "=> b[" << rowStart()
			          << ", " << colStart() << "] e["
			          << rowEnd() << ", "
			          << colEnd() << "]" << std::endl;

	}
}

// --------------------------------------------------------------------------

std::shared_ptr< std::vector< glbInd_t > >
Triplets::nnzPerRow() const
{
	std::shared_ptr< std::vector< glbInd_t > > pNnzPerRow =
			cacheNnzPer[ Triplets::CacheId::NNZ_PER_ROW ].lock();

	if ( !pNnzPerRow )
	{
		pNnzPerRow.reset( new std::vector< glbInd_t >() );

		if ( isEmpty() )
			return pNnzPerRow;

		std::vector< glbInd_t >& nnzPerRow = *pNnzPerRow.get();
		nnzPerRow.resize( _iLength, 0 );
		for ( std::size_t k = 0; k < length(); ++k )
			++nnzPerRow[ _begin[ k ].i - rowStart() ];

		// add to cache
		cacheNnzPer[ Triplets::CacheId::NNZ_PER_ROW ] = pNnzPerRow;
	}

	return pNnzPerRow;
}

std::shared_ptr< std::vector< glbNnz_t > >
Triplets::rowPtr() const
{
	std::shared_ptr< std::vector< glbNnz_t > > pRowPtr =
			cachePtr[ Triplets::CacheId::ROW_PTR ].lock();

	if ( !pRowPtr )
	{
		pRowPtr.reset( new std::vector< glbNnz_t >() );

		if ( isEmpty() )
			return pRowPtr;

		std::vector< glbNnz_t >& rowPtr = *pRowPtr.get();
		rowPtr.resize( _iLength + 1, 0 );
		for ( std::size_t k = 0; k < length(); ++k )
			++rowPtr[ _begin[ k ].i + 1 - rowStart() ];
		for ( ind_t i = 0; i < _iLength; ++i )
			rowPtr[ i + 1 ] += rowPtr[ i ];

		// add to cache
		cachePtr[ Triplets::CacheId::ROW_PTR ] = pRowPtr;
	}

	return pRowPtr;
}

std::shared_ptr< std::vector< glbInd_t > >
Triplets::nnzPerCol() const
{
	std::shared_ptr< std::vector< glbInd_t > > pNnzPerCol =
			cacheNnzPer[ Triplets::CacheId::NNZ_PER_COL ].lock();

	if ( !pNnzPerCol )
	{
		pNnzPerCol.reset( new std::vector< glbInd_t >() );

		if ( isEmpty() )
			return pNnzPerCol;

		std::vector< glbInd_t >& nnzPerCol = *pNnzPerCol.get();
		nnzPerCol.resize( _jLength, 0 );
		for ( std::size_t k = 0; k < length(); ++k )
			++nnzPerCol[ _begin[ k ].j - colStart() ];

		// add to cache
		cacheNnzPer[ Triplets::CacheId::NNZ_PER_COL ] = pNnzPerCol;
	}

	return pNnzPerCol;
}

std::shared_ptr< std::vector< glbNnz_t > >
Triplets::colPtr() const
{
	std::shared_ptr< std::vector< glbNnz_t > > pColPtr =
			cachePtr[ Triplets::CacheId::COL_PTR ].lock();

	if ( !pColPtr )
	{
		pColPtr.reset( new std::vector< glbNnz_t >() );

		if ( isEmpty() )
			return pColPtr;

		std::vector< glbNnz_t >& colPtr = *pColPtr.get();
		colPtr.resize( _jLength + 1, 0 );
		for ( std::size_t k = 0; k < length(); ++k )
			++colPtr[ _begin[ k ].j + 1 - colStart() ];

		for ( ind_t j = 0; j < _jLength; ++j )
			colPtr[ j + 1 ] += colPtr[ j ];

		// add to cache
		cachePtr[ Triplets::CacheId::COL_PTR ] = pColPtr;
	}

	return pColPtr;
}

// Getter & Setter
// --------------------------------------------------------------------------
Triplet* Triplets::begin() { return _begin; }
Triplet* Triplets::end() { return _end; }
Triplet* Triplets::cbegin() const { return _begin; }
Triplet* Triplets::cend() const { return _end; }
bool Triplets::isEmpty() const { return length() <= 0; }
glbInd_t Triplets::rows() const { return _iLength; }
glbInd_t Triplets::columns() const { return _jLength; }
glbInd_t Triplets::rowStart() const { return _iStart; }
glbInd_t Triplets::colStart() const { return _jStart; }
glbInd_t Triplets::rowEnd() const { return rowStart() + rows(); }
glbInd_t Triplets::colEnd() const { return colStart() + columns(); }

double
Triplets::MBs_CSR( const ui32& bytesInInd_t, const ui32& bytesInFp_t,
                   const ui32& bytesInNnz_t ) const
{
	return KBs_CSR( bytesInInd_t, bytesInFp_t, bytesInNnz_t ) / pow( 2, 20 );
}
double
Triplets::KBs_CSR( const ui32& bytesInInd_t, const ui32& bytesInFp_t,
                   const ui32& bytesInNnz_t ) const
{
	return ( ( bytesInInd_t + bytesInFp_t ) * length() +
	         bytesInNnz_t * ( rows() + 1 ) ) / pow( 2, 10 );
}

std::size_t
Triplets::length() const
{
	if ( _begin == nullptr || _end == nullptr )
		return 0;

	return std::distance( _begin, _end );
}

void Triplets::setRowStart( glbInd_t rowStart ) { _iStart = rowStart; }
void Triplets::setColStart( glbInd_t colStart ) { _jStart = colStart; }
void Triplets::setRowCount( glbInd_t rowCount ) { _iLength = rowCount; }
void Triplets::setColCount( glbInd_t colCount ) { _jLength = colCount; }
void Triplets::setBegin( Triplet* begin ) { _begin = begin; }
void Triplets::setEnd( Triplet* end ) { _begin = end; }

// --------------------------------------------------------------------------

std::ostream&
Triplets::str( std::ostream& out ) const
{
	out << "s[" << _iStart << ", " << _jStart << "] ";
	out << "l[" << _iLength << ", " << _jLength << "] => ";
	out << std::distance( _begin, _end ) << std::endl;
	for ( std::size_t i = 0; i < length(); ++i )
		out << "Q" << i << _begin[ i ] << std::endl;
	return out;
}

std::string
Triplets::str() const
{
	std::stringstream ss;
	str( ss );
	return ss.str();
}

std::ostream&
Triplets::stats( std::ostream& out ) const
{
    // Calculate row non-zero stats
	glbInd_t minNnzRow = 0;
	glbInd_t maxNnzRow = 0;
	double avgNnzRow = 0;
	double geoNnzRow = 0;
	double stdDevNnzRow = 0;
	double coefficientOfVariationNnzRow = 0;
	{
		std::shared_ptr< std::vector< glbInd_t > > pNnzPerRow = nnzPerRow();
		std::vector< glbInd_t >& nnzsPerRow = *pNnzPerRow.get();
		minNnzRow = nnzsPerRow[ 0 ];
		maxNnzRow = nnzsPerRow[ 0 ];
		avgNnzRow = nnzsPerRow[ 0 ];
		geoNnzRow = nnzsPerRow[ 0 ] > 0 ? log10( nnzsPerRow[ 0 ] ) : 0;
		for( glbInd_t i = 1; i < _iLength; ++i )
		{
			if ( minNnzRow > nnzsPerRow[ i ] )
				minNnzRow = nnzsPerRow[ i ];
			else if ( maxNnzRow < nnzsPerRow[ i ] )
				maxNnzRow = nnzsPerRow[ i ];

			avgNnzRow += nnzsPerRow[ i ];
			if ( nnzsPerRow[ i ] > 0 )
				geoNnzRow += log10( nnzsPerRow[ i ] );
		}
		avgNnzRow /= rows();
		geoNnzRow = pow( 10, geoNnzRow / rows() );

		for ( glbInd_t i = 0; i < _iLength; ++i )
			stdDevNnzRow += pow( nnzsPerRow[ i ] - avgNnzRow, 2 );
		stdDevNnzRow = sqrt( stdDevNnzRow / _iLength );

		coefficientOfVariationNnzRow = stdDevNnzRow / avgNnzRow;
	}

	out << minNnzRow << "," << avgNnzRow << "," << geoNnzRow << ","
	    << stdDevNnzRow << "," << maxNnzRow << ","
		<< coefficientOfVariationNnzRow << ",";

	// calculate column non-zero stats
	glbInd_t minNnzCol = 0;
	glbInd_t maxNnzCol = 0;
	double avgNnzCol = 0;
	double geoNnzCol = 0;
	double stdDevNnzCol = 0;
	double coefficientOfVariationNnzCol = 0;
	{
		std::shared_ptr< std::vector< glbInd_t > > pNnzPerCol = nnzPerCol();
		std::vector< glbInd_t >& nnzsPerCol = *pNnzPerCol.get();
		minNnzCol = nnzsPerCol[ 0 ];
		maxNnzCol = nnzsPerCol[ 0 ];
		avgNnzCol = nnzsPerCol[ 0 ];
		geoNnzCol = nnzsPerCol[ 0 ] > 0 ? log10( nnzsPerCol[ 0 ] ) : 0;
		for ( glbInd_t i = 1; i < _jLength; ++i )
		{
			if ( minNnzCol > nnzsPerCol[ i ] )
				minNnzCol = nnzsPerCol[ i ];
			else if ( maxNnzCol < nnzsPerCol[ i ] )
				maxNnzCol = nnzsPerCol[ i ];

			avgNnzCol += nnzsPerCol[ i ];
			if ( nnzsPerCol[ i ] > 0 )
				geoNnzCol += log10( nnzsPerCol[ i ] );
		}
		avgNnzCol /= columns();
		geoNnzCol = pow( 10, geoNnzCol / columns() );

		for ( glbInd_t i = 0; i < _jLength; ++i )
			stdDevNnzCol += pow( nnzsPerCol[ i ] - avgNnzCol, 2 );
		stdDevNnzCol = sqrt( stdDevNnzCol / _jLength );

		coefficientOfVariationNnzCol = stdDevNnzCol / avgNnzCol;
	}

	out << minNnzCol << "," << avgNnzCol << "," << geoNnzCol << ","
	    << stdDevNnzCol << "," << maxNnzCol << ","
		<< coefficientOfVariationNnzCol << ",";

	return out;
}

void
Triplets::stats( std::ostream& sstats, std::ostream& header ) const
{
	stats( sstats );
    header << "min,avg,geo,std-dev,max,cv,";
    header << "min,avg,geo,std-dev,max,cv,";
}

std::string
Triplets::stats() const
{
	std::stringstream ss;
	stats( ss );
	return ss.str();
}

std::ostream&
Triplets::summary( std::ostream& out ) const
{
	out << "b[" << _iStart << ", " << _jStart << "]";
	out << " e[" << _iStart + _iLength << ", " << _jStart + _jLength << "]";
	out << " length=" << length();
	out << " columns=" << columns() << " rows=" << rows();
	return out;
}

std::string
Triplets::summary() const
{
	std::stringstream ss;
	summary( ss );
	return ss.str();
}

// Private helper functions
// --------------------------------------------------------------------------

bool
Triplets::isSortedRowMajor() const
{
	if ( isEmpty() )
		return true;

	bool isSorted = true;
	std::size_t len = length();

	for ( std::size_t t = 0; ( t < len - 1 ) && ( isSorted ); ++t )
	{
		const Triplet& lhs = this->operator []( t );
		const Triplet& rhs = this->operator []( t + 1 );

		if ( !Triplet::cmpRowMajor( lhs, rhs ) )
			isSorted = false;
	}

	return isSorted;
}

bool
Triplets::isSortedColMajor() const
{
	if ( isEmpty() )
		return true;

	bool isSorted = true;
	std::size_t len = length();

	for ( std::size_t t = 0; ( t < len - 1 ) && ( isSorted ); ++t )
	{
		const Triplet& lhs = this->operator []( t );
		const Triplet& rhs = this->operator []( t + 1 );

		if ( !Triplet::cmpColMajor( lhs, rhs ) )
			isSorted = false;
	}

	return isSorted;
}

// -----------------------------------------------------------------------------

void
Triplets::permuteRandom( Triplets& A, Triplets& B )
{
	// generate row & column order
	std::vector< glbInd_t > rowMapA;
	std::vector< glbInd_t > columnMapA;
	std::vector< glbInd_t > columnMapB;
	for ( glbInd_t i = 0; i < A.rows(); ++i )
		rowMapA.push_back( i );
	for ( glbInd_t j = 0; j < A.columns(); ++j )
		columnMapA.push_back( j );
	for ( glbInd_t j = 0; j < B.columns(); ++j )
		columnMapB.push_back( j );

	for ( glbInd_t i = 0; i < A.rows(); ++i )
	{
		glbInd_t newIndex = ( rand() % ( A.rows() - i ) ) + i;
		std::swap( rowMapA[ i ], rowMapA[ newIndex ] );
	}
	for ( glbInd_t j = 0; j < A.columns(); ++j )
	{
		glbInd_t newIndex = ( rand() % ( A.columns() - j ) ) + j;
		std::swap( columnMapA[ j ], columnMapA[ newIndex ] );
	}
	for ( glbInd_t j = 0; j < B.columns(); ++j )
	{
		glbInd_t newIndex = ( rand() % ( B.columns() - j ) ) + j;
		std::swap( columnMapB[ j ], columnMapB[ newIndex ] );
	}

	// reorder
	A.remapIJ( rowMapA, columnMapA );
	B.remapIJ( columnMapA, columnMapB );

}

std::shared_ptr< std::vector< ui64 > >
Triplets::calculateMultsPerRow( const Triplets& A, const Triplets& B )
{
	std::shared_ptr< std::vector< ui64 > > pMultCountPerRow(
		new std::vector< ui64 >( A.rows(), 0 ) );
	std::vector< ui64 >& multCountPerRow = *pMultCountPerRow.get();
	std::shared_ptr< std::vector< glbNnz_t > > pARowPtr = A.rowPtr();
	std::shared_ptr< std::vector< glbNnz_t > > pBRowPtr = B.rowPtr();
	std::vector< glbNnz_t >& ARowPtr = *pARowPtr.get();
	std::vector< glbNnz_t >& BRowPtr = *pBRowPtr.get();

	#pragma omp parallel for schedule( static )
	for ( glbInd_t rA = 0; rA < A.rows(); ++rA )
	{
		ui64 multCount = 0;
		for ( glbNnz_t nA = ARowPtr[ rA ]; nA < ARowPtr[ rA + 1]; ++nA )
		{
			glbInd_t cA = A[ nA ].j;
			multCount += BRowPtr[ cA + 1 ] - BRowPtr[ cA ];
		}
		multCountPerRow[ rA ] = multCount;
	}

	return pMultCountPerRow;
}

std::shared_ptr< std::vector< double > >
Triplets::calculateCompressionRatioPerRow(
	const std::vector< ui64 >& multCountPerRow,
	const Triplets& C )
{
	std::shared_ptr< std::vector< glbInd_t > > pCNnzPerRow( C.nnzPerRow() );
	std::vector< glbInd_t >& cNnzPerRow = *pCNnzPerRow.get();

	std::shared_ptr< std::vector< double > > pcr(
		new std::vector< double >( C.rows() ) );
	std::vector< double >& cr = *pcr.get();

	#pragma omp parallel for schedule( static )
	for ( glbInd_t i = 0; i < C.rows(); ++i )
	{
		if ( cNnzPerRow[ i ] > 0 )
			cr[ i ] = (double) multCountPerRow[ i ] / cNnzPerRow[ i ];
		else
			cr[ i ] = 0;
	}

	return pcr;
}

std::shared_ptr< std::vector< double > >
Triplets::calculateCompressionRatioPerRow(
	const Triplets& A, const Triplets& B, const Triplets& C )
{
	std::shared_ptr< std::vector< ui64 > > pMultPerRow =
		calculateMultsPerRow( A, B );

	return calculateCompressionRatioPerRow( *pMultPerRow.get(), C );
}

std::shared_ptr< std::vector< glbInd_t > >
Triplets::calculateMultsPerCNonZero(
	const Triplets& A, const Triplets& B, const Triplets& C )
{
	std::shared_ptr< std::vector< glbInd_t > > pMultCountPerCNonZero(
		new std::vector< glbInd_t >( C.length(), 0 ) );
	std::vector< glbInd_t >& multCountPerC = *pMultCountPerCNonZero.get();
	std::shared_ptr< std::vector< glbNnz_t > > pARowPtr = A.rowPtr();
	std::shared_ptr< std::vector< glbNnz_t > > pBRowPtr = B.rowPtr();
	std::shared_ptr< std::vector< glbNnz_t > > pCRowPtr = C.rowPtr();
	std::vector< glbNnz_t >& ARowPtr = *pARowPtr.get();
	std::vector< glbNnz_t >& BRowPtr = *pBRowPtr.get();
	std::vector< glbNnz_t >& CRowPtr = *pCRowPtr.get();

	std::vector< glbInd_t > accumulator;
	accumulator.resize( B.columns(), 0 );
	for ( glbInd_t j = 0; j < B.columns(); ++j )
		accumulator[ j ] = 0;

	for ( glbInd_t r = 0; r < A.rows(); ++r )
	{
		for ( glbInd_t na = ARowPtr[ r ]; na < ARowPtr[ r + 1 ]; ++na )
		{
			const glbInd_t JA = A[ na ].j;
			for ( glbInd_t nb = BRowPtr[ JA ]; nb < BRowPtr[ JA + 1 ]; ++nb )
				++accumulator[ B[ nb ].j ];
		}

		for ( glbInd_t nc = CRowPtr[ r ]; nc < CRowPtr[ r + 1 ]; ++nc )
		{
			multCountPerC[ nc ] = accumulator[ C[ nc ].j ];
			accumulator[ C[ nc ].j ] = 0;
		}
	}

	return pMultCountPerCNonZero;
}

// ############################################################################

std::size_t
garbage::getIndexFromLeft( Triplet* begin, Triplet* end, glbInd_t j )
{
	Triplet* tBegin = begin;
	Triplet* tEnd = end;

	while( true )
	{
		std::size_t len = std::distance( tBegin, tEnd );
		Triplet* middle = tBegin + len / 2;

		if ( j < middle->j )
			tEnd = middle;
		else if ( j > middle->j )
			tBegin = middle;
		else
		{
			return std::distance( begin, middle );
		}
	}
}
