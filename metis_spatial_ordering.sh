#!/bin/bash

mmfPathAArr=(
    # "/arc/shared/data/ufl/2cubes_sphere/2cubes_sphere.mtx"
	# "/arc/shared/data/ufl/cage12/cage12.mtx"
	# "/arc/shared/data/ufl/filter3D/filter3D.mtx"
	# "/arc/shared/data/ufl/mac_econ_fwd500/mac_econ_fwd500.mtx"
	# "/arc/shared/data/ufl/majorbasis/majorbasis.mtx"
	# "/arc/shared/data/ufl/mario002/mario002.mtx"
	# "/arc/shared/data/ufl/mc2depi/mc2depi.mtx"
	# "/arc/shared/data/ufl/offshore/offshore.mtx"
    # "/arc/shared/data/ufl/scircuit/scircuit.mtx"
	# "/arc/shared/data/ufl/tmt_sym/tmt_sym.mtx"
	# "/arc/shared/data/ufl/torso2/torso2.mtx"
	# "/arc/shared/data/ufl/europe_osm/europe_osm.mtx"
	# "/arc/shared/data/ufl/delaunay_n24/delaunay_n24.mtx"
    # "/arc/shared/data/ufl/web-Stanford/web-Stanford.mtx"
    # "/arc/shared/data/ufl/road_usa/road_usa.mtx"
    # "/arc/shared/data/ufl/Freescale1/Freescale1.mtx"
    # "/arc/shared/data/ufl/rajat31/rajat31.mtx"
    # "/arc/shared/data/ufl/asia_osm/asia_osm.mtx"
    # "/arc/shared/data/ufl/germany_osm/germany_osm.mtx"
    # "/arc/shared/data/ufl/NLR/NLR.mtx"
    # "/arc/shared/data/ufl/patents/patents.mtx"
    # "/arc/shared/data/ufl/cit-Patents/cit-Patents.mtx"
    # "/arc/shared/data/ufl/adaptive/adaptive.mtx"
    # "/arc/shared/data/ufl/italy_osm/italy_osm.mtx"
    # "/arc/shared/data/ufl/M6/M6.mtx"
	# "/arc/shared/data/ufl/memchip/memchip.mtx"
	# "/arc/shared/data/ufl/cage14/cage14.mtx"
	# "/arc/shared/data/ufl/wb-edu/wb-edu.mtx"
	# "/arc/shared/data/ufl/hugebubbles-00020/hugebubbles-00020.mtx"
	# "/arc/shared/data/ufl/ldoor/ldoor.mtx"
	# "/arc/shared/data/ufl/hugebubbles-00010/hugebubbles-00010.mtx"
	# "/arc/shared/data/ufl/hugetrace-00010/hugetrace-00010.mtx"
	# "/arc/shared/data/ufl/road_central/road_central.mtx"
	# "/arc/shared/data/ufl/gsm_106857/gsm_106857.mtx"
	# "/arc/shared/data/ufl/msdoor/msdoor.mtx"
	# "/arc/shared/data/ufl/circuit5M_dc/circuit5M_dc.mtx"
	# "/arc/shared/data/ufl/AS365/AS365.mtx"
	# "/arc/shared/data/ufl/333SP/333SP.mtx"
	# "/arc/shared/data/ufl/atmosmodm/atmosmodm.mtx"
	# "/arc/shared/data/ufl/hugetrace-00000/hugetrace-00000.mtx"
	# "/arc/shared/data/ufl/thermal2/thermal2.mtx"
	# "/arc/shared/data/ufl/G3_circuit/G3_circuit.mtx"
	# "/arc/shared/data/ufl/CurlCurl_2/CurlCurl_2.mtx"
	# "/arc/shared/data/ufl/great-britain_osm/great-britain_osm.mtx"
	# "/arc/shared/data/ufl/delaunay_n21/delaunay_n21.mtx"
	# "/arc/shared/data/ufl/sls/sls.mtx"
	# "/arc/shared/data/ufl/Hamrle3/Hamrle3.mtx"
	# "/arc/shared/data/ufl/delaunay_n22/delaunay_n22.mtx"
	# "/arc/shared/data/ufl/delaunay_n23/delaunay_n23.mtx"
	# "/arc/shared/data/ufl/eu-2005/eu-2005.mtx"
	# "/arc/shared/data/ufl/StocF-1465/StocF-1465.mtx"
	# "/arc/shared/data/ufl/Transport/Transport.mtx"
	# "/arc/shared/data/ufl/venturiLevel3/venturiLevel3.mtx"
	# "/arc/shared/data/ufl/ecology1/ecology1.mtx"
	# "/arc/shared/data/ufl/roadNet-CA/roadNet-CA.mtx"
	# "/arc/shared/data/ufl/webbase-1M/webbase-1M.mtx"
	# "/arc/shared/data/ufl/netherlands_osm/netherlands_osm.mtx"
	# "/arc/shared/data/ufl/roadNet-TX/roadNet-TX.mtx"
	# "/arc/shared/data/ufl/roadNet-PA/roadNet-PA.mtx"
	# "/arc/shared/data/ufl/citationCiteseer/citationCiteseer.mtx"
	# "/arc/shared/data/ufl/belgium_osm/belgium_osm.mtx"
	# "/arc/shared/data/ufl/debr/debr.mtx"
	"/arc/shared/data/ufl/pkustk10/pkustk10.mtx"
	"/arc/shared/data/ufl/Chebyshev4/Chebyshev4.mtx"
	"/arc/shared/data/ufl/amazon-2008/amazon-2008.mtx"
	"/arc/shared/data/ufl/Ga10As10H30/Ga10As10H30.mtx"
	"/arc/shared/data/ufl/pre2/pre2.mtx"
	"/arc/shared/data/ufl/pkustk13/pkustk13.mtx"
	"/arc/shared/data/ufl/hood/hood.mtx"
	"/arc/shared/data/ufl/pwtk/pwtk.mtx"
	"/arc/shared/data/ufl/kkt_power/kkt_power.mtx"
	"/arc/shared/data/ufl/pkustk14/pkustk14.mtx"
	"/arc/shared/data/ufl/F1/F1.mtx"
	"/arc/shared/data/ufl/ML_Laplace/ML_Laplace.mtx"
	"/arc/shared/data/ufl/Fault_639/Fault_639.mtx"
	"/arc/shared/data/ufl/nlpkkt80/nlpkkt80.mtx"
	"/arc/shared/data/ufl/Emilia_923/Emilia_923.mtx"
	"/arc/shared/data/ufl/dielFilterV2real/dielFilterV2real.mtx"
	"/arc/shared/data/ufl/af_shell10/af_shell10.mtx"
	"/arc/shared/data/ufl/Geo_1438/Geo_1438.mtx"
	"/arc/shared/data/ufl/nlpkkt120/nlpkkt120.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_14_2.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_14_4.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_14_8.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_14_16.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_14_32.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_14_64.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_15_2.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_15_4.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_15_8.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_15_16.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_15_32.mtx"
    # "/home/mbasaran/arc/ER/R-MAT_16_2.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_16_4.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_16_8.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_16_16.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_16_32.mtx"
    # "/home/mbasaran/arc/ER/R-MAT_17_2.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_17_4.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_17_8.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_17_16.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_17_32.mtx"
    # "/home/mbasaran/arc/ER/R-MAT_18_2.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_18_4.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_18_8.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_18_16.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_18_32.mtx"
    # "/home/mbasaran/arc/ER/R-MAT_19_2.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_19_4.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_19_8.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_19_16.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_19_32.mtx"
    # "/home/mbasaran/arc/ER/R-MAT_20_2.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_20_4.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_20_8.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_20_16.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_20_32.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_21_2.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_21_4.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_21_8.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_21_16.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_21_32.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_22_2.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_22_4.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_22_8.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_22_16.mtx"
	# # "/home/mbasaran/arc/ER/R-MAT_22_32.mtx"
	# "/home/mbasaran/arc/Graph500/R-MAT_15_2.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_15_4.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_15_8.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_15_16.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_15_32.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_16_2.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_16_4.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_16_8.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_16_16.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_16_32.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_17_2.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_17_4.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_17_8.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_17_16.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_17_32.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_18_2.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_18_4.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_18_8.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_18_16.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_18_32.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_19_2.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_19_4.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_19_8.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_19_16.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_19_32.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_20_2.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_20_4.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_20_8.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_20_16.mtx"
)

kArr=(
	# 38 # 2cubes_sphere
	# 46 # cage12
	# 61 # filter3D
	# 30 # mac_econ_fwd500
	# 40 # majorbasis
	# 46 # mario002
	# 51 # mc2depi
	# 97 # offshore
	# 23 # scircuit
	# 119 # tmt_sym
	# 24 # torso2
	# 1658 # europe_osm
	# 2000 # delaunay_n24
	# 54 # web-Stanford
	# 933 # road_usa
	# 448 # Freescale1
	# 489 # rajat31
	# 390 # asia_osm
	# 381 # germany_osm
	# 496 # NLR
	# 363 # patents
	# 397 # cit-Patents
	# 507 # adaptive
	# 214 # italy_osm
	# 417 # M6
	# 351 # memchip
	# 618 # cage14
	# 1351 # wb-edu
	# 1105 # hugebubbles-00020
	# 1047 # ldoor
	# 1014 # hugebubbles-00010
	# 629 # hugetrace-00010
	# 547 # road_central
	# 491 # gsm_106857
	# 456 # msdoor
	# 455 # circuit5M_dc
	# 452 # AS365
	# 441 # 333SP
	# 242 # atmosmodm
	# 239 # hugetrace-00000
	# 201 # thermal2
	# 183 # G3_circuit
	# 205 # CurlCurl_2
	# 249 # great-britain_osm
	# 250 # delaunay_n21
	# 165 # sls
	# 134 # Hamrle3
	# 500 # delaunay_n22
	# 1000 # delaunay_n23
	# 436 # eu-2005
	# 480 # StocF-1465
	# 537 # Transport
	# 300 # venturiLevel3
	# 119 # ecology1
	# 94 # roadNet-CA
	# 77 # webbase-1M
	# 76 # netherlands_osm
	# 65 # roadNet-TX
	# 48 # citationCiteseer
	# 48 # belgium_osm
	# 78 # debr
	97 # pkustk10
	121 # Chebyshev4
	121 # amazon-2008
	138 # Ga10As10H30
	138 # pre2
	149 # pkustk13
	242 # hood
	262 # pwtk
	333 # kkt_power
	333 # pkustk14
	602 # F1
	622 # ML_Laplace
	644 # Fault_639
	650 # nlpkkt80
	923 # Emilia_923
	1094 # dielFilterV2real
	1189 # af_shell10
	1422 # Geo_1438
	2191 # nlpkkt120
	# 1 # R-MAT_14_2
	# 2 # R-MAT_14_4
	# 3 # R-MAT_14_8
	# 6 # R-MAT_14_16
	# 12 # R-MAT_14_32
	# 24 # R-MAT_14_64
	# 2 # R-MAT_15_2
	# 3 # R-MAT_15_4
	# 6 # R-MAT_15_8
	# 12 # R-MAT_15_16
	# 24 # R-MAT_15_32
	# 3 # R-MAT_16_2
	# 6 # R-MAT_16_4
	# 12 # R-MAT_16_8
	# 24 # R-MAT_16_16
	# 47 # R-MAT_16_32
	# 7 # R-MAT_17_2
	# 13 # R-MAT_17_4
	# 24 # R-MAT_17_8
	# 48 # R-MAT_17_16
	# 95 # R-MAT_17_32
	# 14 # R-MAT_18_2
	# 25 # R-MAT_18_4
	# 49 # R-MAT_18_8
	# 96 # R-MAT_18_16
	# 189 # R-MAT_18_32
	# 27 # R-MAT_19_2
	# 51 # R-MAT_19_4
	# 98 # R-MAT_19_8
	# 191 # R-MAT_19_16
	# 379 # R-MAT_19_32
	# 55 # R-MAT_20_2
	# 102 # R-MAT_20_4
	# 195 # R-MAT_20_8
	# 383 # R-MAT_20_16
	# 758 # R-MAT_20_32
	# 109 # R-MAT_21_2
	# 203 # R-MAT_21_4
	# 391 # R-MAT_21_8
	# 766 # R-MAT_21_16
	# 1516 # R-MAT_21_32
	# 219 # R-MAT_22_2
	# 406 # R-MAT_22_4
	# 781 # R-MAT_22_8
	# 1531 # R-MAT_22_16
	# 2 # R-MAT_15_2
	# 3 # R-MAT_15_4
	# 6 # R-MAT_15_8
	# 12 # R-MAT_15_16
	# 24 # R-MAT_15_32
	# 3 # R-MAT_16_2
	# 6 # R-MAT_16_4
	# 12 # R-MAT_16_8
	# 24 # R-MAT_16_16
	# 47 # R-MAT_16_32
	# 7 # R-MAT_17_2
	# 13 # R-MAT_17_4
	# 24 # R-MAT_17_8
	# 48 # R-MAT_17_16
	# 95 # R-MAT_17_32
	# 14 # R-MAT_18_2
	# 25 # R-MAT_18_4
	# 49 # R-MAT_18_8
	# 96 # R-MAT_18_16
	# 27 # R-MAT_19_2
	# 51 # R-MAT_19_4
	# 98 # R-MAT_19_8
	# 55 # R-MAT_20_2
	# 102 # R-MAT_20_4
)


###############################################################################

binary=mt-metis-0.6.0/build/Linux-x86_64/bin/mtmetis
graphDir=/home/mbasaran/arc/metis_spatial_graphs
graphSuffix="_spatial.graph"
outDir=/home/mbasaran/arc/metis_orderings

noOfMatrices=${#mmfPathAArr[@]}
for (( i = 0; i < $noOfMatrices; i++))
do
	mmfPathA=${mmfPathAArr[i]}
	mtx=$(basename $mmfPathA)
	spatialGraphPath="${graphDir}/${mtx}${graphSuffix}"
	# partVectorPath="${outDir}/${mtx}_${k}_spatialPartVector"
	partVectorPath="${outDir}/${mtx}_spatialPartVector"
	k=${kArr[i]}

	cmd="$binary -p rb ${spatialGraphPath} ${k} ${mtx}_spatialPartVector"
	echo "$cmd" >> spatial_ordering.log
	time $cmd >> spatial_ordering.log 2>&1 3>&1
	mv ${mtx}_spatialPartVector ${outDir}
done    

