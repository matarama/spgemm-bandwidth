#!/bin/bash

###############################################################################

mmfPathAArr=(
    "/arc/shared/data/ufl/2cubes_sphere/2cubes_sphere.mtx"
	"/arc/shared/data/ufl/cage12/cage12.mtx"
	"/arc/shared/data/ufl/filter3D/filter3D.mtx"
	"/arc/shared/data/ufl/mac_econ_fwd500/mac_econ_fwd500.mtx"
	"/arc/shared/data/ufl/majorbasis/majorbasis.mtx"
	"/arc/shared/data/ufl/mario002/mario002.mtx"
	"/arc/shared/data/ufl/mc2depi/mc2depi.mtx"
	"/arc/shared/data/ufl/offshore/offshore.mtx"
    "/arc/shared/data/ufl/scircuit/scircuit.mtx"
	"/arc/shared/data/ufl/tmt_sym/tmt_sym.mtx"
	"/arc/shared/data/ufl/torso2/torso2.mtx"
	"/arc/shared/data/ufl/europe_osm/europe_osm.mtx"
	"/arc/shared/data/ufl/delaunay_n24/delaunay_n24.mtx"
    "/arc/shared/data/ufl/web-Stanford/web-Stanford.mtx"
    "/arc/shared/data/ufl/road_usa/road_usa.mtx"
    "/arc/shared/data/ufl/Freescale1/Freescale1.mtx"
    "/arc/shared/data/ufl/rajat31/rajat31.mtx"
    "/arc/shared/data/ufl/asia_osm/asia_osm.mtx"
    "/arc/shared/data/ufl/germany_osm/germany_osm.mtx"
    "/arc/shared/data/ufl/NLR/NLR.mtx"
    "/arc/shared/data/ufl/patents/patents.mtx"
    "/arc/shared/data/ufl/cit-Patents/cit-Patents.mtx"
    "/arc/shared/data/ufl/adaptive/adaptive.mtx"
    "/arc/shared/data/ufl/italy_osm/italy_osm.mtx"
    "/arc/shared/data/ufl/M6/M6.mtx"
	"/arc/shared/data/ufl/memchip/memchip.mtx"
	"/arc/shared/data/ufl/cage14/cage14.mtx"
	"/arc/shared/data/ufl/wb-edu/wb-edu.mtx"
	"/arc/shared/data/ufl/hugebubbles-00020/hugebubbles-00020.mtx"
	"/arc/shared/data/ufl/ldoor/ldoor.mtx"
	"/arc/shared/data/ufl/hugebubbles-00010/hugebubbles-00010.mtx"
	"/arc/shared/data/ufl/hugetrace-00010/hugetrace-00010.mtx"
	"/arc/shared/data/ufl/road_central/road_central.mtx"
	"/arc/shared/data/ufl/gsm_106857/gsm_106857.mtx"
	"/arc/shared/data/ufl/msdoor/msdoor.mtx"
	"/arc/shared/data/ufl/circuit5M_dc/circuit5M_dc.mtx"
	"/arc/shared/data/ufl/AS365/AS365.mtx"
	"/arc/shared/data/ufl/333SP/333SP.mtx"
	"/arc/shared/data/ufl/atmosmodm/atmosmodm.mtx"
	"/arc/shared/data/ufl/hugetrace-00000/hugetrace-00000.mtx"
	"/arc/shared/data/ufl/thermal2/thermal2.mtx"
	"/arc/shared/data/ufl/G3_circuit/G3_circuit.mtx"
	"/arc/shared/data/ufl/CurlCurl_2/CurlCurl_2.mtx"
	"/arc/shared/data/ufl/great-britain_osm/great-britain_osm.mtx"
	"/arc/shared/data/ufl/delaunay_n21/delaunay_n21.mtx"
	"/arc/shared/data/ufl/sls/sls.mtx"
	"/arc/shared/data/ufl/Hamrle3/Hamrle3.mtx"
	"/arc/shared/data/ufl/delaunay_n22/delaunay_n22.mtx"
	"/arc/shared/data/ufl/delaunay_n23/delaunay_n23.mtx"
	"/arc/shared/data/ufl/eu-2005/eu-2005.mtx"
	"/arc/shared/data/ufl/StocF-1465/StocF-1465.mtx"
	"/arc/shared/data/ufl/Transport/Transport.mtx"
	"/arc/shared/data/ufl/venturiLevel3/venturiLevel3.mtx"
	"/arc/shared/data/ufl/ecology1/ecology1.mtx"
	"/arc/shared/data/ufl/roadNet-CA/roadNet-CA.mtx"
	"/arc/shared/data/ufl/webbase-1M/webbase-1M.mtx"
	"/arc/shared/data/ufl/netherlands_osm/netherlands_osm.mtx"
	"/arc/shared/data/ufl/roadNet-TX/roadNet-TX.mtx"
	"/arc/shared/data/ufl/roadNet-PA/roadNet-PA.mtx"
	"/arc/shared/data/ufl/citationCiteseer/citationCiteseer.mtx"
	"/arc/shared/data/ufl/belgium_osm/belgium_osm.mtx"
	"/arc/shared/data/ufl/debr/debr.mtx"
	"/arc/shared/data/ufl/pkustk10/pkustk10.mtx"
	"/arc/shared/data/ufl/Chebyshev4/Chebyshev4.mtx"
	"/arc/shared/data/ufl/amazon-2008/amazon-2008.mtx"
	"/arc/shared/data/ufl/Ga10As10H30/Ga10As10H30.mtx"
	"/arc/shared/data/ufl/pre2/pre2.mtx"
	"/arc/shared/data/ufl/pkustk13/pkustk13.mtx"
	"/arc/shared/data/ufl/hood/hood.mtx"
	"/arc/shared/data/ufl/pwtk/pwtk.mtx"
	"/arc/shared/data/ufl/kkt_power/kkt_power.mtx"
	"/arc/shared/data/ufl/pkustk14/pkustk14.mtx"
	"/arc/shared/data/ufl/F1/F1.mtx"
	"/arc/shared/data/ufl/ML_Laplace/ML_Laplace.mtx"
	"/arc/shared/data/ufl/Fault_639/Fault_639.mtx"
	"/arc/shared/data/ufl/nlpkkt80/nlpkkt80.mtx"
	"/arc/shared/data/ufl/Emilia_923/Emilia_923.mtx"
	"/arc/shared/data/ufl/dielFilterV2real/dielFilterV2real.mtx"
	"/arc/shared/data/ufl/af_shell10/af_shell10.mtx"
	"/arc/shared/data/ufl/Geo_1438/Geo_1438.mtx"
	"/arc/shared/data/ufl/nlpkkt120/nlpkkt120.mtx"
	# linear programming matrices (rectangular)
	"/arc/shared/data/ufl/spal_004/spal_004.mtx"
	"/arc/shared/data/ufl/rail_1357/rail_1357.mtx"
	"/arc/shared/data/ufl/rail_20209/rail_20209.mtx"
	"/arc/shared/data/ufl/rail_5177/rail_5177.mtx"
	"/arc/shared/data/ufl/rail_79841/rail_79841.mtx"
	"/arc/shared/data/ufl/rail507/rail507.mtx"
	"/arc/shared/data/ufl/rail516/rail516.mtx"
	"/arc/shared/data/ufl/rail582/rail582.mtx"
	"/arc/shared/data/ufl/rail2586/rail2586.mtx"
	"/arc/shared/data/ufl/rail4284/rail4284.mtx"
	"/arc/shared/data/ufl/tp-6/tp-6.mtx"
	"/arc/shared/data/ufl/degme/degme.mtx"
	# "/arc/shared/data/ufl/cont1_l/cont1_l.mtx"
	"/arc/shared/data/ufl/cont11_l/cont11_l.mtx"
	"/arc/shared/data/ufl/web-Google/web-Google.mtx"
	"/arc/shared/data/ufl/stormg2-8/stormg2-8.mtx"
	"/arc/shared/data/ufl/stormg2-27/stormg2-27.mtx"
	"/arc/shared/data/ufl/stormg2-125/stormg2-125.mtx"
	"/arc/shared/data/ufl/stormG2_1000/stormG2_1000.mtx"
	"/arc/shared/data/ufl/stat96v1/stat96v1.mtx"
	"/arc/shared/data/ufl/stat96v2/stat96v2.mtx"
	"/arc/shared/data/ufl/stat96v3/stat96v3.mtx"
	"/arc/shared/data/ufl/stat96v4/stat96v4.mtx"
	"/arc/shared/data/ufl/stat96v5/stat96v5.mtx"
	"/arc/shared/data/ufl/watson_1/watson_1.mtx"
	"/arc/shared/data/ufl/watson_2/watson_2.mtx"
	"/arc/shared/data/ufl/neos/neos.mtx"
	"/arc/shared/data/ufl/neos1/neos1.mtx"
	"/arc/shared/data/ufl/neos2/neos2.mtx"
	"/arc/shared/data/ufl/neos3/neos3.mtx"
	"/arc/shared/data/ufl/karted/karted.mtx"
	"/arc/shared/data/ufl/EternityII_E/EternityII_E.mtx"
	"/arc/shared/data/ufl/dbir1/dbir1.mtx"
	"/arc/shared/data/ufl/dbir2/dbir2.mtx"
	"/arc/shared/data/ufl/dbic1/dbic1.mtx"
	"/arc/shared/data/ufl/image_interp/image_interp.mtx"
	"/arc/shared/data/ufl/sgpf5y6/sgpf5y6.mtx"
	"/arc/shared/data/ufl/bas1lp/bas1lp.mtx"
	"/arc/shared/data/ufl/pds10/pds10.mtx"
	"/arc/shared/data/ufl/pds-30/pds-30.mtx"
	"/arc/shared/data/ufl/pds-40/pds-40.mtx"
	"/arc/shared/data/ufl/pds-50/pds-50.mtx"
	"/arc/shared/data/ufl/pds-60/pds-60.mtx"
	"/arc/shared/data/ufl/pds-70/pds-70.mtx"
	"/arc/shared/data/ufl/pds-80/pds-80.mtx"
	"/arc/shared/data/ufl/pds-90/pds-90.mtx"
	"/arc/shared/data/ufl/pds-100/pds-100.mtx"
	"/arc/shared/data/ufl/c-big/c-big.mtx"
	"/arc/shared/data/ufl/lp_nug12/lp_nug12.mtx"
	"/arc/shared/data/ufl/lp_nug15/lp_nug15.mtx"
	"/arc/shared/data/ufl/lp_nug20/lp_nug20.mtx"
	"/arc/shared/data/ufl/lp_nug30/lp_nug30.mtx"
	"/arc/shared/data/ufl/lp_cre_b/lp_cre_b.mtx"
	"/arc/shared/data/ufl/lp_cre_d/lp_cre_d.mtx"
	"/arc/shared/data/ufl/lp_pds_02/lp_pds_02.mtx"
	"/arc/shared/data/ufl/lp_pds_06/lp_pds_06.mtx"
	"/arc/shared/data/ufl/lp_pds_10/lp_pds_10.mtx"
	"/arc/shared/data/ufl/lp_pds_20/lp_pds_20.mtx"
	"/arc/shared/data/ufl/lp_ken_18/lp_ken_18.mtx"
	"/arc/shared/data/ufl/lp_ken_13/lp_ken_13.mtx"
	"/arc/shared/data/ufl/lp_ken_11/lp_ken_11.mtx"
	"/arc/shared/data/ufl/lp_ken_07/lp_ken_07.mtx"
	"/arc/shared/data/ufl/lp_osa_60/lp_osa_60.mtx"
	"/arc/shared/data/ufl/lp_osa_30/lp_osa_30.mtx"
	"/arc/shared/data/ufl/lp_osa_14/lp_osa_14.mtx"
	"/arc/shared/data/ufl/lp_osa_07/lp_osa_07.mtx"
	"/arc/shared/data/ufl/lp_maros_r7/lp_maros_r7.mtx"
	"/arc/shared/data/ufl/fome21/fome21.mtx"
	"/arc/shared/data/ufl/fome20/fome20.mtx"
	"/arc/shared/data/ufl/fome13/fome13.mtx"
	"/arc/shared/data/ufl/fome12/fome12.mtx"
	"/arc/shared/data/ufl/fome11/fome11.mtx"
	"/arc/shared/data/ufl/nug08-3rd/nug08-3rd.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_14_2.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_14_4.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_14_8.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_14_16.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_14_32.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_14_64.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_15_2.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_15_4.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_15_8.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_15_16.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_15_32.mtx"
    # "/home/mbasaran/arc/ER/R-MAT_16_2.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_16_4.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_16_8.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_16_16.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_16_32.mtx"
    # "/home/mbasaran/arc/ER/R-MAT_17_2.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_17_4.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_17_8.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_17_16.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_17_32.mtx"
    # "/home/mbasaran/arc/ER/R-MAT_18_2.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_18_4.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_18_8.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_18_16.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_18_32.mtx"
    # "/home/mbasaran/arc/ER/R-MAT_19_2.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_19_4.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_19_8.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_19_16.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_19_32.mtx"
    # "/home/mbasaran/arc/ER/R-MAT_20_2.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_20_4.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_20_8.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_20_16.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_20_32.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_21_2.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_21_4.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_21_8.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_21_16.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_21_32.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_22_2.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_22_4.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_22_8.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_22_16.mtx"
	# "/home/mbasaran/arc/ER/R-MAT_22_32.mtx"
	# "/home/mbasaran/arc/Graph500/R-MAT_15_2.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_15_4.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_15_8.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_15_16.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_15_32.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_16_2.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_16_4.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_16_8.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_16_16.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_16_32.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_17_2.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_17_4.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_17_8.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_17_16.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_17_32.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_18_2.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_18_4.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_18_8.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_18_16.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_18_32.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_19_2.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_19_4.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_19_8.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_19_16.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_19_32.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_20_2.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_20_4.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_20_8.mtx"
    # "/home/mbasaran/arc/Graph500/R-MAT_20_16.mtx"
)

numThreadsArr=( 16 )
clb=64                   # bytes in cache line
iterations=3
binaries=bins/*
phfAlpha=80
accMultiplier=1.25
accSizes=16,64,512

# Select a node to use
cores_per_node=( "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17"
                 "18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35"
                 "36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53"
                 "54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71" )

taskset_params=( "0-17"
                 "18-35"
                 "36-53"
                 "54-71" )

node_to_use=3
cores=${cores_per_node[node_to_use]}
pc=${taskset_params[node_to_use]}

export KMP_AFFINITY=granularity=fine,proclist=[$cores],explicit

###############################################################################

noOfMatrices=${#mmfPathAArr[@]}
for numThreads in ${numThreadsArr[@]}
do
    for (( i = 0; i < $noOfMatrices; i++))
    do
        mmfPathA=${mmfPathAArr[i]}
                outMmfPath=$(basename $mmfPathA)_$(basename $mmfPathA).csv
        # mmfPathB=${mmfPathBArr[i]}
		# outMmfPath=$(basename $mmfPathA)_$(basename $mmfPathB).csv
		outMmfPath=$(basename $mmfPathA).csv

        for binary in $binaries
        do
            outBinPath=$(basename $binary).csv
            # outBinMmfPath=$(basename $binary)_$(basename $mmfPathA)_$(basename $mmfPathB).csv
			outBinMmfPath=$(basename $binary)_$(basename $mmfPathA).csv

            cmd_core="$binary mmfPathA=$mmfPathA numBlocks=$numThreads cacheLineBytes=$clb iterations=$iterations  phfAlpha=$phfAlpha accMultiplier=$accMultiplier accSizes=$accSizes shamt=0"
            cmd="$cmd_core"

            echo "$cmd"
            date
            echo ""

            # echo $cmd | tee -a $outMmfPath $outBinPath $outBinMmfPath > /dev/null
            # taskset -c $pc $cmd | tee -a $outBinPath $outBinMmfPath > /dev/null
			taskset -c $pc $cmd >> $outBinPath
			echo "" >> $outBinPath
            # echo "" | tee -a $outMmfPath $outBinPath $outBinMmfPath > /dev/null
            # echo "" | tee -a $outBinPath $outBinMmfPath > /dev/null
        done
        # echo "" | tee -a $outMmfPath > /dev/null
    done    
done
