#!/bin/bash

mmfPaths=/arc/shared/data/ufl/*

for mmfPath in $mmfPaths
do
    mtxName=$(basename $mmfPath)
    mtxPath=${mmfPath}/${mtxName}.mtx
    # echo $mtxPath

    ( cmdpid=$BASHPID; (sleep 900; kill $cmdpid) & exec build/icc/Stats mmfPathA=$mtxPath )
done

