
# Possible build commands
################################################################################
# make all entry=HybridSpgemm build=icc
# make all entry=CAA build=gcc

# Makefile Parameters
################################################################################
# 1) entry: name of the cpp file that includes main function.
# 2) Following MACROS to use in code (passed to code by build specific setting
#    files)
#   + MEMCHECK: Simply for debugging. Disables omp pragmas for valgrind
#               to find memory links.

opt-flag := -g3
ifneq ($(debug), true)
opt-flag := -O3
endif

ifndef build
build := icc
endif

ifndef entry
entry := HybridSpgemm
endif

# Project settings
################################################################################

project_dir := $(shell pwd)
hostname := $(shell hostname)
build_dir := build

ctags := @echo
rtags := @echo
ifeq ($(hostname), memoks-SX58)
ctags := ctags
rtags := rc
endif
ifeq ($(hostname), localhost)
ctags := @echo ctags
rtags := rc
endif

src := src
inc := include
ds := data_structure
par := parallel
sf := storage_format
dec := decomposition
log := logging
util := util
cust := custom
io := io
util := util
main := main

# Generic setting that every build uses
################################################################################

generic_macros := -DNO_MKL

ifeq ($(serial), true)
generic_macros += -DSERIAL
endif
ifeq ($(debug), true)
generic_macros += -DDEBUG
endif

generic_include_paths := -I$(project_dir)/$(src) \
	-I/homes/mehmetbasaran/.opt/metis-5.1.0/install/include \
    -I/home/mbasaran/.usr/opt/metis-5.1.0/install/include
generic_lib_paths := -L/homes/mehmetbasaran/.opt/metis-5.1.0/install/lib
generic_libs := -lpthread -lm -fopenmp
generic_cflags := -c -fopenmp $(opt-flag) -std=c++17
generic_ldflags := -lpthread #/home/mbasaran/.usr/opt/metis-5.1.0/install/lib/libmetis.a
# /homes/mehmetbasaran/.opt/metis-5.1.0/install/lib/libmetis.a
# /homes/mehmetbasaran/.opt/numactl/build/install/lib/libnuma.a 
build_path := $(build_dir)/$(build)


# Build specific settings
################################################################################

include make_build_specific/$(build).mk

# Build
################################################################################

files_io = $(io)/MMInfo \
           $(io)/Cli \
           $(io)/InputReader

files_ds = $(ds)/Triplet

files_sf =

files_log = $(log)/Timer

files_util = $(util)/IO \
             $(util)/Memory \
             $(util)/Generic \
             $(util)/Spgemm

files = $(files_io) \
        $(files_ds) \
        $(files_sf) \
        $(files_par) \
        $(files_log) \
        $(files_util)

objs_io = $(foreach file, $(files_io), $(build_path)/$(file).o)
objs_ds = $(foreach file, $(files_ds), $(build_path)/$(file).o)
objs_sf = $(foreach file, $(files_sf), $(build_path)/$(file).o)
objs_par = $(foreach file, $(files_par), $(build_path)/$(file).o)
objs_log = $(foreach file, $(files_log), $(build_path)/$(file).o)
objs_util = $(foreach file, $(files_util), $(build_path)/$(file).o)
objs = $(foreach file, $(files), $(build_path)/$(file).o)

# Header files and templates (to state extra prerequisites)
################################################################################

headers = $(src)/$(inc)/config.h

$(objs_util): $(src)/$(inc)/config.h
$(build_path)/$(par)/HybridProduct.o: $(src)/$(inc)/config.h

$(objs): $(src)/$(inc)/sparse_types.h

################################################################################

.PHONY: all clean new extra_tags
.DEFAULT: all

all: $(build_path)/$(entry) extra_tags
	@echo ""
	@echo "Project directory path: $(project_dir)"
	@echo "Build path: $(build_path)"
	@echo "Program entry: $(src)/$(main)/$(entry)"
	@echo "Build successful."
	@echo ""

vtune: $(build_path)/GustavsonExperimentRuntime_accumulation $(build_path)/GustavsonExperimentRuntime_compression $(build_path)/GustavsonExperimentRuntime_gustavson $(build_path)/GustavsonExperimentRuntime_phase2 $(build_path)/GustavsonExperimentRuntime_phase23_piped $(build_path)/GustavsonExperimentRuntime_phase3 $(build_path)/GustavsonExperimentRuntime_pipelined

clean:
	touch TAGS
	rm TAGS
	rm -rf $(build_path)
	mkdir $(build_path)
	mkdir $(build_path)/$(inc)
	mkdir $(build_path)/$(sf)
	mkdir $(build_path)/$(ds)
	mkdir $(build_path)/$(dec)
	mkdir $(build_path)/$(dec)/$(cust)
	mkdir $(build_path)/$(log)
	mkdir $(build_path)/$(par)
	mkdir $(build_path)/$(util)
	mkdir $(build_path)/$(io)
	mkdir $(build_path)/$(main)
	@echo "Build directory created."
	@echo ""

new: clean all

extra_tags:
	$(ctags) -ae $(src)/$(inc)/config.h
	$(ctags) -ae $(src)/$(inc)/sparse_types.h

# Binary
################################################################################

$(build_path)/$(entry): $(build_path)/$(main)/$(entry).o $(objs) $(headers)
	$(ld) $< $(objs) $(ldflags) -o $@

# File in which main() resides
################################################################################

$(build_path)/$(main)/$(entry).o: $(src)/$(main)/$(entry).cpp
	$(cc) $(cflags) $< -o $@
	$(rtags) -c $(cc) $(cflags) $< -o $@
	$(ctags) -ae $?

# Everything else
################################################################################

.SECONDEXPANSION:

$(build_path)/$(io)/MMInfo.o: $(src)/$(io)/MMInfo.cpp $(src)/$(inc)/$(io)/MMInfo.h
	$(cc) $(cflags) $< -o $@
	$(rtags) -c $(cc) $(cflags) $< -o $@
	$(ctags) -ae $?
$(build_path)/$(io)/Cli.o: $(src)/$(io)/Cli.cpp $(src)/$(inc)/$(io)/Cli.h
	$(cc) $(cflags) $< -o $@
	$(rtags) -c $(cc) $(cflags) $< -o $@
	$(ctags) -ae $?
$(build_path)/$(io)/InputReader.o: $(src)/$(io)/InputReader.cpp $(src)/$(inc)/$(io)/InputReader.h
	$(cc) $(cflags) $< -o $@
	$(rtags) -c $(cc) $(cflags) $< -o $@
	$(ctags) -ae $?

$(build_path)/$(ds)/Triplet.o: $(src)/$(ds)/Triplet.cpp $(src)/$(inc)/$(ds)/Triplet.h
	$(cc) $(cflags) $< -o $@
	$(rtags) -c $(cc) $(cflags) $< -o $@
	$(ctags) -ae $?

$(build_path)/$(log)/Timer.o: $(src)/$(log)/Timer.cpp $(src)/$(inc)/$(log)/Timer.h
	$(cc) $(cflags) $< -o $@
	$(rtags) -c $(cc) $(cflags) $< -o $@
	$(ctags) -ae $?

$(build_path)/$(util)/IO.o: $(src)/$(util)/IO.cpp $(src)/$(inc)/$(util)/IO.h
	$(cc) $(cflags) $< -o $@
	$(rtags) -c $(cc) $(cflags) $< -o $@
	$(ctags) -ae $?
$(build_path)/$(util)/Memory.o: $(src)/$(util)/Memory.cpp $(src)/$(inc)/$(util)/Memory.h
	$(cc) $(cflags) $< -o $@
	$(rtags) -c $(cc) $(cflags) $< -o $@
	$(ctags) -ae $?
$(build_path)/$(util)/Generic.o: $(src)/$(util)/Generic.cpp $(src)/$(inc)/$(util)/Generic.h
	$(cc) $(cflags) $< -o $@
	$(rtags) -c $(cc) $(cflags) $< -o $@
	$(ctags) -ae $?
$(build_path)/$(util)/Spgemm.o: $(src)/$(util)/Spgemm.cpp $(src)/$(inc)/$(util)/Spgemm.h
	$(cc) $(cflags) $< -o $@
	$(rtags) -c $(cc) $(cflags) $< -o $@
	$(ctags) -ae $?

$(build_path)/$(main)/$(entry).o: $(src)/$(main)/$(entry).cpp
	$(cc) $(cflags) $< -o $@


$(build_path)/GustavsonExperimentRuntime_accumulation: $(build_path)/$(main)/GustavsonExperimentRuntime_accumulation.o $(objs) $(headers)
	$(ld) $< $(objs) $(ldflags) -o $@
$(build_path)/GustavsonExperimentRuntime_compression: $(build_path)/$(main)/GustavsonExperimentRuntime_compression.o $(objs) $(headers)
	$(ld) $< $(objs) $(ldflags) -o $@
$(build_path)/GustavsonExperimentRuntime_gustavson: $(build_path)/$(main)/GustavsonExperimentRuntime_gustavson.o $(objs) $(headers)
	$(ld) $< $(objs) $(ldflags) -o $@
$(build_path)/GustavsonExperimentRuntime_phase2: $(build_path)/$(main)/GustavsonExperimentRuntime_phase2.o $(objs) $(headers)
	$(ld) $< $(objs) $(ldflags) -o $@
$(build_path)/GustavsonExperimentRuntime_phase23_piped: $(build_path)/$(main)/GustavsonExperimentRuntime_phase23_piped.o $(objs) $(headers)
	$(ld) $< $(objs) $(ldflags) -o $@
$(build_path)/GustavsonExperimentRuntime_phase3: $(build_path)/$(main)/GustavsonExperimentRuntime_phase3.o $(objs) $(headers)
	$(ld) $< $(objs) $(ldflags) -o $@
$(build_path)/GustavsonExperimentRuntime_pipelined: $(build_path)/$(main)/GustavsonExperimentRuntime_pipelined.o $(objs) $(headers)
	$(ld) $< $(objs) $(ldflags) -o $@

$(build_path)/$(main)/GustavsonExperimentRuntime_accumulation.o: $(src)/$(main)/GustavsonExperimentRuntime_accumulation.cpp
	$(cc) $(cflags) $< -o $@
$(build_path)/$(main)/GustavsonExperimentRuntime_compression.o: $(src)/$(main)/GustavsonExperimentRuntime_compression.cpp
	$(cc) $(cflags) $< -o $@
$(build_path)/$(main)/GustavsonExperimentRuntime_gustavson.o: $(src)/$(main)/GustavsonExperimentRuntime_gustavson.cpp
	$(cc) $(cflags) $< -o $@
$(build_path)/$(main)/GustavsonExperimentRuntime_phase2.o: $(src)/$(main)/GustavsonExperimentRuntime_phase2.cpp
	$(cc) $(cflags) $< -o $@
$(build_path)/$(main)/GustavsonExperimentRuntime_phase23_piped.o: $(src)/$(main)/GustavsonExperimentRuntime_phase23_piped.cpp
	$(cc) $(cflags) $< -o $@
$(build_path)/$(main)/GustavsonExperimentRuntime_phase3.o: $(src)/$(main)/GustavsonExperimentRuntime_phase3.cpp
	$(cc) $(cflags) $< -o $@
$(build_path)/$(main)/GustavsonExperimentRuntime_pipelined.o: $(src)/$(main)/GustavsonExperimentRuntime_pipelined.cpp
	$(cc) $(cflags) $< -o $@
