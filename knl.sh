#!/bin/bash

export
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/intel/compilers_and_libraries_2017.1.132/linux/ipp/lib/intel64:/opt/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/mkl/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/tbb/lib/intel64/gcc4.7:/opt/intel/debugger_2017/iga/lib:/opt/intel/debugger_2017/libipt/intel64/lib:/opt/intel/compilers_and_libraries_2017.1.132/linux/daal/lib/intel64_lin:/opt/intel/compilers_and_libraries_2017.1.132/linux/mpi/intel64/lib:/opt/intel/compilers_and_libraries_2017.1.132/linux/mpi/mic/lib:/homes/mehmetbasaran/.opt/numactl/build/install/lib

###############################################################################

# home=/homes/mehmetbasaran/
# from=${#home}
# scriptPath=$0
# dirPath="./${scriptPath:from}"

dirPath="./spgemm-bandwidth"

mmfPathAArr=(
    "/export/shared/ufl_sparse/2cubes_sphere/2cubes_sphere.mtx"
	"/export/shared/ufl_sparse/cage12/cage12.mtx"
	"/export/shared/ufl_sparse/filter3D/filter3D.mtx"
	"/export/shared/ufl_sparse/mac_econ_fwd500/mac_econ_fwd500.mtx"
	"/export/shared/ufl_sparse/majorbasis/majorbasis.mtx"
	"/export/shared/ufl_sparse/mario002/mario002.mtx"
	"/export/shared/ufl_sparse/mc2depi/mc2depi.mtx"
	"/export/shared/ufl_sparse/offshore/offshore.mtx"
    "/export/shared/ufl_sparse/scircuit/scircuit.mtx"
	"/export/shared/ufl_sparse/tmt_sym/tmt_sym.mtx"
	"/export/shared/ufl_sparse/torso2/torso2.mtx"
	"/export/shared/ufl_sparse/europe_osm/europe_osm.mtx"
	"/export/shared/ufl_sparse/delaunay_n24/delaunay_n24.mtx"
    "/export/shared/ufl_sparse/web-Stanford/web-Stanford.mtx"
    "/export/shared/ufl_sparse/road_usa/road_usa.mtx"
    "/export/shared/ufl_sparse/Freescale1/Freescale1.mtx"
    "/export/shared/ufl_sparse/rajat31/rajat31.mtx"
    "/export/shared/ufl_sparse/asia_osm/asia_osm.mtx"
    "/export/shared/ufl_sparse/germany_osm/germany_osm.mtx"
    "/export/shared/ufl_sparse/NLR/NLR.mtx"
    "/export/shared/ufl_sparse/patents/patents.mtx"
    "/export/shared/ufl_sparse/cit-Patents/cit-Patents.mtx"
    "/export/shared/ufl_sparse/adaptive/adaptive.mtx"
    "/export/shared/ufl_sparse/italy_osm/italy_osm.mtx"
    "/export/shared/ufl_sparse/M6/M6.mtx"
	"/export/shared/ufl_sparse/memchip/memchip.mtx"
	"/export/shared/ufl_sparse/cage14/cage14.mtx"
	"/export/shared/ufl_sparse/wb-edu/wb-edu.mtx"
	"/export/shared/ufl_sparse/hugebubbles-00020/hugebubbles-00020.mtx"
	"/export/shared/ufl_sparse/ldoor/ldoor.mtx"
	"/export/shared/ufl_sparse/hugebubbles-00010/hugebubbles-00010.mtx"
	"/export/shared/ufl_sparse/hugetrace-00010/hugetrace-00010.mtx"
	"/export/shared/ufl_sparse/road_central/road_central.mtx"
	"/export/shared/ufl_sparse/gsm_106857/gsm_106857.mtx"
	"/export/shared/ufl_sparse/msdoor/msdoor.mtx"
	"/export/shared/ufl_sparse/circuit5M_dc/circuit5M_dc.mtx"
	"/export/shared/ufl_sparse/AS365/AS365.mtx"
	"/export/shared/ufl_sparse/333SP/333SP.mtx"
	"/export/shared/ufl_sparse/atmosmodm/atmosmodm.mtx"
	"/export/shared/ufl_sparse/hugetrace-00000/hugetrace-00000.mtx"
	"/export/shared/ufl_sparse/thermal2/thermal2.mtx"
	"/export/shared/ufl_sparse/G3_circuit/G3_circuit.mtx"
	"/export/shared/ufl_sparse/CurlCurl_2/CurlCurl_2.mtx"
	"/export/shared/ufl_sparse/great-britain_osm/great-britain_osm.mtx"
	"/export/shared/ufl_sparse/delaunay_n21/delaunay_n21.mtx"
	"/export/shared/ufl_sparse/sls/sls.mtx"
	"/export/shared/ufl_sparse/Hamrle3/Hamrle3.mtx"
	"/export/shared/ufl_sparse/delaunay_n22/delaunay_n22.mtx"
	"/export/shared/ufl_sparse/delaunay_n23/delaunay_n23.mtx"
	"/export/shared/ufl_sparse/eu-2005/eu-2005.mtx"
	"/export/shared/ufl_sparse/StocF-1465/StocF-1465.mtx"
	"/export/shared/ufl_sparse/Transport/Transport.mtx"
	"/export/shared/ufl_sparse/venturiLevel3/venturiLevel3.mtx"
	"/export/shared/ufl_sparse/ecology1/ecology1.mtx"
	"/export/shared/ufl_sparse/roadNet-CA/roadNet-CA.mtx"
	"/export/shared/ufl_sparse/webbase-1M/webbase-1M.mtx"
	"/export/shared/ufl_sparse/netherlands_osm/netherlands_osm.mtx"
	"/export/shared/ufl_sparse/roadNet-TX/roadNet-TX.mtx"
	"/export/shared/ufl_sparse/roadNet-PA/roadNet-PA.mtx"
	"/export/shared/ufl_sparse/citationCiteseer/citationCiteseer.mtx"
	"/export/shared/ufl_sparse/belgium_osm/belgium_osm.mtx"
	"/export/shared/ufl_sparse/debr/debr.mtx"
	"/export/shared/ufl_sparse/pkustk10/pkustk10.mtx"
	"/export/shared/ufl_sparse/Chebyshev4/Chebyshev4.mtx"
	"/export/shared/ufl_sparse/amazon-2008/amazon-2008.mtx"
	"/export/shared/ufl_sparse/Ga10As10H30/Ga10As10H30.mtx"
	"/export/shared/ufl_sparse/pre2/pre2.mtx"
	"/export/shared/ufl_sparse/pkustk13/pkustk13.mtx"
	"/export/shared/ufl_sparse/hood/hood.mtx"
	"/export/shared/ufl_sparse/pwtk/pwtk.mtx"
	"/export/shared/ufl_sparse/kkt_power/kkt_power.mtx"
	"/export/shared/ufl_sparse/pkustk14/pkustk14.mtx"
	"/export/shared/ufl_sparse/F1/F1.mtx"
	"/export/shared/ufl_sparse/ML_Laplace/ML_Laplace.mtx"
	"/export/shared/ufl_sparse/Fault_639/Fault_639.mtx"
	"/export/shared/ufl_sparse/nlpkkt80/nlpkkt80.mtx"
	"/export/shared/ufl_sparse/Emilia_923/Emilia_923.mtx"
	"/export/shared/ufl_sparse/dielFilterV2real/dielFilterV2real.mtx"
	"/export/shared/ufl_sparse/af_shell10/af_shell10.mtx"
	"/export/shared/ufl_sparse/Geo_1438/Geo_1438.mtx"
	"/export/shared/ufl_sparse/nlpkkt120/nlpkkt120.mtx"
	# rectangular matrices (lp)
	"/export/shared/ufl_sparse/spal_004/spal_004.mtx"
	"/export/shared/ufl_sparse/rail_1357/rail_1357.mtx"
	"/export/shared/ufl_sparse/rail_20209/rail_20209.mtx"
	"/export/shared/ufl_sparse/rail_5177/rail_5177.mtx"
	"/export/shared/ufl_sparse/rail_79841/rail_79841.mtx"
	"/export/shared/ufl_sparse/rail507/rail507.mtx"
	"/export/shared/ufl_sparse/rail516/rail516.mtx"
	"/export/shared/ufl_sparse/rail582/rail582.mtx"
	"/export/shared/ufl_sparse/rail2586/rail2586.mtx"
	"/export/shared/ufl_sparse/rail4284/rail4284.mtx"
	"/export/shared/ufl_sparse/tp-6/tp-6.mtx"
	"/export/shared/ufl_sparse/degme/degme.mtx"
	# "/export/shared/ufl_sparse/cont1_l/cont1_l.mtx"
	"/export/shared/ufl_sparse/cont11_l/cont11_l.mtx"
	"/export/shared/ufl_sparse/web-Google/web-Google.mtx"
	"/export/shared/ufl_sparse/stormg2-8/stormg2-8.mtx"
	"/export/shared/ufl_sparse/stormg2-27/stormg2-27.mtx"
	"/export/shared/ufl_sparse/stormg2-125/stormg2-125.mtx"
	"/export/shared/ufl_sparse/stormG2_1000/stormG2_1000.mtx"
	"/export/shared/ufl_sparse/stat96v1/stat96v1.mtx"
	"/export/shared/ufl_sparse/stat96v2/stat96v2.mtx"
	"/export/shared/ufl_sparse/stat96v3/stat96v3.mtx"
	"/export/shared/ufl_sparse/stat96v4/stat96v4.mtx"
	"/export/shared/ufl_sparse/stat96v5/stat96v5.mtx"
	"/export/shared/ufl_sparse/watson_1/watson_1.mtx"
	"/export/shared/ufl_sparse/watson_2/watson_2.mtx"
	"/export/shared/ufl_sparse/neos/neos.mtx"
	"/export/shared/ufl_sparse/neos1/neos1.mtx"
	"/export/shared/ufl_sparse/neos2/neos2.mtx"
	"/export/shared/ufl_sparse/neos3/neos3.mtx"
	"/export/shared/ufl_sparse/karted/karted.mtx"
	"/export/shared/ufl_sparse/EternityII_E/EternityII_E.mtx"
	"/export/shared/ufl_sparse/dbir1/dbir1.mtx"
	"/export/shared/ufl_sparse/dbir2/dbir2.mtx"
	"/export/shared/ufl_sparse/dbic1/dbic1.mtx"
	"/export/shared/ufl_sparse/image_interp/image_interp.mtx"
	"/export/shared/ufl_sparse/sgpf5y6/sgpf5y6.mtx"
	"/export/shared/ufl_sparse/bas1lp/bas1lp.mtx"
	"/export/shared/ufl_sparse/pds10/pds10.mtx"
	"/export/shared/ufl_sparse/pds-30/pds-30.mtx"
	"/export/shared/ufl_sparse/pds-40/pds-40.mtx"
	"/export/shared/ufl_sparse/pds-50/pds-50.mtx"
	"/export/shared/ufl_sparse/pds-60/pds-60.mtx"
	"/export/shared/ufl_sparse/pds-70/pds-70.mtx"
	"/export/shared/ufl_sparse/pds-80/pds-80.mtx"
	"/export/shared/ufl_sparse/pds-90/pds-90.mtx"
	"/export/shared/ufl_sparse/pds-100/pds-100.mtx"
	"/export/shared/ufl_sparse/c-big/c-big.mtx"
	"/export/shared/ufl_sparse/lp_nug12/lp_nug12.mtx"
	"/export/shared/ufl_sparse/lp_nug15/lp_nug15.mtx"
	"/export/shared/ufl_sparse/lp_nug20/lp_nug20.mtx"
	"/export/shared/ufl_sparse/lp_nug30/lp_nug30.mtx"
	"/export/shared/ufl_sparse/lp_cre_b/lp_cre_b.mtx"
	"/export/shared/ufl_sparse/lp_cre_d/lp_cre_d.mtx"
	"/export/shared/ufl_sparse/lp_pds_02/lp_pds_02.mtx"
	"/export/shared/ufl_sparse/lp_pds_06/lp_pds_06.mtx"
	"/export/shared/ufl_sparse/lp_pds_10/lp_pds_10.mtx"
	"/export/shared/ufl_sparse/lp_pds_20/lp_pds_20.mtx"
	"/export/shared/ufl_sparse/lp_ken_18/lp_ken_18.mtx"
	"/export/shared/ufl_sparse/lp_ken_13/lp_ken_13.mtx"
	"/export/shared/ufl_sparse/lp_ken_11/lp_ken_11.mtx"
	"/export/shared/ufl_sparse/lp_ken_07/lp_ken_07.mtx"
	"/export/shared/ufl_sparse/lp_osa_60/lp_osa_60.mtx"
	"/export/shared/ufl_sparse/lp_osa_30/lp_osa_30.mtx"
	"/export/shared/ufl_sparse/lp_osa_14/lp_osa_14.mtx"
	"/export/shared/ufl_sparse/lp_osa_07/lp_osa_07.mtx"
	"/export/shared/ufl_sparse/lp_maros_r7/lp_maros_r7.mtx"
	"/export/shared/ufl_sparse/fome21/fome21.mtx"
	"/export/shared/ufl_sparse/fome20/fome20.mtx"
	"/export/shared/ufl_sparse/fome13/fome13.mtx"
	"/export/shared/ufl_sparse/fome12/fome12.mtx"
	"/export/shared/ufl_sparse/fome11/fome11.mtx"
	"/export/shared/ufl_sparse/nug08-3rd/nug08-3rd.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_14_2.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_14_4.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_14_8.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_14_16.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_14_32.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_14_64.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_15_2.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_15_4.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_15_8.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_15_16.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_15_32.mtx"
    # "/export/shared/ufl_sparse/ER/R-MAT_16_2.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_16_4.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_16_8.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_16_16.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_16_32.mtx"
    # "/export/shared/ufl_sparse/ER/R-MAT_17_2.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_17_4.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_17_8.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_17_16.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_17_32.mtx"
    # "/export/shared/ufl_sparse/ER/R-MAT_18_2.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_18_4.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_18_8.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_18_16.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_18_32.mtx"
    # "/export/shared/ufl_sparse/ER/R-MAT_19_2.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_19_4.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_19_8.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_19_16.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_19_32.mtx"
    # "/export/shared/ufl_sparse/ER/R-MAT_20_2.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_20_4.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_20_8.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_20_16.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_20_32.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_21_2.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_21_4.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_21_8.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_21_16.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_21_32.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_22_2.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_22_4.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_22_8.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_22_16.mtx"
	# "/export/shared/ufl_sparse/ER/R-MAT_22_32.mtx"
	# "/export/shared/ufl_sparse/Graph500/R-MAT_15_2.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_15_4.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_15_8.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_15_16.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_15_32.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_16_2.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_16_4.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_16_8.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_16_16.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_16_32.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_17_2.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_17_4.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_17_8.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_17_16.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_17_32.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_18_2.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_18_4.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_18_8.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_18_16.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_18_32.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_19_2.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_19_4.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_19_8.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_19_16.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_19_32.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_20_2.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_20_4.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_20_8.mtx"
    # "/export/shared/ufl_sparse/Graph500/R-MAT_20_16.mtx"
)


clb=64                   # bytes in cache line
iterations=5
binaries=${dirPath}/bins-knl/*
accSizes=16,512
permuteAB=0
# accSizes=1,2,4,8,16,32,64,128

# knl (Knights Landing)
###############################################################################

numThreadsArr=( 68 136 204 272 )
placements=( "68c,1t" "68c,2t" "68c,3t" "68c,3t" )
configs=( 1 )
# configs=( 0 1 2 3 )

###############################################################################

noOfConfigs=${#configs[@]}
targetSize=256
for (( c = 0; c < $noOfConfigs; c++ ))
do
	config=${configs[c]}
	numThreads=${numThreadsArr[config]}
	placement=${placements[config]}

	noOfMatrices=${#mmfPathAArr[@]}
	for (( i = 0; i < $noOfMatrices; i++))
	do
		export OMP_NUM_THREADS=${numThreads}
		export KMP_HW_SUBSET=${placement}
		export KMP_AFFINITY=compact,verbose

		mmfPathA=${mmfPathAArr[i]}
		for binary in $binaries
		do
			outBinPath="${dirPath}/${numThreads}t_$(basename $binary)_${permuteAB}_${targetSize}.csv"

			cmd="$binary mmfPathA=$mmfPathA numBlocks=$numThreads cacheLineBytes=$clb iterations=$iterations accSizes=$accSizes BSizeKB=1024 permuteAB=${permuteAB} targettedCacheSizeKB=${targetSize}"

			echo "$cmd"
			date
			echo ""

			# taskset -c $pc $cmd | tee -a $outBinPath $outBinMmfPath > /dev/null
			$cmd >> $outBinPath
			echo "" >> $outBinPath
		done
	done

done
